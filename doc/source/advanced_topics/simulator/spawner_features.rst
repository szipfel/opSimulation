..
  *******************************************************************************
  Copyright (c) 2021 in-tech GmbH

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

.. _spawner_features:

****************
Spawner Features
****************


.. _spawner_features_prerun:

PreRunSpawner
=============

Spawning on roads, lanes and ranges
-----------------------------------

Non-existent roads
^^^^^^^^^^^^^^^^^^

Requirement
   Road IDs specified in the configuration, but not part of the scenery file are igored.

Verified by
   Unit Test SpawnerPreRunCommonParameterExtractor_GetWayToNextRoad_NoPossibleWay


.. _spawner_features_prerun_on_roads_single_road:

Single road
^^^^^^^^^^^

Requirement
   Spawning on the given road takes place.

Requirement
   No agents are spawned on other roads in the scenery.

Verified by
   End-to-end Test SpawnerPreRun_Highway_SingleRoad


.. _spawner_features_prerun_on_roads_consecutive_roads:

Consecutive roads
^^^^^^^^^^^^^^^^^

Precondition
   Spawner configuration specifies a list of consecutively connected roads in a single ListItem.

Requirement
   Spawning on the given roads takes place.

Requirement
   No agents spawned on other roads in the scenery.

Requirement
   Agents are spawned on all drivable lanes (which are part of the roads being specified in the configuration).

Requirement
   Agents are spawned in the correct driving direction.

Verified by
   End-to-end Test SpawnerPreRun_Highway_FullStream
   End-to-end Test SpawnerPreRun_SingleRoad_DrivingDirection


Non-consecutive roads
^^^^^^^^^^^^^^^^^^^^^

Precondition
   Spawner configuration specifies a list of non-consecutively connected roads.

Requirement
   Spawning on the given roads takes place.

Requirement
   Missing roads between the specified ones are automatically determined by the Spawner and handled like they were specified in the configuration in the first place.
   The resulting path being spawned on is arbitrary if multiple solutions exist for a scenery, but only one path shall be used.

Verified by
   Covered by :ref:`spawner_features_prerun_on_roads_across_junction`


.. _spawner_features_prerun_on_roads_across_junction:

Roads across junction
^^^^^^^^^^^^^^^^^^^^^

Precondition
   Spawner configuration specifies a list of (not necessarily consecutively) connected roads in a single ListItem.

Precondition
   The chain of roads specified has to cross a single junction.

Requirement
   Spawning on the given roads takes place.

Requirement
   Missing roads between the specified ones are automatically determined by the Spawner and handled like they were specified in the configuration in the first place.
   The resulting path being spawned on is arbitrary if multiple solutions exist for a scenery, but only one path shall be used.

Verified by
   End-to-end Test SpawnerPreRun_Highway_EntryExit


Multiple roads (including non existent IDs)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Precondition
   Spawner configuration specifies a list of (at least partially) consecutively connected roads in a single ListItem.

Precondition
   At least one road id given in the configuration is not present in the scenery.

Requirement
   Spawning on the given roads takes place up to the first non-existent road that is encountered.

Requirement
   No spawning takes place, if the first road of the list doesn't exist.

Verified by
   End-to-end Test SpawnerPreRun_Highway_IncorrectStreamBegin
   End-to-end Test SpawnerPreRun_Highway_IncorrectStreamMid


Specified lanes
^^^^^^^^^^^^^^^

Precondition
   Basic setup as as in :ref:`spawner_features_prerun_on_roads_consecutive_roads`.

Precondition
   A list of lanes to be spawned on is specified in the Spawner configuration.

Precondition
   The list of lanes contains at least one lane IDs which exist for the first specified road.

Precondition
   The list of lanes contains at least one lane IDs which doesn't exist for the first specified road.

Requirement
   Spawning takes place on the specified lane IDs (on drivable lanes).

Requirement
   Spawning does not take place on other lane IDs.

Requirement
   Lane IDs not present for one or more roads do not lead to an error.

Verified by
   End-to-end Test SpawnerPreRun_SingleRoad_SpecifiedLanes


Multiple overlapping road streams
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Precondition
   Two road streams are specified in the Spawner configuration (as in :ref:`spawner_features_prerun_on_roads_consecutive_roads`).

Precondition
   The two road streams have at least one road in common.

Precondition
   The Spawner configuration specifies the lanes to spawn on in such a way that there are no overlapping lanes (on the road being common to both streams).

Requirement
   Spawning on the given roads and all given lanes takes place.

Verified by
   End-to-end Test SpawnerPreRun_Highway_RoadOverlap


Spawning on range (SStart + SEnd within road boundaries)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Precondition
   Basic setup as as in :ref:`spawner_features_prerun_on_roads_consecutive_roads`.

Precondition
   SStart and SEnd are set to values within the road boundaries (with the effective start s-coordinate being lower than the effective end s-coordinate when taking the whole road stream into account).

Requirement
   Agents are spawned within the specified range.

Requirement
   No agents are spawned outside of the specified range.

Verified by
   Unit Test SpawnerPreRunCommonParameterExtractor_ExtractSpawnAreas_AllOptionalParameters
   End-to-end Test SpawnerPreRun_SingleRoad_SpecifiedRange



Spawning on range (SStart + SLength within road boundaries)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Precondition
   Basic setup as as in :ref:`spawner_features_prerun_on_roads_consecutive_roads`.

Precondition
   SStart and SLength are set to values within the road boundaries (with the effective start s-coordinate being lower than the effective end s-coordinate when taking the whole road stream into account).

Requirement
   Agents are spawned within the specified range.

Requirement
   No agents are spawned outside of the specified range.

Verified by
   Unit Test SpawnerPreRunCommonParameterExtractor_ExtractSpawnAreas_AllOptionalParameters


Spawning on range (SEnd outside of road boundaries)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Precondition
   Basic setup as as in :ref:`spawner_features_prerun_on_roads_consecutive_roads`.

Precondition
   SStart is set set to a value within the road boundaries, SEnd is set greater than the length of the last road given.

Requirement
   Agents are spawned within the specified range.

Requirement
   The specified range is capped to the length of the last road.

Verified by
   Unit Test SpawnerPreRunCommonParameterExtractor_ExtractSpawnAreasWithSOutOfRange_ReturnsValidS


Spawning on range (SLength outside of road boundaries)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Precondition
   Basic setup as as in :ref:`spawner_features_prerun_on_roads_consecutive_roads`.

Precondition
   SStart is set set to a value within the road boundaries, SLength is set greater than the accumulated length of all roads given.

Requirement
   Agents are spawned within the specified range.

Requirement
   The specified range is capped to the length of the roads.

Verified by
   Unit Test SpawnerPreRunCommonParameterExtractor_ExtractSpawnAreasWithSOutOfRange_ReturnsValidS


Right lane only
^^^^^^^^^^^^^^^

Precondition
   Basic setup as as in :ref:`spawner_features_prerun_on_roads_consecutive_roads`.

Precondition
   ``RightLaneOnly`` set to ``true`` in the Spawner configuration.

Requirement
   Spawning takes place on the specified roads.

Requirement
   Spawning takes place on the rightmost lanes only.

Verified by
   End-to-end Test SpawnerPreRun_SingleRoad_RightLaneOnly


Traffic parameters
------------------

Separation buffer specified
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Precondition
   Basic setup as as in :ref:`spawner_features_prerun_on_roads_single_road`.

Precondition
   ``MinimumSeparationBuffer`` is set in the Spawner configuration.

Requirement
   No two agents are spawned with a net distance lower than the specified separation buffer.

Verified by
   UNTESTED


Separation buffer not specified
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Precondition
   Basic setup as as in :ref:`spawner_features_prerun_on_roads_single_road`.

Precondition
   ``MinimumSeparationBuffer`` is not set in the Spawner configuration.

Requirement
   No two agents are spawned with a net distance lower than 5 m.

Verified by
   UNTESTED


Time to collision
^^^^^^^^^^^^^^^^^

Precondition
   Basic setup as as in :ref:`spawner_features_prerun_on_roads_single_road`.

Requirement
   No agent is spawned with a TTC lower than the configured threshold (with respect to end of lane).

Verified by
   UNTESTED
   Also has to be checked by analysis of simulation output by an external tool


Stochastics
-----------

Traffic groups
^^^^^^^^^^^^^^

Requirement
   The selection of traffic groups is randomized using a uniform distribution.

Verified by
   UNTESTED


Agent profiles
^^^^^^^^^^^^^^

Requirement
   The selection of agent profiles is randomized using a uniform distribution.

Verified by
   UNTESTED


Velocity distribution
^^^^^^^^^^^^^^^^^^^^^

Requirement
   The distribution of the velocities of a traffic group has to match the configured distribution.

Verified by
   UNTESTED
   Also has to be checked by analysis of simulation output by an external tool


TGap distribution
^^^^^^^^^^^^^^^^^

Requirement
   The distribution of the TGaps of a traffic group has to match the configured distribution.

Verified by
   UNTESTED
   Also has to be checked by analysis of simulation output by an external tool


.. _spawner_features_runtime:

RuntimeSpawner
==============

Spawning on roads, lanes and ranges
-----------------------------------

Non-existent roads
^^^^^^^^^^^^^^^^^^

Requirement
   Road IDs specified in the configuration, but not part of the scenery file are igored.

Verified by
   UNTESTED


.. _spawner_features_runtime_on_roads_single_road:

Single road
^^^^^^^^^^^

Requirement
   Spawning on the given road takes place.

Requirement
   No agents are spawned on other roads in the scenery.

Requirement
   Agents are spawned in the correct driving direction.

Verified by
   End-to-end Test SpawnerRuntime_Highway_SingleRoad


Driving direction
^^^^^^^^^^^^^^^^^

Requirement
   Agents are spawned in the correct driving direction (matching lane direction).

Verified by
   End-to-end Test SpawnerRuntime_Highway_SingleRoad_DrivingDirection


Multiple roads (including non existent IDs)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Precondition
   Spawner configuration specifies a list of roads in a single ListItem.

Precondition
   At least one road id given in the configuration is not present in the scenery.

Requirement
   Spawning on the given roads takes place.

Requirement
   Non-existent road IDs are ignored.

Verified by
   End-to-end Test SpawnerRuntime_Highway_MultipleRoads


Specified lanes
^^^^^^^^^^^^^^^

Precondition
   Basic setup as as in :ref:`spawner_features_runtime_on_roads_single_road`.

Precondition
   A list of lanes to be spawned on is specified in the Spawner configuration.

Precondition
   The list of lanes contains lane IDs which don't exist for the specified road.

Requirement
   Spawning takes place on the specified lane IDs (on drivable lanes).

Requirement
   Spawning does not take place on other lane IDs.

Requirement
   Lane IDs not present for the specified road do not lead to an error.

Verified by
   End-to-end Test SpawnerRuntime_SingleRoad_SpecifiedLanes


Right lane only
^^^^^^^^^^^^^^^

Precondition
   Basic setup as as in :ref:`spawner_features_runtime_on_roads_single_road`.

Precondition
   ``RightLaneOnly`` set to ``true`` in the Spawner configuration.

Requirement
   Spawning takes place on the specified road.

Requirement
   Spawning takes place on the rightmost lane only.

Verified by
   End-to-end Test SpawnerRuntime_SingleRoad_RightLaneOnly


Traffic parameters
------------------

Separation buffer specified
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Precondition
   Basic setup as as in :ref:`spawner_features_runtime_on_roads_single_road`.

Precondition
   ``MinimumSeparationBuffer`` is set in the Spawner configuration.

Requirement
   No two agents are spawned with a net distance lower than the specified separation buffer.

Verified by
   Unit Test SpawnerRuntimeCommonParameterExtractor_ExtractMinimumSeparationBuffer_FixedValue


Separation buffer not specified
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Precondition
   Basic setup as as in :ref:`spawner_features_runtime_on_roads_single_road`.

Precondition
   ``MinimumSeparationBuffer`` is not set in the Spawner configuration.

Requirement
   No two agents are spawned with a net distance lower than 5 m.

Verified by
   Unit Test SpawnerRuntimeCommonParameterExtractor_ExtractMinimumSeparationBuffer_NoValue_SetsDefault



Stochastics
-----------

Traffic groups
^^^^^^^^^^^^^^

Requirement
   The selection of traffic groups is randomized using a uniform distribution.

Verified by
   UNTESTED


Agent profiles
^^^^^^^^^^^^^^

Requirement
   The selection of agent profiles is randomized using a uniform distribution.

Verified by
   UNTESTED


Velocity distribution
^^^^^^^^^^^^^^^^^^^^^

Requirement
   The distribution of the velocities of a traffic group has to match the configured distribution.

Verified by
   UNTESTED
   Also has to be checked by analysis of simulation output by an external tool


TGap distribution
^^^^^^^^^^^^^^^^^

Requirement
   The distribution of the TGaps of a traffic group has to match the configured distribution.

Verified by
   UNTESTED
   Also has to be checked by analysis of simulation output by an external tool

