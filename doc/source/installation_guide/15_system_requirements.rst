..
  *******************************************************************************
  Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

.. _system_requirements:

System Requirements
===================

Simulations with |op| are designed to be very lightweight and performant, without imposing huge requirements on the underlying hardware. 
Thus, |op| simulations run as a single core application and do not utilize GPU. 
Only the build process during installation of |op| makes use of multiple cores, so it is recommended to have at least a 4 core CPU.
In the table below, minimal requirements are listed along with a commonly used, well working example setup.

.. table::
   :class: tight-table

   ==================== ============================================================== ==========================
   System Specification Minimal Requirements                                           Tested Setup (exemplarily)
   ==================== ============================================================== ==========================
   Operating System     Windows or Linux (Debian Bookworm or Ubuntu 22.04 recommended) Windows 10 
   CPU                  Intel x64 Architecture with at least 1,50 GHz clock frequency  Intel i5-10310U
   RAM                  at least 8 GB                                                  8 GB
   GPU                  not used by the simulation                                     Intel UHD Graphics 620
   Others               1 GB free hard disk space
   ==================== ============================================================== ==========================

