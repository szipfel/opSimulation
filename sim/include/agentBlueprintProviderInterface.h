/********************************************************************************
 * Copyright (c) 2019 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  agentBlueprintProviderInterface.h
//! @brief This file contains the interface of the agentBlueprintProvider to interact
//!        with the framework.
//-----------------------------------------------------------------------------

#pragma once

#include <optional>

#include "include/agentBlueprintInterface.h"

//! Class representing a agent blue print agent
class AgentBlueprintProviderInterface
{
public:
  virtual ~AgentBlueprintProviderInterface() = default;

  /*!
   * \brief Returns a default blueprint with only the basic components
   *
   * @return           default AgentBlueprint
   */
  virtual System GetDefaultSystem() const = 0;

  /*!
   * \brief Samples an entire agent
   *
   * \details Samples an entired agent from a given SystemConfig or from a dynamically built agent
   *
   *
   * @param agentProfileName    Name of AgentProfile to sample
   *
   * @return           Sampled system if successful
   */
  virtual System SampleSystem(const std::string &agentProfileName) const = 0;

  //! @brief Samples a vehicle model name
  //!
  //! @param agentProfileName    Name of AgentProfile to sample
  //! @return Sampled vehicle model name
  virtual std::string SampleVehicleModelName(const std::string &agentProfileName) const = 0;
};
