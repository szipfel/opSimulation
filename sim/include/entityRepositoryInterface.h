/*******************************************************************************
 * Copyright (c) 2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "MantleAPI/Traffic/i_entity_repository.h"

namespace core
{
class Agent;

//! This class provides the interface for the EntityRepository
class EntityRepositoryInterface : public mantle_api::IEntityRepository
{
public:
  //! Method which spawns ready agents
  //!
  //! @return true if agents are spawned successfully, false otherwise
  virtual bool SpawnReadyAgents() = 0;

  //! Method which creates common entity
  //!
  //! @param[in] properties   Additional properties that describe entity
  //! @return the created common entity
  virtual mantle_api::IVehicle &CreateCommon(const mantle_api::VehicleProperties &properties) = 0;

  //! Method which retrieves the new agents
  //!
  //! @return new agents
  virtual std::vector<Agent *> ConsumeNewAgents() = 0;
};

}  // namespace core