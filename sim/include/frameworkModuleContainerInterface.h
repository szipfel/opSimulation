/********************************************************************************
 * Copyright (c) 2017-2021 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

class DataBufferInterface;
class StochaticsInterface;
class WorldInterface;

namespace core
{

class AgentFactoryInterface;
class CollisionDetectionInterface;
class ObservationNetworkInterface;
class SpawnPointNetworkInterface;

/// @brief Interface representing a framework module container
class FrameworkModuleContainerInterface
{
public:
  virtual ~FrameworkModuleContainerInterface() = default;

  /*!
   * \brief Returns a pointer to the Callbacks
   *
   * @return        Callbacks pointer
   */
  virtual CallbackInterface* GetCallbacks() = 0;

  /*!
   * \brief Returns a pointer to the AgentFactory
   *
   * @return        AgentFactory pointer
   */
  virtual AgentFactoryInterface* GetAgentFactory() = 0;

  /*!
   * \brief Returns a pointer to the data buffer
   *
   * @return   data buffer pointer
   */
  virtual DataBufferInterface* GetDataBuffer() = 0;

  /*!
   * \brief Returns a pointer to the CollisionDetection
   *
   * @return        CollisionDetection pointer
   */
  virtual CollisionDetectionInterface* GetCollisionDetection() = 0;

  /*!
   * \brief Returns a pointer to the ObservationNetwork
   *
   * @return        ObservationNetwork pointer
   */
  virtual ObservationNetworkInterface* GetObservationNetwork() = 0;

  /*!
   * \brief Returns a pointer to the SpawnPointNetwork
   *
   * @return        SpawnPointNetwork pointer
   */
  virtual SpawnPointNetworkInterface* GetSpawnPointNetwork() = 0;

  /*!
   * \brief Returns a pointer to the Stochastics
   *
   * @return        Stochastics pointer
   */
  virtual StochasticsInterface* GetStochastics() = 0;

  /*!
   * \brief Returns a pointer to the World
   *
   * @return        World pointer
   */
  virtual WorldInterface* GetWorld() = 0;
};

}  //namespace core
