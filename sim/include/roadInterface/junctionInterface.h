/********************************************************************************
 * Copyright (c) 2017-2019 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#ifndef JUNCTIONINTERFACE
#define JUNCTIONINTERFACE

#include <map>

#include "connectionInterface.h"

/// Struct to represent priority
struct Priority
{
  std::string high;  ///< High priority of Junction interface
  std::string low;   ///< Low priority of Junction interface
};

//-----------------------------------------------------------------------------
//! Class representing a junction
//-----------------------------------------------------------------------------
class JunctionInterface
{
public:
  JunctionInterface() = default;
  JunctionInterface(const JunctionInterface&) = delete;
  JunctionInterface(JunctionInterface&&) = delete;
  JunctionInterface& operator=(const JunctionInterface&) = delete;
  JunctionInterface& operator=(JunctionInterface&&) = delete;
  virtual ~JunctionInterface() = default;

  /**
   * @brief Add Connection to the junction interface
   *
   * @param id                Id of the connection
   * @param incomingRoadId    Id of the incoming road
   * @param connectingRoadId  Id of the connecting road
   * @param conatctPoint      Type of the contact point
   * @return Returns reference to the connection interface
   */
  virtual ConnectionInterface* AddConnection(std::string id,
                                             std::string incomingRoadId,
                                             std::string connectingRoadId,
                                             ContactPointType conatctPoint)
      = 0;

  /**
   * Add priority to the junction interface
   * @param priority Priority of the junction interface
   */
  virtual void AddPriority(Priority priority) = 0;

  /// @return Returns the map of connection id and the reference to the connection interface
  virtual std::map<std::string, ConnectionInterface*> GetConnections() const = 0;

  /// @return Get the list of priorities
  virtual const std::vector<Priority>& GetPriorities() const = 0;

  /// @return Get Id of the Junction Interface
  virtual std::string GetId() const = 0;

private:
};

#endif  // JUNCTIONINTERFACE
