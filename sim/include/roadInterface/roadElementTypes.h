/********************************************************************************
 * Copyright (c) 2016-2018 ITK Engineering GmbH
 *               2017-2021 in-tech GmbH
 *               2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  roadElementTypes.h
//! @brief This file contains enumerations for different types of roads.
//-----------------------------------------------------------------------------

#pragma once

#include <algorithm>
#include <array>
#include <list>
#include <optional>
#include <string>
#include <units.h>
#include <vector>

using namespace units::literals;

#include <units.h>

using namespace units::literals;

//-----------------------------------------------------------------------------
//! Road link connection orientation
//-----------------------------------------------------------------------------
enum class RoadLinkType
{
  Undefined = 0,
  Predecessor,
  Successor
};

//-----------------------------------------------------------------------------
//! Road link connection type
//-----------------------------------------------------------------------------
enum class RoadLinkElementType
{
  Undefined = 0,
  Road,
  Junction
};

//-----------------------------------------------------------------------------
//! Contact point of connections
//-----------------------------------------------------------------------------
enum class ContactPointType
{
  Undefined = 0,
  Start,
  End
};

//-----------------------------------------------------------------------------
//! Type of lane
//-----------------------------------------------------------------------------
enum class RoadLaneType  // https://releases.asam.net/OpenDRIVE/1.6.0/ASAM_OpenDRIVE_BS_V1-6-0.html#_lanes Section 9.5.3
                         // Lane type
{
  Undefined = 0,
  Shoulder,
  Border,
  Driving,
  Stop,
  None,
  Restricted,
  Parking,
  Median,
  Biking,
  Sidewalk,
  Curb,
  Exit,
  Entry,
  OnRamp,
  OffRamp,
  ConnectingRamp,
  Tram,
  RoadWorks,
  Bidirectional
};

namespace openpass::utils
{

/// @brief constexpr map for transforming the a corresponding enumeration into
///        a string representation: try to_cstr(EnumType) or to_string(EnumType)
static constexpr std::array<const char *, 20> RoadLaneTypeMapping{
    "Undefined", "Shoulder", "Border",         "Driving",  "Stop",      "None",         "Restricted",
    "Parking",   "Median",   "Biking",         "Sidewalk", "Curb",      "Exit",         "Entry",
    "OnRamp",    "OffRamp",  "ConnectingRamp", "Tram",     "RoadWorks", "Bidirectional"};

/// @brief Convert RoadLaneType to cstr (constexpr)
/// @param roadLaneType Type of road lane
/// @return Returns type of road lane as string
constexpr const char *to_cstr(RoadLaneType roadLaneType)
{
  return RoadLaneTypeMapping[static_cast<size_t>(roadLaneType)];
}

/// @brief Convert RoadLaneType to std::string
/// @param roadLaneType Type of road lane
/// @return Returns type of road lane as string
inline std::string to_string(RoadLaneType roadLaneType) noexcept
{
  return std::string(to_cstr(roadLaneType));
}

}  //namespace openpass::utils

//-----------------------------------------------------------------------------
//! Type of lane line
//-----------------------------------------------------------------------------
enum class RoadLaneRoadMarkType  // https://www.asam.net/standards/detail/opendrive/
{
  Undefined = 0,
  None,
  Solid,
  Broken,
  Solid_Solid,    // (for double solid line)
  Solid_Broken,   //(from inside to outside , exception: center lane  - from left to right)
  Broken_Solid,   //(from inside to outside, exception: center lane - from left to right)
  Broken_Broken,  // (from inside to outside, exception: center lane- from left to right)
  Botts_Dots,
  Grass,  //(meaning a grass edge)
  Curb
};

namespace openpass::utils
{

/// @brief constexpr map for transforming the a corresponding enumeration into
///        a string representation: try to_cstr(EnumType) or to_string(EnumType)
static constexpr std::array<const char *, 11> RoadLaneRoadMarkTypeMapping{"Undefined",
                                                                          "None",
                                                                          "Solid",
                                                                          "Broken",
                                                                          "Solid_Solid",
                                                                          "Solid_Broken",
                                                                          "Broken_Solid",
                                                                          "Broken_Broken",
                                                                          "Botts_Dots",
                                                                          "Grass",
                                                                          "Curb"};

/// @brief Convert roadLaneRoadMarkType to cstr (constexpr)
/// @param roadLaneRoadMarkType Type of roadLaneRoadMark
/// @return Returns Type of roadLaneRoadMark as a string
constexpr const char *to_cstr(RoadLaneRoadMarkType roadLaneRoadMarkType)
{
  return RoadLaneRoadMarkTypeMapping[static_cast<size_t>(roadLaneRoadMarkType)];
}

/// @brief Convert RoadLaneRoadMarkType to std::string
/// @param roadLaneRoadMarkType Type of roadLaneRoadMark
/// @return Returns Type of roadLaneRoadMark as a string
inline std::string to_string(RoadLaneRoadMarkType roadLaneRoadMarkType) noexcept
{
  return std::string(to_cstr(roadLaneRoadMarkType));
}

}  //namespace openpass::utils

//-----------------------------------------------------------------------------
//! Lane description: left, right or center
//-----------------------------------------------------------------------------
enum class RoadLaneRoadDescriptionType  // https://www.asam.net/standards/detail/opendrive/
{
  Left,
  Right,
  Center
};

//-----------------------------------------------------------------------------
//! LaneChange of the lane line
//-----------------------------------------------------------------------------
enum class RoadLaneRoadMarkLaneChange  // https://www.asam.net/standards/detail/opendrive/
{
  Undefined = 0,
  None,
  Both,
  Increase,
  Decrease
};

//-----------------------------------------------------------------------------
//! Color of the road mark
//-----------------------------------------------------------------------------
enum class RoadLaneRoadMarkColor  // https://www.asam.net/standards/detail/opendrive/
{
  Undefined = 0,
  Blue,
  Green,
  Red,
  Yellow,
  White,
  Orange
};

//! Weight of the road mark
enum class RoadLaneRoadMarkWeight  // https://www.asam.net/standards/detail/opendrive/
{
  Undefined = 0,
  Standard,
  Bold
};

/// @brief orientation of the road element
enum class RoadElementOrientation
{
  both,
  positive,
  negative,
};

//-----------------------------------------------------------------------------
//! Units used by signals
//-----------------------------------------------------------------------------
enum class RoadSignalUnit  // https://www.asam.net/standards/detail/opendrive/
{
  Undefined = 0,
  Meter,
  Kilometer,
  Feet,
  LandMile,
  MetersPerSecond,
  MilesPerHour,
  KilometersPerHour,
  Kilogram,
  MetricTons,
  Percent
};

/// @brief Validity of an road element
struct RoadElementValidity
{
  bool all{false};         ///< validity of all road element
  std::vector<int> lanes;  ///< list of all lanes
};

///
/// \brief The RoadSignalSpecification struct
///
/// Raw specification of the fields of a signal following
/// OpenDRIVE 1.6
///
struct RoadSignalSpecification
{
  units::length::meter_t s{0};                ///< s coordinate of the road signal
  units::length::meter_t t{0};                ///< t coordinate of the road signal
  std::string id{};                           ///< id of the road signal
  std::string name{};                         ///< name of the road signal
  std::string dynamic{};                      ///< dynmaic of the road signal
  std::string orientation{};                  ///< orientation of the road signal
  units::length::meter_t zOffset{0};          ///< z offset of the road signal
  std::string country{};                      ///< country at which road signal is available
  std::string countryRevision{};              ///< revision of the country at which road signal is available
  std::string type{};                         ///< type of the road signal
  std::string subtype{};                      ///< sub type of the road signal
  std::optional<double> value{std::nullopt};  ///< value of the road signal
  RoadSignalUnit unit{};                      ///< unit of the road signal
  units::length::meter_t height{0};           ///< height of the road signal
  units::length::meter_t width{0};            ///< width of the road signal
  std::string text{};                         ///< text about the road signal
  units::angle::radian_t hOffset{0};          ///< h offset of the road signal
  units::angle::radian_t pitch{0};            ///< pitch of the road signal
  units::angle::radian_t roll{0};             ///< roll of the road signal
  units::angle::radian_t yaw{0};              ///< yaw of the road signal
  RoadElementValidity validity;               ///< Lane validity element
  std::vector<std::string> dependencyIds{};   ///< IDs of the controlled signals
};

/// @brief  Type of road object
enum class RoadObjectType
{
  none = -1,
  obstacle,
  car,
  pole,
  tree,
  vegetation,
  barrier,
  building,
  parkingSpace,
  patch,
  railing,
  trafficIsland,
  crosswalk,
  streetlamp,
  gantry,
  soundBarrier,
  van,
  bus,
  trailer,
  bike,
  motorbike,
  tram,
  train,
  pedestrian,
  wind,
  roadMark
};

namespace openpass::utils
{

/// @brief constexpr map for transforming the a corresponding enumeration into
///        a string representation: try to_cstr(EnumType) or to_string(EnumType)
static constexpr std::array<const char *, 26> RoadObjectTypeMapping{
    "None",     "Obstacle",     "Car",        "Pole",    "Tree",          "Vegetation", "Barrier",
    "Building", "ParkingSpace", "Patch",      "Railing", "TrafficIsland", "Crosswalk",  "StreetLamp",
    "Gantry",   "SoundBarrier", "Van",        "Bus",     "Trailer",       "Bike",       "Motorbike",
    "Tram",     "Train",        "Pedestrian", "Wind",    "RoadMark"};

/// @brief Converts the roadobject type to the string
/// @param roadObjectType Road object type
/// @return The road object type in string
constexpr const char *to_cstr(RoadObjectType roadObjectType)
{
  return RoadObjectTypeMapping[static_cast<size_t>(roadObjectType) - static_cast<size_t>(RoadObjectType::none)];
}

/// @brief Converts the roadobject type to the string
/// @param roadObjectType Road object type
/// @return The road object type in string
inline std::string to_string(RoadObjectType roadObjectType) noexcept
{
  return std::string(to_cstr(roadObjectType));
}

}  //namespace openpass::utils

//! The RoadObjectSpecification struct
//!
//! Raw specification of the fields of a object following
//! OpenDRIVE 1.6 http://www.opendrive.org/docs/OpenDRIVEFormatSpecRev1.4H.pdf page 65
struct RoadObjectSpecification
{
  RoadObjectType type{RoadObjectType::none};  //!< Type of the object
  std::string name{""};                       //!< Name of the object
  std::string id{""};                         //!< OpenDrive ID
  units::length::meter_t s{0};                //!< s-coordinate of object’s origin
  units::length::meter_t t{0};                //!< t-coordinate of object’s origin
  units::length::meter_t zOffset{0};  //!< z-offset of object’s origin relative to the elevation of the reference line
  units::length::meter_t validLength{0};  //!< Validity of object along s-axis (0.0 for point object)
  RoadElementOrientation orientation{RoadElementOrientation::positive};  //!< Orientation of the road object element
  units::length::meter_t width{0};   //!< Width of the angular object’s bounding box, alternative to radius
  units::length::meter_t length{0};  //!< Length of the object’s bounding box, alternative to radius
  units::length::meter_t radius{0};  //!< Radius of the circular object’s bounding box, alternative to length and width
  units::length::meter_t height{0};  //!< Height of the object’s bounding box
  units::angle::radian_t hdg{0};     //!< Heading angle of the object relative to road direction
  units::angle::radian_t pitch{0};   //!< Pitch angle relative to the x/y-plane
  units::angle::radian_t roll{0};    //!< Roll angle relative to the x/y-plane
  bool continuous{false};            //!< Flag which shows whether the object is continuous

  RoadElementValidity validity;  ///< Validity of road element

  /// @brief Check if the road object has met standard compliance
  /// @return true if s, validlength, length, width and radius are greater than or equal to zero
  bool checkStandardCompliance()
  {
    return s >= 0_m && validLength >= 0_m && length >= 0_m && width >= 0_m && radius >= 0_m;
  }

  /// @brief check simulator compliance of road object
  /// @return true if the length, width are greater than zero and radius is zero
  bool checkSimulatorCompliance()
  {
    return length > 0_m &&  // force physical dimensions
           width > 0_m &&   // force physical dimensions
           radius == 0_m;   // do not support radius
  }
};

/// @brief Optional interval
struct OptionalInterval
{
  bool isSet = {false};          ///< is option interval set
  units::length::meter_t start;  ///< start
  units::length::meter_t end;    ///< end
};

/// @brief Repeat data
struct ObjectRepeat
{
  units::length::meter_t s;         //!< s-coordinate of start position
  units::length::meter_t length;    //!< Length of the repeat area, along the reference line in s-direction
  units::length::meter_t distance;  //!< Distance between two instances of the object
  OptionalInterval t;               //!< Lateral offset of object
  OptionalInterval width;           //!< Width of the object
  OptionalInterval height;          //!< Height of the object
  OptionalInterval zOffset;         //!< z-offset of the object, relative to the elevation of the reference line

  /**
   * @brief Function to check limits
   *
   * @return true if s, length and distance values are greater than or equal to zero
   */
  bool checkLimits() { return s >= 0_m && length >= 0_m && distance >= 0_m; }
};

/// http://www.opendrive.org/docs/OpenDRIVEFormatSpecRev1.4H.pdf page 92
enum class RoadTypeInformation
{
  Undefined = 0,
  Unknown,
  Rural,
  Motorway,
  Town,
  LowSpeed,
  Pedestrian,
  Bicycle
};

/// Specifications of a road type
struct RoadTypeSpecification
{
  units::length::meter_t s{0};   ///< s coordinate
  RoadTypeInformation roadType;  ///< Information on road type
};
