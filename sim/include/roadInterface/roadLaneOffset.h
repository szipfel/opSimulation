/********************************************************************************
 * Copyright (c) 2017 ITK Engineering GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  roadLaneOffset.h
//! @brief This file contains a class representing a lane offset,
//!        i.e. a lateral shift of the reference line, as a cubic polynomial.
//-----------------------------------------------------------------------------

#ifndef ROADLANEOFFSET
#define ROADLANEOFFSET

//-----------------------------------------------------------------------------
//! Class representing a lane offset, i.e. a lateral shift of the reference line,
//! as a cubic polynomial.
//-----------------------------------------------------------------------------
class RoadLaneOffset
{
public:
  /**
   * @brief Construct a new Road Lane Offset object
   *
   * @param s //< start position of the offset
   * @param a //< constant factor of the polynomial
   * @param b //< linear factor of the polynomial
   * @param c //< quadratic factor of the polynomial
   * @param d //< cubic factor of the polynomial
   */
  RoadLaneOffset(units::length::meter_t s,
                 units::length::meter_t a,
                 double b,
                 units::unit_t<units::inverse<units::length::meter>> c,
                 units::unit_t<units::inverse<units::squared<units::length::meter>>> d)
      : s(s), a(a), b(b), c(c), d(d)
  {
  }
  RoadLaneOffset(const RoadLaneOffset&) = delete;
  RoadLaneOffset(RoadLaneOffset&&) = delete;
  RoadLaneOffset& operator=(const RoadLaneOffset&) = delete;
  RoadLaneOffset& operator=(RoadLaneOffset&&) = delete;
  virtual ~RoadLaneOffset() = default;

  //-----------------------------------------------------------------------------
  //! Returns the start position (s-coordinate) of the offset.
  //!
  //! @return                         start position of the offset
  //-----------------------------------------------------------------------------
  units::length::meter_t GetS() const { return s; }

  //-----------------------------------------------------------------------------
  //! Returns the constant factor of the polynomial.
  //!
  //! @return                         constant factor of the polynomial
  //-----------------------------------------------------------------------------
  units::length::meter_t GetA() const { return a; }

  //-----------------------------------------------------------------------------
  //! Returns the linear factor of the polynomial.
  //!
  //! @return                         linear factor of the polynomial
  //-----------------------------------------------------------------------------
  double GetB() const { return b; }

  //-----------------------------------------------------------------------------
  //! Returns the quadratic factor of the polynomial.
  //!
  //! @return                         quadratic factor of the polynomial
  //-----------------------------------------------------------------------------
  units::unit_t<units::inverse<units::length::meter>> GetC() const { return c; }

  //-----------------------------------------------------------------------------
  //! Returns the cubic factor of the polynomial.
  //!
  //! @return                         cubic factor of the polynomial
  //-----------------------------------------------------------------------------
  units::unit_t<units::inverse<units::squared<units::length::meter>>> GetD() const { return d; }

private:
  units::length::meter_t s;
  units::length::meter_t a;
  double b;
  units::unit_t<units::inverse<units::length::meter>> c;
  units::unit_t<units::inverse<units::squared<units::length::meter>>> d;
};

#endif  // ROADLANEOFFSET
