/********************************************************************************
 * Copyright (c) 2018-2020 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

enum class RoadLaneRoadMarkType;
enum class RoadLaneRoadMarkColor;
enum class RoadLaneRoadMarkLaneChange;
enum class RoadLaneRoadDescriptionType;
enum class RoadLaneRoadMarkWeight;

/// @brief Class representing a road lane road mark
class RoadLaneRoadMark
{
public:
  /**
   * @brief Construct a new Road Lane Road Mark object
   *
   * @param sOffset           s coordinate where this road mark starts
   * @param descriptionType   Lane boundary location (left, right, center)
   * @param type              Road mark type according to OpenDRIVE (e.g. broken, solid, ...)
   * @param color             Road mark color
   * @param laneChange        Allowed lane change directions
   * @param weight            Weight of the road mark
   */
  RoadLaneRoadMark(units::length::meter_t sOffset,
                   RoadLaneRoadDescriptionType descriptionType,
                   RoadLaneRoadMarkType type,
                   RoadLaneRoadMarkColor color,
                   RoadLaneRoadMarkLaneChange laneChange,
                   RoadLaneRoadMarkWeight weight)
      : sOffset(sOffset),
        type(type),
        color(color),
        laneChange(laneChange),
        descriptionType(descriptionType),
        weight(weight)
  {
  }

  /**
   * Gets the roadmark type (e.g. broken, solid, ...)
   *
   * @return The type of the road mark
   */
  RoadLaneRoadMarkType GetType() const { return type; }

  /**
   * @brief Get s offset
   *
   * @return s offset
   */
  units::length::meter_t GetSOffset() const { return sOffset; }

  /**
   * @brief get s end
   *
   * @return s end
   */
  units::length::meter_t GetSEnd() const { return sEnd; }

  /**
   * @brief Get road mark Color
   *
   * @return Road mark Color
   */
  RoadLaneRoadMarkColor GetColor() const { return color; }

  /**
   * @brief Get the weight
   *
   * @return Road mark weight
   */
  RoadLaneRoadMarkWeight GetWeight() const { return weight; }

  /**
   * @brief Get the Lane Change
   *
   * @return Road mark Lane Change
   */
  RoadLaneRoadMarkLaneChange GetLaneChange() const { return laneChange; }

  /**
   * @brief Get the Description Type
   *
   * @return Road mark description
   */
  RoadLaneRoadDescriptionType GetDescriptionType() const { return descriptionType; }

  /**
   * Limit the end s coordinate of the road mark
   *
   * Setting a limit higher than the current end s coordinate has no effect.
   *
   * @param limit The maximum s coordinate of the road mark
   */
  void LimitSEnd(units::length::meter_t limit) { sEnd = std::min(sEnd, limit); }

private:
  units::length::meter_t sOffset;
  units::length::meter_t sEnd{std::numeric_limits<double>::max()};
  RoadLaneRoadMarkType type;
  RoadLaneRoadMarkColor color;
  RoadLaneRoadMarkLaneChange laneChange;
  RoadLaneRoadDescriptionType descriptionType;
  RoadLaneRoadMarkWeight weight;
};
