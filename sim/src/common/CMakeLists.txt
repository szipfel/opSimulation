################################################################################
# Copyright (c) 2020-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################
add_openpass_target(
  NAME Common TYPE library LINKAGE shared COMPONENT common

  HEADERS
    accelerationSignal.h
    agentCompToCompCtrlSignal.h
    acquirePositionSignal.h
    boostGeometryCommon.h
    commonTools.h
    compCtrlToAgentCompSignal.h
    compatibility.h
    dynamicsSignal.h
    eventTypes.h
    globalDefinitions.h
    lateralSignal.h
    longitudinalSignal.h
    observationLibraryDefinitions.h
    opExport.h
    openPassTypes.h
    openPassUtils.h
    parameter.h
    parametersVehicleSignal.h
    primitiveSignals.h
    runtimeInformation.h
    secondaryDriverTasksSignal.h
    sensorDataSignal.h
    sensorDefinitions.h
    spawnPointLibraryDefinitions.h
    speedActionSignal.h
    steeringSignal.h
    stochasticDefinitions.h
    trajectorySignal.h
    vector2d.h
    vector3d.h
    vectorSignals.h
    version.h
    worldDefinitions.h

  SOURCES
    commonTools.cpp
    vector3d.cpp
)
