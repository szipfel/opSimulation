/********************************************************************************
 * Copyright (c) 2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  DetectedObject.h
//! @brief This class is responsible for handling the data measured by sensors
//-----------------------------------------------------------------------------
#pragma once
#include <map>

#include "include/worldObjectInterface.h"

/// @brief  Class representing detected object for handling the data measured by sensors
class DetectedObject
{
public:
  /**
   * @brief Construct a new Detected Object object
   *
   * @param worldObject Pointer to the world interface
   */
  DetectedObject(const WorldObjectInterface* worldObject) : worldObject(worldObject) {}

  //-----------------------------------------------------------------------------
  //! Return the world object.
  //!
  //! @return  world object
  //-----------------------------------------------------------------------------
  const WorldObjectInterface* GetWorldObject() const;

  //-----------------------------------------------------------------------------
  //! Store desired sensor value.
  //! @param[in] typeOfSensor     sensor type
  //! @param[in] Id               component ID of sensor
  //-----------------------------------------------------------------------------
  void SetSensorMetadata(std::string typeOfSensor, int Id);

  /**
   * @brief Get the Sensor Type object
   *
   * @return Returns the type of the sensor that detected the object
   */
  std::string GetSensorType() const;

  /**
   * @brief Get the Sensor Id object
   *
   * @return Returns the id of the sensor that detected the object
   */
  int GetSensorId() const;

  /**
   * @brief Function to check the equality of two detected objects
   *
   * @param dObj1 first detected object
   * @param dObj2 second detected object
   * @return true if detected objects are considered equal, false otherwise
   */
  friend bool operator==(const DetectedObject& dObj1, const DetectedObject& dObj2);

private:
  const WorldObjectInterface* worldObject;

  std::string sensorType = "";
  int sensorId = -999;
};
