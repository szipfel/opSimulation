/********************************************************************************
 * Copyright (c) 2018 AMFD GmbH
 *               2017-2019 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  accelerationSignal.h
//! @brief This file contains all functions for class
//! AccelerationSignal
//!
//! This class contains all functionality of the module.
//-----------------------------------------------------------------------------

#pragma once

#include <sstream>
#include <string>
#include <units.h>

#include "include/signalInterface.h"

//-----------------------------------------------------------------------------
//! Signal class
//-----------------------------------------------------------------------------
class AccelerationSignal : public ComponentStateSignalInterface
{
public:
  /// COMPONENT NAME
  const std::string COMPONENTNAME = "AccelerationSignal";

  //-----------------------------------------------------------------------------
  //! Constructor
  //-----------------------------------------------------------------------------
  AccelerationSignal() { componentState = ComponentState::Disabled; }

  /**
   * @brief Construct a new Acceleration Signal object
   *
   * @param other acceleration signals
   */
  AccelerationSignal(AccelerationSignal& other) : AccelerationSignal(other.componentState, other.acceleration) {}

  /**
   * @brief Construct a new Acceleration Signal object
   *
   * @param componentState state of the component
   * @param acceleration   acceleration
   */
  AccelerationSignal(ComponentState componentState, units::acceleration::meters_per_second_squared_t acceleration)
      : ComponentStateSignalInterface{componentState}, acceleration(acceleration)
  {
  }

  /**
   * @brief Construct a new Acceleration Signal object
   *
   * @param componentState state of the component
   * @param acceleration   acceleration
   * @param source         name of the source component
   */
  AccelerationSignal(ComponentState componentState,
                     units::acceleration::meters_per_second_squared_t acceleration,
                     const std::string& source)
      : ComponentStateSignalInterface{componentState}, acceleration(acceleration), source(source)
  {
  }

  AccelerationSignal(const AccelerationSignal&) = delete;
  AccelerationSignal(AccelerationSignal&&) = delete;
  AccelerationSignal& operator=(const AccelerationSignal&) = delete;
  AccelerationSignal& operator=(AccelerationSignal&&) = delete;

  virtual ~AccelerationSignal() {}

  /**
   * Returns the content/payload of the signal as an std::string
   *
   * @return string Content/payload of the signal as an std::string
   */
  virtual operator std::string() const
  {
    std::ostringstream stream;
    stream << COMPONENTNAME << std::endl;
    stream << "source: " << source << std::endl;
    stream << "acceleration: " << acceleration << std::endl;
    return stream.str();
  }

  units::acceleration::meters_per_second_squared_t acceleration;  ///< acceleration
  std::string source{};                                           ///< name of the source component
};
