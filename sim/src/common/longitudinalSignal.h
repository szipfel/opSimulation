/********************************************************************************
 * Copyright (c) 2018 AMFD GmbH
 *               2016-2017 ITK Engineering GmbH
 *               2017-2019 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  longitudinalSignal.h
//! @brief This file contains all functions for the class longitudinalSignal
//!
//! This class contains all functionality of the signal.
//-----------------------------------------------------------------------------
#pragma once

#include <sstream>
#include <string>
#include <units.h>

#include "include/signalInterface.h"

/// This class contains all functionality of the longitudinal signal
class LongitudinalSignal : public ComponentStateSignalInterface
{
public:
  /// component name
  static constexpr char COMPONENTNAME[] = "LongitudinalSignal";

  //-----------------------------------------------------------------------------
  //! Constructor
  //-----------------------------------------------------------------------------
  LongitudinalSignal() : ComponentStateSignalInterface{ComponentState::Disabled} {}

  /**
   * @brief Construct a new Longitudinal Signal object
   *
   * @param componentState state of the component
   * @param accPedalPos    position of the accelerator pedal
   * @param brakePedalPos  position of the brake pedal
   * @param gear           gear number
   */
  LongitudinalSignal(ComponentState componentState, double accPedalPos, double brakePedalPos, int gear)
      : ComponentStateSignalInterface{componentState},
        accPedalPos{accPedalPos},
        brakePedalPos{brakePedalPos},
        gear{gear}
  {
  }

  /**
   * @brief Construct a new Longitudinal Signal object
   *
   * @param componentState state of the component
   * @param accPedalPos    position of the accelerator pedal
   * @param brakePedalPos  position of the brake pedal
   * @param gear           gear number
   * @param source         name of the source component
   */
  LongitudinalSignal(
      ComponentState componentState, double accPedalPos, double brakePedalPos, int gear, const std::string& source)
      : ComponentStateSignalInterface{componentState},
        accPedalPos{accPedalPos},
        brakePedalPos{brakePedalPos},
        gear{gear},
        source{source}
  {
  }

  /// default constructor
  LongitudinalSignal(const LongitudinalSignal&) = default;

  /// default constructor
  LongitudinalSignal(LongitudinalSignal&&) = default;

  /// @return Returns LongitudinalSignal with operator= overload
  LongitudinalSignal& operator=(const LongitudinalSignal&) = default;

  /// @return Returns LongitudinalSignal with operator= overload
  LongitudinalSignal& operator=(LongitudinalSignal&&) = default;

  virtual ~LongitudinalSignal() = default;

  /// position of acceleration pedal
  double accPedalPos{};
  /// position of brake pedal
  double brakePedalPos{};
  /// gear
  int gear{};
  /// name of the source component
  std::string source{};

  //-----------------------------------------------------------------------------
  //! Returns the content/payload of the signal as an std::string
  //!
  //! @return     Content/payload of the signal as an std::string
  //-----------------------------------------------------------------------------
  virtual operator std::string() const
  {
    std::ostringstream stream;
    stream << COMPONENTNAME << '\n';
    stream << "source:        " << source << std::endl;
    stream << "accPedalPos:   " << accPedalPos << '\n';
    stream << "brakePedalPos: " << brakePedalPos << '\n';
    stream << "gear:          " << gear << '\n';
    return stream.str();
  }
};
