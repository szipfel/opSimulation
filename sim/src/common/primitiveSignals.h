/********************************************************************************
 * Copyright (c) 2016-2021 ITK Engineering GmbH
 *               2017-2019 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  primitiveSignals.h
//! @brief This file contains an interface to predefined primitive signals for
//!        usage by component models
//-----------------------------------------------------------------------------

#ifndef PRIMITIVESIGNALS_H
#define PRIMITIVESIGNALS_H

#include "include/modelInterface.h"

//-----------------------------------------------------------------------------
//! Primitive signal class for double values
//-----------------------------------------------------------------------------
class DoubleSignal : public ComponentStateSignalInterface
{
public:
  /**
   * @brief Construct a new Double Signal object
   *
   * @param inValue Double value
   * @param state   component state
   */
  DoubleSignal(double inValue, ComponentState state = ComponentState::Undefined) : value(inValue)
  {
    componentState = state;
  }
  DoubleSignal(const DoubleSignal &) = default;  ///< Copy constructor
  DoubleSignal(DoubleSignal &&) = default;       ///< Move constructor
  /// @return Copy assignment constructor
  DoubleSignal &operator=(const DoubleSignal &) = default;
  /// @return Move assignment constructor
  DoubleSignal &operator=(DoubleSignal &&) = default;
  virtual ~DoubleSignal() = default;

  /**
   * @brief Converts signal to string
   *
   * @return signal as string
   */
  virtual operator std::string() const { return std::to_string(value); }

  double value;  //!< signal content
};

//-----------------------------------------------------------------------------
//! Primitive signal class for integer values
//-----------------------------------------------------------------------------
class IntSignal : public ComponentStateSignalInterface
{
public:
  /**
   * @brief Construct a new Int Signal object
   *
   * @param inValue integer value
   * @param state   state of the component
   */
  IntSignal(int inValue, ComponentState state = ComponentState::Undefined) : value(inValue) { componentState = state; }
  IntSignal(const IntSignal &) = default;  ///< Copy constructor
  IntSignal(IntSignal &&) = default;       ///< Move constructor
  /// @return Copy assignment constructor
  IntSignal &operator=(const IntSignal &) = default;
  /// @return Move assignemnt constructor
  IntSignal &operator=(IntSignal &&) = default;
  virtual ~IntSignal() = default;

  /**
   * @brief Converts signal to string
   *
   * @return signal as string
   */
  virtual operator std::string() const { return std::to_string(value); }

  int value;  //!< signal content
};

//-----------------------------------------------------------------------------
//! Primitive signal class for boolean values
//-----------------------------------------------------------------------------
class BoolSignal : public ComponentStateSignalInterface
{
public:
  /**
   * @brief Construct a new Bool Signal object
   *
   * @param inValue Boolean value
   * @param state   State of the component
   */
  BoolSignal(bool inValue, ComponentState state = ComponentState::Undefined) : value(inValue)
  {
    componentState = state;
  }
  BoolSignal(const BoolSignal &) = default;  ///< Copy constructor
  BoolSignal(BoolSignal &&) = default;       ///< Move constructor
  /// @return Copy assignment constructor
  BoolSignal &operator=(const BoolSignal &) = default;
  /// @return Move assignment constructor
  BoolSignal &operator=(BoolSignal &&) = default;
  virtual ~BoolSignal() = default;

  /**
   * @brief converts signal to string
   *
   * @return signal as string
   */
  virtual operator std::string() const { return std::to_string(value); }

  bool value;  ///< signal content
};

//-----------------------------------------------------------------------------
//! Primitive signal class for componentState values
//-----------------------------------------------------------------------------
class ComponentStateSignal : public ComponentStateSignalInterface
{
public:
  /**
   * @brief Construct a new Component State Signal object
   *
   * @param state Component state
   */
  ComponentStateSignal(ComponentState state) { componentState = state; }
  ComponentStateSignal(const ComponentStateSignal &) = default;  ///< Copy constructor
  ComponentStateSignal(ComponentStateSignal &&) = default;       ///< Move constructor
  /// @return Copy assignment constructor
  ComponentStateSignal &operator=(const ComponentStateSignal &) = default;
  /// @return move assignment constructor
  ComponentStateSignal &operator=(ComponentStateSignal &&) = default;
  virtual ~ComponentStateSignal() = default;

  /**
   * @brief Converts signal to string
   *
   * @return signal as string
   */
  virtual operator std::string() const { return ""; }
};

#endif  // PRIMITIVESIGNALS_H
