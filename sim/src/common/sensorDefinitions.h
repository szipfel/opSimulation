/********************************************************************************
 * Copyright (c) 2019 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Common/pose.h>
#include <MantleAPI/Traffic/entity_properties.h>

#include "common/parameter.h"

namespace openpass::sensors
{
/// sensor position information
struct Position
{
  /// identification, e.g "left front"
  std::string name{};
  /// pose of the sensor
  mantle_api::Pose pose{};
};

/// profile of sensor
struct Profile
{
  /// name of sensor profile
  std::string name{};
  /// type of sensor profile
  std::string type{};
  /// parameter set level 1
  openpass::parameter::ParameterSetLevel1 parameter{};
};

/// sensor parameters
struct Parameter
{
  /// id
  int id{};
  /// position name of the sensor
  std::string positionName{};
  /// profile of the sensor
  Profile profile{};
};

static double GetPositionValue(const std::string& valueName,
                               const std::string& positionName,
                               const mantle_api::VehicleProperties& vehicleProperties)
{
  auto value = vehicleProperties.properties.find("SensorPosition/" + positionName + "/" + valueName);
  if (value == vehicleProperties.properties.cend())
  {
    throw std::runtime_error("No value \"" + valueName + "\" for SensorPosition \"" + positionName
                             + "\" defined for VehicleModel \"" + vehicleProperties.model + "\"");
  }
  return std::stod(value->second);
}

static Position GetPosition(const std::string& positionName, const mantle_api::VehicleProperties& vehicleProperties)
{
  Position position;
  position.name = positionName;
  position.pose.position.x = units::length::meter_t(GetPositionValue("Longitudinal", positionName, vehicleProperties));
  position.pose.position.y = units::length::meter_t(GetPositionValue("Lateral", positionName, vehicleProperties));
  position.pose.position.z = units::length::meter_t(GetPositionValue("Height", positionName, vehicleProperties));
  position.pose.orientation.yaw = units::angle::radian_t(GetPositionValue("Yaw", positionName, vehicleProperties));
  position.pose.orientation.pitch = units::angle::radian_t(GetPositionValue("Pitch", positionName, vehicleProperties));
  position.pose.orientation.roll = units::angle::radian_t(GetPositionValue("Roll", positionName, vehicleProperties));
  return position;
}

/// typedef profiles as vector of profiles
using Profiles = std::vector<Profile>;

/// typedef parameters as vector of parameter
using Parameters = std::vector<Parameter>;

}  // namespace openpass::sensors
