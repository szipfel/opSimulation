/********************************************************************************
 * Copyright (c) 2020 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  trajectorySignal.h
//! @brief This file contains all functions for class
//! TrajectorySignal
//!
//! This class contains all functionality of the module.
//-----------------------------------------------------------------------------

#pragma once

#include "MantleAPI/Common/trajectory.h"
#include "include/modelInterface.h"

//-----------------------------------------------------------------------------
//! Signal class
//-----------------------------------------------------------------------------
class TrajectorySignal : public ComponentStateSignalInterface
{
public:
  /// component name
  static constexpr char COMPONENTNAME[] = "TrajectorySignal";

  //-----------------------------------------------------------------------------
  //! Constructor
  //-----------------------------------------------------------------------------
  TrajectorySignal() { componentState = ComponentState::Disabled; }

  /**
   * @brief Construct a new Trajectory Signal object
   *
   * @param other Another trajectory signal
   */
  TrajectorySignal(TrajectorySignal& other) : TrajectorySignal(other.componentState, other.trajectory) {}

  /**
   * @brief Construct a new Trajectory Signal object
   *
   * @param componentState State of the component
   * @param trajectory     Openscenarion trajectory
   */
  TrajectorySignal(ComponentState componentState, mantle_api::Trajectory trajectory) : trajectory(trajectory)
  {
    this->componentState = componentState;
  }

  TrajectorySignal(const TrajectorySignal&) = delete;
  TrajectorySignal(TrajectorySignal&&) = delete;
  TrajectorySignal& operator=(const TrajectorySignal&) = delete;
  TrajectorySignal& operator=(TrajectorySignal&&) = delete;

  virtual ~TrajectorySignal() {}

  //-----------------------------------------------------------------------------
  //! Returns the content/payload of the signal as an std::string
  //!
  //! @return                       Content/payload of the signal as an std::string
  //-----------------------------------------------------------------------------
  virtual operator std::string() const
  {
    std::ostringstream stream;
    stream << COMPONENTNAME << "\n" << trajectory;
    return stream.str();
  }

  /// trajectory
  mantle_api::Trajectory trajectory;
};
