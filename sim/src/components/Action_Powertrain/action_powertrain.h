/********************************************************************************
 * Copyright (c) 2023 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  action_powertrain.h
//! @brief contains DLL export declarations
//-----------------------------------------------------------------------------

#pragma once

#include <QtCore/qglobal.h>

#if defined(ACTION_POWERTRAIN_LIBRARY)
#define ACTION_POWERTRAIN_SHARED_EXPORT Q_DECL_EXPORT
#else
#define ACTION_POWERTRAIN_SHARED_EXPORT Q_DECL_IMPORT
#endif

#include "include/modelInterface.h"
