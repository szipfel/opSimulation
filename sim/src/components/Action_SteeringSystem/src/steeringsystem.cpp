/********************************************************************************
 * Copyright (c) 2023 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** \file  steeringsystem.cpp */
//-----------------------------------------------------------------------------

#include "steeringsystem.h"

#include <exception>

#include <qglobal.h>

#include "common/primitiveSignals.h"
#include "include/parameterInterface.h"

ActionSteeringSystem::ActionSteeringSystem(std::string componentName,
                                           bool isInit,
                                           int priority,
                                           int offsetTime,
                                           int responseTime,
                                           int cycleTime,
                                           StochasticsInterface *stochastics,
                                           WorldInterface *world,
                                           const ParameterInterface *parameters,
                                           PublisherInterface *const publisher,
                                           const CallbackInterface *callbacks,
                                           AgentInterface *agent)
    : ActionInterface(componentName,
                      isInit,
                      priority,
                      offsetTime,
                      responseTime,
                      cycleTime,
                      stochastics,
                      world,
                      parameters,
                      publisher,
                      callbacks,
                      agent)
{
  // Get Steering ratio from vehicle model parameters
  std::shared_ptr<const mantle_api::VehicleProperties> vehicleProperties
      = std::dynamic_pointer_cast<const mantle_api::VehicleProperties>(GetAgent()->GetVehicleModelParameters());

  const auto steeringRatio = helper::map::query(vehicleProperties->properties, "SteeringRatio");
  THROWIFFALSE(steeringRatio.has_value(), "SteeringRatio was not defined in VehicleCatalog");
  SteeringRatio = std::stod(steeringRatio.value());
  // get external parameters
  ParseParameters(parameters);
}

void ActionSteeringSystem::ParseParameters(const ParameterInterface *parameters)
{
  std::vector<double> toe_temp = parameters->GetParametersDoubleVector().count("Toe") == 1
                                   ? parameters->GetParametersDoubleVector().at("Toe")
                                   : parameters->GetParametersDoubleVector().at("0");

  toe.resize(toe_temp.size());

  for (unsigned int idx = 0; idx < toe_temp.size(); idx++)
  {
    toe[idx] = toe_temp[idx] * 1_rad;
  }
}

void ActionSteeringSystem::UpdateInput(int localLinkId, const std::shared_ptr<SignalInterface const> &data, int time)
{
  std::stringstream log;
  log << COMPONENTNAME << " (component " << GetComponentName() << ", agent " << std::to_string(GetAgent()->GetId())
      << ", input data for local link " << localLinkId << ": ";
  LOG(CbkLogLevel::Debug, log.str());

  if (localLinkId == 0)
  {
    const std::shared_ptr<SteeringSignal const> signal = std::dynamic_pointer_cast<SteeringSignal const>(data);
    if (!signal)
    {
      const std::string msg = COMPONENTNAME + "_" + std::to_string(GetAgent()->GetId()) + " invalid signaltype";
      LOG(CbkLogLevel::Debug, msg);
      throw std::runtime_error(msg);
    }
    SteeringWheelAngle = signal->steeringWheelAngle;
  }
}

void ActionSteeringSystem::UpdateOutput(int localLinkId, std::shared_ptr<SignalInterface const> &signal, int time)
{
  Q_UNUSED(time);

  std::stringstream log;
  log << COMPONENTNAME << " UpdateOutput";
  LOG(CbkLogLevel::Debug, log.str());
  log.str(std::string());

  bool success = outputPorts.at(localLinkId)->GetSignalValue(signal);

  if (success)
  {
    log << COMPONENTNAME << " UpdateOutput successful";
    LOG(CbkLogLevel::Debug, log.str());
  }
  else
  {
    log << COMPONENTNAME << " UpdateOutput failed";
    LOG(CbkLogLevel::Error, log.str());
  }
}

void ActionSteeringSystem::CalculateWheelAngle()
{
  std::vector<double> wheel_angle;
  wheel_angle.resize(4);

  for (unsigned int idx = 0; idx < 4; idx++)
  {
    if (idx % 2 == 0)
      wheel_angle[idx] = -toe[idx].value();
    else
      wheel_angle[idx] = toe[idx].value();
  }
  wheel_angle[0] += SteeringWheelAngle.value() / SteeringRatio;
  wheel_angle[1] += SteeringWheelAngle.value() / SteeringRatio;

  WheelAngle.SetValue(wheel_angle);
}

void ActionSteeringSystem::Trigger(int time)
{
  // Set OSI steering wheel angle
  GetAgent()->SetSteeringWheelAngle(SteeringWheelAngle);
  // calculate wheel angle
  CalculateWheelAngle();
}