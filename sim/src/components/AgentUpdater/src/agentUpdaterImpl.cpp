/********************************************************************************
 * Copyright (c) 2018 AMFD GmbH
 *               2017-2019 in-tech GmbH
 *               2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  agentUpdaterImpl.cpp
//! @brief This file contains the implementation of the header file
//-----------------------------------------------------------------------------

#include "agentUpdaterImpl.h"

#include <QtGlobal>

void AgentUpdaterImplementation::UpdateInput(int localLinkId,
                                             const std::shared_ptr<SignalInterface const> &data,
                                             [[maybe_unused]] int time)
{
  if (localLinkId == 0)
  {
    // from DynamicsPrioritizer
    const std::shared_ptr<DynamicsSignal const> signal = std::dynamic_pointer_cast<DynamicsSignal const>(data);
    if (!signal)
    {
      const std::string msg = COMPONENTNAME + " invalid signaltype";
      LOG(CbkLogLevel::Debug, msg);
      throw std::runtime_error(msg);
    }

    dynamicsInformation = signal->dynamicsInformation;
    longitudinalController = signal->longitudinalController;
    lateralController = signal->lateralController;
  }
  else
  {
    const std::string msg = COMPONENTNAME + " invalid link";
    LOG(CbkLogLevel::Debug, msg);
    throw std::runtime_error(msg);
  }
}

void AgentUpdaterImplementation::UpdateOutput([[maybe_unused]] int localLinkId,
                                              [[maybe_unused]] std::shared_ptr<SignalInterface const> &data,
                                              [[maybe_unused]] int time)
{
}

void AgentUpdaterImplementation::Trigger([[maybe_unused]] int time)
{
  AgentInterface *agent = GetAgent();

  Validate(dynamicsInformation.acceleration, "acceleration");
  agent->SetAcceleration(dynamicsInformation.acceleration);
  Validate(dynamicsInformation.velocityX, "velocityX");
  Validate(dynamicsInformation.velocityY, "velocityY");
  agent->SetVelocityVector({dynamicsInformation.velocityX, dynamicsInformation.velocityY, 0.0_mps});
  Validate(dynamicsInformation.positionX, "positionX");
  agent->SetPositionX(dynamicsInformation.positionX);
  Validate(dynamicsInformation.positionY, "positionY");
  agent->SetPositionY(dynamicsInformation.positionY);
  Validate(dynamicsInformation.yaw, "yaw");
  agent->SetYaw(dynamicsInformation.yaw);
  Validate(dynamicsInformation.yawRate, "yawRate");
  agent->SetYawRate(dynamicsInformation.yawRate);
  Validate(dynamicsInformation.yawAcceleration, "yawAcceleration");
  agent->SetYawAcceleration(dynamicsInformation.yawAcceleration);
  Validate(dynamicsInformation.roll, "roll");
  agent->SetRoll(dynamicsInformation.roll);
  Validate(dynamicsInformation.steeringWheelAngle, "steeringWheelAngle");
  agent->SetSteeringWheelAngle(dynamicsInformation.steeringWheelAngle);
  Validate(dynamicsInformation.centripetalAcceleration, "centripetalAcceleration");
  agent->SetCentripetalAcceleration(dynamicsInformation.centripetalAcceleration);
  Validate(dynamicsInformation.travelDistance, "travelDistance");
  agent->SetDistanceTraveled(agent->GetDistanceTraveled() + dynamicsInformation.travelDistance);
  const auto velocity = units::math::hypot(dynamicsInformation.velocityX, dynamicsInformation.velocityY);
  agent->SetWheelsRotationRateAndOrientation(
      dynamicsInformation.steeringWheelAngle, velocity, dynamicsInformation.acceleration);

  GetPublisher()->Publish("LongitudinalController", longitudinalController);
  GetPublisher()->Publish("LateralController", lateralController);
}
