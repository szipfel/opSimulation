/********************************************************************************
 * Copyright (c) 2018 AMFD GmbH
 *               2020 HLRS, University of Stuttgart
 *               2016-2018 ITK Engineering GmbH
 *               2017-2019 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
/**********************************************
***********************************************/

#include "AlgorithmAFDM.h"

#include <exception>

#include "include/callbackInterface.h"
#include "src/followingDriverModel.h"

/// version of this module
const std::string Version = "0.0.1";
static const CallbackInterface *Callbacks = nullptr;

/**
 * dll-function to obtain the version of the current module
 *
 * @return                       Version of the current module
 */
extern "C" ALGORITHM_AGENTFOLLOWINGDRIVERMODEL_SHARED_EXPORT const std::string &OpenPASS_GetVersion()
{
  return Version;
}

/// @brief Create an instance of lateral algorithm
/// @param componentName    Name of the component
/// @param isInit           If instance initialzed
/// @param priority         Priority of the instance
/// @param offsetTime       Offset time
/// @param responseTime     Response time
/// @param cycleTime        Cycle time
/// @param stochastics      Reference to the stochastics interface
/// @param world            Reference to the world interface
/// @param parameters       Reference to the parameter interface
/// @param publisher        Reference to the publisher interface
/// @param agent            Reference to the agent interface
/// @param callbacks        Reference to the callback interface
/// @return  model interface
extern "C" ALGORITHM_AGENTFOLLOWINGDRIVERMODEL_SHARED_EXPORT ModelInterface *OpenPASS_CreateInstance(
    std::string componentName,
    bool isInit,
    int priority,
    int offsetTime,
    int responseTime,
    int cycleTime,
    StochasticsInterface *stochastics,
    WorldInterface *world,
    const ParameterInterface *parameters,
    PublisherInterface *const publisher,
    AgentInterface *agent,
    const CallbackInterface *callbacks)
{
  Callbacks = callbacks;

  try
  {
    return (ModelInterface *)(new (std::nothrow) AlgorithmAgentFollowingDriverModelImplementation(componentName,
                                                                                                  isInit,
                                                                                                  priority,
                                                                                                  offsetTime,
                                                                                                  responseTime,
                                                                                                  cycleTime,
                                                                                                  stochastics,
                                                                                                  world,
                                                                                                  parameters,
                                                                                                  publisher,
                                                                                                  callbacks,
                                                                                                  agent));
  }
  catch (const std::runtime_error &ex)
  {
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, ex.what());
    }

    return nullptr;
  }
  catch (...)
  {
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, "unexpected exception");
    }

    return nullptr;
  }
}
//-----------------------------------------------------------------------------
//! dll-function to destroy/delete an instance of the module.
//!
//! @param[in]     implementation    The instance that should be freed
//-----------------------------------------------------------------------------
extern "C" ALGORITHM_AGENTFOLLOWINGDRIVERMODEL_SHARED_EXPORT void OpenPASS_DestroyInstance(
    ModelInterface *implementation)
{
  delete (AlgorithmAgentFollowingDriverModelImplementation *)implementation;
}

extern "C" ALGORITHM_AGENTFOLLOWINGDRIVERMODEL_SHARED_EXPORT bool OpenPASS_UpdateInput(
    ModelInterface *implementation, int localLinkId, const std::shared_ptr<SignalInterface const> &data, int time)
{
  try
  {
    implementation->UpdateInput(localLinkId, data, time);
  }
  catch (const std::runtime_error &ex)
  {
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, ex.what());
    }

    return false;
  }
  catch (...)
  {
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, "unexpected exception");
    }

    return false;
  }

  return true;
}

extern "C" ALGORITHM_AGENTFOLLOWINGDRIVERMODEL_SHARED_EXPORT bool OpenPASS_UpdateOutput(
    ModelInterface *implementation, int localLinkId, std::shared_ptr<SignalInterface const> &data, int time)
{
  try
  {
    implementation->UpdateOutput(localLinkId, data, time);
  }
  catch (const std::runtime_error &ex)
  {
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, ex.what());
    }

    return false;
  }
  catch (...)
  {
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, "unexpected exception");
    }

    return false;
  }

  return true;
}

extern "C" ALGORITHM_AGENTFOLLOWINGDRIVERMODEL_SHARED_EXPORT bool OpenPASS_Trigger(ModelInterface *implementation,
                                                                                   int time)
{
  try
  {
    implementation->Trigger(time);
  }
  catch (const std::runtime_error &ex)
  {
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, ex.what());
    }

    return false;
  }
  catch (const std::exception &exstd)
  {
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, exstd.what());
    }

    return false;
  }
  catch (...)
  {
    if (Callbacks != nullptr)
    {
      std::exception_ptr p = std::current_exception();
#ifndef _MSC_VER
      const std::string exType = p ? p.__cxa_exception_type()->name() : "null";
#else
      const std::string exType = "null";
#endif

      Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, "unexpected exception - type: " + exType);
    }

    return false;
  }

  return true;
}
