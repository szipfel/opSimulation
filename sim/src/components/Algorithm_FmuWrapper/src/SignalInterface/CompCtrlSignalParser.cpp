/*******************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "CompCtrlSignalParser.h"

std::shared_ptr<const SignalInterface> CompCtrlSignalParser::translate(
    std::set<SignalType> outputSignals,
    ComponentState componentState,
    size_t tempFMI,
    FmuEnumerations fmuEnumerations,
    std::function<FmuValue &(SignalValue, VariableType)> GetFmuSignalValue)
{
  if (std::find(outputSignals.cbegin(), outputSignals.cend(), SignalType::CompCtrlSignal) != outputSignals.cend())
  {
    MovementDomain movementDomain = MovementDomain::Undefined;
    std::vector<ComponentWarningInformation> warnings;
    bool withWarningDirection
        = std::find(outputSignals.cbegin(), outputSignals.cend(), SignalType::CompCtrlSignalWarningDirection)
       != outputSignals.cend();

    auto movementDomains = fmuEnumerations.movementDomains.find(
        GetFmuSignalValue(SignalValue::CompCtrlSignal_MovementDomain, VariableType::Enum).intValue);
    if (movementDomains == fmuEnumerations.movementDomains.cend())
    {
      throw std::runtime_error(
          std::string("CompCtrlSignal: Enumeration is not defined in FMU or enumeration value is invalid for output "
                      "type CompCtrlSignal_MovementDomain"));
    }
    movementDomain = movementDomains->second;
    bool warningActivity = GetFmuSignalValue(SignalValue::CompCtrlSignal_WarningActivity, VariableType::Bool).boolValue;
    auto warningLevels = fmuEnumerations.warningLevels.find(
        GetFmuSignalValue(SignalValue::CompCtrlSignal_WarningLevel, VariableType::Enum).intValue);
    if (warningLevels == fmuEnumerations.warningLevels.cend())
    {
      throw std::runtime_error(
          std::string("CompCtrlSignal: Enumeration is not defined in FMU or enumeration value is invalid for output "
                      "type CompCtrlSignal_WarningLevel"));
    }
    ComponentWarningLevel warningLevel = warningLevels->second;
    auto warningTypes = fmuEnumerations.warningTypes.find(
        GetFmuSignalValue(SignalValue::CompCtrlSignal_WarningType, VariableType::Enum).intValue);
    if (warningTypes == fmuEnumerations.warningTypes.cend())
    {
      throw std::runtime_error(
          std::string("CompCtrlSignal: Enumeration is not defined in FMU or enumeration value is invalid for output "
                      "type CompCtrlSignal_WarningType"));
    }
    ComponentWarningType warningType = warningTypes->second;
    auto warningIntensities = fmuEnumerations.warningIntensities.find(
        GetFmuSignalValue(SignalValue::CompCtrlSignal_WarningIntensity, VariableType::Enum).intValue);
    if (warningIntensities == fmuEnumerations.warningIntensities.cend())
    {
      throw std::runtime_error(
          std::string("CompCtrlSignal: Enumeration is not defined in FMU or enumeration value is invalid for output "
                      "type CompCtrlSignal_WarningIntensity"));
    }
    ComponentWarningIntensity warningIntensity = warningIntensities->second;
    auto warningDirections = fmuEnumerations.warningDirections.find(
        GetFmuSignalValue(SignalValue::CompCtrlSignal_WarningDirection, VariableType::Enum).intValue);
    if (warningDirections == fmuEnumerations.warningDirections.cend())
    {
      throw std::runtime_error(
          std::string("CompCtrlSignal: Enumeration is not defined in FMU or enumeration value is invalid for output "
                      "type CompCtrlSignal_WarningDirection"));
    }
    auto direction = withWarningDirection ? std::make_optional(warningDirections->second) : std::nullopt;
    warnings.push_back(
        ComponentWarningInformation{warningActivity, warningLevel, warningType, warningIntensity, direction});

    return std::make_shared<VehicleCompToCompCtrlSignal const>(
        ComponentType::VehicleComponent, "FMU", componentState, movementDomain, warnings, AdasType::Safety);
  }
  else
  {
    return std::make_shared<VehicleCompToCompCtrlSignal const>(ComponentType::VehicleComponent,
                                                               "FMU",
                                                               ComponentState::Disabled,
                                                               MovementDomain::Undefined,
                                                               std::vector<ComponentWarningInformation>{},
                                                               AdasType::Safety);
  }
}