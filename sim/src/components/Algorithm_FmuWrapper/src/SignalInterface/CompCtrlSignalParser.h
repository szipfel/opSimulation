/*******************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <memory>

#include "common/agentCompToCompCtrlSignal.h"
#include "components/Algorithm_FmuWrapper/src/ChannelDefinitionParser.h"
#include "include/agentInterface.h"
#include "include/signalInterface.h"

//! @brief Parser for ComponentController signal
struct CompCtrlSignalParser
{
  //! @brief Translator function for ComponentController signal
  //!
  //! @param outputSignals      Container that contains a sorted set of output signal
  //! @param componentState     State of the component
  //! @param tempFMI            FMI version
  //! @param fmuEnumerations    Enumerations defined for the FMU
  //! @param GetFmuSignalValue  Function that creates FmuValue from value in an output signal and VariableType
  //! @return pointer to the signal interface
  static std::shared_ptr<const SignalInterface> translate(
      std::set<SignalType> outputSignals,
      ComponentState componentState,
      size_t tempFMI,
      FmuEnumerations fmuEnumerations,
      std::function<FmuValue &(SignalValue, VariableType)> GetFmuSignalValue);
};