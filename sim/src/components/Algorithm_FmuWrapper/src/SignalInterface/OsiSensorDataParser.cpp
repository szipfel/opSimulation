/*******************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "OsiSensorDataParser.h"

std::shared_ptr<const SignalInterface> OSISensorDataParser::translate(AgentInterface &agent,
                                                                      const osi3::SensorData &sensorDataOut)
{
  auto messageIn = static_cast<const google::protobuf::Message *>(&sensorDataOut);
  return std::make_shared<SensorDataSignal const>(*dynamic_cast<const osi3::SensorData *const>(messageIn));
}
