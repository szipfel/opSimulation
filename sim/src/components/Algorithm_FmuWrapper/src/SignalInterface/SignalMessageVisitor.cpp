/*******************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "SignalMessageVisitor.h"

namespace SignalMessage
{

template <>
std::shared_ptr<const SignalInterface> parse(AgentInterface &agent,
                                             const std::string &componentName,
                                             SignalType outputType,
                                             const osi3::SensorData *sensorDataOut)
{
  return OSISensorDataParser::translate(agent, *sensorDataOut);
}

template <>
std::shared_ptr<const SignalInterface> parse(AgentInterface &agent,
                                             const std::string &componentName,
                                             SignalType outputType,
                                             const osi3::TrafficUpdate *trafficUpdate)
{
  return TrafficUpdateSignalParser::translate(agent, componentName, outputType, *trafficUpdate);
}

template <>
std::shared_ptr<const SignalInterface> parse<SignalType::DynamicsSignal>(
    std::set<SignalType> outputSignals,
    const std::string &componentName,
    ComponentState componentState,
    size_t tempFMI,
    std::function<FmuValue &(SignalValue, VariableType)> getFmuSignalValue)
{
  return DynamicsSignalParser::translate(outputSignals, componentName, componentState, getFmuSignalValue);
}

template <>
std::shared_ptr<const SignalInterface> parse<SignalType::AccelerationSignal>(
    std::set<SignalType> outputSignals,
    const std::string &componentName,
    ComponentState componentState,
    size_t tempFMI,
    std::function<FmuValue &(SignalValue, VariableType)> getFmuSignalValue)
{
  return AccelerationSignalParser::translate(outputSignals, componentName, componentState, getFmuSignalValue);
}

template <>
std::shared_ptr<const SignalInterface> parse<SignalType::LongitudinalSignal>(
    std::set<SignalType> outputSignals,
    const std::string &componentName,
    ComponentState componentState,
    size_t tempFMI,
    std::function<FmuValue &(SignalValue, VariableType)> getFmuSignalValue)
{
  return LongitudinalSignalParser::translate(outputSignals, componentName, componentState, getFmuSignalValue);
}

template <>
std::shared_ptr<const SignalInterface> parse<SignalType::SteeringSignal>(
    std::set<SignalType> outputSignals,
    const std::string &componentName,
    ComponentState componentState,
    size_t tempFMI,
    std::function<FmuValue &(SignalValue, VariableType)> getFmuSignalValue)
{
  return SteeringSignalParser::translate(outputSignals, componentName, componentState, getFmuSignalValue);
}

template <>
std::shared_ptr<const SignalInterface> parse<SignalType::CompCtrlSignal>(
    std::set<SignalType> outputSignals,
    const std::string &componentName,
    ComponentState componentState,
    size_t tempFMI,
    FmuEnumerations fmuEnumerations,
    std::function<FmuValue &(SignalValue, VariableType)> getFmuSignalValue)
{
  return CompCtrlSignalParser::translate(outputSignals, componentState, tempFMI, fmuEnumerations, getFmuSignalValue);
}

}  // namespace SignalMessage
