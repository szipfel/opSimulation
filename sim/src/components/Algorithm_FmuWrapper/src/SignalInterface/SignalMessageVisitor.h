/*******************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <memory>

#include "AccelerationSignalParser.h"
#include "CompCtrlSignalParser.h"
#include "DynamicsSignalParser.h"
#include "LongitudinalSignalParser.h"
#include "OsiSensorDataParser.h"
#include "SteeringSignalParser.h"
#include "TrafficUpdateSignalParser.h"
#include "common/accelerationSignal.h"
#include "common/agentCompToCompCtrlSignal.h"
#include "common/dynamicsSignal.h"
#include "common/longitudinalSignal.h"
#include "common/steeringSignal.h"
#include "components/Algorithm_FmuWrapper/src/ChannelDefinitionParser.h"

namespace SignalMessage
{

template <SignalType T>
std::shared_ptr<const SignalInterface> parse(std::set<SignalType> outputSignals,
                                             const std::string &componentName,
                                             ComponentState componentState,
                                             size_t tempFMI,
                                             std::function<FmuValue &(SignalValue, VariableType)> getFmuSignalValue)
{
  throw std::runtime_error(std::string("SignalMessage::parse() not implemented for SignalType ") + typeid(T).name());
}

template <SignalType T>
std::shared_ptr<const SignalInterface> parse(std::set<SignalType> outputSignals,
                                             const std::string &componentName,
                                             ComponentState componentState,
                                             size_t tempFMI,
                                             FmuEnumerations fmuEnumerations,
                                             std::function<FmuValue &(SignalValue, VariableType)> getFmuSignalValue)
{
  throw std::runtime_error(std::string("SignalMessage::parse() not implemented for SignalType ") + typeid(T).name());
}

template <typename OsiMessageType>
std::shared_ptr<const SignalInterface> parse(AgentInterface &agent,
                                             const std::string &componentName,
                                             SignalType outputType,
                                             OsiMessageType signal)
{
  throw std::runtime_error(std::string("SignalMessage::parse() not implemented for type ")
                           + typeid(OsiMessageType).name());
}

template <>
std::shared_ptr<const SignalInterface> parse(AgentInterface &agent,
                                             const std::string &componentName,
                                             SignalType outputType,
                                             const osi3::SensorData *sensorDataOut);

template <>
std::shared_ptr<const SignalInterface> parse(AgentInterface &agent,
                                             const std::string &componentName,
                                             SignalType outputType,
                                             const osi3::TrafficUpdate *trafficUpdate);

template <>
std::shared_ptr<const SignalInterface> parse<SignalType::DynamicsSignal>(
    std::set<SignalType> outputSignals,
    const std::string &componentName,
    ComponentState componentState,
    size_t tempFMI,
    std::function<FmuValue &(SignalValue, VariableType)> getFmuSignalValue);

template <>
std::shared_ptr<const SignalInterface> parse<SignalType::AccelerationSignal>(
    std::set<SignalType> outputSignals,
    const std::string &componentName,
    ComponentState componentState,
    size_t tempFMI,
    std::function<FmuValue &(SignalValue, VariableType)> getFmuSignalValue);

template <>
std::shared_ptr<const SignalInterface> parse<SignalType::LongitudinalSignal>(
    std::set<SignalType> outputSignals,
    const std::string &componentName,
    ComponentState componentState,
    size_t tempFMI,
    std::function<FmuValue &(SignalValue, VariableType)> getFmuSignalValue);

template <>
std::shared_ptr<const SignalInterface> parse<SignalType::SteeringSignal>(
    std::set<SignalType> outputSignals,
    const std::string &componentName,
    ComponentState componentState,
    size_t tempFMI,
    std::function<FmuValue &(SignalValue, VariableType)> getFmuSignalValue);

template <>
std::shared_ptr<const SignalInterface> parse<SignalType::CompCtrlSignal>(
    std::set<SignalType> outputSignals,
    const std::string &componentName,
    ComponentState componentState,
    size_t tempFMI,
    FmuEnumerations fmuEnumerations,
    std::function<FmuValue &(SignalValue, VariableType)> getFmuSignalValue);

}  // namespace SignalMessage