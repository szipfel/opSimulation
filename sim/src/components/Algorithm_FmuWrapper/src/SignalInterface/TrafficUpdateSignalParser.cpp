/*******************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "TrafficUpdateSignalParser.h"

std::shared_ptr<const SignalInterface> TrafficUpdateSignalParser::translate(AgentInterface &agent,
                                                                            const std::string &componentName,
                                                                            SignalType outputType,
                                                                            const osi3::TrafficUpdate &trafficUpdate)
{
  switch (outputType)
  {
    case SignalType::DynamicsSignal:
    {
      if (trafficUpdate.update_size() > 0)
      {
        DynamicsInformation dynamicsInformation{};
        dynamicsInformation.yaw = 0_rad;
        const auto bb_center_offset_x = agent.GetVehicleModelParameters()->bounding_box.geometric_center.x;
        const auto previousPositionX = agent.GetPositionX();
        const auto previousPositionY = agent.GetPositionY();
        const auto &baseMoving = trafficUpdate.update(0).base();
        dynamicsInformation.velocityX = units::velocity::meters_per_second_t(baseMoving.velocity().x());
        dynamicsInformation.velocityY = units::velocity::meters_per_second_t(baseMoving.velocity().y());
        dynamicsInformation.yaw = units::angle::radian_t(baseMoving.orientation().yaw());
        dynamicsInformation.roll = units::angle::radian_t(baseMoving.orientation().roll());
        dynamicsInformation.acceleration
            = units::acceleration::meters_per_second_squared_t(baseMoving.acceleration().x())
                * units::math::cos(dynamicsInformation.yaw)
            + units::acceleration::meters_per_second_squared_t(baseMoving.acceleration().y())
                  * units::math::sin(dynamicsInformation.yaw);
        dynamicsInformation.centripetalAcceleration
            = units::acceleration::meters_per_second_squared_t(-baseMoving.acceleration().x())
                * units::math::sin(dynamicsInformation.yaw)
            + units::acceleration::meters_per_second_squared_t(baseMoving.acceleration().y())
                  * units::math::cos(dynamicsInformation.yaw);
        dynamicsInformation.positionX = units::length::meter_t(baseMoving.position().x())
                                      - bb_center_offset_x * units::math::cos(dynamicsInformation.yaw);
        dynamicsInformation.positionY = units::length::meter_t(baseMoving.position().y())
                                      - bb_center_offset_x * units::math::sin(dynamicsInformation.yaw);
        dynamicsInformation.yawRate
            = units::angular_velocity::radians_per_second_t(baseMoving.orientation_rate().yaw());
        dynamicsInformation.yawAcceleration
            = units::angular_acceleration::radians_per_second_squared_t(baseMoving.orientation_acceleration().yaw());

        auto deltaX = dynamicsInformation.positionX - previousPositionX;
        auto deltaY = dynamicsInformation.positionY - previousPositionY;
        dynamicsInformation.travelDistance = units::math::sqrt(deltaX * deltaX + deltaY * deltaY);

        return std::make_shared<DynamicsSignal const>(
            ComponentState::Acting, dynamicsInformation, componentName, componentName);
      }
      else
      {
        return std::make_shared<DynamicsSignal const>(
            ComponentState::Disabled, DynamicsInformation{}, componentName, componentName);
      }
    }
    case SignalType::AccelerationSignal:
    {
      if (trafficUpdate.update_size() > 0)
      {
        const auto &baseMoving = trafficUpdate.update(0).base();
        const auto yaw = units::angle::radian_t(baseMoving.orientation().yaw());
        const auto acceleration
            = units::acceleration::meters_per_second_squared_t(baseMoving.acceleration().x()) * units::math::cos(yaw)
            + units::acceleration::meters_per_second_squared_t(baseMoving.acceleration().y()) * units::math::sin(yaw);
        return std::make_shared<AccelerationSignal const>(ComponentState::Acting, acceleration, componentName);
      }
      else
      {
        return std::make_shared<AccelerationSignal const>(ComponentState::Disabled, 0.0_mps_sq, componentName);
      }
    }
    case SignalType::SteeringSignal:
    {
      if (trafficUpdate.internal_state_size() > 0)
      {
        const auto &hostVehicleData = trafficUpdate.internal_state(0);
        const auto steeringWheelAngle
            = units::angle::radian_t(hostVehicleData.vehicle_steering().vehicle_steering_wheel().angle());
        return std::make_shared<SteeringSignal const>(ComponentState::Acting, steeringWheelAngle, componentName);
      }
      else
      {
        return std::make_shared<SteeringSignal const>(ComponentState::Disabled, 0.0_rad, componentName);
      }
    }
    case SignalType::LongitudinalSignal:
    {
      if (trafficUpdate.internal_state_size() > 0)
      {
        const auto &hostVehicleData = trafficUpdate.internal_state(0);
        const auto accPedalPos = hostVehicleData.vehicle_powertrain().pedal_position_acceleration();
        const auto brakePedalPos = hostVehicleData.vehicle_brake_system().pedal_position_brake();
        const auto gear = hostVehicleData.vehicle_powertrain().gear_transmission();
        return std::make_shared<LongitudinalSignal const>(
            ComponentState::Acting, accPedalPos, brakePedalPos, gear, componentName);
      }
      else
      {
        return std::make_shared<LongitudinalSignal const>(ComponentState::Disabled, 0, 0, 0, componentName);
      }
    }
    default:
      throw std::runtime_error{"Unsupported signal type"};
  }
}
