/********************************************************************************
 * Copyright (c) 2020-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <iostream>

template <typename ErrorCallback>
void logAndThrow(const std::string& message, const ErrorCallback& errorCallback) noexcept(false)
{
  if (errorCallback) errorCallback(message);
  std::cerr << message;
  throw std::runtime_error(message);
}
