/********************************************************************************
 * Copyright (c) 2020 HLRS, University of Stuttgart
 *               2016-2017 ITK Engineering GmbH
 *               2019 in-tech GmbH
 *               2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** @file  longCalcs.cpp */
//-----------------------------------------------------------------------------

#include "longCalcs.h"

#include <array>
#include <cassert>
#include <iostream>
#include <limits>
#include <map>

#include "common/commonTools.h"
#include "components/common/vehicleProperties.h"

using namespace units::literals;

AlgorithmLongitudinalCalculations::AlgorithmLongitudinalCalculations(
    units::velocity::meters_per_second_t velocity,
    units::acceleration::meters_per_second_squared_t accelerationWish,
    const mantle_api::VehicleProperties &vehicleModelParameters,
    std::function<void(CbkLogLevel, const char *, int, const std::string &)> Log)
    : Log(Log), velocity(velocity), accelerationWish(accelerationWish), vehicleModelParameters(vehicleModelParameters)
{
}

double AlgorithmLongitudinalCalculations::GetBrakePedalPosition() const
{
  return brakePedalPosition;
}

double AlgorithmLongitudinalCalculations::GetAcceleratorPedalPosition() const
{
  return acceleratorPedalPosition;
}

units::angular_velocity::revolutions_per_minute_t AlgorithmLongitudinalCalculations::GetEngineSpeed() const
{
  return engineSpeed;
}

int AlgorithmLongitudinalCalculations::GetGear() const
{
  return gear;
}

units::torque::newton_meter_t AlgorithmLongitudinalCalculations::GetEngineTorqueAtGear(
    int gear, const units::acceleration::meters_per_second_squared_t &acceleration)
{
  if (acceleration == 0_mps_sq || gear == 0)
  {
    return 0.0_Nm;
  }

  if (gear > GetVehicleProperty(vehicle::properties::NumberOfGears) || gear < 0)
  {
    throw std::runtime_error("Gear in AlgorithmLongitudinal is invalid");
  }

  units::torque::newton_meter_t wheelSetTorque = vehicleModelParameters.mass * 0.5
                                               * vehicleModelParameters.rear_axle.wheel_diameter
                                               * units::acceleration::meters_per_second_squared_t(acceleration);
  auto engineTorqueAtGear = wheelSetTorque
                          / (GetVehicleProperty(vehicle::properties::AxleRatio)
                             * GetVehicleProperty(vehicle::properties::GearRatio + std::to_string(gear)));

  return engineTorqueAtGear;
}

double AlgorithmLongitudinalCalculations::GetVehicleProperty(const std::string &propertyName)
{
  const auto property = helper::map::query(vehicleModelParameters.properties, propertyName);
  THROWIFFALSE(property.has_value(), "Vehicle property \"" + propertyName + "\" was not set in the VehicleCatalog");
  return std::stod(property.value());
}

units::angular_velocity::revolutions_per_minute_t AlgorithmLongitudinalCalculations::GetEngineSpeedByVelocity(
    const units::velocity::meters_per_second_t &xVel, int gear)
{
  return 1_rad
       * (xVel * GetVehicleProperty(vehicle::properties::AxleRatio)
          * GetVehicleProperty(vehicle::properties::GearRatio + std::to_string(gear)))
       / (vehicleModelParameters.rear_axle.wheel_diameter
          * 0.5);  // an dynamic wheel radius rDyn must actually be used here
}

void AlgorithmLongitudinalCalculations::CalculateGearAndEngineSpeed()
{
  std::map<units::angular_velocity::revolutions_per_minute_t, int> nEngineSet;
  std::map<units::acceleration::meters_per_second_squared_t,
           std::tuple<int,
                      units::angular_velocity::revolutions_per_minute_t,
                      units::acceleration::meters_per_second_squared_t>>
      minDeltaAccWheelBased;

  const auto numberOfGears = GetVehicleProperty(vehicle::properties::NumberOfGears);
  for (int gear = 1; gear <= numberOfGears; ++gear)
  {
    const auto engineSpeed = GetEngineSpeedByVelocity(velocity, gear);
    units::acceleration::meters_per_second_squared_t limitWheelAcc;
    units::acceleration::meters_per_second_squared_t accDelta;

    if (accelerationWish >= 0_mps_sq)
    {
      auto MMax = GetEngineTorqueMax(engineSpeed);
      limitWheelAcc = GetAccFromEngineTorque(MMax, gear);

      if (accelerationWish == 0_mps_sq)
        accDelta = 0_mps_sq;
      else
        accDelta = units::math::fabs(accelerationWish - limitWheelAcc);
    }
    else
    {
      auto MMin = GetEngineTorqueMin(engineSpeed);
      limitWheelAcc = GetAccFromEngineTorque(MMin, gear);
      accDelta = units::math::fabs(accelerationWish - limitWheelAcc);
    }

    nEngineSet[engineSpeed] = gear;
    minDeltaAccWheelBased[accDelta] = {gear, engineSpeed, limitWheelAcc};
  }

  bool foundGear = false;

  for (const auto &[engineSpeedResult, gearResult] : nEngineSet)
  {
    if (isWithinEngineLimits(gearResult, engineSpeedResult, accelerationWish))  //&&isChangeOfGearPossibleNow
    {
      gear = gearResult;
      engineSpeed = engineSpeedResult;

      foundGear = true;
    }
    else if (foundGear)  // leaving possible range
    {
      return;
    }
  }

  if (foundGear)
  {
    return;
  }

  // take lowest delta for gear and engineSpeed
  auto val = minDeltaAccWheelBased.begin()->second;

  // trim wish acceleration to possible value
  gear = std::get<0>(val);
  engineSpeed = std::get<1>(val);
  accelerationWish = units::math::min(accelerationWish, std::get<2>(val));
}

bool AlgorithmLongitudinalCalculations::isWithinEngineLimits(
    int gear,
    const units::angular_velocity::revolutions_per_minute_t &engineSpeed,
    const units::acceleration::meters_per_second_squared_t &acceleration)
{
  if (!isEngineSpeedWithinEngineLimits(engineSpeed))
  {
    return false;
  }

  auto currentWishTorque = GetEngineTorqueAtGear(gear, acceleration);

  return isTorqueWithinEngineLimits(currentWishTorque, engineSpeed);
}

bool AlgorithmLongitudinalCalculations::isTorqueWithinEngineLimits(
    const units::torque::newton_meter_t &torque, const units::angular_velocity::revolutions_per_minute_t &engineSpeed)
{
  auto currentMEngMax = GetEngineTorqueMax(engineSpeed);

  return torque <= currentMEngMax;
}

inline bool AlgorithmLongitudinalCalculations::isEngineSpeedWithinEngineLimits(
    const units::angular_velocity::revolutions_per_minute_t &engineSpeed)
{
  return (engineSpeed >= units::angular_velocity::revolutions_per_minute_t(
              GetVehicleProperty(vehicle::properties::MinimumEngineSpeed))
          && engineSpeed <= units::angular_velocity::revolutions_per_minute_t(
                 GetVehicleProperty(vehicle::properties::MaximumEngineSpeed)));
}

units::torque::newton_meter_t AlgorithmLongitudinalCalculations::GetEngineTorqueMax(
    const units::angular_velocity::revolutions_per_minute_t &engineSpeed)
{
  const units::torque::newton_meter_t maximumEngineTorque{GetVehicleProperty(vehicle::properties::MaximumEngineTorque)};
  const units::angular_velocity::revolutions_per_minute_t maximumEngineSpeed{
      GetVehicleProperty(vehicle::properties::MaximumEngineSpeed)};
  const units::angular_velocity::revolutions_per_minute_t minimumEngineSpeed{
      GetVehicleProperty(vehicle::properties::MinimumEngineSpeed)};

  auto torqueMax = maximumEngineTorque;  // initial value at max
  auto speed = engineSpeed;

  bool isLowerSection = engineSpeed < minimumEngineSpeed + 1000_rpm;
  bool isBeyondLowerSectionBorder = engineSpeed < minimumEngineSpeed;
  bool isUpperSection = engineSpeed > maximumEngineSpeed - 1000_rpm;
  bool isBeyondUpperSectionBorder = engineSpeed > maximumEngineSpeed;

  if (isLowerSection)
  {
    if (isBeyondLowerSectionBorder)  // not within limits
    {
      speed = minimumEngineSpeed;
    }

    const auto tempEngineSpeed = 1000_rpm - (speed - minimumEngineSpeed);
    torqueMax
        = units::inverse_radian(0.5 / M_PI)
            * (tempEngineSpeed)*units::unit_t<units::compound_unit<units::torque::newton_meter, units::time::minute>>(
                -0.1)
        + maximumEngineTorque;
  }
  else if (isUpperSection)
  {
    if (isBeyondUpperSectionBorder)
    {
      speed = maximumEngineSpeed;
    }

    const auto tempEngineSpeed = speed - maximumEngineSpeed + 1000_rpm;
    torqueMax = units::inverse_radian(0.5 / M_PI) * tempEngineSpeed
                  * units::unit_t<units::compound_unit<units::torque::newton_meter, units::time::minute>>(-0.04)
              + maximumEngineTorque;
  }

  return torqueMax;
}

units::torque::newton_meter_t AlgorithmLongitudinalCalculations::GetEngineTorqueMin(
    const units::angular_velocity::revolutions_per_minute_t &engineSpeed)
{
  return GetEngineTorqueMax(engineSpeed) * -.1;
}

units::acceleration::meters_per_second_squared_t AlgorithmLongitudinalCalculations::GetAccFromEngineTorque(
    const units::torque::newton_meter_t &engineTorque, int chosenGear)
{
  const auto wheelSetTorque = engineTorque
                            * (GetVehicleProperty(vehicle::properties::AxleRatio)
                               * GetVehicleProperty(vehicle::properties::GearRatio + std::to_string(chosenGear)));
  const units::force::newton_t wheelSetForce = wheelSetTorque / (0.5 * vehicleModelParameters.rear_axle.wheel_diameter);
  return wheelSetForce / vehicleModelParameters.mass;
}

void AlgorithmLongitudinalCalculations::CalculatePedalPositions()
{
  const units::acceleration::meters_per_second_squared_t oneG{9.81};

  units::angular_velocity::revolutions_per_minute_t engineSpeedInUnits(engineSpeed);

  if (accelerationWish < 0.0_mps_sq)  // speed shall be reduced with drag or brake
  {
    const auto engineTorque = GetEngineTorqueAtGear(gear, accelerationWish);
    const auto MDragMax = GetEngineTorqueMin(engineSpeedInUnits);

    if (engineTorque < MDragMax)
    {  // brake

      // calculate acceleration of MDragMax and substract
      // this from in_aVehicle since brake and drag work in parallel while clutch is closed
      const auto accMDragMax = GetAccFromEngineTorque(MDragMax, gear);

      acceleratorPedalPosition = 0.0;

      const auto pedalPositionBasedOnAcceleration = -(accelerationWish - accMDragMax) / oneG;
      brakePedalPosition = std::min(pedalPositionBasedOnAcceleration.value(), 1.0);
      return;
    }
  }

  // cases of acceleration and drag => use engine here

  const auto MDragMax = GetEngineTorqueMin(engineSpeedInUnits);
  const auto MTorqueMax = GetEngineTorqueMax(engineSpeedInUnits);

  const auto wishTorque = GetEngineTorqueAtGear(gear, accelerationWish);

  acceleratorPedalPosition = std::min(units::unit_cast<double>((wishTorque - MDragMax) / (MTorqueMax - MDragMax)), 1.0);
  brakePedalPosition = 0.0;
}
