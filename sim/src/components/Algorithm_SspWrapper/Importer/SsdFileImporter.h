/********************************************************************************
 * Copyright (c) 2020-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <filesystem>
#include <iostream>
#include <map>
#include <memory>
#include <string>

#include <QtXml/QDomDocument>

#include "SsdFile.h"
#include "common/parameter.h"
#include "common/xmlParser.h"
#include "include/modelInterface.h"
#include "sim/src/components/Algorithm_SspWrapper/SspLogger.h"
#include "src/core/opSimulation/modelElements/parameters.h"

/// @brief Class representing an importer of a ssd file
class SsdFileImporter
{
public:
  SsdFileImporter(const SsdFileImporter &) = delete;

  SsdFileImporter(SsdFileImporter &&) = delete;

  SsdFileImporter &operator=(const SsdFileImporter &) = delete;

  SsdFileImporter &operator=(SsdFileImporter &&) = delete;

  /// @brief Import SSD file
  /// @param filename     Name of the file
  /// @param ssdFile      Reference to the Ssd file
  /// @return true, when the import is successful
  static bool Import(const std::filesystem::path &filename, std::vector<std::shared_ptr<SsdFile>> &ssdFile);

private:
  static bool ImportSsdFileContent(const std::filesystem::path &filename, QDomDocument &document);

  static void ImportSystemComponents(QDomElement &componentsElement,
                                     const std::shared_ptr<SsdSystem> &ssdSystem,
                                     const std::filesystem::path &filename);

  static SspParserTypes::Enumerations ImportSsdEnumerations(const QDomElement &enumerationsElement);

  static void ImportComponentParameters(QDomElement &parameterBindingsElement,
                                        const std::shared_ptr<SsdComponent> &component,
                                        const std::filesystem::path &filename);

  static void ImportComponentAnnotations(QDomElement &annotationsElement,
                                         const std::shared_ptr<SsdComponent> &component);

  static void ImportComponentParameterSets(QDomElement &parameterBindingsElement,
                                           const std::shared_ptr<SsdComponent> &component);

  static void ImportFmuParameters(QDomElement &parameterBindingsElement,
                                  const std::shared_ptr<SsdComponent> &component);

  static void ImportWriteMessageParameters(QDomElement &parameterBindingsElement,
                                           const std::shared_ptr<SsdComponent> &component);

  static void ImportParameterConnectorInitialization(QDomElement &parameterBindingsElement,
                                                     const std::shared_ptr<SsdComponent> &component);

  template <class T>
  static void ImportConnectors(const QDomElement &element,
                               const std::shared_ptr<T> &sharedPtr,
                               const std::shared_ptr<SsdSystem> &ssdSystem);

  static void ImportSystemConnections(const QDomElement &connectionsElement,
                                      const std::shared_ptr<SsdSystem> &ssdSystem);

  static void ImportSystem(const std::filesystem::path &filename,
                           const QDomElement &systemElement,
                           const std::shared_ptr<SsdSystem> &ssdSystem);

  static SspParserTypes::ConnectorTypeAndAttributes FetchConnectorType(const QDomElement &connectorElement,
                                                                       const std::string &connectorName);

  static void Log(CbkLogLevel logLevel, const char *file, int line, const std::string &message);
};

//OPENPASS_SSDFILEIMPORTER_H
