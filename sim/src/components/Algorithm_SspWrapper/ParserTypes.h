/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * https://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <components/Algorithm_FmuWrapper/src/variant_visitor.h>

#include "components/Algorithm_FmuWrapper/src/fmuFileHelper.h"
#include "include/fmuHandlerInterface.h"
#include "include/fmuWrapperInterface.h"
#include "sim/src/components/Algorithm_SspWrapper/SSPElements/Component.h"
#include "type_helper.h"

enum OsiType
{
  SensorView,
  SensorViewConfiguration,
  SensorViewConfigurationRequest,
  SensorData,
  TrafficUpdate,
  GroundTruth,
  TrafficCommand,
  HostVehicleData
};

/// @brief Structure representing an OSMP connector
struct OSMPConnector
{
  std::string name;          ///< Name of the OSMP connector as string
  std::string osmpLinkName;  ///< Name of the OSMP link in string format

  /// Enumerated type of OSMP link role
  enum OsmpLinkRole
  {
    base_lo,
    base_hi,
    size
  } osmpLinkRole;  ///< OSMP link role in OSMPLinkRole format

  OsiType osiType;  ///< OSI type

  /// @brief Construct an OSMP connector object
  /// @param name             Name of the OSMP connector as string
  /// @param osmpLinkName     Name of the OSMP link as string
  /// @param osmpLinkRole     Reference to the OSMP link role as string
  /// @param osiType          Reference to the OSI type as string
  OSMPConnector(std::string name, std::string osmpLinkName, const std::string &osmpLinkRole, const std::string &osiType)
      : name(std::move(name)),
        osmpLinkName(std::move(osmpLinkName)),
        osmpLinkRole(RoleFromString(osmpLinkRole)),
        osiType(OsiFromString(osiType))
  {
  }

  /// @brief Function to convert the OSMP link role from string format to OSMPLinkrole type
  /// @param role OSMP link role in string format
  /// @return OSMP link role in OSMPLinkRole type
  static OsmpLinkRole RoleFromString(const std::string &role)
  {
    if (role == "base.lo") return base_lo;
    if (role == "base.hi") return base_hi;
    if (role == "size") return size;
    if (role == ".base.lo") return base_lo;
    if (role == ".base.hi") return base_hi;
    if (role == ".size") return size;
    throw std::runtime_error("Unknown OSMP role: " + role);
  }

  /// @brief Function to return the OSI type in OSI type format
  /// @param type Reference to the OSI type in string format
  /// @return Returns OSItype in OSI type format
  static OsiType OsiFromString(const std::string &type)
  {
    if (type == "SensorView") return SensorView;
    if (type == "SensorViewConfiguration") return SensorViewConfiguration;
    if (type == "SensorViewConfigurationRequest") return SensorViewConfigurationRequest;
    if (type == "SensorData") return SensorData;
    if (type == "TrafficUpdate") return TrafficUpdate;
    if (type == "GroundTruth") return GroundTruth;
    if (type == "TrafficCommand") return TrafficCommand;
    if (type == "HostVehicleData") return HostVehicleData;
    throw std::runtime_error("Unknown OSI type: " + type);
  }

  /// @brief Function to return the OSI type in string format
  /// @param osiType Reference to the OSI type
  /// @return Returns OSItype in string format
  static std::string StringFromOsiType(const OsiType &osiType)
  {
    switch (osiType)
    {
      case SensorView:
        return "SensorView";
      case SensorViewConfiguration:
        return "SensorViewConfiguration";
      case SensorViewConfigurationRequest:
        return "SensorViewConfigurationRequest";
      case SensorData:
        return "SensorData";
      case TrafficUpdate:
        return "TrafficUpdate";
      case TrafficCommand:
        return "TrafficCommand";
      case GroundTruth:
        return "GroundTruth";
      case HostVehicleData:
        return "HostVehicleData";
      default:
        throw std::runtime_error("Unknown OSI type " + std::to_string(static_cast<int>(osiType)));
    }
  }

  /// @brief Function to convert the OSMPLink role to string
  /// @param osmpLinkRole OSMP link role
  /// @return OSMP link role suffix in string format
  static std::string ConvertToSuffix(OsmpLinkRole osmpLinkRole)
  {
    switch (osmpLinkRole)
    {
      case base_lo:
        return ".base.lo";
      case base_hi:
        return ".base.hi";
      case size:
        return ".size";
      default:
        throw std::runtime_error("Unknown OSMP role");
    }
  }

  /// @brief Less than operator overload
  /// @param rhs reference to the another OSMP connector on the right handside of the operator
  /// @return True, if the operation is successfull
  bool operator<(const OSMPConnector &rhs) const
  {
    return std::tie(name, osmpLinkName) < std::tie(rhs.name, rhs.osmpLinkName);
  }

  /// @brief Greater than operator overload
  /// @param rhs reference to the another OSMP connector on the right handside of the operator
  /// @return True, if the operation is successfull
  bool operator>(const OSMPConnector &rhs) const { return rhs < *this; }

  /// @brief Less than or equal to operator overload
  /// @param rhs reference to the another OSMP connector on the right handside of the operator
  /// @return True, if the operation is successfull
  bool operator<=(const OSMPConnector &rhs) const { return !(rhs < *this); }

  /// @brief Greater than or equal to operator overload
  /// @param rhs reference to the another OSMP connector on the right handside of the operator
  /// @return True, if the operation is successfull
  bool operator>=(const OSMPConnector &rhs) const { return !(*this < rhs); }
};

enum class ConnectorType
{
  None,
  Real,
  Integer,
  Boolean,
  Enumeration
};

namespace SspParserTypes
{
using Component = std::string;  // name
using ConnectorTypeAndAttributes = std::pair<ConnectorType, std::map<std::string, std::string>>;
using ScalarConnector = std::pair<std::string, ConnectorTypeAndAttributes>;
using OSMPSingleLink = std::pair<Component, OSMPConnector>;
using Connector = std::variant<OSMPSingleLink, ScalarConnector>;
using Connection = std::pair<Connector, Connector>;
using EnumerationItem = std::pair<std::string, int>;
using Enumeration = std::pair<std::string, std::vector<EnumerationItem>>;
using Enumerations = std::vector<std::pair<std::string, std::vector<EnumerationItem>>>;
}  // namespace SspParserTypes

/// @brief Class representing a logic error for ssp parser
class SspParserLogicError : public std::runtime_error
{
public:
  /// @brief Construct a SSPparser logic error
  /// @param msg Logic error message
  SspParserLogicError(const std::string &msg) : std::runtime_error(msg){};
};

/// @brief Class representing a helper for ssp parser
class SSPParserHelper
{
public:
  /// @brief Remove OSMP role from the OSMP annotated string
  /// @param annotatedString OSMP annotated string
  /// @return Returns string with OSMP role removed
  static std::string RemoveOSMPRoleFromOSMPAnnotated(std::string annotatedString);

  /// @brief Function to get the value from enumeration
  /// @param enumerationName  Reference to the name of the enumeration
  /// @param itemName         Reference to the name of the enumerator item
  /// @param enumerations     Reference to the SSP parser type enumerations
  /// @return Returns the value from enumeration, optional
  static std::optional<std::string> GetValueFromEnumeration(const std::string &enumerationName,
                                                            const std::string &itemName,
                                                            const SspParserTypes::Enumerations &enumerations);
};

enum class ConnectorKind
{
  None = 0,
  Input,
  Output,
  Inout,
  Parameter,
  Calculated_Parameter
};

const variant_visitor osmpConnectorNameVisitor{
    [](const SspParserTypes::ScalarConnector &connector) { return connector.first; },
    [](const SspParserTypes::OSMPSingleLink &link)
    { return SSPParserHelper::RemoveOSMPRoleFromOSMPAnnotated(link.second.name); }};

const variant_visitor connectorOSMPRoleVisitor{[](const SspParserTypes::ScalarConnector &connector)
                                               {
                                                 const std::string msg = "ScalarConnector has no osmp role";
                                                 throw SspParserLogicError(msg);
                                                 return std::basic_string<char>();
                                               },
                                               [](const SspParserTypes::OSMPSingleLink &link)
                                               { return OSMPConnector::ConvertToSuffix(link.second.osmpLinkRole); }};

const variant_visitor connectorIsOSMP{[](const SspParserTypes::ScalarConnector &connector) { return false; },
                                      [](const SspParserTypes::OSMPSingleLink &link) { return true; }};

const variant_visitor fmuOSMPLinkNameVisitor{
    [](const SspParserTypes::ScalarConnector &connector) { return connector.first; },
    [](const SspParserTypes::OSMPSingleLink &link) { return link.second.osmpLinkName; }};

const variant_visitor scalarTypeVisitor{
    [](const SspParserTypes::ScalarConnector &connector) { return connector.second.first; },
    [](const SspParserTypes::OSMPSingleLink &link) { return ConnectorType::Integer; }};

const variant_visitor osiTypeVisitor{[](const SspParserTypes::ScalarConnector &connector)
                                     {
                                       const std::basic_string<char> msg = "ScalarConnector has no osi type";
                                       throw SspParserLogicError(msg);
                                       return std::basic_string<char>();
                                     },
                                     [](const SspParserTypes::OSMPSingleLink &link)
                                     { return OSMPConnector::StringFromOsiType(link.second.osiType); }};

constexpr variant_visitor connectorNameVisitor{
    [](const SspParserTypes::ScalarConnector &connector) { return connector.first; },
    [](const SspParserTypes::OSMPSingleLink &link) { return link.second.name; }};
