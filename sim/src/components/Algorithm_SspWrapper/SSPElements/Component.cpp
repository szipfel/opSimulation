/*******************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Component.h"

#include "../Visitors/Network/SspNetworkVisitorInterface.h"

void ssp::FmuComponent::Accept(SspNetworkVisitorInterface& visitorInterface)
{
  visitorInterface.Visit(this);
}

ssp::FmuComponent::FmuComponent(const std::string& elementName,
                                std::vector<std::shared_ptr<ssp::VisitableNetworkElement>>&& elements,
                                std::vector<std::shared_ptr<ConnectorInterface>>&& input_connectors,
                                std::vector<std::shared_ptr<ConnectorInterface>>&& output_connectors,
                                std::shared_ptr<FmuWrapperInterface> fmuWrapperInterface)
    : Component(elementName, std::move(elements), std::move(input_connectors), std::move(output_connectors)),
      fmuWrapperInterface(fmuWrapperInterface)
{
}

ssp::Component::Component(const std::string& elementName,
                          std::vector<std::shared_ptr<ssp::VisitableNetworkElement>>&& elements,
                          std::vector<std::shared_ptr<ssp::ConnectorInterface>>&& input_connectors,
                          std::vector<std::shared_ptr<ssp::ConnectorInterface>>&& output_connectors)
    : NetworkElement(elementName, std::move(elements), std::move(input_connectors), std::move(output_connectors))
{
}