/*******************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <memory>
#include <utility>
#include <vector>

#include "NetworkElement.h"

namespace ssp
{

/// Class representing an component
class Component : public NetworkElement
{
public:
  /// @brief creating an component object
  /// @param elementName          Name of the element
  /// @param elements             List of references to the visitable network element
  /// @param input_connectors     List of references to input connector interface
  /// @param output_connectors    List of references to output connector interface
  Component(const std::string &elementName,
            std::vector<std::shared_ptr<ssp::VisitableNetworkElement>> &&elements,
            std::vector<std::shared_ptr<ssp::ConnectorInterface>> &&input_connectors,
            std::vector<std::shared_ptr<ssp::ConnectorInterface>> &&output_connectors);

  ~Component() override = default;
};

/// Class representing an fmu component
class FmuComponent : public Component
{
public:
  /// @brief creating an Fmu component object
  /// @param elementName          Name of the element
  /// @param elements             List of references to the visitable network element
  /// @param input_connectors     List of references to input connector interface
  /// @param output_connectors    List of references to output connector interface
  /// @param fmuWrapperInterface  List of references to Fmu wrapper interface
  FmuComponent(const std::string &elementName,
               std::vector<std::shared_ptr<ssp::VisitableNetworkElement>> &&elements,
               std::vector<std::shared_ptr<ConnectorInterface>> &&input_connectors,
               std::vector<std::shared_ptr<ConnectorInterface>> &&output_connectors,
               std::shared_ptr<FmuWrapperInterface> fmuWrapperInterface);

  /// @brief Create an FMU Component
  /// @param elementName Name of the element
  explicit FmuComponent(const std::string &elementName);

  ~FmuComponent() override = default;

  void Accept(SspNetworkVisitorInterface &visitorInterface) override;

  int Priority() final { return fmuWrapperInterface->getPriority(); }

  const std::shared_ptr<FmuWrapperInterface> fmuWrapperInterface;  ///< Reference to the FMU wrapper interface
};
}  //namespace ssp