/*******************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "ConnectorHelper.h"

fmi2_value_reference_t ssp::ConnectorHelper::GetScalarVariableReference(
    const std::shared_ptr<FmuWrapperInterface> &fmuWrapperInterface, const std::string &fmuScalarVariableName)
{
  switch (fmuWrapperInterface->getFmiVersion())
  {
    case fmi_version_1_enu:
      return std::get<FMI1>(fmuWrapperInterface->GetFmuVariables()).at(fmuScalarVariableName).valueReference;
    case fmi_version_2_0_enu:
      return std::get<FMI2>(fmuWrapperInterface->GetFmuVariables()).at(fmuScalarVariableName).valueReference;
    default:
      return std::get<FMI2>(fmuWrapperInterface->GetFmuVariables()).at(fmuScalarVariableName).valueReference;
  }
}