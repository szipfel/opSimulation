/*******************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <memory>
#include <vector>

#include "../../Visitors/Connector/ConnectorVisitorInterface.h"

namespace ssp
{
/// Class representing an interface for the connector
class ConnectorInterface
{
public:
  /// @brief Accept the reference connector visitor interface
  virtual void Accept(ConnectorVisitorInterface &) = 0;

  /// @return Returns the priority of the connector interface
  virtual int GetPriority() const = 0;

  /// @return Return if the connector interface is a parameter connector
  virtual bool IsParameterConnector() const = 0;

  /// @return Returns the reference to the name of the connector interface
  virtual const std::string &GetConnectorName() const = 0;

  /// @return Returns the list of reference to the non parameter connector interface
  virtual std::vector<std::shared_ptr<ConnectorInterface>> GetNonParameterConnectors() const = 0;

  /// list of connector interfaces
  std::vector<std::shared_ptr<ConnectorInterface>> connectors;
};

}  //namespace ssp