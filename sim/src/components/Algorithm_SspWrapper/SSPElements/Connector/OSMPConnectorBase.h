/*******************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "Connector.h"
#include "ScalarConnector.h"

namespace ssp
{

/// Class representing a base for OSMP connector
class OSMPConnectorBase : public Connector
{
public:
  OSMPConnectorBase() = delete;
  virtual ~OSMPConnectorBase() = default;

  void Accept(ConnectorVisitorInterface &visitor) override;

  /// @return Get function to return the reference to FmuWrapperInterface
  virtual std::shared_ptr<FmuWrapperInterface> GetFmuWrapperInterface();

  /// @return Returns the reference to the name of the connector
  const std::string &GetConnectorName() const override;

  /// @return Get function to return name of the osmp link
  [[nodiscard]] const std::string &GetOsmpLinkName() const;

  /// @return Returns the pointer to the message
  virtual std::shared_ptr<google::protobuf::Message> GetMessage() = 0;

  /// @brief Set the message from OSMP connector base
  virtual void SetMessage(const google::protobuf::Message *) = 0;

  /// @brief Function to handle file writing
  /// @param time
  virtual void HandleFileWriting(int time) = 0;

private:
  OSMPConnectorBase(std::unique_ptr<ScalarConnector<VariableTypeInt>> base_lo,
                    std::unique_ptr<ScalarConnector<VariableTypeInt>> base_hi,
                    std::unique_ptr<ScalarConnector<VariableTypeInt>> size,
                    int priority,
                    std::string variableReference,
                    std::string osmpLinkName);

  std::string osmpLinkName;

protected:
  /// @brief Create an OSMPCOnnectorBase object
  /// @param connectorName    Name of the connector
  /// @param osmpLinkName     Name of the OSMP link
  /// @param fmuWrapper       Reference to the FMU Wrapper
  /// @param fmuPriority      Priority to FMU
  OSMPConnectorBase(const std::string &connectorName,
                    const std::string &osmpLinkName,
                    const std::shared_ptr<FmuWrapperInterface> &fmuWrapper,
                    int fmuPriority);

  const std::unique_ptr<ScalarConnector<VariableTypeInt>> base_lo;  ///< lower base
  const std::unique_ptr<ScalarConnector<VariableTypeInt>> base_hi;  ///< higher base
  const std::unique_ptr<ScalarConnector<VariableTypeInt>> size;     ///< Size of the scalar connector variable type
  std::string connectorName;                                        ///< Name of the OSMP connector base

  /// For writeJson and writeTrace
  std::optional<std::string> componentName;
};
}  //namespace ssp