/*******************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "ConnectorHelper.h"
//#include "sim/src/components/Algorithm_SspWrapper/ParserTypes.h"
#include "ScalarConnectorBase.h"

namespace ssp
{

/// @brief Class representing an scalar connector
/// @tparam UnderlyingScalarVariableType
template <typename UnderlyingScalarVariableType>
class ScalarConnector : public ScalarConnectorBase
{
public:
  static_assert(is_same_as_any_of_v<cvt_t<UnderlyingScalarVariableType>, bool, int, double, std::string>);

  ScalarConnector() = delete;
  ~ScalarConnector() override = default;

  /// @brief Construct a scalar connector object
  /// @param fmuWrapperInterface      Pointer to the fmu wrapper interface
  /// @param fmuScalarVariableName    Name of the fmu scalar variable
  /// @param fmuPriority              Priority of the fmu
  ScalarConnector(std::shared_ptr<FmuWrapperInterface> fmuWrapperInterface,
                  std::string fmuScalarVariableName,
                  int fmuPriority);

  /// @return Function to return type of underlying scalar variable
  auto GetValue() -> cvt_t<UnderlyingScalarVariableType>;

  /// @brief Set the value of underlying scalar variable type
  /// @param value value of underlying scalar variable type
  void SetValue(cvt_t<UnderlyingScalarVariableType> value);

  void PropagateData() override;
};

template <typename UnderlyingScalarVariableType>
ScalarConnector<UnderlyingScalarVariableType>::ScalarConnector(std::shared_ptr<FmuWrapperInterface> fmuWrapperInterface,
                                                               std::string fmuScalarVariableName,
                                                               int fmuPriority)
    : ScalarConnectorBase(fmuWrapperInterface, fmuScalarVariableName, fmuPriority)
{
}

template <typename UnderlyingScalarVariableType>
auto ScalarConnector<UnderlyingScalarVariableType>::GetValue() -> cvt_t<UnderlyingScalarVariableType>
{
  auto value
      = ConnectorHelper::GetFmuWrapperValue<UnderlyingScalarVariableType>(fmuWrapperInterface, fmuScalarVariableName);
  LOGDEBUG("SSP FMU Connector: Get scalar variable " + fmuScalarVariableName + "->"
           + CvtToString<UnderlyingScalarVariableType>(value));
  return value;
};

template <typename UnderlyingScalarVariableType>
void ScalarConnector<UnderlyingScalarVariableType>::SetValue(cvt_t<UnderlyingScalarVariableType> value)
{
  LOGDEBUG("SSP FMU Connector: Set scalar variable " + fmuScalarVariableName + " -> "
           + CvtToString<UnderlyingScalarVariableType>(value));
  ConnectorHelper::SetFmuWrapperValue<UnderlyingScalarVariableType>(fmuWrapperInterface, fmuScalarVariableName, value);
};

template <typename UnderlyingScalarVariableType>
void ScalarConnector<UnderlyingScalarVariableType>::PropagateData()
{
  for (auto &connector : GetNonParameterConnectors())
  {
    if (auto scalarConnector = std::dynamic_pointer_cast<ScalarConnector<UnderlyingScalarVariableType>>(connector))
    {
      scalarConnector->SetValue(GetValue());
    }
  }
};
}  //namespace ssp