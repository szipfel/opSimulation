/*******************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "Connector.h"
#include "sim/include/fmuWrapperInterface.h"

namespace ssp
{

class Connector;

/// @brief Class representing a base for scalar connector
class ScalarConnectorBase : public Connector
{
public:
  ScalarConnectorBase() = delete;

  /// @brief Constructor a scalar connector base object
  /// @param fmuWrapperInterface      Pointer to the fmu wrapper interface
  /// @param fmuScalarVariableName    Name of the fmu scalar variable
  /// @param priority                 Priority of the FMU component
  ScalarConnectorBase(std::shared_ptr<FmuWrapperInterface> fmuWrapperInterface,
                      std::string fmuScalarVariableName,
                      int priority);

  /**
   * @brief Accept a connector visitor interface
   *
   * @param visitor Reference to the connector visitor interface
   */
  void Accept(ConnectorVisitorInterface &visitor) final;

  const std::string &GetConnectorName() const override;

  /// @brief function to propage data
  virtual void PropagateData() = 0;

  /// Fmu wrapper interface
  const std::shared_ptr<FmuWrapperInterface> fmuWrapperInterface;
  /// fmu scalar variable name
  const std::string fmuScalarVariableName;
};
}  //namespace ssp