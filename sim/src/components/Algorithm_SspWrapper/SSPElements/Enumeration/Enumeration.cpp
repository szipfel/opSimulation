/********************************************************************************
 * Copyright (c) 2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Enumeration.h"
ssp::Enumeration::~Enumeration()
{
  if (!this->items->empty()) this->items->erase(items->cbegin(), items->cend());
}
ssp::Enumeration::Enumeration()
{
  this->name = "";
  this->items = std::make_shared<std::vector<EnumeratorItem>>();
}
ssp::Enumeration::Enumeration(std::string, std::shared_ptr<std::vector<EnumeratorItem>> items)
    : name(name), items(items)
{
}
