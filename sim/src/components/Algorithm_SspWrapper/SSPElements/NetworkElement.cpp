/*******************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "NetworkElement.h"

#include "sim/src/components/Algorithm_SspWrapper/SspLogger.h"

ssp::NetworkElement::NetworkElement(const std::string &elementName,
                                    std::vector<std::shared_ptr<ssp::VisitableNetworkElement>> &&elements,
                                    std::vector<std::shared_ptr<ssp::ConnectorInterface>> &&input_connectors,
                                    std::vector<std::shared_ptr<ssp::ConnectorInterface>> &&output_connectors)
    : elementName(elementName),
      elements(std::move(elements)),
      input_connectors(std::move(input_connectors)),
      output_connectors(std::move(output_connectors))
{
}
ssp::NetworkElement::NetworkElement(std::string elementName) : elementName(std::move(elementName)) {}
std::string ssp::NetworkElement::GetName()
{
  return elementName;
}
void ssp::NetworkElement::EmplaceElement(std::shared_ptr<VisitableNetworkElement> &&element)
{
  elements.emplace_back(std::move(element));
}
const std::vector<std::shared_ptr<ssp::VisitableNetworkElement>> &ssp::NetworkElement::GetElements()
{
  return elements;
}
std::vector<std::shared_ptr<ssp::ConnectorInterface>> ssp::NetworkElement::GetInputConnectors()
{
  return input_connectors;
}
std::vector<std::shared_ptr<ssp::ConnectorInterface>> ssp::NetworkElement::GetOutputConnectors()
{
  return output_connectors;
}

void ssp::NetworkElement::Log(CbkLogLevel logLevel, const char *file, int line, const std::string &message) const
{
  SspLogger::Log(logLevel, file, line, message);
}