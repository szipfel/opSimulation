/*******************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "System.h"

#include <iostream>
#include <utility>

#include "sim/src/components/Algorithm_SspWrapper/ParserTypes.h"
#include "sim/src/components/Algorithm_SspWrapper/SspLogger.h"
#include "sim/src/components/Algorithm_SspWrapper/Visitors/Network/SspNetworkVisitorInterface.h"

ssp::System::System(const std::string &systemName) : NetworkElement(systemName)
{
  targetToTracesMap = std::make_shared<std::map<std::string, FmuFileHelper::TraceEntry>>();
}

ssp::System::System(const std::string &elementName,
                    std::vector<std::shared_ptr<ssp::VisitableNetworkElement>> &&elements,
                    std::vector<std::shared_ptr<ssp::ConnectorInterface>> &&input_connectors,
                    std::vector<std::shared_ptr<ssp::ConnectorInterface>> &&output_connectors,
                    std::shared_ptr<std::map<std::string, FmuFileHelper::TraceEntry>> targetToTracesMap)
    : NetworkElement(elementName, std::move(elements), std::move(input_connectors), std::move(output_connectors)),
      targetToTracesMap(std::move(targetToTracesMap))
{
  this->enumerations = std::make_shared<std::vector<Enumeration>>();
}

ssp::System::System(const std::string &elementName,
                    std::vector<std::shared_ptr<ssp::VisitableNetworkElement>> &&elements,
                    std::shared_ptr<std::map<std::string, FmuFileHelper::TraceEntry>> targetToTracesMap)
    : NetworkElement(elementName,
                     std::move(elements),
                     std::move(std::vector<std::shared_ptr<ssp::ConnectorInterface>>{}),
                     std::move(std::vector<std::shared_ptr<ssp::ConnectorInterface>>{})),
      targetToTracesMap(std::move(targetToTracesMap))
{
}

void ssp::System::Accept(ssp::SspNetworkVisitorInterface &visitorInterface)
{
  visitorInterface.Visit(this);
}

void ssp::System::ConnectInOutConnectors(const Connection &connection)
{
  auto *startElement = FindNetworkElement(connection.componentStartName);
  auto *endElement = FindNetworkElement(connection.componentEndName);
  auto endConnector = std::find_if(endElement->input_connectors.begin(),
                                   endElement->input_connectors.end(),
                                   [connection](auto connector)
                                   { return connector->GetConnectorName() == connection.variableReferenceEnd; });
  auto startConnector = std::find_if(startElement->output_connectors.begin(),
                                     startElement->output_connectors.end(),
                                     [connection](auto connector)
                                     { return connector->GetConnectorName() == connection.variableReferenceStart; });

  if (startConnector != startElement->output_connectors.end() && endConnector != startElement->input_connectors.end())
  {
    if (!*endConnector || !*startConnector) LOGWARN("SSP Parser: Could not parse connection");
    (*startConnector)->connectors.push_back(*endConnector);
    LOGINFO("SSP connect connector " + endConnector->get()->GetConnectorName() + " and connector "
            + startConnector->get()->GetConnectorName());
  }
}
int ssp::System::Priority()
{
  return 0;
}

void ssp::System::ManageConnection(const OSMPConnection &connection)
{
  auto completeConnection = oSMPConnectionCompletenessCheck.ReturnOSMPConnectionIfComplete(connection);

  if (completeConnection.has_value())
  {
    ManageConnection(static_cast<Connection>(connection));
  }
}

void ssp::System::ManageConnection(const Connection &connection)
{
  if (connection.componentStartName == GetName())
  {
    ConnectSystemWithEndConnector(connection);
  }
  else if (connection.componentEndName == GetName())
  {
    ConnectSystemWithStartConnector(connection);
  }
  else
  {
    ConnectInOutConnectors(connection);
  }
}

void ssp::System::ConnectSystemWithStartConnector(const Connection &connection)
{
  auto startElement = FindNetworkElement(connection.componentStartName);
  auto startConnector = std::find_if(startElement->output_connectors.begin(),
                                     startElement->output_connectors.end(),
                                     [connection](auto connector)
                                     { return connector->GetConnectorName() == connection.variableReferenceStart; });
  auto systemConnector
      = std::find_if(output_connectors.begin(),
                     output_connectors.end(),
                     [connection](auto connector)
                     {
                       return SSPParserHelper::RemoveOSMPRoleFromOSMPAnnotated(connector->GetConnectorName())
                           == connection.variableReferenceEnd;
                     });
  if (systemConnector == output_connectors.end())
  {
    LOGWARN("SSP connect system with start connector: Connection misses connector " + connection.componentEndName
            + ", connection can't be established");
  }
  else if (startElement)
  {
    if (startConnector != startElement->output_connectors.end())
    {
      this->systemOutputConnector->connectors.push_back(*startConnector);
      LOGINFO("SSP connect system with start connector: Connector " + startConnector->get()->GetConnectorName()
              + " connected to System " + GetName());
    }
    else
    {
      LOGWARN("SSP connect system with start connector: Connector not found and therefore could not connect to System "
              + GetName());
    }
  }
}

void ssp::System::ConnectSystemWithEndConnector(const Connection &connection)
{
  auto endElement = FindNetworkElement(connection.componentEndName);
  auto endConnector = std::find_if(endElement->input_connectors.begin(),
                                   endElement->input_connectors.end(),
                                   [connection](auto connector)
                                   { return connector->GetConnectorName() == connection.variableReferenceEnd; });
  auto systemConnector
      = std::find_if(input_connectors.begin(),
                     input_connectors.end(),
                     [connection](auto connector)
                     {
                       return SSPParserHelper::RemoveOSMPRoleFromOSMPAnnotated(connector->GetConnectorName())
                           == connection.variableReferenceStart;
                     });
  if (systemConnector == input_connectors.end())
  {
    LOGWARN("SSP connect system with end connector: Connection misses connector " + connection.componentEndName
            + " on system level, connection can't be established");
  }
  else if (endElement)
  {
    if (endConnector != endElement->input_connectors.end())
    {
      this->systemInputConnector->connectors.push_back(*endConnector);
      LOGINFO("SSP connect system with end connector: Connector " + endConnector->get()->GetConnectorName()
              + " connected to System " + GetName());
    }
    else
    {
      LOGWARN("SSP connect system with end connector: Connector not found and therefore could not connect to System "
              + GetName());
    }
  }
}

ssp::NetworkElement *ssp::System::FindNetworkElement(const std::string &componentName)
{
  auto component = (std::find_if(elements.begin(),
                                 elements.end(),
                                 [componentName](const auto &element) { return element->GetName() == componentName; }));
  auto *element = dynamic_cast<NetworkElement *>(component->get());

  if (component == elements.end())
    LOGWARN("SSP System FindNetworkElement: Could not find network element " + componentName);

  return element;
}

ssp::System::~System()
{
  if (outputDir.has_value())
  {
    FmuFileHelper::WriteTracesToFile(outputDir.value(), *(targetToTracesMap));
  }
}

void ssp::System::SetOutputDir(const std::filesystem::path &outDir)
{
  outputDir = outDir;
}

const std::filesystem::path &ssp::System::GetOutputDir()
{
  if (outputDir.has_value())
  {
    return outputDir.value();
  }
  else
  {
    LOGERRORANDTHROW("SSP System: Outdir has no value. No traces will be written for " + this->GetName() + ".");
  }
}

void ssp::System::SetEnumerations(std::shared_ptr<std::vector<std::shared_ptr<Enumeration>>> &enumerations)
{
  for (auto enumeration : *enumerations)
  {
    this->enumerations->push_back(*enumeration);
  }
}

bool ssp::System::HasEnumeration(const std::string name)
{
  auto itFoundEnum = std::find_if(
      this->enumerations->begin(), this->enumerations->end(), [name](const Enumeration e) { return e.name == name; });

  return (itFoundEnum != this->enumerations->end() ? true : false);
}
