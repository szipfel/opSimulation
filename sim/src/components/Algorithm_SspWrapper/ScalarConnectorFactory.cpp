/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * https://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "ScalarConnectorFactory.h"

#include "OSMPConnectorFactory.h"
#include "SSPElements/Connector/ScalarConnector.h"

RegisteredConnectorDirection ScalarConnectorFactory::RegisterConnector(
    const SspParserTypes::Connector &connector,
    const std::shared_ptr<FmuWrapperInterface> &fmuWrapper,
    int priority,
    ConnectorKind kind)
{
  if (kind == ConnectorKind::Input || kind == ConnectorKind::Inout)
  {
    RegisterScalarConnector(scalarInput,
                            std::visit(osmpConnectorNameVisitor, connector),
                            std::visit(scalarTypeVisitor, connector),
                            fmuWrapper,
                            priority,
                            false);
    return RegisteredConnectorDirection::Input;
  }
  else if (kind == ConnectorKind::Output || kind == ConnectorKind::Inout)
  {
    RegisterScalarConnector(scalarOutput,
                            std::visit(osmpConnectorNameVisitor, connector),
                            std::visit(scalarTypeVisitor, connector),
                            fmuWrapper,
                            priority,
                            false);
    return RegisteredConnectorDirection::Output;
  }
  else if (kind == ConnectorKind::Parameter)
  {
    RegisterScalarConnector(scalarInput,
                            std::visit(osmpConnectorNameVisitor, connector),
                            std::visit(scalarTypeVisitor, connector),
                            fmuWrapper,
                            priority,
                            true);
    return RegisteredConnectorDirection::Input;
  }
  else if (kind == ConnectorKind::Calculated_Parameter)
  {
    RegisterScalarConnector(scalarOutput,
                            std::visit(osmpConnectorNameVisitor, connector),
                            std::visit(scalarTypeVisitor, connector),
                            fmuWrapper,
                            priority,
                            true);
    return RegisteredConnectorDirection::Output;
  }
  return RegisteredConnectorDirection::None;
}

void ScalarConnectorFactory::RegisterScalarConnector(std::vector<ScalarData> scalarData,
                                                     const std::string &connectorName,
                                                     const ConnectorType connectorType,
                                                     const std::shared_ptr<FmuWrapperInterface> &fmuWrapper,
                                                     int priority,
                                                     bool isParameter)
{
  ScalarData data{};
  data.connectorName = connectorName, data.connectorType = connectorType, data.priority = priority,
  data.fmuWrapperInterface = fmuWrapper;
  data.isParameter = isParameter;
  scalarData.emplace_back(data);
}

std::vector<std::shared_ptr<ssp::ConnectorInterface>> ScalarConnectorFactory::CreateScalarConnectors(
    std::vector<ScalarData> scalarData,
    const std::vector<std::pair<std::string, std::string>> &writeMessageParameters,
    const std::filesystem::path &outputDir,
    const std::shared_ptr<std::map<std::string, FmuFileHelper::TraceEntry>> &targetOutputTracesMap)
{
  std::vector<std::shared_ptr<ssp::ConnectorInterface>> connectors{};
  ScalarConnectorBlueprint blueprint{};
  blueprint.writeMessageParameters = writeMessageParameters;
  blueprint.outputDir = outputDir;
  blueprint.targetOutputTracesMap = targetOutputTracesMap;
  for (const auto &data : scalarData)
  {
    UpdateBlueprint(data, blueprint);
    connectors.push_back(MakeGenericConnector(blueprint));
  }
  return connectors;
}

std::vector<std::shared_ptr<ssp::ConnectorInterface>> ScalarConnectorFactory::CreateInputScalarConnectors(
    const std::vector<std::pair<std::string, std::string>> &writeMessageParameters,
    const std::filesystem::path &outputDir,
    const std::shared_ptr<std::map<std::string, FmuFileHelper::TraceEntry>> &targetOutputTracesMap)
{
  return CreateScalarConnectors(scalarInput, writeMessageParameters, outputDir, targetOutputTracesMap);
}

std::vector<std::shared_ptr<ssp::ConnectorInterface>> ScalarConnectorFactory::CreateOutputScalarConnectors(
    const std::vector<std::pair<std::string, std::string>> &writeMessageParameters,
    const std::filesystem::path &outputDir,
    const std::shared_ptr<std::map<std::string, FmuFileHelper::TraceEntry>> &targetOutputTracesMap)
{
  return CreateScalarConnectors(scalarOutput, writeMessageParameters, outputDir, targetOutputTracesMap);
}
//ToDo add write Output capabilities
std::shared_ptr<ssp::Connector> ScalarConnectorFactory::MakeGenericConnector(const ScalarConnectorBlueprint &blueprint)
{
  if (blueprint.data.connectorType == ConnectorType::Integer)
  {
    auto connector = std::make_shared<ssp::ScalarConnector<ssp::VariableTypeInt>>(
        blueprint.data.fmuWrapperInterface, blueprint.data.connectorName, blueprint.data.priority);
    return SetScalarConnector(connector, blueprint);
  }
  if (blueprint.data.connectorType == ConnectorType::Real)
  {
    auto connector = std::make_shared<ssp::ScalarConnector<ssp::VariableTypeDouble>>(
        blueprint.data.fmuWrapperInterface, blueprint.data.connectorName, blueprint.data.priority);
    return SetScalarConnector(connector, blueprint);
  }
  if (blueprint.data.connectorType == ConnectorType::Boolean)
  {
    auto connector = std::make_shared<ssp::ScalarConnector<ssp::VariableTypeBool>>(
        blueprint.data.fmuWrapperInterface, blueprint.data.connectorName, blueprint.data.priority);
    return SetScalarConnector(connector, blueprint);
  }
  if (blueprint.data.connectorType == ConnectorType::Enumeration)
  {
    //handle the Enumerations somehow
    auto connector = std::make_shared<ssp::ScalarConnector<ssp::VariableTypeBool>>(
        blueprint.data.fmuWrapperInterface, blueprint.data.connectorName, blueprint.data.priority);
    return SetScalarConnector(connector, blueprint);
  }
  return std::shared_ptr<ssp::Connector>();
}

void ScalarConnectorFactory::UpdateBlueprint(const ScalarData &data, ScalarConnectorBlueprint &blueprint)
{
  blueprint.data.fmuWrapperInterface = data.fmuWrapperInterface;
  blueprint.data.priority = data.priority;
  blueprint.data.connectorType = data.connectorType;
  blueprint.data.connectorName = data.connectorName;
}