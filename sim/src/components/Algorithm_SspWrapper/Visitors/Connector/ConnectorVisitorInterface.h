/*******************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "../SspVisitorHelper.h"
#include "sim/src/components/Algorithm_SspWrapper/SspLogger.h"
#include "sim/src/components/Algorithm_SspWrapper/type_helper.h"

namespace ssp
{

/// Class representing an interface for an connector visitor
class ConnectorVisitorInterface
{
public:
  virtual ~ConnectorVisitorInterface() = default;

  /// @brief Visitor function to update output
  /// Pointer to the scalar connector base
  virtual void Visit(ScalarConnectorBase *) = 0;

  /// @brief Visitor function to update output
  /// Reference to the group connector
  virtual void Visit(GroupConnector &) = 0;

  /// @brief Visitor function to update output
  /// Pointer to OSMP Connector base
  virtual void Visit(OSMPConnectorBase *) = 0;

protected:
  //-------------------------------------------------------------------------
  //! Provides callback to LOG() macro.
  //!
  //! @param[in]     logLevel    Importance of log
  //! @param[in]     file        Name of file where log is called
  //! @param[in]     line        Line within file where log is called
  //! @param[in]     message     Message to log
  //-------------------------------------------------------------------------
  virtual void Log(CbkLogLevel logLevel, const char *file, int line, const std::string &message) const = 0;
};

}  //namespace ssp
