/*******************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "../../SSPElements/Connector/ConnectorHelper.h"
#include "../../SSPElements/Connector/OSMPConnectorBase.h"
#include "../../SignalInterface/SignalTranslator.h"
#include "../SspVisitorHelper.h"
#include "ConnectorVisitorInterface.h"

namespace ssp
{

/// Class representing a visitor to update output signal
class UpdateOutputSignalVisitor : public ConnectorVisitorInterface
{
public:
  /// @brief Constructor for update output signal
  /// @param localLinkId Local Link id
  /// @param data        A pointer to the signal interface
  /// @param time        Time
  /// @param world       world
  /// @param agent       ego agent
  /// @param callbacks   callbacks for logging
  UpdateOutputSignalVisitor(const int localLinkId,
                            std::shared_ptr<const SignalInterface> &data,
                            const int time,
                            WorldInterface *world,
                            AgentInterface *agent,
                            const CallbackInterface *callbacks);

  virtual ~UpdateOutputSignalVisitor() = default;

  const int localLinkId;                         ///< Local link id
  std::shared_ptr<const SignalInterface> &data;  ///< A pointer to the signal interface
  const int time;                                ///< Time
  WorldInterface *world;                         //!< Pointer to the WorldInterface
  AgentInterface *agent;                         //!< Pointer to the AgentInterface
  const CallbackInterface *callbacks;            //!< Pointer to the CallbackInterface

  /// @brief Visitor function to update output
  /// @param connector Pointer to the scalar connector base
  void Visit(ScalarConnectorBase *connector) override;

  /// @brief Visitor function to update output
  /// @param groupConnector Reference to the group connector
  void Visit(GroupConnector &groupConnector) override;

  /// @brief Visitor function to update output
  /// @param connector Reference to OSMP Connector base
  void Visit(OSMPConnectorBase *connector) override;

protected:
  //-------------------------------------------------------------------------
  //! Provides callback to LOG() macro.
  //!
  //! @param[in]     logLevel    Importance of log
  //! @param[in]     file        Name of file where log is called
  //! @param[in]     line        Line within file where log is called
  //! @param[in]     message     Message to log
  //-------------------------------------------------------------------------
  void Log(CbkLogLevel logLevel, const char *file, int line, const std::string &message) const override;
};
}  //namespace ssp