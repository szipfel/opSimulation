/*******************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#pragma once

#include "include/callbackInterface.h"
#include "sim/src/components/Algorithm_SspWrapper/SspLogger.h"

namespace ssp
{

class System;
class FmuComponent;
class SspComponent;

/// Class representing an interface for ssp network visitor
class SspNetworkVisitorInterface
{
public:
  virtual ~SspNetworkVisitorInterface() = default;

  /// @brief Visitor function for system
  virtual void Visit(const System *) = 0;

  /// @brief Visitor function for Fmu component
  virtual void Visit(const FmuComponent *) = 0;

  /// @brief Visitor function for ssp component
  virtual void Visit(const SspComponent *) = 0;

protected:
  //-------------------------------------------------------------------------
  //! Provides callback to LOG() macro.
  //!
  //! @param[in]     logLevel    Importance of log
  //! @param[in]     file        Name of file where log is called
  //! @param[in]     line        Line within file where log is called
  //! @param[in]     message     Message to log
  //-------------------------------------------------------------------------
  virtual void Log(CbkLogLevel logLevel, const char *file, int line, const std::string &message) const = 0;
};

}  //namespace ssp