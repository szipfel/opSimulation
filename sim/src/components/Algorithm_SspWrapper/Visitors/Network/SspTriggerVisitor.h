/*******************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#pragma once

#include <algorithm>
#include <memory>
#include <utility>
#include <vector>

#include <queue>

#include "sim/src/components/Algorithm_SspWrapper/Visitors/Network/SspNetworkVisitorInterface.h"

namespace ssp
{

/// Class representing a visitor for Ssp trigger
class SspTriggerVisitor : public SspNetworkVisitorInterface
{
public:
  ~SspTriggerVisitor() override = default;

  /// @brief Consturctor for SSP Trigger visitor
  /// @param time Time
  explicit SspTriggerVisitor(int time);

  /// @brief Visitor function for SSD system
  /// @param ssdSystem Pointer to the ssd system
  void Visit(const System *ssdSystem) override;

  /// @brief Visitor function for SSP Trigger
  /// @param component Fmu Component
  void Visit(const FmuComponent *component) override;

  /// @brief Visitor function for SSP Trigger with sspcomponent type
  void Visit(const SspComponent *) override;

protected:
  void Log(CbkLogLevel logLevel, const char *file, int line, const std::string &message) const override;

private:
  const int time;
};

}  //namespace ssp