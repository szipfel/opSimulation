/*******************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <algorithm>
#include <cassert>
#include <iostream>
#include <map>
#include <memory>
#include <stdexcept>
#include <string>
#include <utility>

#include <queue>

#include "sim/src/components/Algorithm_FmuWrapper/src/fmuWrapper.h"
#include "sim/src/core/common/log.h"

namespace ssp
{

class Connector;
class ScalarConnectorBase;
class OSMPConnectorBase;
class GroupConnector;
class ConnectorVisitorInterface;
class SspNetworkVisitorInterface;

/// Class representing an helper for ssp visitor
class SSPVisitorHelper
{
public:
  /// @brief compare priorities of two connectors
  /// @param val1 first connector
  /// @param val2 second connector
  /// @return
  static bool ComparePriorities(const std::shared_ptr<Connector> &val1, const std::shared_ptr<Connector> &val2);

  /// @brief Function to trigger once the scalar connector
  /// @param alreadyTriggered List of references to the already triggered fmuwrapper interface
  /// @param connector        Pointer to the scalar connector base
  /// @param time             Time of trigger
  static void TriggerScalarConnectorOnce(std::vector<std::weak_ptr<FmuWrapperInterface>> &alreadyTriggered,
                                         const ScalarConnectorBase *connector,
                                         int time);

  /// @brief Trigger an OSMP connecter for once
  /// @param alreadyTriggered A pointer to the Fmu Wrapper interface
  /// @param connector        OSMP connector base
  /// @param time             Time
  static void TriggerOMSPConnectorOnce(std::vector<std::weak_ptr<FmuWrapperInterface>> &alreadyTriggered,
                                       OSMPConnectorBase *connector,
                                       int time);

  /// @brief Accept priority
  /// @param groupConnector   Reference to the group connector
  /// @param connectorVisitor Reference to the connector visitor interface
  static void PriorityAccept(const GroupConnector &groupConnector, ConnectorVisitorInterface &connectorVisitor);

  /// @brief For each heap
  /// @tparam Container
  /// @tparam Comparator
  /// @tparam Action
  /// @param container    Reference to the container
  /// @param comparator   Reference to the comparator
  /// @param action       Reference to the action
  template <typename Container, typename Comparator, typename Action>
  static void ForEachHeap(const Container &container, const Comparator &comparator, const Action &action)
  {
    typedef const std::remove_cv_t<typename Container::value_type> *elementType;
    const auto dereferencedCompare = [&comparator](const auto &lhs, const auto &rhs) { return comparator(*lhs, *rhs); };
    std::priority_queue<elementType, std::vector<elementType>, decltype(dereferencedCompare)> priorityQueue{
        dereferencedCompare};
    std::for_each(
        cbegin(container), cend(container), [&priorityQueue](const auto &element) { priorityQueue.emplace(&element); });
    while (!priorityQueue.empty())
    {
      action(*priorityQueue.top());
      priorityQueue.pop();
    }
  }

  /// @brief Function to accept priority of visitable network elements
  /// @tparam Container
  /// @param visitor      Reference to the SSP network visitor interface
  /// @param container    Reference to the container
  template <typename Container>
  static void PriorityAcceptVisitableNetworkElements(ssp::SspNetworkVisitorInterface &visitor,
                                                     const Container &container)
  {
    ForEachHeap(
        container,
        [](const auto &lhs, const auto &rhs) { return lhs->Priority() < rhs->Priority(); },
        [&visitor](const auto &element) { element->Accept(visitor); });
  }
};
}  //namespace ssp