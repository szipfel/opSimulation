/*******************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <osi3/osi_groundtruth.pb.h>
#include <osi3/osi_sensordata.pb.h>
#include <osi3/osi_sensorview.pb.h>
#include <osi3/osi_sensorviewconfiguration.pb.h>
#include <osi3/osi_trafficcommand.pb.h>
#include <osi3/osi_trafficupdate.pb.h>
#include <type_traits>
#include <variant>

#include "include/fmuHandlerInterface.h"

namespace ssp
{

typedef std::integral_constant<VariableType, VariableType::Bool> VariableTypeBool;
typedef std::integral_constant<VariableType, VariableType::Int> VariableTypeInt;
typedef std::integral_constant<VariableType, VariableType::Double> VariableTypeDouble;
typedef std::integral_constant<VariableType, VariableType::String> VariableTypeString;
typedef std::integral_constant<VariableType, VariableType::Enum> VariableTypeEnum;

typedef std::variant<osi3::SensorView,
                     osi3::SensorViewConfiguration,
                     osi3::TrafficUpdate,
                     osi3::SensorData,
                     osi3::GroundTruth,
                     osi3::TrafficCommand,
                     osi3::HostVehicleData>
    SupportedOsiTypes;

template <typename T>
class ScalarConnector;  ///< including a scalar connector class

typedef std::variant<VariableTypeBool, VariableTypeInt, VariableTypeDouble, VariableTypeString, VariableTypeEnum>
    SupportedScalarValueTypes;
typedef std::variant<ScalarConnector<VariableTypeBool>,
                     ScalarConnector<VariableTypeInt>,
                     ScalarConnector<VariableTypeDouble>,
                     ScalarConnector<VariableTypeString>,
                     ScalarConnector<VariableTypeEnum>>
    SupportedScalarConnectorVariants;

template <typename T, typename... U>
inline static constexpr bool is_same_as_any_of_v = (std::is_same_v<T, U> || ...);

template <typename T, typename... U>
inline constexpr bool is_same_as_any_of_variant(std::variant<U...>)
{
  return is_same_as_any_of_v<T, U...>;
}

template <typename T, typename Variant, auto... I>
inline static constexpr auto expand_variant_types_into_is_same_as_any_of_v(std::index_sequence<I...>)
{
  return is_same_as_any_of_v<T, std::variant_alternative_t<I, Variant>...>;
}

template <typename T, typename V>
inline static constexpr bool is_same_as_any_of_variant_v
    = expand_variant_types_into_is_same_as_any_of_v<T, V>(std::make_index_sequence<std::variant_size_v<V>>());

template <typename T, typename = std::is_invocable<T>>
struct integral_constant_type_converter  ///< Convertor of a constant variable type
{
private:
  static constexpr auto GetRetType()
  {
    if constexpr (T() == VariableType::Bool)
      return bool{};
    else if constexpr (T() == VariableType::Int)
      return int{};
    else if constexpr (T() == VariableType::Double)
      return double{};
    else if constexpr (T() == VariableType::String)
      return std::string{};
    else if constexpr (T() == VariableType::Enum)
      return int{};
    else
      return;
  }

public:
  //! Type of the values from FMUs
  typedef std::invoke_result_t<decltype(&integral_constant_type_converter<T>::GetRetType)> type;
};

template <typename T>
using cvt_t = typename integral_constant_type_converter<T>::type;

template <typename T, typename = std::is_invocable<T>>
auto constexpr inline GetUnionValue(FmuValue fmuValueUnion) -> cvt_t<T>
{
  if constexpr (T() == VariableType::Bool)
    return fmuValueUnion.boolValue;
  else if constexpr (T() == VariableType::Int)
    return fmuValueUnion.intValue;
  else if constexpr (T() == VariableType::Double)
    return fmuValueUnion.realValue;
  else if constexpr (T() == VariableType::String)
    return fmuValueUnion.stringValue;
  else if constexpr (T() == VariableType::Enum)
    return fmuValueUnion.intValue;
  else
    return;
}

template <typename T, typename = std::is_invocable<T>>
std::string inline CvtToString(const cvt_t<T> value)
{
  if constexpr (T() == VariableType::Bool)
    return value ? "true" : "false";
  else
    return std::to_string(value);
}

template <typename T, typename = std::is_invocable<T>>
auto constexpr inline ToFmuValue(cvt_t<T> value)
{
  FmuValue fmuValue{};
  if constexpr (T() == VariableType::Bool)
    fmuValue.boolValue = value;
  else if constexpr (T() == VariableType::Int)
    fmuValue.intValue = value;
  else if constexpr (T() == VariableType::Double)
    fmuValue.realValue = value;
  else if constexpr (T() == VariableType::String)
    fmuValue.stringValue = value;
  else if constexpr (T() == VariableType::Enum)
    fmuValue.intValue = value;
  else
    return;
  return fmuValue;
}

template <typename OsiType>
constexpr auto StringFromOsiType()
{
  if constexpr (std::is_same_v<OsiType, osi3::SensorData>)
  {
    return "sd";
  }
  else if (std::is_same_v<OsiType, osi3::SensorView>)
  {
    return "sv";
  }
  else if (std::is_same_v<OsiType, osi3::SensorViewConfiguration>)
  {
    return "svc";
  }
  else if (std::is_same_v<OsiType, osi3::GroundTruth>)
  {
    return "gt";
  }
  else if (std::is_same_v<OsiType, osi3::TrafficUpdate>)
  {
    return "tu";
  }
  else if (std::is_same_v<OsiType, osi3::TrafficCommand>)
  {
    return "tc";
  }
  else if (std::is_same_v<OsiType, osi3::HostVehicleData>)
  {
    return "hv";
  }
  else
  {
    throw std::runtime_error("Unknown OSI type");
  }
}

}  // namespace ssp