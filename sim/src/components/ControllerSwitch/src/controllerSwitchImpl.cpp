/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** \file  controllerSwitchImpl.cpp */
//-----------------------------------------------------------------------------

#include "controllerSwitchImpl.h"

#include <exception>

#include <qglobal.h>

#include "common/primitiveSignals.h"

ControllerSwitchImplementation::ControllerSwitchImplementation(
    std::string componentName,
    bool isInit,
    int priority,
    int offsetTime,
    int responseTime,
    int cycleTime,
    StochasticsInterface *stochastics,
    WorldInterface *world,
    const ParameterInterface *parameters,
    PublisherInterface *const publisher,
    const CallbackInterface *callbacks,
    AgentInterface *agent,
    std::shared_ptr<ScenarioControlInterface> const scenarioControl)
    : UnrestrictedControllStrategyModelInterface(componentName,
                                                 isInit,
                                                 priority,
                                                 offsetTime,
                                                 responseTime,
                                                 cycleTime,
                                                 stochastics,
                                                 world,
                                                 parameters,
                                                 publisher,
                                                 callbacks,
                                                 agent,
                                                 scenarioControl)
{
}

void ControllerSwitchImplementation::UpdateInput(int localLinkId,
                                                 const std::shared_ptr<SignalInterface const> &signal,
                                                 int time)
{
  Q_UNUSED(time);

  if (localLinkId == 0)
  {
    defaultControllerSignal = signal;
  }
  else if (localLinkId == 1)
  {
    customControllerSignal = signal;
  }
  else
  {
    const std::string msg = COMPONENTNAME + " invalid link";
    LOG(CbkLogLevel::Debug, msg);
    throw std::runtime_error(msg);
  }
}

void ControllerSwitchImplementation::UpdateOutput(int localLinkId,
                                                  std::shared_ptr<SignalInterface const> &signal,
                                                  int time)
{
  Q_UNUSED(localLinkId);
  Q_UNUSED(time);

  std::shared_ptr<SignalInterface const> outputSignal
      = GetScenarioControl()->UseCustomController() ? customControllerSignal : defaultControllerSignal;

  if (outputSignal != nullptr)
  {
    signal = outputSignal;
  }
  else
  {
    signal = std::make_shared<ComponentStateSignal>(ComponentState::Undefined);
  }
}
