/********************************************************************************
 * Copyright (c) 2020-2021 ITK Engineering GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#ifndef WHEELOSCILLATION_H
#define WHEELOSCILLATION_H

#include "units.h"

//! Wheel force perpendicular.
class WheelOscillation
{
public:
  WheelOscillation() = default;
  /**
   * @brief Construct a new Wheel Oscillation object
   *
   * @param wid TODO
   * @param k
   * @param q
   */
  WheelOscillation(int wid, double k, double q);
  WheelOscillation(const WheelOscillation &) = delete;
  WheelOscillation(WheelOscillation &&) = delete;
  WheelOscillation &operator=(const WheelOscillation &) = delete;
  WheelOscillation &operator=(WheelOscillation &&) = delete;

  virtual ~WheelOscillation() = default;

  /**
   * @brief Initialze the object
   *
   * @param id  TODO
   * @param time_step
   * @param k
   * @param q
   */
  void Init(int id, double time_step, double k, double q);

  /**
   * @brief Perform simulation
   *
   * @param forceZ force in z direction
   * @param mass   mass of the wheel
   */
  void Perform(units::force::newton_t forceZ, units::mass::kilogram_t mass);

  /**
   * @brief Get the Cur Z Pos object
   *
   * @return curZ TODO
   */
  double GetCurZPos() const { return curZ; }

private:
  int id = -1;
  double prevVz = 0.0;

  double curAz = 0.0;
  double curVz = 0.0;
  double curZ = 0.0;

  double maxZ = 0.01;
  double minZ = -0.04;

  double timeStep;  // 0.01 second = 10 millisecond
  double coeffSpring;
  double coeffDamp = 2000.0;
};

#endif  //WHEELOSCILLATION_H
