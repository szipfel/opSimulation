/********************************************************************************
 * Copyright (c) 2020-2021 ITK Engineering GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "dynamics_collision_implementation.h"

#include <QString>
#include <QtGlobal>

Dynamics_Collision_Implementation::Dynamics_Collision_Implementation(std::string componentName,
                                                                     bool isInit,
                                                                     int priority,
                                                                     int offsetTime,
                                                                     int responseTime,
                                                                     int cycleTime,
                                                                     StochasticsInterface *stochastics,
                                                                     WorldInterface *world,
                                                                     const ParameterInterface *parameters,
                                                                     PublisherInterface *const publisher,
                                                                     const CallbackInterface *callbacks,
                                                                     AgentInterface *agent)
    : DynamicsInterface(componentName,
                        isInit,
                        priority,
                        offsetTime,
                        responseTime,
                        cycleTime,
                        stochastics,
                        world,
                        parameters,
                        publisher,
                        callbacks,
                        agent)
{
  LOGINFO("Constructing Dynamics_Collision");
  cycleTimeSec = units::time::millisecond_t(cycleTime);
  LOGINFO("Constructing Dynamics_Collision successful");
}

void Dynamics_Collision_Implementation::UpdateInput(int localLinkId,
                                                    const std::shared_ptr<SignalInterface const> &data,
                                                    int time)
{
  Q_UNUSED(time);

  std::stringstream log;
  log << COMPONENTNAME << " UpdateInput";
  LOG(CbkLogLevel::Debug, log.str());
  log.str(std::string());

  bool success = inputPorts.at(localLinkId)->SetSignalValue(data);

  if (success)
  {
    log << COMPONENTNAME << " UpdateInput successful";
    LOG(CbkLogLevel::Debug, log.str());
  }
  else
  {
    log << COMPONENTNAME << " UpdateInput failed";
    LOG(CbkLogLevel::Error, log.str());
  }
}

void Dynamics_Collision_Implementation::UpdateOutput(int localLinkId,
                                                     std::shared_ptr<SignalInterface const> &data,
                                                     int time)
{
  // no outputs of the module
  Q_UNUSED(localLinkId);
  Q_UNUSED(data);
  Q_UNUSED(time);

  std::runtime_error("invalid link");
}

void Dynamics_Collision_Implementation::Trigger(int time)
{
  Q_UNUSED(time);

  CollisionState nextState = CollisionState::NOCOLLISION;
  bool in_collisionOccured = collisionOccured.GetValue();
  switch (collisionState)
  {
    case CollisionState::NOCOLLISION:
      if (in_collisionOccured)
      {
        nextState = CollisionState::COLLISIONIMPULS;
      }
      else
      {
        nextState = CollisionState::NOCOLLISION;
      }
      break;
    case CollisionState::COLLISIONIMPULS:
      if (in_collisionOccured)
      {
        nextState = CollisionState::COLLISION;
      }
      else
      {
        nextState = CollisionState::NOCOLLISION;
      }
      break;
    case CollisionState::COLLISION:
      if (in_collisionOccured)
      {
        nextState = CollisionState::COLLISION;
      }
      else
      {
        nextState = CollisionState::NOCOLLISION;
      }
      break;
  }

  collisionState = nextState;  // immediate transition

  if (collisionState == CollisionState::COLLISIONIMPULS)
  {
    auto collisionPartners = GetAgent()->GetCollisionPartners();

    if (collisionPartners.size() > 0)
    {
      PostCrashVelocity postCrashVel = GetAgent()->GetPostCrashVelocity();

      GetAgent()->SetPositionX(GetAgent()->GetPositionX() + GetAgent()->GetVelocity().x * cycleTimeSec);  // global CS
      GetAgent()->SetPositionY(GetAgent()->GetPositionY() + GetAgent()->GetVelocity().y * cycleTimeSec);  // global CS
      GetAgent()->SetYaw(GetAgent()->GetYaw() + GetAgent()->GetYawRate() * cycleTimeSec);

      Common::Vector2d<units::velocity::meters_per_second_t> velocityImpact(postCrashVel.velocityAbsolute, 0.0_mps);
      velocityImpact.Rotate(postCrashVel.velocityDirection);
      GetAgent()->SetVelocityVector({velocityImpact.x, velocityImpact.y, 0.0_mps});
      GetAgent()->SetYawRate(postCrashVel.yawVelocity);
      LOGDEBUG(QString()
                   .sprintf("Override Velocity by Dynamics_CollisionPCM for agent %d: %f, %f, %f",
                            GetAgent()->GetId(),
                            velocityImpact.x.value(),
                            velocityImpact.y.value(),
                            postCrashVel.yawVelocity.value())
                   .toStdString());
    }
    Common::Vector2d accelerationImpact(0.0, 0.0);
    units::angular_acceleration::radians_per_second_squared_t yawAccel{0.0};
    GetAgent()->SetTangentialAcceleration(units::acceleration::meters_per_second_squared_t(accelerationImpact.x));
    GetAgent()->SetCentripetalAcceleration(units::acceleration::meters_per_second_squared_t(accelerationImpact.y));
    GetAgent()->SetYawAcceleration(yawAccel);

    // for logging purposes: only longitudinal acceleration in vehicle CS
    GetAgent()->SetAcceleration(units::acceleration::meters_per_second_squared_t(accelerationImpact.x));

    // Application of acceleration instead of setting velocities is not possible:
    // The acceleration values are far too high for a single simulation step (200g and above)

    LOGDEBUG(QString()
                 .sprintf("Override Acceleration by Dynamics_CollisionPCM for agent %d: %f, %f, %f",
                          GetAgent()->GetId(),
                          accelerationImpact.x,
                          accelerationImpact.y,
                          yawAccel.value())
                 .toStdString());
  }
}
