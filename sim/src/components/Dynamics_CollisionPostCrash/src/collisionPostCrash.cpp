/********************************************************************************
 * Copyright (c) 2017-2019 ITK Engineering GmbH
 *               2017-2019 in-tech GmbH
 *               2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  collisionPostCrash.cpp
//! @brief This file contains the implementation header file
//-----------------------------------------------------------------------------

/**
 * @defgroup module_coll Collision post crash model
 * The dynamics after a collision is modelled.
 *
 * image html @cond \ref collAngles.png @endcond "Definition of original collision angles at time of first contact"
 * The transformed collision angles HCPA and OCPA are scaled such that width and length of the
 * vehicle are 1.
 * For more information see http://indexsmart.mirasmart.com/26esv/PDFfiles/26ESV-000177.pdf
 * image html @cond \ref collision.png @endcond "Calculation of post crash dynamics takes place after penetration time"
 *
 */

/**
 * @ingroup module_coll
 * @defgroup retrieve_coll Retrieve collision partners
 */
/**
 * @ingroup module_coll
 * @defgroup pc_fading Post crash fading out
 */

#include "collisionPostCrash.h"

#include <QtGlobal>

DynamicsPostCrashImplementation::DynamicsPostCrashImplementation(std::string componentName,
                                                                 bool isInit,
                                                                 int priority,
                                                                 int offsetTime,
                                                                 int responseTime,
                                                                 int cycleTime,
                                                                 StochasticsInterface *stochastics,
                                                                 WorldInterface *world,
                                                                 const ParameterInterface *parameters,
                                                                 PublisherInterface *const publisher,
                                                                 const CallbackInterface *callbacks,
                                                                 AgentInterface *agent)
    : UnrestrictedModelInterface{componentName,
                                 isInit,
                                 priority,
                                 offsetTime,
                                 responseTime,
                                 cycleTime,
                                 stochastics,
                                 world,
                                 parameters,
                                 publisher,
                                 callbacks,
                                 agent},
      dynamicsSignal{}
{
  dynamicsSignal.componentState = ComponentState::Disabled;
}

void DynamicsPostCrashImplementation::UpdateInput(int localLinkId,
                                                  const std::shared_ptr<SignalInterface const> &data,
                                                  int time)
{
  Q_UNUSED(localLinkId);
  Q_UNUSED(data);
  Q_UNUSED(time);
}

void DynamicsPostCrashImplementation::UpdateOutput(int localLinkId,
                                                   std::shared_ptr<SignalInterface const> &data,
                                                   int time)
{
  Q_UNUSED(time);

  if (localLinkId == 0)
  {
    try
    {
      data = std::make_shared<DynamicsSignal const>(dynamicsSignal);
    }
    catch (const std::bad_alloc &)
    {
      const std::string msg = COMPONENTNAME + " could not instantiate signal";
      LOG(CbkLogLevel::Debug, msg);
      throw std::runtime_error(msg);
    }
  }
  else
  {
    const std::string msg = COMPONENTNAME + " invalid link";
    LOG(CbkLogLevel::Debug, msg);
    throw std::runtime_error(msg);
  }
}

void DynamicsPostCrashImplementation::Trigger(int time)
{
  bool signalSet = false;
  /** @addtogroup retrieve_coll
   * - For the given host agent, retrieve the list of collision partners from the simulation core.
   * - Iterate over collision partners.
   * - Trigger calculation of post crash dynamics.
   */
  if (GetAgent()->GetCollisionPartners().size() > numberOfCollisionPartners)
  {
    numberOfCollisionPartners = GetAgent()->GetCollisionPartners().size();
    isActive = true;
    dynamicsSignal.componentState = ComponentState::Acting;

    signalSet = TriggerPostCrashCheck(time);
  }

  /** @addtogroup pc_fading
   * In case the collision has occurred before:
   * - Calculate a dynamics signal for fading out.
   */
  else if (isActive)
  {
    SetFadingDynamics();
    signalSet = true;
  }
  if (!signalSet)
  {
    SetFadingDynamics();
  }
}

void DynamicsPostCrashImplementation::SetFadingDynamics()
{
  dynamicsSignal.dynamicsInformation.yaw = GetAgent()->GetYaw();
  dynamicsSignal.dynamicsInformation.yawRate
      = GetAgent()->GetYawRate();  // This should be zero if yaw is not changing anymore
  dynamicsSignal.dynamicsInformation.yawAcceleration
      = GetAgent()->GetYawAcceleration();  // This should be zero if yaw and yaw rate are not changing anymore (except
                                           // for the moment in which yawRate changes to zero)
  const units::acceleration::meters_per_second_squared_t deceleration{10.0};
  velocity -= deceleration * units::time::millisecond_t(GetCycleTime());
  velocity = units::math::max(0.0_mps, velocity);
  // change of path coordinate since last time step [m]
  units::length::meter_t ds = velocity * units::time::millisecond_t(GetCycleTime());
  // change of inertial x-position due to ds and yaw [m]
  units::length::meter_t dx = ds * units::math::cos(movingDirection);
  // change of inertial y-position due to ds and yaw [m]
  units::length::meter_t dy = ds * units::math::sin(movingDirection);
  // new inertial x-position [m]
  units::length::meter_t x = GetAgent()->GetPositionX() + dx;
  // new inertial y-position [m]
  units::length::meter_t y = GetAgent()->GetPositionY() + dy;

  dynamicsSignal.dynamicsInformation.velocityX = velocity * units::math::cos(movingDirection);
  dynamicsSignal.dynamicsInformation.velocityY = velocity * units::math::sin(movingDirection);
  dynamicsSignal.dynamicsInformation.acceleration = 0.0_mps_sq;
  dynamicsSignal.dynamicsInformation.positionX = x;
  dynamicsSignal.dynamicsInformation.positionY = y;
  dynamicsSignal.dynamicsInformation.travelDistance = ds;
}

bool DynamicsPostCrashImplementation::TriggerPostCrashCheck(int time)
{
  PostCrashVelocity postCrashVel = GetAgent()->GetPostCrashVelocity();
  if (!postCrashVel.isActive)
  {
    return false;
  }

  units::time::millisecond_t cycleTime{double(GetCycleTime())};
  auto yawAngle = GetAgent()->GetYaw();
  auto posX = GetAgent()->GetPositionX() + GetAgent()->GetVelocity().x * cycleTime;  //global CS
  auto posY = GetAgent()->GetPositionY() + GetAgent()->GetVelocity().y * cycleTime;  //global CS
  auto yawRatePrevious = GetAgent()->GetYawRate();
  yawAngle = yawAngle + yawRatePrevious * cycleTime;

  velocity = postCrashVel.velocityAbsolute;
  movingDirection = postCrashVel.velocityDirection;
  units::angular_velocity::radians_per_second_t yawRate = postCrashVel.yawVelocity;
  auto yawAcceleration = (yawRate - yawRatePrevious) / cycleTime;

  Common::Vector2d<units::velocity::meters_per_second_t> velocityVector((velocity * units::math::cos(movingDirection)),
                                                                        (velocity * units::math::sin(movingDirection)));
  velocityVector.Rotate(-yawAngle);

  units::acceleration::meters_per_second_squared_t acceleration{0.0};
  units::length::meter_t travelDist = velocity * cycleTime;

  dynamicsSignal.dynamicsInformation.yaw = yawAngle;
  dynamicsSignal.dynamicsInformation.yawRate = yawRate;
  dynamicsSignal.dynamicsInformation.yawAcceleration = yawAcceleration;
  dynamicsSignal.dynamicsInformation.velocityX = velocity * units::math::cos(movingDirection);
  dynamicsSignal.dynamicsInformation.velocityY = velocity * units::math::sin(movingDirection);
  dynamicsSignal.dynamicsInformation.acceleration = acceleration;
  dynamicsSignal.dynamicsInformation.positionX = posX;
  dynamicsSignal.dynamicsInformation.positionY = posY;
  dynamicsSignal.dynamicsInformation.travelDistance = travelDist;

  GetAgent()->SetPostCrashVelocity({false, 0.0_mps, 0.0_rad, 0.0_rad_per_s});

  return true;
}
