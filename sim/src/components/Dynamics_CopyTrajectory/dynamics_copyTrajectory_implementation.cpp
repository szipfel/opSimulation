/********************************************************************************
 * Copyright (c) 2020-2021 ITK Engineering GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "dynamics_copyTrajectory_implementation.h"

#include <memory>

#include <QtGlobal>

Dynamics_CopyTrajectory_Implementation::Dynamics_CopyTrajectory_Implementation(std::string componentName,
                                                                               bool isInit,
                                                                               int priority,
                                                                               int offsetTime,
                                                                               int responseTime,
                                                                               int cycleTime,
                                                                               StochasticsInterface *stochastics,
                                                                               WorldInterface *world,
                                                                               const ParameterInterface *parameters,
                                                                               PublisherInterface *const publisher,
                                                                               const CallbackInterface *callbacks,
                                                                               AgentInterface *agent)
    : DynamicsInterface(componentName,
                        isInit,
                        priority,
                        offsetTime,
                        responseTime,
                        cycleTime,
                        stochastics,
                        world,
                        parameters,
                        publisher,
                        callbacks,
                        agent)
{
  LOGINFO(QString().sprintf("Constructing Dynamics_CopyTrajectory for agent %d...", agent->GetId()).toStdString());
  timeStep = units::time::millisecond_t(cycleTime);
  LOGINFO("Constructing Dynamics_CopyTrajectory successful");
}

void Dynamics_CopyTrajectory_Implementation::UpdateInput(int localLinkId,
                                                         const std::shared_ptr<SignalInterface const> &data,
                                                         int time)
{
  Q_UNUSED(time);
  Q_UNUSED(localLinkId);

  LOGDEBUG(QString().sprintf("%s UpdateInput", COMPONENTNAME.c_str()).toStdString());

  // bool success = false;
  const auto trajectorySignal = std::dynamic_pointer_cast<TrajectorySignal const>(data);

  try
  {
    if (!std::holds_alternative<mantle_api::PolyLine>(trajectorySignal->trajectory.type))
    {
      throw std::runtime_error("Component: " + GetComponentName() + "can only use PolyLines as trajectory.");
    }
    trajectory = std::get<mantle_api::PolyLine>(trajectorySignal->trajectory.type);
    ReadWayPointData();
    LOGDEBUG(QString().sprintf("%s UpdateInput successful", COMPONENTNAME.c_str()).toStdString());
  }
  catch (...)
  {
    LOGERROR(QString().sprintf("%s UpdateInput failed", COMPONENTNAME.c_str()).toStdString());
  }
}

void Dynamics_CopyTrajectory_Implementation::UpdateOutput(int localLinkId,
                                                          std::shared_ptr<SignalInterface const> &data,
                                                          int time)
{
  // no outputs of the module
  Q_UNUSED(localLinkId);
  Q_UNUSED(data);
  Q_UNUSED(time);
}

void Dynamics_CopyTrajectory_Implementation::Trigger(int time)
{
  // LOGDEBUG("Triggering Dynamics_CopyTrajectory...");

  units::time::second_t timeSec = units::time::millisecond_t(time);
  units::length::meter_t x, y;
  units::velocity::meters_per_second_t vx, vy;
  units::angle::radian_t yaw;
  units::angular_velocity::radians_per_second_t w;

  if (timeSec <= timeVec.front())
  {
    x = posX.front();
    y = posY.front();
    vx = velX.front();
    vy = velY.front();
    yaw = angleYaw.front();
    w = rateYaw.front();
  }
  else if (timeSec >= timeVec.back())
  {
    x = posX.back();
    y = posY.back();
    vx = velX.back();
    vy = velY.back();
    yaw = angleYaw.back();
    w = rateYaw.back();
  }
  else
  {
    while (timeSec > timeVecNext)
    {
      indexVecNext++;
      timeVecNext = timeVec[indexVecNext];
    }
    unsigned int indexVecPassed = indexVecNext - 1;
    auto dT = (timeSec - timeVec[indexVecPassed]);
    if (dT < units::time::second_t(1e-6) && dT > units::time::second_t(-1e-6))
    {
      dT = 0.0_s;
    }
    x = posX[indexVecPassed] + velX[indexVecPassed] * dT;
    y = posY[indexVecPassed] + velY[indexVecPassed] * dT;
    yaw = angleYaw[indexVecPassed] + rateYaw[indexVecPassed] * dT;
    vx = velX[indexVecPassed];
    vy = velY[indexVecPassed];
    w = rateYaw[indexVecPassed];

    // LOGDEBUG(QString().sprintf(
    //              "t = %f; iPassed = %d; iNext = %d",
    //              timeSec, indexVecPassed, indexVecNext).toStdString());
  }

  // LOGDEBUG(QString().sprintf(
  //              "x = %f; y = %f; yaw = %f; vx = %f; vy = %f; w = %f",
  //              x, y, yaw, vx, vy, w).toStdString());

  GetAgent()->SetPositionX(x);
  GetAgent()->SetPositionY(y);
  GetAgent()->SetVelocityVector({vx, vy, 0.0_mps});
  GetAgent()->SetYaw(yaw);
  GetAgent()->SetYawRate(w);

  // LOGDEBUG("Trigger Dynamics_CopyTrajectory executed!");
}

void Dynamics_CopyTrajectory_Implementation::ReadWayPointData()
{
  unsigned int n = trajectory.size();

  units::velocity::meters_per_second_t vX{0.0};
  units::velocity::meters_per_second_t vY{0.0};
  units::angular_velocity::radians_per_second_t w{0.0};

  timeVec.resize(trajectory.size());
  posX.resize(trajectory.size());
  posY.resize(trajectory.size());
  velX.resize(trajectory.size());
  velY.resize(trajectory.size());
  angleYaw.resize(trajectory.size());
  rateYaw.resize(trajectory.size());

  for (unsigned int i = 0; i < n; ++i)
  {
    auto point = trajectory.at(i);

    if (!point.time.has_value())
    {
      throw std::runtime_error("Timestamp is requried for PolyLinePoints in component: " + GetComponentName() + ".");
    }

    timeVec[i] = point.time.value();
    posX[i] = point.pose.position.x;
    posY[i] = point.pose.position.y;
    angleYaw[i] = point.pose.orientation.yaw;
    if (i < n - 1)
    {
      auto pointNext = trajectory.at(i + 1);
      vX = (pointNext.pose.position.x - point.pose.position.x) / timeStep;           // uniform motion approximation
      vY = (pointNext.pose.position.y - point.pose.position.y) / timeStep;           // uniform motion approximation
      w = (pointNext.pose.orientation.yaw - point.pose.orientation.yaw) / timeStep;  // uniform motion approximation
    }
    velX[i] = vX;
    velY[i] = vY;
    rateYaw[i] = w;
  }

  indexVecNext = 1;
  timeVecNext = timeVec[indexVecNext];
}
