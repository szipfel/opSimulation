/********************************************************************************
 * Copyright (c) 2020-2021 ITK Engineering GmbH
 *               2023 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

/** @addtogroup Motion_Model
 * Abbreviations:
 * - COG = center-of-gravity
 * - COS = coordinate system
 */

/**
 * @ingroup Motion_Model
 * @defgroup init_tt Initialization
 */
/**
 * @ingroup Motion_Model
 * @defgroup sim_step_00_tt Entry
 */
/**
 * @ingroup Motion_Model
 * @defgroup sim_step_10_tt Process
 */
/**
 * @ingroup Motion_Model
 * @defgroup sim_step_20_tt Output
 */

#include "motionmodel.h"

#include <memory>

#include <QString>
#include <qglobal.h>

#include "common/commonTools.h"
#include "components/common/vehicleProperties.h"

Dynamics_MotionModel_Implementation::Dynamics_MotionModel_Implementation(std::string componentName,
                                                                         bool isInit,
                                                                         int priority,
                                                                         int offsetTime,
                                                                         int responseTime,
                                                                         int cycleTime,
                                                                         StochasticsInterface *stochastics,
                                                                         WorldInterface *world,
                                                                         const ParameterInterface *parameters,
                                                                         PublisherInterface *const publisher,
                                                                         const CallbackInterface *callbacks,
                                                                         AgentInterface *agent)
    : UnrestrictedModelInterface(componentName,
                                 isInit,
                                 priority,
                                 offsetTime,
                                 responseTime,
                                 cycleTime,
                                 stochastics,
                                 world,
                                 parameters,
                                 publisher,
                                 callbacks,
                                 agent)
{
  LOGINFO(
      QString().sprintf("Constructing Dynamics_MotionModel_TwoTrack for agent %d...", agent->GetId()).toStdString());

  timeStep = (double)GetCycleTime() / 1000.0;
  mCycle = timeStep;

  vehicle = new TwoTrackVehicleModel();

  vehicle->SetYawVelocity(0.0_rad_per_s);
  vehicle->SetYawAcceleration(0.0_rad_per_s_sq);

  std::shared_ptr<const mantle_api::VehicleProperties> vehicleProperties
      = std::dynamic_pointer_cast<const mantle_api::VehicleProperties>(GetAgent()->GetVehicleModelParameters());

  auto weight = GetAgent()->GetVehicleModelParameters()->mass;

  auto coeffDrag = helper::map::query(vehicleProperties->properties, vehicle::properties::AirDragCoefficient);
  THROWIFFALSE(coeffDrag.has_value(), "AirDragCoefficient was not defined in VehicleCatalog");

  auto areaFace = helper::map::query(vehicleProperties->properties, vehicle::properties::FrontSurface);
  THROWIFFALSE(areaFace.has_value(), "FrontSurface was not defined in VehicleCatalog");

  units::length::meter_t XCOG, YCOG;

  try
  {
    XCOG = std::stod(helper::map::query(vehicleProperties->properties, "XPositionCOG").value()) * 1_m;
  }
  catch (...)
  {
    XCOG = units::math::abs(vehicleProperties->rear_axle.bb_center_to_axle_center.x) / 2;
  }
  try
  {
    YCOG = std::stod(helper::map::query(vehicleProperties->properties, "YPositionCOG").value()) * 1_m;
  }
  catch (...)
  {
    YCOG = 0_m;
  }

  // get front & rear axle of vehicle
  std::vector<mantle_api::Axle> axles;
  axles.resize(2);
  axles[0] = vehicleProperties->front_axle;
  axles[1] = vehicleProperties->rear_axle;

  units::length::meter_t bb_center = units::math::abs(vehicleProperties->bounding_box.geometric_center.x);

  /** @addtogroup init_tt
   *   Init vehicle's properties:
   *  - set the longitudinal position of the COG
   *  - set the wheelbase
   *  - set the track width
   *  - set vehicle mass
   *  - set coefficient of drag
   *  - set front surface of vehicle
   */
  vehicle->InitVehicleProperties(
      axles, XCOG, YCOG, weight, std::stod(coeffDrag.value()), std::stod(areaFace.value()) * 1_sq_m, bb_center);
  ReadPreviousState();
  LOGINFO("Constructing Dynamics_MotionModel successful");
}

Dynamics_MotionModel_Implementation::~Dynamics_MotionModel_Implementation()
{
  delete (vehicle);
}

void Dynamics_MotionModel_Implementation::UpdateInput(int localLinkId,
                                                      const std::shared_ptr<SignalInterface const> &data,
                                                      int time)
{
  Q_UNUSED(time);

  std::stringstream log;
  log << COMPONENTNAME << " UpdateInput";
  LOG(CbkLogLevel::Debug, log.str());
  log.str(std::string());

  bool success = inputPorts.at(localLinkId)->SetSignalValue(data);

  if (success)
  {
    log << COMPONENTNAME << " UpdateInput successful";
    LOG(CbkLogLevel::Debug, log.str());
  }
  else
  {
    log << COMPONENTNAME << " UpdateInput failed";
    LOG(CbkLogLevel::Error, log.str());
  }
}

void Dynamics_MotionModel_Implementation::UpdateOutput(int localLinkId,
                                                       std::shared_ptr<SignalInterface const> &data,
                                                       int time)
{
  Q_UNUSED(time);

  std::stringstream log;
  log << COMPONENTNAME << " UpdateOutput";
  LOG(CbkLogLevel::Debug, log.str());
  log.str(std::string());

  bool success = outputPorts.at(localLinkId)->GetSignalValue(data);

  if (success)
  {
    log << COMPONENTNAME << " UpdateOutput successful";
    LOG(CbkLogLevel::Debug, log.str());
  }
  else
  {
    log << COMPONENTNAME << " UpdateOutput failed";
    LOG(CbkLogLevel::Error, log.str());
  }
}

void Dynamics_MotionModel_Implementation::Trigger(int time)
{
  timeClock = (double)time;

  /** @addtogroup sim_step_00_tt
   * Update vehicle's input data:
   *  - tire angle
   *  - tire forces
   */
  vehicle->SetTireAngle(wheelAngle.GetValue());
  vehicle->SetTireForce(longitudinalTireForces.GetValue(), lateralTireForces.GetValue());
  vehicle->SetTireSelfAligningTorque(wheelSelfAligningTorque.GetValue());

  /** @addtogroup sim_step_10_tt
   * Combine local tire forces to a global force at the vehicle's body:
   *  - total longitudinal force
   *  - total lateral force
   *  - air drag
   *  - total rotational momentum
   */
  vehicle->ForceGlobal();

  /** @addtogroup sim_step_20_tt
   * Perform a translational Euler step:
   *  - calculate next vehicle's position from prevoius velocity values
   *  - calculate new velocity from previous acceleration values
   *  - calculate new acceleration from global forces
   */
  NextStateTranslation();

  /** @addtogroup sim_step_20_tt
   * Perform a rotational Euler step:
   *  - calculate vehicle's orientation from previous rotational velocity
   *  - calculate vehicle's rotational velocity from previous rotational acceleration
   *  - calculate vehicle's rotational acceleration from the total rotational momentum
   */
  NextStateRotation();
  /** @addtogroup sim_step_20_tt
   * Write actual vehicle's state:
   *  - global position (cartesian coordinates)
   *  - direction of vehicle's longitudinal axes (angle in polar coordinates)
   *  - vehicle's longitudinal and lateral velocity in vehicle's CS
   *  - vehicle's rotational velociy
   *  - vehicle's longitudinal and lateral acceleration in vehicle's CS
   *  - vehicle's rotational acceleration
   *  - inertia forces on vehicle's COG
   */

  NextStateSet();
}

void Dynamics_MotionModel_Implementation::ReadPreviousState()
{
  vehicle->SetYawAngle(GetAgent()->GetYaw());

  Common::Vector2d<units::length::meter_t> positionCar;
  Common::Vector2d<units::velocity::meters_per_second_t> velocityCar;
  Common::Vector2d<units::acceleration::meters_per_second_squared_t> accelerationCar;

  positionCar.x = GetAgent()->GetPositionX()
                + ((vehicle->GetPositionCOG().x) * units::math::cos(vehicle->GetYawAngle())
                   - (vehicle->GetPositionCOG().y)
                         * units::math::sin(vehicle->GetYawAngle()));  // position of vehicle'S COG in global CS
  positionCar.y = GetAgent()->GetPositionY()
                + ((vehicle->GetPositionCOG().x) * units::math::sin(vehicle->GetYawAngle())
                   + (vehicle->GetPositionCOG().y)
                         * units::math::cos(vehicle->GetYawAngle()));  // position of vehicle's COG in global CS

  vehicle->SetPosition(positionCar);

  // global CS
  vehicle->SetYawAcceleration(GetAgent()->GetYawAcceleration());
  vehicle->SetYawVelocity(GetAgent()->GetYawRate());

  velocityCar.x = GetAgent()->GetVelocity().x
                - 1_mps * vehicle->GetYawVelocity().value() * (vehicle->GetPositionCOG().y.value());  // global
  velocityCar.y = GetAgent()->GetVelocity().y
                + 1_mps * vehicle->GetYawVelocity().value() * (vehicle->GetPositionCOG().x.value());  // global
  velocityCar.Rotate(-vehicle->GetYawAngle());  // vehicle CS

  vehicle->SetVelocity(velocityCar);
  accelerationCar = GetAgent()->GetAcceleration();  // global

  accelerationCar.x = accelerationCar.x
                    + 1_mps_sq * (-vehicle->GetYawAcceleration().value() * (vehicle->GetPositionCOG().y.value()))
                    + 1_mps_sq
                          * (-vehicle->GetYawVelocity().value() * vehicle->GetYawVelocity().value()
                             * (vehicle->GetPositionCOG().x.value()));
  accelerationCar.y = accelerationCar.y
                    + 1_mps_sq * (vehicle->GetYawAcceleration().value() * (vehicle->GetPositionCOG().x.value()))
                    + 1_mps_sq
                          * (-vehicle->GetYawVelocity().value() * vehicle->GetYawVelocity().value()
                             * (vehicle->GetPositionCOG().y.value()));

  accelerationCar.Rotate(-vehicle->GetYawAngle());  // vehicle CS

  vehicle->SetAcceleration(accelerationCar);
  LOGDEBUG(QString()
               .sprintf("Prev Velocity for Dynamics_MotionModel_TwoTrack for agent %d: %f, %f, %f",
                        GetAgent()->GetId(),
                        vehicle->GetVelocity().x,
                        vehicle->GetVelocity().y,
                        vehicle->GetYawVelocity())
               .toStdString());
  LOGDEBUG(QString()
               .sprintf("Prev Acceleration for Dynamics_MotionModel_TwoTrack for agent %d: %f, %f, %f",
                        GetAgent()->GetId(),
                        vehicle->GetAcceleration().x,
                        vehicle->GetAcceleration().y,
                        vehicle->GetYawAcceleration())
               .toStdString());
}

void Dynamics_MotionModel_Implementation::NextStateTranslation()
{
  // update position COG (constant velocity step) in global COS
  Common::Vector2d<units::velocity::meters_per_second_t> velocity = vehicle->GetVelocity();
  velocity.Rotate(vehicle->GetYawAngle());
  vehicle->SetPosition({vehicle->GetPosition().x + velocity.x * (timeStep * 1_s),
                        vehicle->GetPosition().y + velocity.y * (timeStep * 1_s)});

  // update velocity
  Common::Vector2d<units::velocity::meters_per_second_t> velocityCarNew
      = {vehicle->GetVelocity().x + vehicle->GetAcceleration().x * timeStep * 1_s,
         vehicle->GetVelocity().y + vehicle->GetAcceleration().y * timeStep * 1_s};

  vehicle->SetVelocity(velocityCarNew);

  // update acceleration
  vehicle->SetAcceleration({vehicle->GetForceTotalXY().x * (1 / GetAgent()->GetVehicleModelParameters()->mass),
                            vehicle->GetForceTotalXY().y * (1 / GetAgent()->GetVehicleModelParameters()->mass)});
}

void Dynamics_MotionModel_Implementation::NextStateRotation()
{
  // preserve directions of velocity and acceleration
  Common::Vector2d velocity = vehicle->GetVelocity();
  Common::Vector2d acceleration = vehicle->GetAcceleration();
  velocity.Rotate(vehicle->GetYawAngle());
  acceleration.Rotate(vehicle->GetYawAngle());

  // update yaw angle
  vehicle->SetYawAngle(
      units::math::fmod(vehicle->GetYawAngle() + timeStep * 1_s * vehicle->GetYawVelocity(), 2 * M_PI * 1_rad));

  // update yaw velocity
  units::angular_velocity::radians_per_second_t yawVelocityNew
      = vehicle->GetYawVelocity() + vehicle->GetYawAcceleration() * timeStep * 1_s;
  vehicle->SetYawVelocity(yawVelocityNew);

  // update acceleration
  auto momentInertiaYaw = helper::map::query(GetAgent()->GetVehicleModelParameters()->properties, "MomentInertiaYaw");
  THROWIFFALSE(momentInertiaYaw.has_value(), "MomentInertiaYaw was not defined in VehicleCatalog");

  vehicle->SetYawAcceleration(1_rad_per_s_sq * vehicle->GetMomentZ().value() / std::stod(momentInertiaYaw.value()));

  // reassign directions of velocity and acceleration
  velocity.Rotate(-vehicle->GetYawAngle());
  acceleration.Rotate(-vehicle->GetYawAngle());

  vehicle->SetVelocity(velocity);
  vehicle->SetAcceleration(acceleration);
}

void Dynamics_MotionModel_Implementation::NextStateSet()
{
  // update Position transformation COG-> middle rear axle
  GetAgent()->SetPositionX(vehicle->GetPosition().x
                           + (-vehicle->GetPositionCOG().x) * units::math::cos(vehicle->GetYawAngle())
                           - (-vehicle->GetPositionCOG().y) * units::math::sin(vehicle->GetYawAngle()));
  GetAgent()->SetPositionY(vehicle->GetPosition().y
                           + (-vehicle->GetPositionCOG().x) * units::math::sin(vehicle->GetYawAngle())
                           + (-vehicle->GetPositionCOG().y) * units::math::cos(vehicle->GetYawAngle()));

  GetAgent()->SetYaw(vehicle->GetYawAngle());

  // update global velocity
  Common::Vector2d<units::velocity::meters_per_second_t> velocityCar = vehicle->GetVelocity();
  velocityCar.Rotate(vehicle->GetYawAngle());  // global CS Reference COG

  // Transformation COG -> middle rear Axle
  GetAgent()->SetVelocityVector(
      {velocityCar.x - vehicle->GetYawVelocity().value() * (-vehicle->GetPositionCOG().y.value() * 1_mps),
       velocityCar.y + vehicle->GetYawVelocity().value() * (-vehicle->GetPositionCOG().x.value() * 1_mps),
       0.0_mps});
  velocityCar.Rotate(-vehicle->GetYawAngle());  // vehicle CS

  // update yaw
  GetAgent()->SetYawRate(vehicle->GetYawVelocity());
  GetAgent()->SetYawAcceleration(vehicle->GetYawAcceleration());

  // update acceleration

  Common::Vector2d acceleration = vehicle->GetAcceleration();
  acceleration.Rotate(vehicle->GetYawAngle());
  GetAgent()->SetAccelerationVector(
      {acceleration.x + 1_mps_sq * (-vehicle->GetYawAcceleration().value() * (-vehicle->GetPositionCOG().y.value()))
           + 1_mps_sq
                 * (-vehicle->GetYawVelocity().value() * vehicle->GetYawVelocity().value()
                    * (-vehicle->GetPositionCOG().x.value())),
       acceleration.y + 1_mps_sq * (vehicle->GetYawAcceleration().value() * (-vehicle->GetPositionCOG().x.value()))
           + 1_mps_sq
                 * (-vehicle->GetYawVelocity().value() * vehicle->GetYawVelocity().value()
                    * (-vehicle->GetPositionCOG().y.value())),
       0.0_mps_sq});

  // update outputs
  std::vector<double> forceInert = {-vehicle->GetForceTotalXY().x.value(), -vehicle->GetForceTotalXY().y.value()};
  forceGlobalInertia.SetValue(forceInert);

  LOGDEBUG(QString()
               .sprintf("Setting Acceleration by Dynamics_MotionModel_TwoTrack for agent %d: %f, %f, %f",
                        GetAgent()->GetId(),
                        vehicle->GetAcceleration().x,
                        vehicle->GetAcceleration().y,
                        vehicle->GetYawAcceleration())
               .toStdString());
}
