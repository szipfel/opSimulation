/********************************************************************************
 * Copyright (c) 2020-2021 ITK Engineering GmbH
 *               2023 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#ifndef DYNAMICS_MOTIONMODEL_IMPLEMENTATION_H
#define DYNAMICS_MOTIONMODEL_IMPLEMENTATION_H

#include "common/componentPorts.h"
#include "common/primitiveSignals.h"
#include "common/vector2d.h"
#include "common/vectorSignals.h"
#include "include/modelInterface.h"
#include "include/parameterInterface.h"
#include "twotrackmodel.h"

using namespace Common;

/**
 * @{
 * \addtogroup Vehicle_Dynamics
 *
 * @{
 * \addtogroup Motion_Model
 * \brief Dynamic component to model the dynamic of a two track vehicle.
 *
 * \details open-loop two-track model.
 *
 *
 * \section inputs Inputs
 * name | meaning
 * -----|---------
 * longitudinalTireForces | longitudinal tire forces from tire model
 * lateralTireForces | lateral tire forces from tire model
 * wheelAngle | tire angle from steering system
 * wheelSelfAligningTorque | self aligning torque of each tire from tire model
 *
 *
 *
 *
 * @}
 * @}
 */

/*!
 * \copydoc Motion_Model
 * \ingroup Motion_Model
 */
class Dynamics_MotionModel_Implementation : public UnrestrictedModelInterface
{
public:
    const std::string COMPONENTNAME = "Dynamics_MotionModel_TwoTrack";

    //! Constructor
    //!
    //! @param[in]     componentName  Name of the component
    //! @param[in]     isInit         Corresponds to "init" of "Component"
    //! @param[in]     priority       Corresponds to "priority" of "Component"
    //! @param[in]     offsetTime     Corresponds to "offsetTime" of "Component"
    //! @param[in]     responseTime   Corresponds to "responseTime" of "Component"
    //! @param[in]     cycleTime      Corresponds to "cycleTime" of "Component"
    //! @param[in]     stochastics    Pointer to the stochastics class loaded by the framework
    //! @param[in]     parameters     Pointer to the parameters of the module
    //! @param[in]     publisher      Pointer to the publisher instance
    //! @param[in]     callbacks      Pointer to the callbacks
    //! @param[in]     agent          Pointer to agent instance
    Dynamics_MotionModel_Implementation(
        std::string componentName,
        bool isInit,
        int priority,
        int offsetTime,
        int responseTime,
        int cycleTime,
        StochasticsInterface *stochastics,
        WorldInterface *world,
        const ParameterInterface *parameters,
        PublisherInterface *const publisher,
        const CallbackInterface *callbacks,
        AgentInterface *agent);
    Dynamics_MotionModel_Implementation(const Dynamics_MotionModel_Implementation &) = delete;
    Dynamics_MotionModel_Implementation(Dynamics_MotionModel_Implementation &&) = delete;
    Dynamics_MotionModel_Implementation &operator=(const Dynamics_MotionModel_Implementation &) = delete;
    Dynamics_MotionModel_Implementation &operator=(Dynamics_MotionModel_Implementation &&) = delete;

    //! Destructor
    ~Dynamics_MotionModel_Implementation();

    //! Function is called by framework when another component delivers a signal over
    //! a channel to this component (scheduler calls update taks of other component).
    //!
    //! @param[in]     localLinkId    Corresponds to "id" of "ComponentInput"
    //! @param[in]     data           Referenced signal (copied by sending component)
    //! @param[in]     time           Current scheduling time
    //! @return                       True on success
    void UpdateInput(int localLinkId, const std::shared_ptr<SignalInterface const> &data, int time);

    //! Function is called by framework when this component has to deliver a signal over
    //! a channel to another component (scheduler calls update task of this component).
    //!
    //! @param[in]     localLinkId    Corresponds to "id" of "ComponentOutput"
    //! @param[out]    data           Referenced signal (copied by this component)
    //! @param[in]     time           Current scheduling time
    //! @return                       True on success
    void UpdateOutput(int localLinkId, std::shared_ptr<SignalInterface const> &data, int time);

    //! Function is called by framework when the scheduler calls the trigger task
    //! of this component
    //!
    //! @param[in]     time           Current scheduling time
    //! @return                       True on success
    void Trigger(int time);

private:
    /** \addtogroup Motion_Model
     *  @{
     *      \name InputPorts
     *      All input ports with PortId
     *      @{
     */
    std::map<int, ComponentPort *> inputPorts;                                                  //!< map for all InputPort
    InputPort<SignalVectorDouble, std::vector<double>> longitudinalTireForces{0, &inputPorts};  //!< wheel drive torque
    InputPort<SignalVectorDouble, std::vector<double>> lateralTireForces{1, &inputPorts};       //!< Wheel brake torque
    InputPort<SignalVectorDouble, std::vector<double>> wheelAngle{2, &inputPorts};              //!< Steering Angle of the wheels
    InputPort<SignalVectorDouble, std::vector<double>> wheelSelfAligningTorque{3, &inputPorts}; //!< Wheel self aligning torque
    /**
     *      @}
     *  @}
     */

    /** \addtogroup Motion_Model
     *  @{
     *      \name OutputPorts
     *      All output ports with PortId
     *      @{
     */
    std::map<int, ComponentPort *> outputPorts;                                              //!< map for all OutputPort
    OutputPort<SignalVectorDouble, std::vector<double>> forceGlobalInertia{0, &outputPorts}; //!< inertia force on vehicle's Center of Gravity (Input for Dynamics Chassis)
                                                                                             /**
                                                                                              *      @}
                                                                                              *  @}
                                                                                              */

    /** \addtogroup Motion_Model
     *  @{
     *    \name Internal states
     *    @{
     */
    //! Time step as double in s
    double timeStep;
    //! Actual time double in s
    double timeClock;
    //! Cycle time
    double mCycle;
    /**
     *    @}
     *  @}
     */

    /** \addtogroup MotionModel
     *  @{
     *    \name Internal objects
     *    @{
     */
    //! two tracck vehicle
    TwoTrackVehicleModel *vehicle;
    /**
     *    @}
     *  @}
     */

    //! Update data on agent's actual position, velocity and acceleration
    void ReadPreviousState();

    //! Calculate next position, translation velocity and translation acceleration of the agent
    void NextStateTranslation();

    //! Calculate next yaw angle, rotation velocity and rotation acceleration of the agent
    void NextStateRotation();

    //! Write next position, velocity and acceleration of the agent
    void NextStateSet();
};

#endif // DYNAMICS_TWOTRACK_IMPLEMENTATION_H
