/********************************************************************************
 * Copyright (c) 2018-2019 AMFD GmbH
 *               2020 HLRS, University of Stuttgart
 *               2018-2020 in-tech GmbH
 *               2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//-----------------------------------------------------------------------------
//! @file  regularDriving.cpp
//! @brief This file contains the implementation header file
//-----------------------------------------------------------------------------

#include "regularDriving.h"

#include <algorithm>
#include <cassert>
#include <memory>

#include <QtGlobal>

#include "common/accelerationSignal.h"
#include "common/commonTools.h"
#include "common/globalDefinitions.h"
#include "common/longitudinalSignal.h"
#include "common/opMath.h"
#include "common/parametersVehicleSignal.h"
#include "common/rollSignal.h"
#include "common/steeringSignal.h"
#include "components/common/vehicleProperties.h"
#include "include/worldInterface.h"

void DynamicsRegularDrivingImplementation::UpdateInput(int localLinkId,
                                                       const std::shared_ptr<SignalInterface const> &data,
                                                       int time)
{
  Q_UNUSED(time);

  if (localLinkId == 0)
  {
    const std::shared_ptr<ComponentStateSignalInterface const> stateSignal
        = std::dynamic_pointer_cast<ComponentStateSignalInterface const>(data);
    if (stateSignal->componentState == ComponentState::Acting)
    {
      const std::shared_ptr<LongitudinalSignal const> signal
          = std::dynamic_pointer_cast<LongitudinalSignal const>(data);
      if (!signal)
      {
        const std::string msg = COMPONENTNAME + " invalid signaltype";
        LOG(CbkLogLevel::Debug, msg);
        throw std::runtime_error(msg);
      }
      in_accPedalPos = signal->accPedalPos;
      in_brakePedalPos = signal->brakePedalPos;
      ApplyPedalPositionLimits();
      in_gear = signal->gear;
      ApplyGearLimit();
      dynamicsSignal.longitudinalController = signal->source;
    }
  }
  else if (localLinkId == 1)
  {
    const std::shared_ptr<ComponentStateSignalInterface const> stateSignal
        = std::dynamic_pointer_cast<ComponentStateSignalInterface const>(data);
    if (stateSignal->componentState == ComponentState::Acting)
    {
      const std::shared_ptr<SteeringSignal const> signal = std::dynamic_pointer_cast<SteeringSignal const>(data);
      if (!signal)
      {
        const std::string msg = COMPONENTNAME + " invalid signaltype";
        LOG(CbkLogLevel::Debug, msg);
        throw std::runtime_error(msg);
      }
      in_steeringWheelAngle = signal->steeringWheelAngle;
      dynamicsSignal.lateralController = signal->source;
    }
  }
  else if (localLinkId == 2)
  {
    const std::shared_ptr<ComponentStateSignalInterface const> stateSignal
        = std::dynamic_pointer_cast<ComponentStateSignalInterface const>(data);
    if (stateSignal->componentState == ComponentState::Acting)
    {
      const std::shared_ptr<RollSignal const> signal = std::dynamic_pointer_cast<RollSignal const>(data);
      if (!signal)
      {
        const std::string msg = COMPONENTNAME + " invalid signaltype";
        LOG(CbkLogLevel::Debug, msg);
        throw std::runtime_error(msg);
      }
      dynamicsSignal.dynamicsInformation.roll = signal->rollAngle;
    }
  }
  else if (localLinkId == 100)
  {
    // from ParametersAgent
    const std::shared_ptr<ParametersVehicleSignal const> signal
        = std::dynamic_pointer_cast<ParametersVehicleSignal const>(data);
    if (!signal)
    {
      const std::string msg = COMPONENTNAME + " invalid signaltype";
      LOG(CbkLogLevel::Debug, msg);
      throw std::runtime_error(msg);
    }

    vehicleModelParameters = signal->vehicleParameters;
  }
  else
  {
    const std::string msg = COMPONENTNAME + " invalid link";
    LOG(CbkLogLevel::Debug, msg);
    throw std::runtime_error(msg);
  }
}

void DynamicsRegularDrivingImplementation::UpdateOutput(int localLinkId,
                                                        std::shared_ptr<SignalInterface const> &data,
                                                        int time)
{
  Q_UNUSED(time);

  if (localLinkId == 0)
  {
    try
    {
      data = std::make_shared<DynamicsSignal const>(dynamicsSignal);
    }
    catch (const std::bad_alloc &)
    {
      const std::string msg = COMPONENTNAME + " could not instantiate signal";
      LOG(CbkLogLevel::Debug, msg);
      throw std::runtime_error(msg);
    }
  }
  else
  {
    const std::string msg = COMPONENTNAME + " invalid link";
    LOG(CbkLogLevel::Debug, msg);
    throw std::runtime_error(msg);
  }
}

void DynamicsRegularDrivingImplementation::ApplyGearLimit()
{
  in_gear = std::min(std::max(in_gear, 1), static_cast<int>(GetVehicleProperty(vehicle::properties::NumberOfGears)));
}

void DynamicsRegularDrivingImplementation::ApplyPedalPositionLimits()
{
  in_accPedalPos = std::min(std::max(in_accPedalPos, 0.0), 1.0);
  in_brakePedalPos = std::min(std::max(in_brakePedalPos, 0.0), 1.0);
}

units::angular_velocity::revolutions_per_minute_t DynamicsRegularDrivingImplementation::GetEngineSpeedByVelocity(
    units::velocity::meters_per_second_t xVel, int gear)
{
  return 1_rad
       * (xVel * GetVehicleProperty(vehicle::properties::AxleRatio)
          * GetVehicleProperty(vehicle::properties::GearRatio + std::to_string(gear)))
       / (vehicleModelParameters.rear_axle.wheel_diameter
          * 0.5);  // an dynamic wheel radius rDyn must actually be used here
}

units::torque::newton_meter_t DynamicsRegularDrivingImplementation::GetEngineMomentMax(
    units::angular_velocity::revolutions_per_minute_t engineSpeed)
{
  const units::torque::newton_meter_t maximumEngineTorque{GetVehicleProperty(vehicle::properties::MaximumEngineTorque)};
  const units::angular_velocity::revolutions_per_minute_t maximumEngineSpeed{
      GetVehicleProperty(vehicle::properties::MaximumEngineSpeed)};
  const units::angular_velocity::revolutions_per_minute_t minimumEngineSpeed{
      GetVehicleProperty(vehicle::properties::MinimumEngineSpeed)};

  auto torqueMax = maximumEngineTorque;  // initial value at max
  auto speed = engineSpeed;

  bool isLowerSection = engineSpeed < minimumEngineSpeed + 1000.0_rpm;
  bool isBeyondLowerSectionBorder = engineSpeed < minimumEngineSpeed;
  bool isUpperSection = engineSpeed > maximumEngineSpeed - 1000.0_rpm;
  bool isBeyondUpperSectionBorder = engineSpeed > maximumEngineSpeed;

  if (isLowerSection)
  {
    if (isBeyondLowerSectionBorder)  // not within limits
    {
      speed = minimumEngineSpeed;
    }
    torqueMax = units::inverse_radian(0.5 / M_PI) * (1000_rpm - (speed - minimumEngineSpeed))
                  * units::unit_t<units::compound_unit<units::torque::newton_meter, units::time::minute>>(-0.1)
              + maximumEngineTorque;
  }
  else if (isUpperSection)
  {
    if (isBeyondUpperSectionBorder)
    {
      speed = maximumEngineSpeed;
    }

    torqueMax = units::inverse_radian(0.5 / M_PI) * (speed - maximumEngineSpeed + 1000_rpm)
                  * units::unit_t<units::compound_unit<units::torque::newton_meter, units::time::minute>>(-0.04)
              + maximumEngineTorque;
  }
  return torqueMax;
}

units::acceleration::meters_per_second_squared_t
DynamicsRegularDrivingImplementation::GetAccelerationFromRollingResistance()
{
  double rollingResistanceCoeff
      = .0125;  // Dummy value, get via vehicle Parameters (vehicleModelParameters.rollingDragCoefficient)
  const units::acceleration::meters_per_second_squared_t accDueToRollingResistance = -rollingResistanceCoeff * _oneG;
  return accDueToRollingResistance;
}

units::acceleration::meters_per_second_squared_t DynamicsRegularDrivingImplementation::GetAccelerationFromAirResistance(
    units::velocity::meters_per_second_t velocity)
{
  units::force::newton_t forceAirResistance
      = -.5 * _rho * GetVehicleProperty(vehicle::properties::AirDragCoefficient)
      * units::area::square_meter_t(GetVehicleProperty(vehicle::properties::FrontSurface)) * velocity * velocity;
  const units::acceleration::meters_per_second_squared_t accDueToAirResistance
      = forceAirResistance / GetAgent()->GetVehicleModelParameters()->mass;
  return accDueToAirResistance;
}

units::torque::newton_meter_t DynamicsRegularDrivingImplementation::GetEngineMomentMin(
    units::angular_velocity::revolutions_per_minute_t engineSpeed)
{
  return GetEngineMomentMax(engineSpeed) * -.1;
}

double DynamicsRegularDrivingImplementation::GetFrictionCoefficient()
{
  return GetWorld()->GetFriction() * GetVehicleProperty(vehicle::properties::FrictionCoefficient);
}

double DynamicsRegularDrivingImplementation::GetVehicleProperty(const std::string &propertyName)
{
  const auto property = helper::map::query(vehicleModelParameters.properties, propertyName);
  THROWIFFALSE(property.has_value(), "Vehicle property \"" + propertyName + "\" was not set in the VehicleCatalog");
  return std::stod(property.value());
}

units::torque::newton_meter_t DynamicsRegularDrivingImplementation::GetEngineMoment(double gasPedalPos, int gear)
{
  const auto xVel = GetAgent()->GetVelocity().Length();

  const auto engineSpeedAtGear = GetEngineSpeedByVelocity(xVel, gear);

  GetAgent()->SetEngineSpeed(engineSpeedAtGear);

  const auto max = GetEngineMomentMax(engineSpeedAtGear);
  const auto maxAccAtGear = GetAccFromEngineMoment(xVel, max, gear, GetCycleTime());

  GetAgent()->SetMaxAcceleration(maxAccAtGear * GetFrictionCoefficient());

  const auto min = GetEngineMomentMin(engineSpeedAtGear);

  return (units::math::fabs(min) + max) * gasPedalPos + min;
}

units::acceleration::meters_per_second_squared_t DynamicsRegularDrivingImplementation::GetAccFromEngineMoment(
    units::velocity::meters_per_second_t xVel,
    units::torque::newton_meter_t engineMoment,
    int chosenGear,
    int cycleTime)
{
  Q_UNUSED(xVel);
  Q_UNUSED(cycleTime);

  units::torque::newton_meter_t wheelSetMoment
      = engineMoment
      * (GetVehicleProperty(vehicle::properties::AxleRatio)
         * GetVehicleProperty(vehicle::properties::GearRatio + std::to_string(chosenGear)));
  units::force::newton_t wheelSetForce = wheelSetMoment / (0.5 * vehicleModelParameters.rear_axle.wheel_diameter);

  const auto vehicleSetForce = wheelSetForce;
  const units::acceleration::meters_per_second_squared_t acc
      = vehicleSetForce / GetAgent()->GetVehicleModelParameters()->mass;

  return acc;
}

units::acceleration::meters_per_second_squared_t DynamicsRegularDrivingImplementation::GetAccVehicle(
    double accPedalPos, double brakePedalPos, int gear)
{
  units::acceleration::meters_per_second_squared_t resultAcc{0};

  const auto xVel = GetAgent()->GetVelocity().Length();

  if (brakePedalPos > 0.)  // Brake
  {
    units::acceleration::meters_per_second_squared_t accelerationDueToPedal{brakePedalPos * _oneG * -1.};
    units::angular_velocity::revolutions_per_minute_t engineSpeed{GetEngineSpeedByVelocity(xVel, gear)};
    auto engineDrag{GetEngineMomentMin(engineSpeed)};
    auto accelerationDueToDrag = GetAccFromEngineMoment(xVel, engineDrag, gear, GetCycleTime());
    if (accelerationDueToPedal > 0.0_mps_sq || accelerationDueToDrag > 0.0_mps_sq)
    {
      throw std::runtime_error("DynamicsRegularDrivingImplementation - Wrong sign for acceleration!");
    }

    resultAcc = accelerationDueToPedal + accelerationDueToDrag;

    const auto maxDecel = GetAgent()->GetMaxDeceleration();
    resultAcc = units::math::fmax(maxDecel, resultAcc);
  }
  else  // Gas
  {
    const auto engineMoment = GetEngineMoment(accPedalPos, gear);
    GetPublisher()->Publish("EngineMoment", engineMoment.value());
    resultAcc = GetAccFromEngineMoment(xVel, engineMoment, gear, GetCycleTime());
  }

  const auto accelerationDueToAirResistance = GetAccelerationFromAirResistance(xVel);
  const auto accelerationDueToRollingResistance = GetAccelerationFromRollingResistance();

  return resultAcc + accelerationDueToAirResistance + accelerationDueToRollingResistance;
}

void DynamicsRegularDrivingImplementation::Trigger(int time)
{
  Q_UNUSED(time);

  const auto agent = GetAgent();

  //Lateral behavior
  const units::acceleration::meters_per_second_squared_t maxDecel = _oneG * GetFrictionCoefficient() * -1;
  agent->SetMaxDeceleration(maxDecel);

  units::velocity::meters_per_second_t v;
  const auto yawAngle = agent->GetYaw();

  auto accVehicle = GetAccVehicle(in_accPedalPos, in_brakePedalPos, in_gear);

  v = agent->GetVelocity().Length() + accVehicle * units::time::millisecond_t(GetCycleTime());

  if (v < VLowerLimit)
  {
    accVehicle = 0.0_mps_sq;
    v = VLowerLimit;
  }

  // change of path coordinate since last time step [m]
  const units::length::meter_t ds = v * units::time::millisecond_t(GetCycleTime());
  // change of inertial x-position due to ds and yaw [m]
  const units::length::meter_t dx = ds * units::math::cos(yawAngle);
  // change of inertial y-position due to ds and yaw [m]
  const units::length::meter_t dy = ds * units::math::sin(yawAngle);
  // new inertial x-position [m]
  auto x = agent->GetPositionX() + dx;
  // new inertial y-position [m]
  auto y = agent->GetPositionY() + dy;

  dynamicsSignal.dynamicsInformation.acceleration = accVehicle;
  dynamicsSignal.dynamicsInformation.velocityX = v * units::math::cos(yawAngle);
  dynamicsSignal.dynamicsInformation.velocityY = v * units::math::sin(yawAngle);
  dynamicsSignal.dynamicsInformation.positionX = x;
  dynamicsSignal.dynamicsInformation.positionY = y;
  dynamicsSignal.dynamicsInformation.travelDistance = ds;

  // convert steering wheel angle to steering angle of front wheels [radian]
  const auto steering_angle = std::clamp(in_steeringWheelAngle / GetVehicleProperty(vehicle::properties::SteeringRatio),
                                         -vehicleModelParameters.front_axle.max_steering,
                                         vehicleModelParameters.front_axle.max_steering);
  dynamicsSignal.dynamicsInformation.steeringWheelAngle
      = steering_angle * GetVehicleProperty(vehicle::properties::SteeringRatio);
  GetPublisher()->Publish("SteeringAngle", steering_angle.value());
  const auto wheelbase = vehicleModelParameters.front_axle.bb_center_to_axle_center.x
                       - vehicleModelParameters.rear_axle.bb_center_to_axle_center.x;
  // calculate curvature (Ackermann model; reference point of yawing = rear axle!) [radian]
  units::curvature::inverse_meter_t steeringCurvature = units::math::tan(steering_angle) / wheelbase;
  // change of yaw angle due to ds and curvature [radian]
  units::angle::radian_t dpsi{units::math::atan(steeringCurvature * ds)};
  dynamicsSignal.dynamicsInformation.yawRate = dpsi / units::time::millisecond_t(GetCycleTime());
  dynamicsSignal.dynamicsInformation.yawAcceleration
      = (dynamicsSignal.dynamicsInformation.yawRate - yawRatePrevious) / units::time::second_t(GetCycleTime() * 0.001);
  yawRatePrevious = dynamicsSignal.dynamicsInformation.yawRate;
  dynamicsSignal.dynamicsInformation.centripetalAcceleration
      = units::inverse_radian(1) * dynamicsSignal.dynamicsInformation.yawRate * v;
  // new yaw angle in current time step [radian]
  const auto psi = agent->GetYaw() + dpsi;
  dynamicsSignal.dynamicsInformation.yaw = psi;
}
