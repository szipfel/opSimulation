/********************************************************************************
 * Copyright (c) 2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** @file  dynamics_scenario.h
 *	@brief This file provides the exported methods.
 *
 *   This file provides the exported methods which are available outside of the library. */
//-----------------------------------------------------------------------------

#pragma once

#include <QtGlobal>

#if defined(DYNAMICS_SCENARIO_LIBRARY)
#define DYNAMICS_SCENARIO_SHARED_EXPORT Q_DECL_EXPORT  //!< Export of the dll-functions
#else
#define DYNAMICS_SCENARIO_SHARED_EXPORT Q_DECL_IMPORT  //!< Import of the dll-functions
#endif

#include "include/modelInterface.h"
