/********************************************************************************
 * Copyright (c) 2023 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  dynamics_tiremodel.h
//! @brief contains DLL export declarations
//-----------------------------------------------------------------------------

#include <QtCore/qglobal.h>

#include "include/modelInterface.h"

#if defined(DYNAMICS_TIREMODEL_LIBRARY)
#define DYNAMICS_TIREMODELSHARED_EXPORT Q_DECL_EXPORT
#else
#define DYNAMICS_TIREMODELSHARED_EXPORT Q_DECL_IMPORT
#endif
