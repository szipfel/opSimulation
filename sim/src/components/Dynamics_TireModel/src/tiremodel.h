/********************************************************************************
 * Copyright (c) 2023 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

/**
 * \addtogroup Vehicle_Dynamics
 *@{
 * \addtogroup Tire_Model
 * \brief Tire component to model the tire forces of a vehicle dynamics model.
 *
 * \details Tire Model of vehicle dynamics model. In this case, the TMEASY tire model is integrated
 *
 * @}
 */

#ifndef DYNAMICSTIREMODEL_H
#define DYNAMICSTIREMODEL_H

#include "common/componentPorts.h"
#include "common/primitiveSignals.h"
#include "common/vector2d.h"
#include "common/vectorSignals.h"
#include "include/modelInterface.h"
#include "include/parameterInterface.h"
#include "tmeasytire.h"

using namespace Common;

/*!
 * \copydoc Tire_Model
 * \ingroup Tire_Model
 */
class DynamicsTireModel : public UnrestrictedModelInterface
{
public:
  const std::string COMPONENTNAME = "Dynamics_TMeasyTireModel";

  //! Constructor
  //!
  //! @param[in]     componentName  Name of the component
  //! @param[in]     isInit         Corresponds to "init" of "Component"
  //! @param[in]     priority       Corresponds to "priority" of "Component"
  //! @param[in]     offsetTime     Corresponds to "offsetTime" of "Component"
  //! @param[in]     responseTime   Corresponds to "responseTime" of "Component"
  //! @param[in]     cycleTime      Corresponds to "cycleTime" of "Component"
  //! @param[in]     stochastics    Pointer to the stochastics class loaded by the framework
  //! @param[in]     parameters     Pointer to the parameters of the module
  //! @param[in]     publisher      Pointer to the publisher instance
  //! @param[in]     callbacks      Pointer to the callbacks
  //! @param[in]     agent          Pointer to agent instance
  DynamicsTireModel(std::string componentName,
                    bool isInit,
                    int priority,
                    int offsetTime,
                    int responseTime,
                    int cycleTime,
                    StochasticsInterface *stochastics,
                    WorldInterface *world,
                    const ParameterInterface *parameters,
                    PublisherInterface *const publisher,
                    const CallbackInterface *callbacks,
                    AgentInterface *agent);
  DynamicsTireModel(const DynamicsTireModel &) = delete;
  DynamicsTireModel(DynamicsTireModel &&) = delete;
  DynamicsTireModel &operator=(const DynamicsTireModel &) = delete;
  DynamicsTireModel &operator=(DynamicsTireModel &&) = delete;

  //! Destructor
  ~DynamicsTireModel();

  //! Function is called by framework when another component delivers a signal over
  //! a channel to this component (scheduler calls update taks of other component).
  //!
  //! @param[in]     localLinkId    Corresponds to "id" of "ComponentInput"
  //! @param[in]     data           Referenced signal (copied by sending component)
  //! @param[in]     time           Current scheduling time
  //! @return                       True on success
  void UpdateInput(int localLinkId, const std::shared_ptr<SignalInterface const> &data, int time);

  //! Function is called by framework when this component has to deliver a signal over
  //! a channel to another component (scheduler calls update task of this component).
  //!
  //! @param[in]     localLinkId    Corresponds to "id" of "ComponentOutput"
  //! @param[out]    data           Referenced signal (copied by this component)
  //! @param[in]     time           Current scheduling time
  //! @return                       True on success
  void UpdateOutput(int localLinkId, std::shared_ptr<SignalInterface const> &data, int time);

  //! Function is called by framework when the scheduler calls the trigger task
  //! of this component
  //!
  //! @param[in]     time           Current scheduling time
  //! @return                       True on success
  void Trigger(int time);

private:
  /** \addtogroup Tire_Model
   *  @{
   *      \name External Tire Parameter
   *      Parameter which are set externally in agentConfiguration file.
   *      @{
   */
  void ParseParameters(const ParameterInterface *parameters);
  std::vector<double> muTireMaxXFRef;   //!<
  std::vector<double> muTireMaxX2FRef;  //!<

  std::vector<double> muTireSlideXFRef;   //!<
  std::vector<double> muTireSlideX2FRef;  //!<

  std::vector<double> slipTireMaxXFRef;   //!<
  std::vector<double> slipTireMaxX2FRef;  //!<

  std::vector<double> slipTireSlideXFRef;   //!<
  std::vector<double> slipTireSlideX2FRef;  //!<

  std::vector<double> F0pXFRef;   //!<
  std::vector<double> F0pX2FRef;  //!<

  std::vector<double> muTireMaxYFRef;   //!<
  std::vector<double> muTireMaxY2FRef;  //!<

  std::vector<double> muTireSlideYFRef;   //!<
  std::vector<double> muTireSlideY2FRef;  //!<

  std::vector<double> slipTireMaxYFRef;   //!<
  std::vector<double> slipTireMaxY2FRef;  //!<

  std::vector<double> slipTireSlideYFRef;   //!<
  std::vector<double> slipTireSlideY2FRef;  //!<

  std::vector<double> F0pYFRef;   //!<
  std::vector<double> F0pY2FRef;  //!<

  std::vector<double> FRef;          //!<
  std::vector<bool> FRefNormalized;  //!<

  std::vector<double> inertia;         //!<
  std::vector<double> pneumaticTrail;  //!<
  /**
   *      @}
   *  @}
   */

  std::map<int, ComponentPort *> inputPorts;  //!< map for all InputPort
  /** \addtogroup Tire_Model
   *  @{
   *      \name InputPorts
   *      All input ports with PortId
   *      @{
   */

  InputPort<SignalVectorDouble, std::vector<double>> driveTorque{0, &inputPorts};  //!< DriveTorque of the wheels
  InputPort<SignalVectorDouble, std::vector<double>> brakeTorque{1, &inputPorts};  //!< BrakeTorque of the wheels
  InputPort<SignalVectorDouble, std::vector<double>> wheelAngle{2, &inputPorts};   //!< Steering Angle of the wheels
  InputPort<SignalVectorDouble, std::vector<double>> forceWheelVertical{3,
                                                                        &inputPorts};  //!< Vertical force on the wheels

  /**
   *      @}
   *  @}
   */

  std::map<int, ComponentPort *> outputPorts;  //!< map for all OutputPorts
  /** \addtogroup Tire_Model
   *  @{
   *      \name OutputPorts
   *      All output ports with PortId
   *      @{
   */
  OutputPort<SignalVectorDouble, std::vector<double>> out_longitudinalTireForce{
      0, &outputPorts};  //!< longitudinal tire force
  OutputPort<SignalVectorDouble, std::vector<double>> out_lateralTireForce{1, &outputPorts};  //!< lateral tire force
  OutputPort<SignalVectorDouble, std::vector<double>> out_selfAligningTorque{2,
                                                                             &outputPorts};    //!< self aligning torque
  OutputPort<SignalVectorDouble, std::vector<double>> out_wheelRotationRate{3, &outputPorts};  //!< wheelRotationRate
  /**
   *      @}
   *  @}
   */

  /** \addtogroup Tire_Model
   *  @{
   *    \name Internal states
   *    @{
   */
  //! Time step as double in s
  double timeStep;
  //! Actual time double in s
  double timeClock;

  //! CycleTime in ms
  int mCycle;
  //! Yaw angle
  units::angle::radian_t yawAngle;
  //! Yaw rate
  units::angular_velocity::radians_per_second_t yawVelocity;
  //! Car's velocity
  Common::Vector2d<units::velocity::meters_per_second_t> velocityCar;
  //! Yaw acceleration
  double yawAcceleration;
  //! Car's Position COG
  Common::Vector2d<units::length::meter_t> PositionCOG;
  /**
   *    @}
   *  @}
   */

  /** \addtogroup Tire_Model
   *  @{
   *    \name Internal objects
   *    @{
   */
  //! Tire vector
  std::vector<Tire *> tires;
  /**
   *    @}
   *  @}
   */

  //! read vehicle state
  void ReadPreviousState();
  //! calculate tire forces
  void TireForce();
  //! init tires
  void InitTires(Common::Vector2d<units::velocity::meters_per_second_t> velocityCar,
                 std::vector<double> mu_tire_maxX,
                 std::vector<double> mu_tire_maxY,
                 std::vector<double> mu_tire_slideX,
                 std::vector<double> mu_tire_slideY,
                 std::vector<double> s_maxX,
                 std::vector<double> s_maxY,
                 std::vector<double> mu_tire_maxX2,
                 std::vector<double> mu_tire_maxY2,
                 std::vector<double> mu_tire_slideX2,
                 std::vector<double> mu_tire_slideY2,
                 std::vector<double> s_maxX2,
                 std::vector<double> s_maxY2,
                 std::vector<double> F_ref,
                 std::vector<bool> F_ref_norm,
                 double mu_scale,
                 std::vector<double> s_slideX,
                 std::vector<double> s_slideY,
                 std::vector<double> F0pX,
                 std::vector<double> F0pY,
                 std::vector<double> s_slideX2,
                 std::vector<double> s_slideY2,
                 std::vector<double> F0pX2,
                 std::vector<double> F0pY2,
                 std::vector<mantle_api::Axle> axles,
                 std::vector<double> Inertia,
                 std::vector<double> PneumaticTrail,
                 std::vector<double> forceWheelVertical,
                 units::length::meter_t bb_center);
};

#endif  // DYNAMICSTIREMODEL_H
