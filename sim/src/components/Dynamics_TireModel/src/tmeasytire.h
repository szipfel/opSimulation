/********************************************************************************
 * Copyright (c) 2023 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#ifndef tmeasytire_H
#define tmeasytire_H

#include "common/commonTools.h"
#include "common/vector2d.h"
#include "units.h"

/**
 * \addtogroup Vehicle_Dynamics
 *@{
 * \addtogroup Tire_Model
 * @{
 * \addtogroup TMEasyTireModel
 * \brief TMEasy tire model
 *
 * \details TMEasy Tire Model of vehicle dynamics model. The tire model based on TMEASY by Rill et al.
 *
 * @}
 * @}
 */

/*!
 * \copydoc TMEasyTireModel
 * \ingroup TMEasyTireModel
 */
class Tire
{
public:
  Tire(const units::force::newton_t F_ref,
       const Common::Vector2d<double> mu_tire_max,
       const Common::Vector2d<double> mu_tire_slide,
       const Common::Vector2d<double> s_max,
       const Common::Vector2d<double> mu_tire_max2,
       const Common::Vector2d<double> mu_tire_slide2,
       const Common::Vector2d<double> s_max2,
       const units::length::meter_t r,
       const double mu_scale,
       const Common::Vector2d<double> s_slide,
       const Common::Vector2d<double> s_slide2,
       const Common::Vector2d<units::force::newton_t> F0p,
       const Common::Vector2d<units::force::newton_t> F0p2,
       const Common::Vector2d<units::length::meter_t> positiontire,
       const units::frequency::hertz_t rotationVelocity,
       const double Inertia,
       const units::length::meter_t PneumaticTrail);

  virtual ~Tire() = default;
  //! Calculation of tire Force (longitudinal & lateral) based on TMEasy tire model
  void CalcTireForce();
  //! Calculation of longitudinal tire slip depending on tire velocity and rotation velocity
  //!
  void CalcLongSlip();
  //! Calculation of lateral tire slip depending on tire velocity and rotation velocity
  //!
  void CalcLatSlip();
  //! Get tire roll friction
  //!
  //! @return                       tire roll friction
  units::force::newton_t GetRollFriction();
  //! Update tire parameters depending on the tire vertical force
  //!
  //! @param[in]     foreZ_update           Current vertical tire force
  void Rescale(const units::force::newton_t forceZ_update);
  //! Calculation of tire rotation acceleration dependung on tire torque and inertia
  //!
  void CalcRotAcc();
  //! Calculation of tire rotation speed depending on tire acceleration and timestep
  //!
  //! @param[in]     dt           integration timestep
  void CalcRotVel(const double dt);
  //! Calculate tire velocity in tire coordinate system depending on vehicle velocity, yaw velocity and tire position
  //!
  //! @param[in]     yawVelocity           car yaw velocity (center of gravity)
  //! @param[in]     velocityCar           car velocity (center of gravity)
  void CalcVelTire(const units::angular_velocity::radians_per_second_t yawVelocity,
                   const Common::Vector2d<units::velocity::meters_per_second_t> velocityCar);
  //! Calculation of tire self aligning torquen dependung on tire lateral force and pneumatic trail
  //!
  void CalcSelfAligningTorque();
  //! Get lateral tire force
  //!
  //! @return                       tire lateral force
  units::force::newton_t GetLateralForce();
  //! Get longitudinal tire force
  //!
  //! @return                       tire longitudinal force
  units::force::newton_t GetLongitudinalForce();
  //! Get tire radius
  //!
  //! @return                       tire radius
  units::length::meter_t GetTireRadius();
  //! Set tire torque
  //!
  //! @param[in]   torque torque on tire
  void SetTorque(units::torque::newton_meter_t torque);
  //! Get tire velocity
  //!
  //! @return                       tire velocity
  Common::Vector2d<units::velocity::meters_per_second_t> GetVelocityTire();
  //! Get tire rotation velocity
  //!
  //! @return                       tire rotation velocity
  units::frequency::hertz_t GetRotationVelocity();
  //! Set tire rotation velocity
  //!
  //! @param[in]  rotVelocity tire rotation velocity
  void SetRotationVelocity(units::frequency::hertz_t rotVelocity);
  //! Get tire rotational acceleration
  //!
  //! @return                       tire rotational acceleration
  double GetRotAcceleration();
  //! Set tire rotational acceleration
  //!
  //! @param[in]  rotAcceleration tire rotational acceleration
  void SetRotAcceleration(double rotAcceleration);
  //! Set tire angle
  //!
  //! @param[in]                       angle tire angle
  void SetTireAngle(units::angle::radian_t angle);
  //! Get tire self aligning torque
  //!
  //! @return                       tire self aligning torque
  units::torque::newton_meter_t GetSelfAligningTorque();

private:
  //! VerticalTireForce
  units::force::newton_t forceZ;

  /** \addtogroup TMEasyTireModel
   *  @{
   *      \name TireParameters
   *      TMEasy tire parameters
   *      @{
   */
  Common::Vector2d<double> slipPeak;                   //!< slip at maximum force at current vertical force
  Common::Vector2d<double> slipSat;                    //!< saturation slip at current vertical force
  Common::Vector2d<units::force::newton_t> forcePeak;  //!< maximum tire force [N] at current vertical force
  Common::Vector2d<units::force::newton_t> forceSat;   //!< saturation tire force [N] at current vertical force
  Common::Vector2d<double> slipSlide;                  //!< slide slip at current vertical force
  Common::Vector2d<units::force::newton_t> dF0P;       //!< initial slope at current vertical force

  Common::Vector2d<units::force::newton_t> dF0PRef;       //!< initial slope at reference vertical force
  Common::Vector2d<double> slipPeakRef;                   //!< slip at maximum force at reference vertical force
  Common::Vector2d<double> slipSatRef;                    //!< saturation slip at reference vertical force
  Common::Vector2d<units::force::newton_t> forcePeakRef;  //!< maximum tire force at reference vertical force
  Common::Vector2d<units::force::newton_t> forceSatRef;   //!< saturation tire force at reference vertical force
  Common::Vector2d<double> slipSlideRef;                  //!< slide slip at reference vertical force

  Common::Vector2d<units::force::newton_t> dF0P2Ref;       //!< initial slope at double reference vertical force
  Common::Vector2d<double> slipPeak2Ref;                   //!< slip at maximum force at double reference vertical force
  Common::Vector2d<double> slipSat2Ref;                    //!< saturation slip at double reference vertical force
  Common::Vector2d<units::force::newton_t> forcePeak2Ref;  //!< maximum tire force at double reference vertical force
  Common::Vector2d<units::force::newton_t> forceSat2Ref;   //!< saturation tire force at double reference vertical force
  Common::Vector2d<double> slipSlide2Ref;                  //!< slide slip at double reference vertical force
  /**
   *      @}
   *  @}
   */

  units::force::newton_t FRef;  //! tire reference vertical force

  units::length::meter_t radius;                                   //!< tire radius [m]
  double inertia;                                                  //!< tire inertia [kgm2]
  units::length::meter_t pneumaticTrail;                           //!< tire pneumatic trail [m]
  const double frictionRoll = 0.01;                                //!< tire friction roll coefficient
  const units::velocity::meters_per_second_t velocityLimit{0.27};  //!< tire velocity limit [m/s]

  units::frequency::hertz_t rotationVelocity;                           //!< tire rotation velocity [1/s]
  Common::Vector2d<units::velocity::meters_per_second_t> velocityTire;  //!< tire velocity [m/s]
  units::angle::radian_t angleTire;                                     //!< tire angle [rad]
  units::torque::newton_meter_t Torque;                                 //!< tire torque [Nm]

  Common::Vector2d<units::length::meter_t>
      positionTire;  //!< tire positions in car CS (Reference: Center of Gravity) [m]

  Common::Vector2d<units::force::newton_t> TireForce;  //!< tire force [N]

  units::torque::newton_meter_t SelfAligningTorque;  //!< tire self aligning torque due to pneumatic trail

  double rotationAcceleration;  //!< tire rotational acceleration

  Common::Vector2d<double> slipNorm;  //!< tire normalized slip
  Common::Vector2d<double> slipTire;  //!< tire slip
};

#endif  // TIRE_TMeasy_H
