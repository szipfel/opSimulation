/********************************************************************************
 * Copyright (c) 2018-2019 AMFD GmbH
 *               2019 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  sensorDriverSignal.h
//! @brief This file contains all functions for class
//! SensorDriverSignal
//!
//! This class contains all functionality of the signal.
//-----------------------------------------------------------------------------
#pragma once

#include <string>

#include "include/signalInterface.h"
#include "sensor_driverDefinitions.h"

/// Class representing sensor driver signal
class SensorDriverSignal : public SignalInterface
{
public:
  static constexpr char COMPONENTNAME[]{"SensorDriverHumanSignal"};  ///< Name of the component

  /**
   * @brief Construct a new Sensor Driver Signal object
   *
   * @param ownVehicleInformation     state of ego vehicle
   * @param trafficRuleInformation    traffic rules at ego position
   * @param geometryInformation       basic geometry information of adjacent lanes
   * @param surroundingObjects        objects around ego
   */
  SensorDriverSignal(OwnVehicleInformation ownVehicleInformation,
                     TrafficRuleInformation trafficRuleInformation,
                     GeometryInformation geometryInformation,
                     SurroundingObjects surroundingObjects)
      : ownVehicleInformation(ownVehicleInformation),
        trafficRuleInformation(trafficRuleInformation),
        geometryInformation(geometryInformation),
        surroundingObjects(surroundingObjects)
  {
  }

  SensorDriverSignal(const SensorDriverSignal&) = default;  ///< copy constructor
  SensorDriverSignal(SensorDriverSignal&&) = default;       ///< move constructor

  /// @return copy assignment operator
  SensorDriverSignal& operator=(const SensorDriverSignal&) = default;

  /// @return move assignment operator
  SensorDriverSignal& operator=(SensorDriverSignal&&) = default;

  virtual ~SensorDriverSignal() {}

  //-----------------------------------------------------------------------------
  //! Returns the content/payload of the signal as an std::string
  //!
  //! @return                       Content/payload of the signal as an std::string
  //-----------------------------------------------------------------------------
  virtual operator std::string() const { return COMPONENTNAME; }

  /**
   * @brief Get the Own Vehicle Information object
   *
   * @return Returns the information about the own vehicle
   */
  virtual OwnVehicleInformation GetOwnVehicleInformation() const { return ownVehicleInformation; }

  /**
   * @brief Get the Traffic Rule Information object
   *
   * @return Returns the traffic rule information
   */
  virtual TrafficRuleInformation GetTrafficRuleInformation() const { return trafficRuleInformation; }

  /**
   * @brief Get the Geometry Information object
   *
   * @return Returns the lane geometry information
   */
  virtual GeometryInformation GetGeometryInformation() const { return geometryInformation; }

  /**
   * @brief Get the Surrounding Objects object
   *
   * @return Returns the information about the surrouding objects
   */
  virtual SurroundingObjects GetSurroundingObjects() const { return surroundingObjects; }

private:
  //! \brief Struct for all sensor data concerning the own vehicle
  OwnVehicleInformation ownVehicleInformation;
  //! \brief Struct for all sensor data concerning mesosopic information
  TrafficRuleInformation trafficRuleInformation;
  //! \brief Struct for all sensor data concerning mesosopic information
  GeometryInformation geometryInformation;
  //! \brief Struct for all sensor data concerning surrounding objects
  SurroundingObjects surroundingObjects;
};
