/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** \brief sensorCar2X.cpp */
//-----------------------------------------------------------------------------

#include "sensorCar2X.h"

#include <numeric>

#include "include/parameterInterface.h"
#include "include/radioInterface.h"
#include "include/worldInterface.h"

SensorCar2X::SensorCar2X(std::string componentName,
                         bool isInit,
                         int priority,
                         int offsetTime,
                         int responseTime,
                         int cycleTime,
                         StochasticsInterface *stochastics,
                         WorldInterface *world,
                         const ParameterInterface *parameters,
                         PublisherInterface *const publisher,
                         const CallbackInterface *callbacks,
                         AgentInterface *agent)
    : ObjectDetectorBase(componentName,
                         isInit,
                         priority,
                         offsetTime,
                         responseTime,
                         cycleTime,
                         stochastics,
                         world,
                         parameters,
                         publisher,
                         callbacks,
                         agent)
{
  sensitivity = units::sensitivity(parameters->GetParametersDouble().at("Sensitivity"));
}

void SensorCar2X::Trigger(int time)
{
  auto newSensorData = DetectObjects();
  sensorData = ApplyLatency(time, newSensorData);
}

void SensorCar2X::UpdateInput(int, const std::shared_ptr<SignalInterface const> &, int) {}

osi3::SensorData SensorCar2X::DetectObjects()
{
  RadioInterface &radio = GetWorld()->GetRadio();
  Position absolutePosition = GetAbsolutePosition();

  std::set<OWL::Id> detectedIds;

  sensorData = {};
  std::vector<osi3::MovingObject> detectedObjects
      = radio.Receive(absolutePosition.xPos, absolutePosition.yPos, sensitivity);

  const auto ownId = GetAgent()->GetId();
  const auto sensorPosition = GetSensorPosition();
  const auto ownYaw = GetAgent()->GetYaw() + position.pose.orientation.yaw;
  const ObjectPointCustom mountingPosition{position.pose.position.x, position.pose.position.y};
  const auto ownVelocity = GetAgent()->GetVelocity(mountingPosition);
  const auto ownAcceleration = GetAgent()->GetAcceleration(mountingPosition);

  for (auto &object : detectedObjects)
  {
    if (HasDetectionError() || object.id().value() == ownId)
    {
      continue;
    }

    detectedIds.insert(object.id().value());
    osi3::DetectedMovingObject *detectedObject = sensorData.add_moving_object();
    detectedObject->mutable_header()->add_ground_truth_id()->set_value(object.id().value());
    detectedObject->mutable_header()->add_sensor_id()->set_value(id);

    if (object.base().position().has_x() && object.base().position().has_y())
    {
      point_t objectReferencePointGlobal{object.base().position().x(), object.base().position().y()};
      point_t objectReferencePointLocal
          = TransformPointToLocalCoordinates(objectReferencePointGlobal, sensorPosition, ownYaw.value());
      detectedObject->mutable_base()->mutable_position()->set_x(objectReferencePointLocal.x());
      detectedObject->mutable_base()->mutable_position()->set_y(objectReferencePointLocal.y());
    }
    if (object.base().orientation().has_yaw())
    {
      detectedObject->mutable_base()->mutable_orientation()->set_yaw(object.base().orientation().yaw()
                                                                     - ownYaw.value());
    }
    if (object.base().velocity().has_x() && object.base().velocity().has_y())
    {
      point_t objectVelocity{object.base().velocity().x(), object.base().velocity().y()};
      point_t relativeVelocity
          = CalculateRelativeVector(objectVelocity, {ownVelocity.x.value(), ownVelocity.y.value()}, ownYaw.value());
      detectedObject->mutable_base()->mutable_velocity()->set_x(relativeVelocity.x());
      detectedObject->mutable_base()->mutable_velocity()->set_y(relativeVelocity.y());
    }
    if (object.base().acceleration().has_x() && object.base().acceleration().has_y())
    {
      point_t objectAcceleration{object.base().acceleration().x(), object.base().acceleration().y()};
      point_t relativeAcceleration = CalculateRelativeVector(
          objectAcceleration, {ownAcceleration.x.value(), ownAcceleration.y.value()}, ownYaw.value());
      detectedObject->mutable_base()->mutable_acceleration()->set_x(relativeAcceleration.x());
      detectedObject->mutable_base()->mutable_acceleration()->set_y(relativeAcceleration.y());
    }
  }

  GetPublisher()->Publish("Sensor" + std::to_string(id) + "_DetectedAgents", CreateObjectIdListString(detectedIds));
  GetPublisher()->Publish("Sensor" + std::to_string(id) + "_VisibleAgents", CreateObjectIdListString(detectedIds));

  return sensorData;
}

std::string SensorCar2X::CreateObjectIdListString(const std::set<OWL::Id> &owlIds) const
{
  return std::accumulate(owlIds.begin(),
                         owlIds.end(),
                         std::string(""),
                         [](const auto &first, const auto &second) -> std::string
                         { return first + ';' + std::to_string(second); })
      .erase(0, 1);
}
