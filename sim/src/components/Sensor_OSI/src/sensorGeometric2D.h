/********************************************************************************
 * Copyright (c) 2020 HLRS, University of Stuttgart
 *               2017-2020 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** \file  sensorGeometric2D.h
 *	\brief This file models a sensor which only detects agents in a 2D area (x/y) in front of the agent
 */
//-----------------------------------------------------------------------------

#pragma once

#include <osi3/osi_sensordata.pb.h>
#include <osi3/osi_sensorview.pb.h>

#include "core/opSimulation/modules/World_OSI/WorldData.h"
#include "objectDetectorBase.h"

/// view of an object
struct ObjectView
{
  /// id of the object
  long objectId;
  /// min angle in senor coordinates
  double minAngle;
  /// maximum angle in senor coordinates
  double maxAngle;
  /// distance to the sensor
  units::length::meter_t distance;
};

/// Results of a sensor detection
struct SensorDetectionResults
{
  /// list of visible moving objects
  std::vector<osi3::MovingObject> visibleMovingObjects;
  /// list of detected moving objects
  std::vector<osi3::MovingObject> detectedMovingObjects;
  /// list of visible stationary objects
  std::vector<osi3::StationaryObject> visibleStationaryObjects;
  /// list of detected stationary objects
  std::vector<osi3::StationaryObject> detectedStationaryObjects;
};

//-----------------------------------------------------------------------------
/** \brief This file models a sensor which only detects agents in a 2D area (x/y) in front of the agent
 * 	\details This sensor does not consider height.
 *
 * 	\ingroup SensorOSI
 */
//-----------------------------------------------------------------------------
class SensorGeometric2D : public ObjectDetectorBase
{
public:
  /**
   * @brief Construct a new Sensor Geometric 2 D object
   *
   * \param [in] componentName   Name of the component
   * \param [in] isInit          Query whether the component was just initialized
   * \param [in] priority        Priority of the component
   * \param [in] offsetTime      Offset time of the component
   * \param [in] responseTime    Response time of the component
   * \param [in] cycleTime       Cycle time of this components trigger task [ms]
   * \param [in] stochastics     Provides access to the stochastics functionality of the framework
   * \param [in] world           Provides access to world representation
   * \param [in] parameters      Interface provides access to the configuration parameters
   * \param [in] publisher       Instance  provided by the framework
   * \param [in] callbacks       Interface for callbacks to framework
   * \param [in] agent           This interface provides access to agent parameters, properties, attributes and dynamic states
   */
  SensorGeometric2D(std::string componentName,
                    bool isInit,
                    int priority,
                    int offsetTime,
                    int responseTime,
                    int cycleTime,
                    StochasticsInterface* stochastics,
                    WorldInterface* world,
                    const ParameterInterface* parameters,
                    PublisherInterface* const publisher,
                    const CallbackInterface* callbacks,
                    AgentInterface* agent);

  SensorGeometric2D(const SensorGeometric2D&) = delete;
  SensorGeometric2D(SensorGeometric2D&&) = delete;
  SensorGeometric2D& operator=(const SensorGeometric2D&) = delete;
  SensorGeometric2D& operator=(SensorGeometric2D&&) = delete;
  ~SensorGeometric2D() = default;

  void UpdateInput(int, const std::shared_ptr<SignalInterface const>&, int) override;
  void Trigger(int time) override;

  /**
   * \brief Calculate which objects are inside the detection field
   *
   * For further explanation of the calculation see the [documentation] (@cond \ref dev_agent_modules_geometric2d
   * @endcond)
   * @param time time at which objects are detected
   * @return returns results of sensor detections
   */
  SensorDetectionResults DetectObjects(int time);

protected:
  /*!
   * \brief Generates the SensorViewConfiguration to send the world data for filtering of the SensorView
   *
   * @return SensorViewConfiguration for parametrization of Sensorview
   */
  osi3::SensorViewConfiguration GenerateSensorViewConfiguration() override;

private:
  void Observe(const int time, const SensorDetectionResults& results);
  SensorDetectionResults ApplyLatencyToResults(const int time, const SensorDetectionResults& results);
  /**
   * \brief Calculate if objects in the detection field visually obstruct other objects and remove those whose visibility percentage is below the threshold
   *
   * For further explanation of the calculation see the [documentation](\ref dev_agent_modules_geometric2d_obstruction)
   *
   * \param movingObjects         list of moving objects in the detection field for which visual obstruction should be applied
   * \param stationaryObjects     list of stationary objects in the detection field for which visual obstruction should be applied
   * \param sensorPositionGlobal  sensor postion in global coordinates
   */
  template <typename T>
  std::pair<std::vector<T>, std::vector<T>> CalcVisualObstruction(const std::vector<const T*>& objects,
                                                                  const multi_polygon_t& brightArea);

  /**
   * Calculate the polygon to approximate the detection area
   *
   * \returns bright area polygon
   */
  polygon_t CalcInitialBrightArea(point_t sensorPosition);

  /**
   * Calculate the shadow drawn by an object with the sensorPosition as light source
   *
   * \param boundingBox  boundingBox of the objects which draws the shadow
   * \param sensorPosition Position of the sensor (light source)
   * \returns shadow polygon
   */
  multi_polygon_t CalcObjectShadow(const polygon_t& boundingBox, point_t sensorPosition);

  /**
   * Calculate how many percent of an object are inside the bright area
   * \param boundingBox boundingBox of the object
   * \param brightArea
   * \returns percentage of the visible area of the object
   */
  static double CalcObjectVisibilityPercentage(const polygon_t& boundingBox, const multi_polygon_t& brightArea);

  /*!
   * \brief Returns true if opening angle is smaller than pi
   */
  bool OpeningAngleWithinHalfCircle() const;

  /*!
   * \brief Returns true if opening angle is smaller than two pi
   */
  bool OpeningAngleWithinFullCircle() const;

  /*!
   * \brief Creates the detection field for angles smaller than pi
   */
  polygon_t CreateFourPointDetectionField() const;

  /*!
   * \brief Creates the detection field for angles between pi and two pi
   */
  polygon_t CreateFivePointDetectionField() const;

  std::pair<point_t, polygon_t> CreateSensorDetectionField(const osi3::MovingObject* hostVehicle) const;

  template <typename T>
  void ApplyVisualObstructionToDetectionArea(multi_polygon_t& brightArea,
                                             const point_t& sensorPositionGlobal,
                                             const std::vector<const T*>& objects);
  template <typename T>
  bool ObjectIsInDetectionArea(const T& object,
                               const point_t& sensorPositionGlobal,
                               const polygon_t& detectionField) const;

  std::pair<std::vector<const osi3::MovingObject*>, std::vector<const osi3::StationaryObject*>>
  GetObjectsInDetectionAreaFromSensorView(const osi3::SensorView& sensorView,
                                          const point_t& sensorPositionGlobal,
                                          const polygon_t& detectionField) const;

  std::string CreateObjectIdListString(const std::set<OWL::Id>& owlIds) const;

  bool enableVisualObstruction = false;
  double requiredPercentageOfVisibleArea = 0.001;
  units::length::meter_t detectionRange;
  units::angle::radian_t openingAngleH;
  std::map<int, SensorDetectionResults> latentSensorDetectionResultsBuffer;

  static constexpr double MIN_VISIBLE_UNOBSTRUCTED_PERCENTAGE = 0.0001;
  static constexpr double WARNING_THRESHOLD_SCALE = 1e6;
};
