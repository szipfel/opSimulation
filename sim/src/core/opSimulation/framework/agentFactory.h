/********************************************************************************
 * Copyright (c) 2020 HLRS, University of Stuttgart
 *               2016-2018 ITK Engineering GmbH
 *               2017-2021 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  agentFactory.h
//! @brief This file contains the singleton which creates agent instances
//!
//! The agent instances are created based on the agent types which are given by
//! configuration. Agent instances will be created on the fly during the
//! simulation run by spawn points.
//-----------------------------------------------------------------------------

#pragma once

#include <list>
#include <map>
#include <memory>

#include "common/opExport.h"
#include "include/agentFactoryInterface.h"
#include "include/worldInterface.h"

class DataBufferWriteInterface;

namespace core
{
class Agent;
class AgentType;
class ModelBinding;
class Stochastics;
class SpawnItemParameter;
class ObservationNetworkInterface;

/// Class representing the factory of agent
class SIMULATIONCOREEXPORT AgentFactory : public AgentFactoryInterface
{
public:
  /**
   * @brief AgentFactory constructor
   *
   * @param[in] modelBinding          Pointer to the model binding
   * @param[in] world                 The world interface
   * @param[in] stochastics           Pointer to the stochastic
   * @param[in] observationNetwork    Pointer to observation network
   * @param[in] dataBuffer            Pointer to the data buffer that provides write-only access to the data
   */
  AgentFactory(ModelBinding *modelBinding,
               WorldInterface *world,
               Stochastics *stochastics,
               ObservationNetworkInterface *observationNetwork,
               DataBufferWriteInterface *dataBuffer);
  ~AgentFactory() override = default;

  void Clear() override;
  Agent *AddAgent(const AgentBuildInstructions &agentBuildInstructions) override;

private:
  //-----------------------------------------------------------------------------
  //! @brief Links all channels of the agent components.
  //!
  //! Gets the channels of the agent from all its components' output links,
  //! creates for each a channel buffer using its source and attaches it to the
  //! channel.
  //!
  //!
  //! @param[in]  agent               The agent whose channels are connected
  //!
  //! @return                         Flag if the connect was successful
  //-----------------------------------------------------------------------------
  bool ConnectAgentLinks(Agent *agent);

  //-----------------------------------------------------------------------------
  //! @brief Creates a new agent based on the provided parameters,
  //!        initializes it, and links its agent channels.
  //!
  //! @param[in]  agentBlueprint      agentBlueprint contains all necessary
  //!                                 informations to create an agent
  //! @return                         The created agent
  //-----------------------------------------------------------------------------
  std::unique_ptr<Agent> CreateAgent(const AgentBuildInstructions &agentBuildInstructions);

  void PublishProperties(const AgentBuildInstructions &agentBuildInstructions, int agentId);

  ModelBinding *modelBinding;
  WorldInterface *world;
  Stochastics *stochastics;
  ObservationNetworkInterface *observationNetwork;
  DataBufferWriteInterface *dataBuffer;

  std::vector<std::unique_ptr<Agent>> agentList;
};

}  // namespace core
