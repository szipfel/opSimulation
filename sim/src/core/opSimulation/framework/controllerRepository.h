/*******************************************************************************
 * Copyright (c) 2021 in-tech GmbH
 *               2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <functional>
#include <map>

#include "MantleAPI/Traffic/i_controller_repository.h"
#include "agentBlueprintProvider.h"
#include "common/opExport.h"
#include "controller.h"

namespace core
{

class IdManagerInterface;

//! This class provides CRUD functionality for controllers
class SIMULATIONCOREEXPORT ControllerRepository final : public mantle_api::IControllerRepository
{
public:
  //! ControllerRepository constructor
  //!
  //! @param[in] idManager                References the idManager that handles unique IDs
  //! @param[in] agentBlueprintProvider   References the agent blueprint provider
  ControllerRepository(IdManagerInterface &idManager, const AgentBlueprintProvider &agentBlueprintProvider)
      : idManager{idManager}, agentBlueprintProvider(agentBlueprintProvider)
  {
  }

  mantle_api::IController &Create(std::unique_ptr<mantle_api::IControllerConfig> config) override final;
  mantle_api::IController &Create(mantle_api::UniqueId id,
                                  std::unique_ptr<mantle_api::IControllerConfig> config) override final
  {
    throw std::runtime_error("ControllerRepository: Use of unsupported deprecated function \"Create(id, config)\"");
  }

  std::optional<std::reference_wrapper<mantle_api::IController>> Get(mantle_api::UniqueId id) override final;
  bool Contains(mantle_api::UniqueId id) const override final;

  void Delete(mantle_api::UniqueId id) override final;

  mantle_api::UniqueId nextId{0};  //!< References the next index
  std::map<mantle_api::UniqueId, std::shared_ptr<Controller>>
      controllers;  //!< The map of all controllers with their UniqueId

private:
  std::reference_wrapper<IdManagerInterface> idManager;
  std::reference_wrapper<const AgentBlueprintProvider> agentBlueprintProvider;
};
}  // namespace core