/********************************************************************************
 * Copyright (c) 2020-2021 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "idManager.h"

#include <algorithm>

namespace core
{

IdManager::IdManager()
{
  static constexpr bool AS_PERSISTENT_GROUP{true};
  static constexpr bool AS_NON_PERSISTENT_GROUP{false};

  RegisterGroup(EntityType::kVehicle, AS_NON_PERSISTENT_GROUP);
  RegisterGroup(EntityType::kController, AS_NON_PERSISTENT_GROUP);
  RegisterGroup(EntityType::kObject, AS_NON_PERSISTENT_GROUP);
  RegisterGroup(EntityType::kUnknown, AS_PERSISTENT_GROUP);
}

IdManager::~IdManager() {}

mantle_api::UniqueId IdManager::Generate()
{
  return Generate(EntityType::kUnknown);
}

mantle_api::UniqueId IdManager::Generate(EntityType entityType)
{
  return _repository.at(entityType).GetNextIndex();
}

void IdManager::Reset()
{
  std::for_each(_repository.begin(), _repository.end(), [](auto &repoEntries) { repoEntries.second.Reset(); });
}

void IdManager::RegisterGroup(EntityType entityType, bool persistence)
{
  _repository.emplace(entityType,
                      EntityGroup(MAX_ENTITIES_PER_GROUP, MAX_ENTITIES_PER_GROUP * _repository.size(), persistence));
}

}  // namespace core