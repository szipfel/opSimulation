/*******************************************************************************
 * Copyright (c) 2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "laneLocationQueryService.h"

#include "common/log.h"

using namespace units::literals;

namespace core
{

mantle_api::Orientation3<units::angle::radian_t> LaneLocationQueryService::GetLaneOrientation(
    const mantle_api::Vec3<units::length::meter_t>& position) const
{
  const auto lanePosition = world->WorldCoord2LaneCoord(position.x, position.y, 0_rad);
  if (lanePosition.empty())
  {
    throw std::runtime_error("Position is outside of road network!");
  }

  const auto direction = world->GetLaneDirection(lanePosition.cbegin()->second.roadId,
                                                 lanePosition.cbegin()->second.laneId,
                                                 lanePosition.cbegin()->second.roadPosition.s);
  return {direction, 0_rad, 0_rad};
}

mantle_api::Vec3<units::length::meter_t> LaneLocationQueryService::GetUpwardsShiftedLanePosition(
    const mantle_api::Vec3<units::length::meter_t>& position, double upwards_shift, bool allow_invalid_positions) const
{
  return {position.x, position.y, position.z + units::length::meter_t(upwards_shift)};
}

bool LaneLocationQueryService::IsPositionOnLane(const mantle_api::Vec3<units::length::meter_t>& position) const
{
  const auto lanePosition = world->WorldCoord2LaneCoord(position.x, position.y, 0_rad);
  return !lanePosition.empty();
}

std::vector<mantle_api::UniqueId> LaneLocationQueryService::GetLaneIdsAtPosition(
    const mantle_api::Vec3<units::length::meter_t>& position) const
{
  const auto lanePositions = world->WorldCoord2LaneCoord(position.x, position.y, 0_rad);

  std::vector<mantle_api::UniqueId> laneIds{};
  for (const auto& lanePosition : lanePositions)
  {
    auto laneId = world->GetUniqueLaneId(
        lanePosition.second.roadId, lanePosition.second.laneId, lanePosition.second.roadPosition.s);
    laneIds.push_back(laneId);
  }
  return laneIds;
}

std::optional<mantle_api::Pose> LaneLocationQueryService::FindLanePoseAtDistanceFrom(
    const mantle_api::Pose& reference_pose_on_lane,
    units::length::meter_t distance,
    mantle_api::Direction direction) const
{
  if (IsPositionOnLane(reference_pose_on_lane.position))
  {
    const auto lanePosition = world->WorldCoord2LaneCoord(
        reference_pose_on_lane.position.x, reference_pose_on_lane.position.y, reference_pose_on_lane.orientation.yaw);

    bool forwardDirection = (direction == mantle_api::Direction::kForward)
                        xor (std::abs(lanePosition.cbegin()->second.roadPosition.hdg.value()) <= M_PI_2);
    auto roadStream = world->GetRoadStream({{lanePosition.cbegin()->second.roadId, forwardDirection}});
    auto laneStream = roadStream->GetLaneStream(lanePosition.cbegin()->second);
    auto laneStreamPosition = laneStream->GetStreamPosition(lanePosition.cbegin()->second);
    StreamPosition targetStreamPosition = {laneStreamPosition.s + distance, laneStreamPosition.t};
    auto targetRoadPosition = laneStream->GetRoadPosition(targetStreamPosition);

    mantle_api::Pose newPose;
    auto worldPosition = world->LaneCoord2WorldCoord(targetRoadPosition.roadPosition.s,
                                                     targetRoadPosition.roadPosition.t,
                                                     targetRoadPosition.roadId,
                                                     targetRoadPosition.laneId);
    if (!worldPosition)
    {
      return std::nullopt;
    }
    newPose.position = {worldPosition->xPos, worldPosition->yPos, 0.0_m};
    newPose.orientation.yaw
        = CommonHelper::SetAngleToValidRange(worldPosition->yawAngle + reference_pose_on_lane.orientation.yaw);
    return newPose;
  }
  return std::nullopt;
}

std::optional<units::length::meter_t> LaneLocationQueryService::GetLongitudinalLaneDistanceBetweenPositions(
    const mantle_api::Vec3<units::length::meter_t>& start_position,
    const mantle_api::Vec3<units::length::meter_t>& target_position) const
{
  const auto startLanePosition = world->WorldCoord2LaneCoord(start_position.x, start_position.y, 0_rad);
  const auto targetLanePosition = world->WorldCoord2LaneCoord(target_position.x, target_position.y, 0_rad);
  if (!startLanePosition.empty() && !targetLanePosition.empty())
  {
    constexpr int maxDepth = 10;
    auto [roadGraph, start] = world->GetRoadGraph(
        {startLanePosition.cbegin()->second.roadId, startLanePosition.cbegin()->second.laneId < 0}, maxDepth);
    auto distances = world->GetDistanceBetweenObjects(
        roadGraph, start, startLanePosition.cbegin()->second.roadPosition.s, targetLanePosition);

    for (auto& distance : distances)
    {
      if (distance.second != std::nullopt)
      {
        return distance.second;
      }
    }
  }
  return std::nullopt;
}

std::optional<mantle_api::Pose> LaneLocationQueryService::FindRelativeLanePoseAtDistanceFrom(
    const mantle_api::Pose& reference_pose_on_lane,
    int relative_target_lane,
    units::length::meter_t distance,
    units::length::meter_t lateral_offset) const
{
  if (IsPositionOnLane(reference_pose_on_lane.position))
  {
    const auto lanePosition = world->WorldCoord2LaneCoord(
        reference_pose_on_lane.position.x, reference_pose_on_lane.position.y, reference_pose_on_lane.orientation.yaw);

    bool direction = (std::abs(lanePosition.cbegin()->second.roadPosition.hdg.value()) <= M_PI_2);
    auto roadStream = world->GetRoadStream({{lanePosition.cbegin()->second.roadId, direction}});
    auto laneStream = roadStream->GetLaneStream(lanePosition.cbegin()->second);
    auto laneStreamPosition = laneStream->GetStreamPosition(lanePosition.cbegin()->second);
    StreamPosition targetStreamPosition = {laneStreamPosition.s + distance, laneStreamPosition.t};
    auto targetRoadPosition = laneStream->GetRoadPosition(targetStreamPosition);

    mantle_api::Pose newPose;
    auto worldPosition = world->LaneCoord2WorldCoord(targetRoadPosition.roadPosition.s,
                                                     lateral_offset,
                                                     targetRoadPosition.roadId,
                                                     targetRoadPosition.laneId + relative_target_lane);
    if (!worldPosition)
    {
      return std::nullopt;
    }
    newPose.position = {worldPosition->xPos, worldPosition->yPos, 0.0_m};
    newPose.orientation.yaw
        = CommonHelper::SetAngleToValidRange(worldPosition->yawAngle + reference_pose_on_lane.orientation.yaw);
    return newPose;
  }
  return std::nullopt;
}

std::optional<mantle_api::LaneId> LaneLocationQueryService::GetRelativeLaneId(
    const mantle_api::Pose& reference_pose_on_lane, int relative_lane_target) const
{
  if (IsPositionOnLane(reference_pose_on_lane.position))
  {
    const auto lanePosition
        = world->WorldCoord2LaneCoord(reference_pose_on_lane.position.x, reference_pose_on_lane.position.y, 0_rad);
    return lanePosition.cbegin()->second.laneId + relative_lane_target;
  }
  return std::nullopt;
}

std::optional<mantle_api::Vec3<units::length::meter_t>> LaneLocationQueryService::GetPosition(
    const mantle_api::Pose& reference_pose,
    mantle_api::LateralDisplacementDirection direction,
    units::length::meter_t distance) const
{
  return std::nullopt;
}

std::optional<mantle_api::Pose> LaneLocationQueryService::GetProjectedPoseAtLane(
    const mantle_api::Vec3<units::length::meter_t>& reference_position_on_lane, mantle_api::LaneId target_lane_id) const
{
  LogErrorAndThrow("Method GetProjectedPoseAtLane in LaneLocationQueryService is not yet implemented");
}
std::optional<mantle_api::Vec3<units::length::meter_t>> LaneLocationQueryService::GetProjectedCenterLinePoint(
    const mantle_api::Vec3<units::length::meter_t>& position) const
{
  LogErrorAndThrow("Method GetProjectedCenterLinePoint in LaneLocationQueryService is not yet implemented");
}

}  // namespace core