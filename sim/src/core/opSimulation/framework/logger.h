/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Common/i_logger.h>
#include <stdexcept>

#include "include/callbackInterface.h"

namespace core
{

namespace detail
{

/// Converts the LogLevel of openPASS into the mantle_api::LogLevel
/// @param logLevel openPASS loglevel
/// @returns representation within mantle_api::ILogger specification
[[nodiscard]] inline mantle_api::LogLevel ConvertLogLevel(int logLevel) noexcept
{
  switch (logLevel)
  {
    case 0:
      return mantle_api::LogLevel::kError;
    case 1:
      return mantle_api::LogLevel::kWarning;
    case 2:
      return mantle_api::LogLevel::kInfo;
    case 3:
      return mantle_api::LogLevel::kDebug;
    default:
      return mantle_api::LogLevel::kDebug;
  }
}
}  // namespace detail

/// This class represents a logger that logs information from OpenScenarioEngine
class Logger : public mantle_api::ILogger
{
public:
  /// Logger constructor
  ///
  /// @param[in] logLevel     Importance of log
  /// @param[in] callbacks    Pointer to the callback
  Logger(int logLevel, const CallbackInterface *callbacks)
      : logLevel(detail::ConvertLogLevel(logLevel)), callbacks(callbacks)
  {
  }

  [[nodiscard]] mantle_api::LogLevel GetCurrentLogLevel() const noexcept override
  {
    return mantle_api::LogLevel::kDebug;
  }

  void Log(mantle_api::LogLevel log_level, std::string_view message) noexcept override
  {
    switch (log_level)
    {
      case mantle_api::LogLevel::kCritical:
        LOGERROR(std::string(message));
        return;
      case mantle_api::LogLevel::kError:
        LOGERROR(std::string(message));
        return;
      case mantle_api::LogLevel::kWarning:
        LOGWARN(std::string(message));
        return;
      case mantle_api::LogLevel::kInfo:
        LOGINFO(std::string(message));
        return;
      case mantle_api::LogLevel::kDebug:
        LOGDEBUG(std::string(message));
        return;
      default:
        std::runtime_error("logger: log_level out of range");
    }
  }

private:
  void Log(CbkLogLevel logLevel, const char *file, int line, const std::string &message) const
  {
    callbacks->Log(logLevel, file, line, message);
  }

  const mantle_api::LogLevel logLevel;
  const CallbackInterface *callbacks;
};

}  //namespace core