/********************************************************************************
 * Copyright (c) 2016-2018 ITK Engineering GmbH
 *               2017-2021 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "runInstantiator.h"

#include <functional>
#include <sstream>

#include <QDir>
#include <qdir.h>

#include "agentFactory.h"
#include "agentType.h"
#include "bindings/dataBuffer.h"
#include "bindings/stochastics.h"
#include "channel.h"
#include "common/log.h"
#include "component.h"
#include "directories.h"
#include "importer/entityPropertiesImporter.h"
#include "include/collisionDetectionInterface.h"
#include "include/observationNetworkInterface.h"
#include "modelElements/parameters.h"
#include "observationModule.h"
#include "parameterbuilder.h"
#include "sampler.h"
#include "scheduler/runResult.h"
#include "scheduler/scheduler.h"
#include "spawnPointNetwork.h"

const std::string SCENERYPATH = "full_map_path";
const std::string VEHICLEMODELSFILENAME = "VehicleModelsCatalog.xosc";

constexpr char SPAWNER[] = {"Spawner"};

namespace core
{

std::tuple<std::string, std::string, std::string> RunInstantiator::GetScenarioPaths()
{
  // currently it's a little hacky to get catalog paths
  // we need to make sure, that there are no entities created in between
  // so we clear the run before we continue
  OpenScenarioEngine::v1_2::OpenScenarioEngine scenarioEngine(
      configurationContainer.GetSimulationConfig()->GetScenarioConfig().scenarioPath, environment, logger);
  scenarioEngine.Init();
  const auto configurationDir
      = QString::fromStdString(configurationContainer.GetRuntimeInformation().directories.configuration)
      + QDir::separator();
  const QString relativeSceneryPath
      = QString::fromStdString(scenarioEngine.GetScenarioInfo().additional_information.at(SCENERYPATH));
  const QDir absoluteSceneryPath = configurationDir + relativeSceneryPath;
  const QDir vehicleCatalogFilepath
      = configurationDir
      + QString::fromStdString(scenarioEngine.GetScenarioInfo().additional_information.at("vehicle_catalog_path"))
      + QDir::separator() + QString::fromStdString(VEHICLEMODELSFILENAME);
  ClearRun();

  return {relativeSceneryPath.toStdString(),
          absoluteSceneryPath.absolutePath().toStdString(),
          vehicleCatalogFilepath.absolutePath().toStdString()};
}

bool RunInstantiator::ExecuteRun()
{
  LOG_INTERN(LogLevel::DebugCore) << std::endl << "### execute run ###";

  stopMutex.lock();
  stopped = false;
  stopMutex.unlock();

  const auto &simulationConfig = *configurationContainer.GetSimulationConfig();
  const auto &profiles = *configurationContainer.GetProfiles();
  const auto &experimentConfig = simulationConfig.GetExperimentConfig();
  const auto &environmentConfig = simulationConfig.GetEnvironmentConfig();

  ThrowIfFalse(dataBuffer.Instantiate(), "Failed to instantiate DataBuffer");

  ThrowIfFalse(stochastics.Instantiate(frameworkModules.stochasticsLibrary), "Failed to instantiate Stochastics");

  agentBlueprintProvider.Init(&configurationContainer, &stochastics);

  environment->SetWorld(&world);

  const auto &[relativeSceneryPath, absoluteSceneryPath, vehicleCatalogFilepath] = GetScenarioPaths();
  Importer::EntityPropertiesImporter entityPropertiesImporter;
  entityPropertiesImporter.Import(vehicleCatalogFilepath);
  vehicles = entityPropertiesImporter.GetVehicleProperties();

  if (!InitPreRun(absoluteSceneryPath))
  {
    LOG_INTERN(LogLevel::DebugCore) << std::endl << "### initialization failed ###";
    return false;
  }

  dataBuffer.PutStatic("SceneryFile", relativeSceneryPath, true);
  ThrowIfFalse(observationNetwork.InitAll(), "Failed to initialize ObservationNetwork");

  core::scheduling::Scheduler scheduler(
      world, spawnPointNetwork, collisionDetection, observationNetwork, dataBuffer, *environment);
  bool scheduler_state{false};

  for (auto invocation = 0; invocation < experimentConfig.numberOfInvocations; invocation++)
  {
    RunResult runResult;

    LOG_INTERN(LogLevel::DebugCore) << std::endl << "### run number: " << invocation << " ###";
    stochastics.InitGenerator(static_cast<std::uint32_t>(experimentConfig.randomSeed + invocation));
    if (!InitRun(environmentConfig, profiles, runResult))
    {
      LOG_INTERN(LogLevel::DebugCore) << std::endl << "### run initialization failed ###";
      break;
    }

    LOG_INTERN(LogLevel::DebugCore) << std::endl << "### run started ###";
    scheduler_state = scheduler.Run(
        0,
        runResult,
        std::make_unique<OpenScenarioEngine::v1_2::OpenScenarioEngine>(
            configurationContainer.GetSimulationConfig()->GetScenarioConfig().scenarioPath, environment));

    if (scheduler_state == core::scheduling::Scheduler::FAILURE)
    {
      LOG_INTERN(LogLevel::DebugCore) << std::endl << "### run aborted ###";
      break;
    }
    LOG_INTERN(LogLevel::DebugCore) << std::endl << "### run successful ###";

    observationNetwork.FinalizeRun(runResult);
    ClearRun();
  }

  LOG_INTERN(LogLevel::DebugCore) << std::endl << "### end of all runs ###";
  bool observations_state = observationNetwork.FinalizeAll();

  return (scheduler_state && observations_state);
}

bool RunInstantiator::InitPreRun(const std::string &sceneryPath)
{
  try
  {
    ThrowIfFalse(observationNetwork.Instantiate(
                     frameworkModules.observationLibraries, &stochastics, &world, sceneryPath, &dataBuffer),
                 "Failed to instantiate ObservationNetwork");

    return true;
  }
  catch (const std::exception &error)
  {
    LOG_INTERN(LogLevel::Error) << std::endl << "### could not init: " << error.what() << "###";
    return false;
  }

  LOG_INTERN(LogLevel::Error) << std::endl << "### exception caught, which is not of type std::exception! ###";
  return false;
}

void RunInstantiator::InitializeSpawnPointNetwork()
{
  const auto &profileGroups = configurationContainer.GetProfiles()->GetProfileGroups();
  bool existingSpawnProfiles = profileGroups.find(SPAWNER) != profileGroups.end();

  ThrowIfFalse(spawnPointNetwork.Instantiate(
                   frameworkModules.spawnPointLibraries,
                   &agentFactory,
                   &stochastics,
                   existingSpawnProfiles ? std::make_optional(profileGroups.at(SPAWNER)) : std::nullopt,
                   &configurationContainer,
                   environment,
                   vehicles),
               "Failed to instantiate SpawnPointNetwork");
}

std::unique_ptr<ParameterInterface> RunInstantiator::SampleWorldParameters(
    const EnvironmentConfig &environmentConfig,
    const ProfileGroup &trafficRules,
    StochasticsInterface *stochastics,
    const openpass::common::RuntimeInformation &runtimeInformation)
{
  auto trafficRule = helper::map::query(trafficRules, environmentConfig.trafficRules);
  ThrowIfFalse(trafficRule.has_value(),
               "No traffic rule set with name " + environmentConfig.trafficRules + " defined in ProfilesCatalog");
  auto parameters = trafficRule.value();
  parameters.emplace_back("TimeOfDay", Sampler::Sample(environmentConfig.timeOfDays, stochastics));
  parameters.emplace_back("VisibilityDistance", Sampler::Sample(environmentConfig.visibilityDistances, stochastics));
  parameters.emplace_back("Friction", Sampler::Sample(environmentConfig.frictions, stochastics));
  parameters.emplace_back("Weather", Sampler::Sample(environmentConfig.weathers, stochastics));

  return openpass::parameter::make<SimulationCommon::Parameters>(runtimeInformation, parameters);
}
bool RunInstantiator::InitRun(const EnvironmentConfig &environmentConfig,
                              const ProfilesInterface &profiles,
                              RunResult &runResult)
{
  try
  {
    auto trafficRules = helper::map::query(profiles.GetProfileGroups(), "TrafficRules");
    ThrowIfFalse(trafficRules.has_value(), "No traffic rules defined in ProfilesCatalog");
    worldParameter = SampleWorldParameters(
        environmentConfig, trafficRules.value(), &stochastics, configurationContainer.GetRuntimeInformation());
    world.ExtractParameter(worldParameter.get());

    observationNetwork.InitRun();
    InitializeSpawnPointNetwork();

    return true;
  }
  catch (const std::exception &error)
  {
    LOG_INTERN(LogLevel::Error) << std::endl << "### could not init run: " << error.what() << "###";
    return false;
  }

  LOG_INTERN(LogLevel::Error) << std::endl << "### exception caught, which is not of type std::exception! ###";
  return false;
}

void RunInstantiator::ClearRun()
{
  environment->Reset();
  agentFactory.Clear();
  spawnPointNetwork.Clear();
  dataBuffer.ClearRun();
}

}  // namespace core