/********************************************************************************
 * Copyright (c) 2020 HLRS, University of Stuttgart
 *               2016-2018 ITK Engineering GmbH
 *               2017-2021 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  runInstantiator.h
//! @brief This file contains the component which triggers the simulation runs.
//-----------------------------------------------------------------------------

#pragma once

#include <map>
#include <string>
#include <tuple>

#include <QMutex>

#include "OpenScenarioEngine/OpenScenarioEngine.h"
#include "agentBlueprintProvider.h"
#include "common/opExport.h"
#include "environment.h"
#include "frameworkModules.h"
#include "include/agentFactoryInterface.h"
#include "include/configurationContainerInterface.h"
#include "include/dataBufferInterface.h"
#include "include/frameworkModuleContainerInterface.h"
#include "include/observationNetworkInterface.h"
#include "include/parameterInterface.h"
#include "include/stochasticsInterface.h"
#include "logger.h"

namespace core
{

/// class representing the component which triggers the simulation runs
class SIMULATIONCOREEXPORT RunInstantiator
{
public:
  /**
   * @brief RunInstantiator constructor
   *
   * @param[in] configurationContainer    Pointer to the configuration container
   * @param[in] frameworkModuleContainer  Pointer to the framework module container
   * @param[in] frameworkModules          Framework modules
   */
  RunInstantiator(ConfigurationContainerInterface &configurationContainer,
                  FrameworkModuleContainerInterface &frameworkModuleContainer,
                  FrameworkModules &frameworkModules)
      : configurationContainer(configurationContainer),
        observationNetwork(*frameworkModuleContainer.GetObservationNetwork()),
        agentFactory(*frameworkModuleContainer.GetAgentFactory()),
        world(*frameworkModuleContainer.GetWorld()),
        spawnPointNetwork(*frameworkModuleContainer.GetSpawnPointNetwork()),
        stochastics(*frameworkModuleContainer.GetStochastics()),
        collisionDetection(*frameworkModuleContainer.GetCollisionDetection()),
        dataBuffer(*frameworkModuleContainer.GetDataBuffer()),
        frameworkModules(frameworkModules),
        logger(std::make_shared<Logger>(frameworkModules.logLevel, frameworkModuleContainer.GetCallbacks())),
        environment(std::make_shared<Environment>(
            configurationContainer.GetRuntimeInformation().directories.configuration,
            agentBlueprintProvider,
            &agentFactory,
            &stochastics,
            configurationContainer.GetSimulationConfig()->GetEnvironmentConfig().turningRates,
            frameworkModuleContainer.GetCallbacks()))
  {
  }

  //-----------------------------------------------------------------------------
  //! @brief Executes the run by preparing the stochastics, world and observation
  //!     network instances, then scheduling for each run invocation an update
  //!     on the observation network, finalizing the run invocation and, after all
  //!     invocations have terminated, the observation network itself.
  //!
  //! Executes the run by initializing the stochastics generator with the random
  //! seed from the run configuration, instantiating and initializing the observation
  //! network with the observation instances from the run configuration and the
  //! observation result path from the framework configuration. For each run
  //! invocation:
  //! - configure the world paremeters from the run configuration
  //! - import the scenery file and the world global objects
  //! - init the observation network run
  //! - instantiate the world's spawn point network
  //! - start the scheduling with the observation networks's UpdateTimeStep as
  //!     update callback
  //! - finalize the run invocation using the result on the observation network
  //!
  //! Finally, finalize the observation network and clear teh world.
  //!
  //! @return                             Flag if the update was successful
  //-----------------------------------------------------------------------------
  bool ExecuteRun();

  //-----------------------------------------------------------------------------
  //! Stops the current run.
  //-----------------------------------------------------------------------------
  //void StopRun();

private:
  bool InitPreRun(const std::string &sceneryPath);
  bool InitRun(const EnvironmentConfig &environmentConfig, const ProfilesInterface &profiles, RunResult &runResult);
  void InitializeSpawnPointNetwork();
  std::unique_ptr<ParameterInterface> SampleWorldParameters(
      const EnvironmentConfig &environmentConfig,
      const ProfileGroup &trafficRules,
      StochasticsInterface *stochastics,
      const openpass::common::RuntimeInformation &runtimeInformation);
  std::tuple<std::string, std::string, std::string> GetScenarioPaths();

  void ClearRun();

  QMutex stopMutex;
  bool stopped = true;

  ConfigurationContainerInterface &configurationContainer;
  ObservationNetworkInterface &observationNetwork;
  AgentFactoryInterface &agentFactory;

  SpawnPointNetworkInterface &spawnPointNetwork;
  StochasticsInterface &stochastics;
  CollisionDetectionInterface &collisionDetection;
  WorldInterface &world;
  DataBufferInterface &dataBuffer;
  FrameworkModules &frameworkModules;

  AgentBlueprintProvider agentBlueprintProvider;

  std::unique_ptr<ParameterInterface> worldParameter;
  std::shared_ptr<Logger> logger;
  std::shared_ptr<Environment> environment{nullptr};
  std::shared_ptr<Vehicles> vehicles{};
};

}  // namespace core