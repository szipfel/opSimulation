/********************************************************************************
 * Copyright (c) 2016-2018 ITK Engineering GmbH
 *               2017-2021 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <chrono>
#include <functional>
#include <memory>

#include "MantleAPI/Execution/i_scenario_engine.h"
#include "include/worldInterface.h"
#include "schedulerTasks.h"

class DataBufferInterface;

namespace core
{
class Agent;
class RunResult;
class CollisionDetectionInterface;
class ObservationNetworkInterface;
class SchedulePolicy;
class SpawnPointNetworkInterface;
class EnvironmentInterface;

namespace scheduling
{

//-----------------------------------------------------------------------------
/** \brief execute all tasks for an simulation run
 * 	\details The scheduler triggers TaskBuilder to build up common tasks and
 *           SchedulerTasks to manage sorting of all tasks. Each timestep all
 *           given tasks are executed.
 *
 * 	\ingroup opSimulation
 */
//-----------------------------------------------------------------------------
class Scheduler
{
public:
  static constexpr bool FAILURE{false};             ///< Has scheduler failed
  static constexpr bool SUCCESS{true};              ///< Has scheduler succeed
  static constexpr int FRAMEWORK_UPDATE_RATE{100};  ///< Rate at which framework is updated

  /**
   * @brief Scheduler constructor
   *
   * @param[in] world                 The world interface
   * @param[in] spawnPointNetwork     Pointer to spawn point network
   * @param[in] collisionDetection    Pointer to the collision detection
   * @param[in] observationNetwork    Pointer to observation network
   * @param[in] dataInterface         Pointer to the data buffer
   * @param[in] environment           Pointer to the environment
   */
  Scheduler(WorldInterface &world,
            core::SpawnPointNetworkInterface &spawnPointNetwork,
            core::CollisionDetectionInterface &collisionDetection,
            core::ObservationNetworkInterface &observationNetwork,
            DataBufferInterface &dataInterface,
            EnvironmentInterface &environment);

  /*!
   * \brief Run
   *
   * \details execute all tasks for one simulation run
   *
   * @param[in]     startTime              simulation start
   * @param[out]    runResult              RunResult
   * @param[in]     scenarioEngine         ScenarioEngine
   * @returns true if simulation ends withuot error
   */
  bool Run(int startTime, RunResult &runResult, std::unique_ptr<mantle_api::IScenarioEngine> scenarioEngine);

  /*!
   * \brief ScheduleAgentTasks
   *
   * \details schedule all tasks of an new agent
   *           e.g. for respawning
   *
   * @param[in]     taskList current task list
   * @param[in]     agent    new agent
   */
  void ScheduleAgentTasks(SchedulerTasks &taskList, const Agent &agent);

private:
  WorldInterface &world;
  SpawnPointNetworkInterface &spawnPointNetwork;
  CollisionDetectionInterface &collisionDetection;
  ObservationNetworkInterface &observationNetwork;
  DataBufferInterface &dataInterface;
  EnvironmentInterface &environment;

  int currentTime;

  /*!
   * \brief UpdateAgents
   *
   * \details schedule new agents and remove deleted ones
   *
   * @param[in]     taskList               current task list
   * @param[out]    WorldInterface         world
   */
  void UpdateAgents(SchedulerTasks &taskList, WorldInterface &world);

  /*!
   * \brief ExecuteTasks
   *
   * \details execute function of given task
   *
   *
   * @param[in]     tasks     execute function of given tasks
   * @return                  false, if a task reports error
   */
  template <typename T>
  bool ExecuteTasks(T tasks);
};

}  // namespace scheduling

}  // namespace core
