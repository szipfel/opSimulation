/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
                 2017-2021 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** \file  taskBuilder.cpp */
//-----------------------------------------------------------------------------

#include "taskBuilder.h"

#include <MantleAPI/Common/time_utils.h>
#include <algorithm>
#include <units.h>

using namespace core;
namespace core::scheduling
{

TaskBuilder::TaskBuilder(const int &currentTime,
                         RunResult &runResult,
                         const int frameworkUpdateRate,
                         WorldInterface *const world,
                         SpawnPointNetworkInterface *const spawnPointNetwork,
                         ObservationNetworkInterface *const observationNetwork,
                         CollisionDetectionInterface *const collisionDetection,
                         DataBufferInterface *const dataInterface,
                         EnvironmentInterface *environment)
    : currentTime{currentTime},
      runResult{runResult},
      frameworkUpdateRate{frameworkUpdateRate},
      world{world},
      spawnPointNetwork{spawnPointNetwork},
      observationNetwork{observationNetwork},
      collisionDetection{collisionDetection},
      dataInterface{dataInterface},
      environment{environment}
{
}

std::vector<TaskItem> TaskBuilder::CreateBootstrapTasks()
{
  return {
      SpawningTaskItem(frameworkUpdateRate, [&] { return environment->GetEntityRepository().SpawnReadyAgents(); }),
      SpawningTaskItem(frameworkUpdateRate, [&] { return spawnPointNetwork->TriggerPreRunSpawnZones(); }),
  };
}

std::vector<TaskItem> TaskBuilder::CreatePostScenarioTasks()
{
  return {SpawningTaskItem(frameworkUpdateRate, [&] { return environment->GetEntityRepository().SpawnReadyAgents(); }),
          SyncEnvironmentTaskItem(ScheduleAtEachCycle, [&] { environment->SyncWorld(); })};
}

std::vector<TaskItem> TaskBuilder::CreateSpawningTasks()
{
  return {
      SpawningTaskItem(frameworkUpdateRate, [&] { return spawnPointNetwork->TriggerRuntimeSpawnPoints(currentTime); })};
}

std::vector<TaskItem> TaskBuilder::CreatePreAgentTasks()
{
  return {SyncWorldTaskItem(ScheduleAtEachCycle, [&] { world->PublishGlobalData(currentTime); }),
          CollisionDetectorTaskItem(ScheduleAtEachCycle, [&] { collisionDetection->Trigger(currentTime); })};
}

std::vector<TaskItem> TaskBuilder::CreateSynchronizeTasks()
{
  return {ObservationTaskItem(ScheduleAtEachCycle,
                              [&] { return observationNetwork->UpdateTimeStep(currentTime, runResult); }),
          ClearTaskItem(ScheduleAtEachCycle, [&] { dataInterface->ClearTimeStep(); }),
          SyncEnvironmentTaskItem(ScheduleAtEachCycle, [&] { environment->SyncWorld(); }),
          SyncEnvironmentTaskItem(ScheduleAtEachCycle, [&] { environment->ResetControlStrategyStatus(); })};
}

std::vector<TaskItem> TaskBuilder::CreateFinalizeTasks()
{
  return {CollisionDetectorTaskItem(ScheduleAtEachCycle, [&] { collisionDetection->Trigger(currentTime); })};
}

}  // namespace core::scheduling
