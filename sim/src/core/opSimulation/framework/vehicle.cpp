/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "vehicle.h"

#include "agent.h"

namespace core
{
Vehicle::Vehicle(mantle_api::UniqueId id,
                 const std::string &name,
                 std::shared_ptr<mantle_api::VehicleProperties> properties,
                 const RouteSamplerInterface *routeSampler,
                 AgentCategory category)
    : Entity(id, name, routeSampler, properties, category), properties(properties)
{
}

void Vehicle::SetProperties(std::unique_ptr<mantle_api::EntityProperties> properties) {}

mantle_api::VehicleProperties *Vehicle::GetProperties() const
{
  return properties.get();
}

void Vehicle::SetIndicatorState(mantle_api::IndicatorState state)
{
  indicatorState = state;
}

mantle_api::IndicatorState Vehicle::GetIndicatorState() const
{
  return indicatorState;
}
}  // namespace core
