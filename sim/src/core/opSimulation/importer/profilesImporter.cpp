/********************************************************************************
 * Copyright (c) 2019-2020 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "profilesImporter.h"

#include "common/sensorDefinitions.h"
#include "importerLoggingHelper.h"

using namespace Importer;
using namespace SimulationCommon;

namespace TAG = openpass::importer::xml::profilesImporter::tag;
namespace ATTRIBUTE = openpass::importer::xml::profilesImporter::attribute;

void ProfilesImporter::ImportAgentProfiles(QDomElement agentProfilesElement, Profiles& profiles)
{
  QDomElement agentProfileElement;
  ThrowIfFalse(GetFirstChildElement(agentProfilesElement, TAG::agentProfile, agentProfileElement),
               agentProfilesElement,
               "Tag " + std::string(TAG::agentProfile) + " is missing.");

  while (!agentProfileElement.isNull())
  {
    std::string agentProfileName;
    ThrowIfFalse(ParseAttributeString(agentProfileElement, ATTRIBUTE::name, agentProfileName),
                 agentProfileElement,
                 "Attribute " + std::string(ATTRIBUTE::name) + " is missing.");

    std::string profileType;
    ThrowIfFalse(ParseAttributeString(agentProfileElement, ATTRIBUTE::type, profileType),
                 agentProfileElement,
                 "Attribute " + std::string(ATTRIBUTE::type) + " is missing.");

    AgentProfile agentProfile;

    if (profileType == "Dynamic")
    {
      agentProfile.type = AgentProfileType::Dynamic;

      //Parses all driver profiles
      QDomElement driverProfilesElement;
      ThrowIfFalse(GetFirstChildElement(agentProfileElement, TAG::driverProfiles, driverProfilesElement),
                   agentProfileElement,
                   "Tag " + std::string(TAG::driverProfiles) + " is missing.");
      ThrowIfFalse(ImportProbabilityMap(driverProfilesElement,
                                        ATTRIBUTE::name,
                                        TAG::driverProfile,
                                        agentProfile.driverProfiles,
                                        LogErrorAndThrow),
                   driverProfilesElement,
                   "Invalid probalities");

      //Parses all vehicle profiles
      QDomElement systemProfilesElement;
      ThrowIfFalse(GetFirstChildElement(agentProfileElement, TAG::systemProfiles, systemProfilesElement),
                   agentProfileElement,
                   "Tag " + std::string(TAG::systemProfiles) + " is missing.");
      ThrowIfFalse(ImportProbabilityMap(systemProfilesElement,
                                        ATTRIBUTE::name,
                                        TAG::systemProfile,
                                        agentProfile.systemProfiles,
                                        LogErrorAndThrow),
                   systemProfilesElement,
                   "Invalid probalities");

      //Parses all vehicle profiles
      QDomElement vehicleModelsElement;
      ThrowIfFalse(GetFirstChildElement(agentProfileElement, TAG::vehicleModels, vehicleModelsElement),
                   agentProfileElement,
                   "Tag " + std::string(TAG::vehicleModels) + " is missing.");
      ThrowIfFalse(
          ImportProbabilityMap(
              vehicleModelsElement, ATTRIBUTE::name, TAG::vehicleModel, agentProfile.vehicleModels, LogErrorAndThrow),
          vehicleModelsElement,
          "Invalid probalities");
    }
    else
    {
      ThrowIfFalse(profileType == "Static", "Invalid agent profile type.");

      agentProfile.type = AgentProfileType::Static;
      QDomElement systemElement;
      ThrowIfFalse(GetFirstChildElement(agentProfileElement, TAG::system, systemElement),
                   agentProfileElement,
                   "Tag " + std::string(TAG::system) + " is missing.");

      std::string systemConfigFile;
      ThrowIfFalse(ParseString(systemElement, ATTRIBUTE::file, systemConfigFile),
                   systemElement,
                   "Attribute " + std::string(ATTRIBUTE::file) + " is missing.");

      agentProfile.systemConfigFile = systemConfigFile;

      int systemId;
      ThrowIfFalse(ParseInt(systemElement, ATTRIBUTE::id, systemId),
                   systemElement,
                   "Attribute " + std::string(ATTRIBUTE::id) + " is missing.");

      agentProfile.systemId = systemId;

      std::string vehicleModel;
      ThrowIfFalse(ParseString(agentProfileElement, ATTRIBUTE::vehicleModel, vehicleModel),
                   agentProfileElement,
                   "Attribute " + std::string(ATTRIBUTE::vehicleModel) + " is missing.");

      agentProfile.vehicleModel = vehicleModel;
    }

    ThrowIfFalse(profiles.AddAgentProfile(agentProfileName, agentProfile),
                 agentProfileElement,
                 "AgentProfile names need to be unique.");

    agentProfileElement = agentProfileElement.nextSiblingElement(TAG::agentProfile);
  }
}

void ProfilesImporter::ImportProfileGroups(Profiles& profiles, QDomElement& profilesElement)
{
  QDomElement profileGroupElement;
  ThrowIfFalse(GetFirstChildElement(profilesElement, TAG::profileGroup, profileGroupElement),
               "ProfileGroup element is missing.");

  while (!profileGroupElement.isNull())
  {
    std::string profileType;
    ThrowIfFalse(ParseAttributeString(profileGroupElement, ATTRIBUTE::type, profileType),
                 profileGroupElement,
                 "Attribute " + std::string(ATTRIBUTE::type) + " is missing.");

    QDomElement profileElement;
    GetFirstChildElement(profileGroupElement, TAG::profile, profileElement);

    while (!profileElement.isNull())
    {
      std::string profileName;
      ThrowIfFalse(ParseAttributeString(profileElement, ATTRIBUTE::name, profileName),
                   profileElement,
                   "Attribute " + std::string(ATTRIBUTE::name) + " is missing.");

      openpass::parameter::ParameterSetLevel1 parameters{};
      try
      {
        parameters = openpass::parameter::Import(profileElement, profilesElement);
      }
      catch (const std::runtime_error& error)
      {
        LogErrorAndThrow("Could not import spawner profile parameters: " + std::string(error.what()));
      }

      ThrowIfFalse(profiles.AddProfileGroup(profileType, profileName, parameters),
                   profileElement,
                   "Profile names need to be unique within a ProfileGroup");

      profileElement = profileElement.nextSiblingElement(TAG::profile);
    }
    profileGroupElement = profileGroupElement.nextSiblingElement(TAG::profileGroup);
  }
}

void ProfilesImporter::ImportSensorLinksOfComponent(QDomElement sensorLinksElement,
                                                    std::vector<SensorLink>& sensorLinks)
{
  QDomElement sensorLinkElement;
  GetFirstChildElement(sensorLinksElement, TAG::sensorLink, sensorLinkElement);

  while (!sensorLinkElement.isNull())
  {
    int sensorId;
    std::string inputId;

    ThrowIfFalse(ParseAttributeInt(sensorLinkElement, ATTRIBUTE::sensorId, sensorId),
                 sensorLinkElement,
                 "Attribute " + std::string(ATTRIBUTE::sensorId) + " is missing.");
    ThrowIfFalse(ParseAttributeString(sensorLinkElement, ATTRIBUTE::inputId, inputId),
                 sensorLinkElement,
                 "Attribute " + std::string(ATTRIBUTE::inputId) + " is missing.");

    SensorLink sensorLink{};
    sensorLink.sensorId = sensorId;
    sensorLink.inputId = inputId;
    sensorLinks.push_back(sensorLink);

    sensorLinkElement = sensorLinkElement.nextSiblingElement(TAG::sensorLink);
  }
}

void ProfilesImporter::ImportVehicleComponent(QDomElement vehicleComponentElement, VehicleComponent& vehicleComponent)
{
  ThrowIfFalse(ParseAttributeString(vehicleComponentElement, ATTRIBUTE::type, vehicleComponent.type),
               vehicleComponentElement,
               "Attribute " + std::string(ATTRIBUTE::type) + " is missing.");

  QDomElement profilesElement;
  ThrowIfFalse(GetFirstChildElement(vehicleComponentElement, TAG::profiles, profilesElement),
               vehicleComponentElement,
               "Tag " + std::string(TAG::profiles) + " is missing.");
  ThrowIfFalse(
      ImportProbabilityMap(
          profilesElement, ATTRIBUTE::name, "Profile", vehicleComponent.componentProfiles, LogErrorAndThrow, false),
      profilesElement,
      "Attribute " + std::string(ATTRIBUTE::name) + " is missing.");

  QDomElement sensorLinksElement;
  GetFirstChildElement(vehicleComponentElement, TAG::sensorLinks, sensorLinksElement);
  ImportSensorLinksOfComponent(sensorLinksElement, vehicleComponent.sensorLinks);
}

void ProfilesImporter::ImportAllVehicleComponentsOfSystemProfile(QDomElement systemProfileElement,
                                                                 SystemProfile& systemProfile)
{
  QDomElement vehicleComponentsElement;
  ThrowIfFalse(GetFirstChildElement(systemProfileElement, TAG::components, vehicleComponentsElement),
               systemProfileElement,
               "Tag " + std::string(TAG::components) + " is missing.");

  QDomElement componentElement;
  GetFirstChildElement(vehicleComponentsElement, TAG::component, componentElement);

  while (!componentElement.isNull())
  {
    VehicleComponent vehicleComponent;
    ImportVehicleComponent(componentElement, vehicleComponent);

    systemProfile.vehicleComponents.push_back(vehicleComponent);
    componentElement = componentElement.nextSiblingElement(TAG::component);
  }
}

void ProfilesImporter::ImportSensorParameters(QDomElement sensorElement, openpass::sensors::Parameter& sensor)
{
  ThrowIfFalse(ParseAttributeInt(sensorElement, ATTRIBUTE::id, sensor.id),
               sensorElement,
               "Attribute " + std::string(ATTRIBUTE::id) + " is missing.");
  ThrowIfFalse(ParseAttributeString(sensorElement, ATTRIBUTE::position, sensor.positionName),
               sensorElement,
               "Attribute " + std::string(ATTRIBUTE::position) + " is missing.");

  QDomElement profileElement;
  ThrowIfFalse(GetFirstChildElement(sensorElement, TAG::profile, profileElement),
               sensorElement,
               "Tag " + std::string(TAG::profile) + " is missing.");
  ThrowIfFalse(ParseAttributeString(profileElement, ATTRIBUTE::type, sensor.profile.type),
               profileElement,
               "Attribute " + std::string(ATTRIBUTE::type) + " is missing.");
  ThrowIfFalse(ParseAttributeString(profileElement, ATTRIBUTE::name, sensor.profile.name),
               profileElement,
               "Attribute " + std::string(ATTRIBUTE::name) + " is missing.");
}

void ProfilesImporter::ImportAllSensorsOfSystemProfile(QDomElement systemProfileElement, SystemProfile& systemProfile)
{
  QDomElement sensorsElement;
  ThrowIfFalse(GetFirstChildElement(systemProfileElement, TAG::sensors, sensorsElement),
               systemProfileElement,
               "Tag " + std::string(TAG::sensors) + " is missing.");

  QDomElement sensorElement;
  GetFirstChildElement(sensorsElement, TAG::sensor, sensorElement);

  while (!sensorElement.isNull())
  {
    openpass::sensors::Parameter sensor;
    ImportSensorParameters(sensorElement, sensor);

    systemProfile.sensors.push_back(sensor);
    sensorElement = sensorElement.nextSiblingElement(TAG::sensor);
  }
}

SystemProfile ProfilesImporter::ImportSystemProfile(QDomElement systemProfileElement)
{
  SystemProfile systemProfile;

  ImportAllVehicleComponentsOfSystemProfile(systemProfileElement, systemProfile);
  ImportAllSensorsOfSystemProfile(systemProfileElement, systemProfile);

  return systemProfile;
}

void ProfilesImporter::ImportSystemProfiles(QDomElement systemProfilesElement, Profiles& profiles)
{
  QDomElement systemProfileElement;
  GetFirstChildElement(systemProfilesElement, TAG::systemProfile, systemProfileElement);

  while (!systemProfileElement.isNull())
  {
    std::string profileName;
    ThrowIfFalse(ParseAttributeString(systemProfileElement, ATTRIBUTE::name, profileName),
                 systemProfileElement,
                 "Attribute " + std::string(ATTRIBUTE::name) + " is missing.");

    auto systemProfile = ImportSystemProfile(systemProfileElement);
    profiles.AddSystemProfile(profileName, systemProfile);

    systemProfileElement = systemProfileElement.nextSiblingElement(TAG::systemProfile);
  }
}

bool ProfilesImporter::Import(const std::string& filename, Profiles& profiles)
{
  try
  {
    std::locale::global(std::locale("C"));

    QFile xmlFile(filename.c_str());  // automatic object will be closed on destruction
    ThrowIfFalse(xmlFile.open(QIODevice::ReadOnly), "an error occurred during profilesCatalog import");

    QByteArray xmlData(xmlFile.readAll());
    QDomDocument document;
    QString errorMsg{};
    int errorLine{};
    ThrowIfFalse(
        document.setContent(xmlData, &errorMsg, &errorLine),
        "Invalid xml format (" + filename + ") in line " + std::to_string(errorLine) + ": " + errorMsg.toStdString());

    ImporterCommon::validateProfilesCatalogXMLSchema(xmlFile.fileName(), xmlData);

    QDomElement documentRoot = document.documentElement();
    ThrowIfFalse(!documentRoot.isNull(), "invalid document root " + filename);

    std::string configVersion;
    ParseAttributeString(documentRoot, ATTRIBUTE::schemaVersion, configVersion);
    ThrowIfFalse(
        configVersion.compare(supportedConfigVersion) == 0,
        "ProfilesCatalog version is not supported. Supported version is " + std::string(supportedConfigVersion));

    QDomElement agentProfilesElement;
    ThrowIfFalse(GetFirstChildElement(documentRoot, TAG::agentProfiles, agentProfilesElement),
                 "AgentProfiles element is missing.");
    ImportAgentProfiles(agentProfilesElement, profiles);

    QDomElement systemProfilesElement;
    if (GetFirstChildElement(documentRoot, TAG::systemProfiles, systemProfilesElement))
    {
      ImportSystemProfiles(systemProfilesElement, profiles);
    }

    ImportProfileGroups(profiles, documentRoot);

    return true;
  }
  catch (std::runtime_error& e)
  {
    LOG_INTERN(LogLevel::Error) << "Profiles import failed: " + std::string(e.what());
    return false;
  }
}
