/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  scenarioControl.cpp
//! @brief This class holds all control strategies for a single entity
//-----------------------------------------------------------------------------

#include "scenarioControl.h"

#include <stdexcept>

std::vector<std::shared_ptr<mantle_api::ControlStrategy>> ScenarioControl::GetStrategies(
    mantle_api::ControlStrategyType type)
{
  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> result{};

  for (auto strategy : strategies)
  {
    if (type == strategy->type)
    {
      result.push_back(strategy);
    }
  }

  return result;
}

/// @brief Function to compare movement domains
/// @param lhs First Movement domain
/// @param rhs Another movement domain
/// @return True, if the movement domains are equal
bool ScenarioControl::CorrespondentMovementDomain(mantle_api::MovementDomain lhs, mantle_api::MovementDomain rhs)
{
  switch (lhs)
  {
    case mantle_api::MovementDomain::kUndefined:
      return false;
    case mantle_api::MovementDomain::kNone:
      return rhs == mantle_api::MovementDomain::kNone;
    case mantle_api::MovementDomain::kLateral:
      return rhs == mantle_api::MovementDomain::kLateral || rhs == mantle_api::MovementDomain::kBoth;
    case mantle_api::MovementDomain::kLongitudinal:
      return rhs == mantle_api::MovementDomain::kLongitudinal || rhs == mantle_api::MovementDomain::kBoth;
    case mantle_api::MovementDomain::kBoth:
      return rhs == mantle_api::MovementDomain::kLateral || rhs == mantle_api::MovementDomain::kLongitudinal
          || rhs == mantle_api::MovementDomain::kBoth;
  }
  throw std::runtime_error("ScenarioControl: Unable to evaluate CorrespondentMovementDomain");
}

std::vector<std::shared_ptr<mantle_api::ControlStrategy>> ScenarioControl::GetStrategies(
    mantle_api::MovementDomain domain)
{
  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> result{};

  for (auto strategy : strategies)
  {
    if (CorrespondentMovementDomain(domain, strategy->movement_domain))
    {
      result.push_back(strategy);
    }
  }

  return result;
}

void ScenarioControl::SetStrategies(std::vector<std::shared_ptr<mantle_api::ControlStrategy>> strategies)
{
  bool affectsLateral = false;
  bool affectsLongitudinal = false;
  for (const auto strategy : strategies)
  {
    reachedStrategies.erase(strategy->type);
    switch (strategy->movement_domain)
    {
      case mantle_api::MovementDomain::kLateral:
        affectsLateral = true;
        break;
      case mantle_api::MovementDomain::kLongitudinal:
        affectsLongitudinal = true;
        break;
      case mantle_api::MovementDomain::kBoth:
        affectsLateral = true;
        affectsLongitudinal = true;
        break;
      default:
        break;
    }
  }
  for (const auto oldStrategy : this->strategies)
  {
    switch (oldStrategy->movement_domain)
    {
      case mantle_api::MovementDomain::kLateral:
        if (!affectsLateral)
        {
          strategies.push_back(oldStrategy);
        }
        break;
      case mantle_api::MovementDomain::kLongitudinal:
        if (!affectsLongitudinal)
        {
          strategies.push_back(oldStrategy);
        }
        break;
      case mantle_api::MovementDomain::kBoth:
        if (!affectsLateral && !affectsLongitudinal)
        {
          strategies.push_back(oldStrategy);
        }
        break;
      default:
        strategies.push_back(oldStrategy);
        break;
    }
  }
  this->strategies = strategies;
  newLongitudinalStrategy = newLongitudinalStrategy || affectsLongitudinal;
  newLateralStrategy = newLateralStrategy || affectsLateral;
}

void ScenarioControl::SetControlStrategyGoalReached(mantle_api::ControlStrategyType type)
{
  reachedStrategies.insert(type);
}

bool ScenarioControl::HasControlStrategyGoalBeenReached(mantle_api::ControlStrategyType type) const
{
  return reachedStrategies.count(type) == 1;
}

const std::vector<std::string>& ScenarioControl::GetCustomCommands()
{
  return customCommands;
}

void ScenarioControl::AddCustomCommand(const std::string& command)
{
  customCommands.push_back(command);
}

void ScenarioControl::UpdateForNextTimestep()
{
  customCommands.clear();
  newLongitudinalStrategy = false;
  newLateralStrategy = false;
}

void ScenarioControl::AddCommand(ScenarioCommand::Any command)
{
  uint64_t counter = 0;
  if (auto scenario_command = std::get_if<ScenarioCommand::AssignRoute>(&command))
  {
    for (const auto& existingCommand : commands)
    {
      if (auto existingScenarioCommand = std::get_if<ScenarioCommand::AssignRoute>(&existingCommand))
      {
        if (existingScenarioCommand->id > counter)
        {
          counter = existingScenarioCommand->id;
        }
      }
    }
    scenario_command->id = scenario_command->id + 1;
  }

  commands.push_back(std::move(command));
}

const ScenarioCommands& ScenarioControl::GetCommands()
{
  return commands;
}
