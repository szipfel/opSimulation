/********************************************************************************
 * Copyright (c) 2019-2021 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <string>
#include <vector>

#include "MantleAPI/Traffic/entity_helper.h"
#include "common/commonTools.h"
#include "framework/sampler.h"
#include "framework/vehicle.h"
#include "include/callbackInterface.h"
#include "include/entityRepositoryInterface.h"
#include "include/parameterInterface.h"

namespace SpawnPointDefinitions
{
constexpr char SPAWNPOINTS[] = {"SpawnPoints"};
constexpr char SPAWNZONES[] = {"SpawnZones"};
constexpr char ROADS[] = {"Roads"};
constexpr char LANES[] = {"Lanes"};
constexpr double DEFAULT_MINIMUM_SEPARATION_BUFFER = 5.0;

using RoadId = std::string;
using RoadIds = std::vector<RoadId>;
using LaneId = int;
using LaneIds = std::vector<LaneId>;
using SPosition = units::length::meter_t;
using Range = std::pair<SPosition, SPosition>;
using Ranges = std::vector<Range>;
using VehicleRearAndFrontCoordinates = std::pair<SPosition, SPosition>;

//! Defines a AgentProfile for spawning and its associated spawn parameters
struct SpawningAgentProfile
{
  const std::string name;  ///< Profile name of the spawing agent
  const openpass::parameter::StochasticDistribution
      velocity;  ///< Velocity of one of the StochasticDistribution as one of NormalDistribution, LogNormalDistribution,
                 ///< UniformDistribution, ExponentialDistribution, GammaDistribution
  const std::vector<double> homogeneities;  ///< velocity increase (multiplicative) between neighbouring lanes
  const openpass::parameter::StochasticDistribution tGap;  ///< time gap between to agents

  //! Comparison operator
  //!
  //! @param[in] other SpawningAgentProfile to compare to
  //! @return true if SpawningAgentProfiles are considered equal, false otherwise
  bool operator==(const SpawningAgentProfile& other) const
  {
    return this->name == other.name && this->velocity == other.velocity && this->homogeneities == other.homogeneities
        && this->tGap == other.tGap;
  }
};

using AgentProfiles = std::vector<std::pair<SpawningAgentProfile, double>>;

//! Defines possible AgentProfiles for a SpawnPoint
struct AgentProfileLaneMaps
{
  AgentProfiles leftLanes;   ///< Agent profile of the left lane
  AgentProfiles rightLanes;  ///< Agent profile of the right lane
};

#define SPAWNER_THROWIFFALSE(success, message)                       \
  if (!(success))                                                    \
  {                                                                  \
    callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, message); \
    throw std::runtime_error(message);                               \
  }

//! Read the (fixed or stochastic) MinimumSeparationBuffer parameter or sets it to the default value if not defined
//!
//! \param parameter SpawnPointParameter
//! \return MinimumSeparationBuffer
static std::variant<double, openpass::parameter::StochasticDistribution> ExtractMinimumSpawnBuffer(
    const ParameterInterface& parameter)
{
  const auto& separationBufferStochastic
      = helper::map::query(parameter.GetParametersStochastic(), "MinimumSeparationBuffer");
  if (separationBufferStochastic.has_value())
  {
    return separationBufferStochastic.value();
  }
  const auto& separationBufferDouble = helper::map::query(parameter.GetParametersDouble(), "MinimumSeparationBuffer");
  return separationBufferDouble.value_or(DEFAULT_MINIMUM_SEPARATION_BUFFER);
}

//! Reads the possible AgentProfiles and their spawn parameters from the Spawner profile
//!
//! \param parameter    Spawner profile
//! \param callbacks    CallbackInterface for logging
//! \return AgentProfile
static AgentProfileLaneMaps ExtractAgentProfileLaneMaps(const ParameterInterface& parameter,
                                                        const CallbackInterface* callbacks)
{
  using namespace helper;

  AgentProfileLaneMaps agentProfileLaneMaps;

  const auto& trafficGroupsList = map::query(parameter.GetParameterLists(), "TrafficGroups");

  for (const auto& trafficGroupParameter : trafficGroupsList.value())
  {
    const auto weightOfGroup = map::query(trafficGroupParameter->GetParametersDouble(), "Weight");
    const auto velocity = map::query(trafficGroupParameter->GetParametersStochastic(), "Velocity");
    const auto homogeneity = map::query(trafficGroupParameter->GetParametersDoubleVector(), "Homogeneity");
    const auto tGap = map::query(trafficGroupParameter->GetParametersStochastic(), "TGap");
    const auto rightLaneOnly = map::query(trafficGroupParameter->GetParametersBool(), "RightLaneOnly");

    const auto& agentProfilesList = map::query(trafficGroupParameter->GetParameterLists(), "AgentProfiles");

    SPAWNER_THROWIFFALSE(velocity.has_value(), "No velocity provided in TrafficGroup for SpawnerRuntimeCommon")
    SPAWNER_THROWIFFALSE(tGap.has_value(), "No t gap provided in TrafficGroup for SpawnerRuntimeCommon")
    SPAWNER_THROWIFFALSE(agentProfilesList.has_value(),
                         "No AgentProfile provided in TrafficGroup for SpawnerRuntimeCommon")
    for (const auto& agentProfileParameter : agentProfilesList.value())
    {
      const auto weightOfProfile = map::query(agentProfileParameter->GetParametersDouble(), "Weight");
      const auto name = map::query(agentProfileParameter->GetParametersString(), "Name");
      SPAWNER_THROWIFFALSE(name.has_value(), "No AgentProfile name provided in TrafficGroup for SpawnerRuntimeCommon")
      agentProfileLaneMaps.rightLanes.push_back(std::make_pair(
          SpawningAgentProfile{
              name.value(), velocity.value(), homogeneity.value_or(std::vector<double>{1.0}), tGap.value()},
          weightOfGroup.value_or(1.0) * weightOfProfile.value_or(1.0)));
      if (!rightLaneOnly.value_or(false))
      {
        agentProfileLaneMaps.leftLanes.push_back(std::make_pair(
            SpawningAgentProfile{
                name.value(), velocity.value(), homogeneity.value_or(std::vector<double>{1.0}), tGap.value()},
            weightOfGroup.value_or(1.0) * weightOfProfile.value_or(1.0)));
      }
    }
  }

  return agentProfileLaneMaps;
}

static double GetStochasticOrPredefinedValue(std::variant<double, openpass::parameter::StochasticDistribution> value,
                                             StochasticsInterface* stochastics)
{
  if (std::holds_alternative<openpass::parameter::StochasticDistribution>(value))
  {
    return Sampler::RollForStochasticAttribute(std::get<openpass::parameter::StochasticDistribution>(value),
                                               stochastics);
  }
  else
  {
    return std::get<double>(value);
  }
}

//! Contains all information of the entity
struct EntityInformation
{
  //! @brief EntityInformation constructor
  //!
  //! @param[in] agentProfileName     Name of the AgentProfile
  //! @param[in] entityName           Name of the entity
  //! @param[in] vehicleProperties    Vehicle properties
  //! @param[in] spawnParameter       Spawn parameter
  EntityInformation(std::string agentProfileName,
                    std::string entityName,
                    std::shared_ptr<const mantle_api::VehicleProperties> vehicleProperties,
                    SpawnParameter spawnParameter = {})
      : agentProfileName(agentProfileName),
        entityName(entityName),
        vehicleProperties(vehicleProperties),
        spawnParameter(spawnParameter)
  {
  }

  std::string agentProfileName;                                            //!< Name of the AgentProfile
  std::string entityName;                                                  //!< Name of the entity
  std::shared_ptr<const mantle_api::VehicleProperties> vehicleProperties;  //!< Vehicle properties
  SpawnParameter spawnParameter{};                                         //!< Spawn parameter
};

static mantle_api::UniqueId CreateSpawnReadyEntity(const EntityInformation& entityInformation,
                                                   std::shared_ptr<mantle_api::IEnvironment> environment)
{
  auto controllerConfig = std::make_unique<mantle_api::ExternalControllerConfig>();
  controllerConfig->parameters.insert({"AgentProfile", entityInformation.agentProfileName});

  // TODO CK name is still missing. Empty for now. Therefore the Get can't work either
  auto& entityRepository{dynamic_cast<core::EntityRepositoryInterface&>(environment->GetEntityRepository())};
  auto& entity = entityRepository.CreateCommon(*(entityInformation.vehicleProperties));
  auto& controller = environment->GetControllerRepository().Create(std::move(controllerConfig));

  environment->AddEntityToController(entity, controller.GetUniqueId());
  controller.ChangeState(mantle_api::IController::LateralState::kActivate, mantle_api::IController::LongitudinalState::kActivate);
  auto localPosition = environment->GetGeometryHelper()->TranslateGlobalPositionLocally(
      entityInformation.spawnParameter.position,
      entityInformation.spawnParameter.orientation,
      entityInformation.vehicleProperties->bounding_box.geometric_center);
  entity.SetPosition(localPosition);
  entity.SetOrientation(entityInformation.spawnParameter.orientation);
  mantle_api::SetSpeed(&entity, entityInformation.spawnParameter.velocity);

  return entity.GetUniqueId();
}

}  // namespace SpawnPointDefinitions
