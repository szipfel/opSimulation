/********************************************************************************
 * Copyright (c) 2019-2021 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include "SpawnerDefinitions.h"
#include "include/agentBlueprintInterface.h"
#include "include/stochasticsInterface.h"
#include "include/worldInterface.h"

using namespace SpawnPointDefinitions;

enum class Direction
{
  FORWARD = 0,
  BACKWARD
};

//! This class provides utility functions for spawners to query information about the world
class WorldAnalyzer
{
public:
  /**
   * @brief WorldAnalyzer constructor
   *
   * @param[in] world     The world interface
   */
  WorldAnalyzer(WorldInterface* const world) : world{world}, loggingCallback([](const std::string&) {}) {}

  /// @brief  WorldAnalyzer constructor
  /// @param world                The world interface
  /// @param loggingCallback      Function for logging callback
  WorldAnalyzer(WorldInterface* const world, std::function<void(const std::string&)> loggingCallback)
      : world{world}, loggingCallback(loggingCallback)
  {
  }

  WorldAnalyzer(const WorldAnalyzer&) = delete;
  WorldAnalyzer(WorldAnalyzer&&) = delete;
  WorldAnalyzer& operator=(const WorldAnalyzer&) = delete;
  WorldAnalyzer& operator=(WorldAnalyzer&&) = delete;
  ~WorldAnalyzer() = default;

  //! Returns the ranges where the spawner can spawn common ranges.
  //! It excludes s positions where a scenario agent is standing as well as the range
  //! inbetween two scenario agents.
  //!
  //! \param laneStream           LaneStream to spawn in
  //! \param sStart               start of the search range
  //! \param sEnd                 end of the search range
  //! \param supportedLaneTypes   Container of all valid lanetypes
  //! \return valid spawning ranges
  Ranges GetValidLaneSpawningRanges(const std::unique_ptr<LaneStreamInterface>& laneStream,
                                    const SPosition sStart,
                                    const SPosition sEnd,
                                    const LaneTypes& supportedLaneTypes) const;

  //! Calculates the next s coordinate to spawn an agent on
  //!
  //! \param laneStream               LaneStream to spawn in
  //! \param bounds                   s range to spawn in
  //! \param agentFrontLength         distance from reference point to front of agent
  //! \param agentRearLength          distance from reference point to rear of agent
  //! \param intendedVelocity         spawning velocity
  //! \param gapInSeconds             desired TGap between spawned agent and next agent
  //! \param minimumSeparationBuffer  minimum distance between agents
  //! \return s position or nullopt if this would be outside the range
  std::optional<units::length::meter_t> GetNextSpawnPosition(
      const std::unique_ptr<LaneStreamInterface>& laneStream,
      const Range& bounds,
      const units::length::meter_t agentFrontLength,
      const units::length::meter_t agentRearLength,
      const units::velocity::meters_per_second_t intendedVelocity,
      const units::time::second_t gapInSeconds,
      const units::length::meter_t minimumSeparationBuffer) const;

  //! Calculates the adjusted spawn distance such that the minimum ttc to the end of lane is not violated
  //!
  //! \param laneId                   lane to spawn
  //! \param sCoordinate              s coordinate on road
  //! \param intendedSpawnPosition    initially calcuted spawn distance on stream
  //! \param intendedVelocity         velocity of the agent
  //! \param route                    route of the agent
  //! \param supportedLaneTypes   Container of all valid lanetypes
  //! \return adjusted spawn distance
  units::length::meter_t CalculateAdjustedSpawnDistanceToEndOfLane(
      const LaneId laneId,
      const units::length::meter_t sCoordinate,
      const units::length::meter_t intendedSpawnPosition,
      const units::velocity::meters_per_second_t intendedVelocity,
      const Route& route,
      const LaneTypes& supportedLaneTypes) const;

  //! Adjust spawning velocity so that the spawned agent won't immediately crash.
  //!
  //! \param laneId                   Id of the lane
  //! \param intendedSpawnPosition    s coordinate
  //! \param agentFrontLength         distance from reference point to front of agent
  //! \param agentRearLength          distance from reference point to rear of agent
  //! \param intendedVelocity         spawning velocity
  //! \param route                    initial route of the agent
  //! \return possibly adjusted velocity
  units::velocity::meters_per_second_t CalculateSpawnVelocityToPreventCrashing(
      const LaneId laneId,
      const units::length::meter_t intendedSpawnPosition,
      const units::length::meter_t agentFrontLength,
      const units::length::meter_t agentRearLength,
      const units::velocity::meters_per_second_t intendedVelocity,
      const Route& route) const;

  //! Adjust spawning velocity so that the spawned agent won't immediately crash.
  //!
  //! \param laneStream               LaneStream to spawn in
  //! \param intendedSpawnPosition    s coordinate
  //! \param agentFrontLength         distance from reference point to front of agent
  //! \param agentRearLength          distance from reference point to rear of agent
  //! \param intendedVelocity         spawning velocity
  //! \return possibly adjusted velocity
  units::velocity::meters_per_second_t CalculateSpawnVelocityToPreventCrashing(
      const std::unique_ptr<LaneStreamInterface>& laneStream,
      const units::length::meter_t intendedSpawnPosition,
      const units::length::meter_t agentFrontLength,
      const units::length::meter_t agentRearLength,
      const units::velocity::meters_per_second_t intendedVelocity) const;

  //! Check whether the minimum distance the next object is satisfied
  //!
  //! \param laneId                   Id of the lane
  //! \param sPosition                s coordinate of own agent
  //! \param route                    initial route of the agent
  //! \param vehicleModelParameters   parameters of the vehicle model
  //! \param minimumSeparationBuffer  minimum distance between agents
  //! \return true if minimum distance is met
  bool ValidMinimumSpawningDistanceToObjectInFront(
      const LaneId laneId,
      const SPosition sPosition,
      const Route& route,
      std::shared_ptr<const mantle_api::VehicleProperties> vehicleModelParameters,
      const units::length::meter_t minimumSeparationBuffer) const;

  //! Check if it is allowed the spawn an agent at the given coordinate
  //!
  //! \param roadId                   Id of the road
  //! \param laneId                   Id of the lane
  //! \param sPosition                s coordinate of own agent
  //! \param offset                   lateral position in lane
  //! \param minimumSeparationBuffer  minimum distance between agents
  //! \param route                    initial route of the agent
  //! \param vehicleModelParameters   parameters of the vehicle model
  //! \return true if spawning coordinates are valid
  bool AreSpawningCoordinatesValid(const RoadId& roadId,
                                   const LaneId laneId,
                                   const SPosition sPosition,
                                   const units::length::meter_t offset,
                                   const units::length::meter_t minimumSeparationBuffer,
                                   const Route& route,
                                   std::shared_ptr<const mantle_api::VehicleProperties> vehicleModelParameters) const;

  //! Check whether spawning an agent with given parameters will result in a crash
  //!
  //! \param laneStream               LaneStream to spawn in
  //! \param sPosition                s coordinate
  //! \param agentFrontLength         Distance from reference point to front of agent
  //! \param agentRearLength          Distance from reference point to rear of agent
  //! \param velocity                 Spawning velocity
  //! \param direction                Lane direction
  //! \return true if spawning would cause a crash
  bool SpawnWillCauseCrash(const std::unique_ptr<LaneStreamInterface>& laneStream,
                           const SPosition sPosition,
                           const units::length::meter_t agentFrontLength,
                           const units::length::meter_t agentRearLength,
                           const units::velocity::meters_per_second_t velocity,
                           const Direction direction) const;

  //! Returns the number of lanes to the right of the given lane at the given s coordinate
  //!
  //! \param roadId       Id of the road
  //! \param laneId       Id of the lane
  //! \param sPosition    s coordinate
  //! \return number of lanes to the right
  size_t GetRightLaneCount(const RoadId& roadId, const LaneId& laneId, const units::length::meter_t sPosition) const;

  //! Returns the number of lanes to the right the given lane at the given s coordinate
  //!
  //! \param laneStream   LaneStream to spawn in
  //! \param sPosition    s coordinate
  //! \return number of lanes to the right
  size_t GetRightLaneCount(const std::unique_ptr<LaneStreamInterface>& laneStream,
                           const units::length::meter_t sPosition) const;

  //! Checks if a roadId exists in direction of a laneId
  //!
  //! \param roadId       Id of the road
  //! \param laneId       Id of the lane
  //! \param distanceOnLane   s Coordinate along road
  //! \param validLaneTypes   Container of all valid lane types
  //!
  //! \return true, if roadId exists in laneId direction
  bool ValidateRoadIdInDirection(const RoadId& roadId,
                                 const LaneId laneId,
                                 const units::length::meter_t distanceOnLane,
                                 const LaneTypes& validLaneTypes) const;

  //! Randomly generates a route based on a starting roadId
  //!
  //! \param roadId       Id of the road
  //! \param laneId       Id of the lane
  //! \param stochastics  Stochastics module which randomizes the route
  //! \return route
  Route SampleRoute(const RoadId& roadId, const LaneId laneId, StochasticsInterface* stochastics) const;

  //! Convert the route into mantle api format
  //!
  //! \param route       Route in openPASS format
  //! \return routeDefinition
  mantle_api::RouteDefinition ConvertToMantleRouteWayPoints(Route route) const;

private:
  static Ranges GetValidSpawningInformationForRange(const units::length::meter_t sStart,
                                                    const units::length::meter_t sEnd,
                                                    const units::length::meter_t firstScenarioAgentSPosition,
                                                    const units::length::meter_t lastScenarioAgentSPosition);

  bool IsOffsetValidForLane(const RoadId& roadId,
                            const LaneId laneId,
                            const SPosition distanceFromStart,
                            const units::length::meter_t offset,
                            const units::length::meter_t vehicleWidth) const;

  static std::vector<RouteElement> SetWayToTargetRoute(RoadGraphVertex targetVertex,
                                                       RoadGraph roadGraph,
                                                       RoadGraphVertex current);

  WorldInterface* const world;
  const std::function<void(const std::string&)> loggingCallback;
};
