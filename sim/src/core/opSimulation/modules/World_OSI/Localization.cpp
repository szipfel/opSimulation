/********************************************************************************
 * Copyright (c) 2017-2021 in-tech GmbH
 *               2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "Localization.h"

#include <boost/function_output_iterator.hpp>
#include <exception>
#include <numeric>

namespace World
{
namespace Localization
{

std::function<void(const RTreeElement &)> LocateOnGeometryElement(
    const OWL::Interfaces::WorldData &worldData,
    const std::vector<Common::Vector2d<units::length::meter_t>> &objectBoundary,
    const Common::Vector2d<units::length::meter_t> &referencePoint,
    const units::angle::radian_t &hdg,
    LocatedObject &locatedObject)
{
  return [&](auto const &value)
  {
    const LocalizationElement &localizationElement = *value.second;

    auto intersection = CommonHelper::IntersectionCalculation::GetIntersectionPoints(
        localizationElement.polygon, objectBoundary, false);

    if (intersection.size() < 3)
    {
      //Actual polygons do not intersect -> Skip this GeometryElement
      return;
    }

    WorldToRoadCoordinateConverter converter(localizationElement);

    const OWL::Interfaces::Lane *lane = localizationElement.lane;
    const auto laneOdId = lane->GetOdId();
    const auto roadId = lane->GetRoad().GetId();

    if (converter.IsConvertible(referencePoint))
    {
      auto [s, t, yaw] = converter.GetRoadCoordinate(referencePoint, hdg);
      locatedObject.referencePoint[roadId] = GlobalRoadPosition(roadId, laneOdId, s, t, yaw);
    }

    for (const auto &point : intersection)
    {
      auto [s, t, yaw] = converter.GetRoadCoordinate(point, hdg);

      assert(s >= 0.0_m);

      auto &laneOverlap = locatedObject.laneOverlaps[lane];
      if (s < laneOverlap.sMin.roadPosition.s)
      {
        laneOverlap.sMin = GlobalRoadPosition(roadId, laneOdId, s, t, yaw);
      }
      if (s > laneOverlap.sMax.roadPosition.s)
      {
        laneOverlap.sMax = GlobalRoadPosition(roadId, laneOdId, s, t, yaw);
      }
      if (t < laneOverlap.tMin.roadPosition.t)
      {
        laneOverlap.tMin = GlobalRoadPosition(roadId, laneOdId, s, t, yaw);
      }
      if (t > laneOverlap.tMax.roadPosition.t)
      {
        laneOverlap.tMax = GlobalRoadPosition(roadId, laneOdId, s, t, yaw);
      }
    }
  };
}

LocatedObject LocateOnGeometryElements(const bg_rTree &rTree,
                                       const OWL::Interfaces::WorldData &worldData,
                                       const std::vector<Common::Vector2d<units::length::meter_t>> &agentBoundary,
                                       CoarseBoundingBox theAgent,
                                       const Common::Vector2d<units::length::meter_t> referencePoint,
                                       units::angle::radian_t hdg)
{
  LocatedObject locatedObject;
  rTree.query(bgi::intersects(theAgent),
              boost::make_function_output_iterator(
                  LocateOnGeometryElement(worldData, agentBoundary, referencePoint, hdg, locatedObject)));

  return locatedObject;
}

std::function<void(const RTreeElement &)> LocateOnGeometryElement(const OWL::Interfaces::WorldData &worldData,
                                                                  const Common::Vector2d<units::length::meter_t> &point,
                                                                  const units::angle::radian_t &hdg,
                                                                  GlobalRoadPositions &result)
{
  return [&](auto const &value)
  {
    const LocalizationElement &localizationElement = *value.second;

    boost::geometry::de9im::mask mask("*****F***");  // within
    if (!boost::geometry::relate(point_t{point.x.value(), point.y.value()},
                                 localizationElement.boost_polygon,
                                 mask))  //Check whether point is inside polygon
    {
      return;
    }

    WorldToRoadCoordinateConverter converter(localizationElement);

    const OWL::Interfaces::Lane *lane = localizationElement.lane;
    const auto laneOdId = lane->GetOdId();
    const auto roadId = lane->GetRoad().GetId();

    if (converter.IsConvertible(point))
    {
      auto [s, t, yaw] = converter.GetRoadCoordinate(point, hdg);
      result[roadId] = GlobalRoadPosition(roadId, laneOdId, s, t, yaw);
    }
  };
}

GlobalRoadPositions LocateOnGeometryElements(const bg_rTree &rTree,
                                             const OWL::Interfaces::WorldData &worldData,
                                             const Common::Vector2d<units::length::meter_t> point,
                                             units::angle::radian_t hdg)
{
  GlobalRoadPositions result;
  CoarseBoundingBox box = GetSearchBox({point});
  rTree.query(bgi::intersects(box),
              boost::make_function_output_iterator(LocateOnGeometryElement(worldData, point, hdg, result)));

  return result;
}

polygon_t GetBoundingBox(units::length::meter_t x,
                         units::length::meter_t y,
                         units::length::meter_t length,
                         units::length::meter_t width,
                         units::angle::radian_t rotation,
                         units::length::meter_t center)
{
  double halfWidth = units::unit_cast<double>(width / 2);

  point_t boxPoints[]{point_t{units::unit_cast<double>(center - length), -halfWidth},
                      point_t{units::unit_cast<double>(center - length), halfWidth},
                      point_t{units::unit_cast<double>(center), halfWidth},
                      point_t{units::unit_cast<double>(center), -halfWidth},
                      point_t{units::unit_cast<double>(center - length), -halfWidth}};

  polygon_t box;
  polygon_t boxTemp;
  bg::append(box, boxPoints);

  bt::translate_transformer<double, 2, 2> translate(units::unit_cast<double>(x), units::unit_cast<double>(y));

  // rotation in mathematical negativ order (boost) -> invert to match
  bt::rotate_transformer<bg::radian, double, 2, 2> rotate(-rotation.value());

  bg::transform(box, boxTemp, rotate);
  bg::transform(boxTemp, box, translate);

  return box;
}

void CreateLaneAssignments(OWL::Interfaces::WorldObject &object, const LocatedObject &locatedObject)
{
  for (auto [lane, laneOverlap] : locatedObject.laneOverlaps)
  {
    auto movingObject = dynamic_cast<OWL::MovingObject *>(&object);
    const auto referencePoint = helper::map::query(locatedObject.referencePoint, lane->GetRoad().GetId());
    if (movingObject)
    {
      const_cast<OWL::Interfaces::Lane *>(lane)->AddMovingObject(*movingObject, laneOverlap);
      object.AddLaneAssignment(
          *lane, referencePoint ? std::make_optional<RoadPosition>(referencePoint->roadPosition) : std::nullopt);
      continue;
    }

    auto stationaryObject = dynamic_cast<OWL::StationaryObject *>(&object);
    if (stationaryObject)
    {
      const_cast<OWL::Interfaces::Lane *>(lane)->AddStationaryObject(*stationaryObject, laneOverlap);
      object.AddLaneAssignment(
          *lane, referencePoint ? std::make_optional<RoadPosition>(referencePoint->roadPosition) : std::nullopt);
      continue;
    }
  }
}

Result Localizer::BuildResult(const LocatedObject &locatedObject) const
{
  std::set<int> touchedLaneIds;
  RoadIntervals touchedRoads;
  auto &roads = worldData.GetRoads();

  bool isOnRoute = !locatedObject.referencePoint.empty();

  for (const auto &[lane, laneOverlap] : locatedObject.laneOverlaps)
  {
    std::string roadId = lane->GetRoad().GetId();
    touchedRoads[roadId].lanes.push_back(lane->GetOdId());
    if (touchedRoads[roadId].sMin.roadPosition.s > laneOverlap.sMin.roadPosition.s)
    {
      touchedRoads[roadId].sMin = laneOverlap.sMin;
    }
    if (touchedRoads[roadId].sMax.roadPosition.s < laneOverlap.sMax.roadPosition.s)
    {
      touchedRoads[roadId].sMax = laneOverlap.sMax;
    }
    if (touchedRoads[roadId].tMin.laneId > laneOverlap.tMin.laneId
        || touchedRoads[roadId].tMin.laneId == laneOverlap.tMin.laneId
               && touchedRoads[roadId].tMin.roadPosition.t > laneOverlap.tMin.roadPosition.t)
    {
      touchedRoads[roadId].tMin = laneOverlap.tMin;
    }
    if (touchedRoads[roadId].tMax.laneId < laneOverlap.tMax.laneId
        || touchedRoads[roadId].tMax.laneId == laneOverlap.tMax.laneId
               && touchedRoads[roadId].tMax.roadPosition.t < laneOverlap.tMax.roadPosition.t)
    {
      touchedRoads[roadId].tMax = laneOverlap.tMax;
    }
  }

  for (auto &touchedRoad : touchedRoads)
  {
    touchedRoad.second.sMax.roadPosition.s = units::math::min(touchedRoad.second.sMax.roadPosition.s,
                                                              roads.at(touchedRoad.second.sMax.roadId)->GetLength());
    touchedRoad.second.sMax.roadPosition.s = units::math::max(touchedRoad.second.sMax.roadPosition.s, 0_m);
    touchedRoad.second.sMin.roadPosition.s = units::math::min(touchedRoad.second.sMin.roadPosition.s,
                                                              roads.at(touchedRoad.second.sMin.roadId)->GetLength());
    touchedRoad.second.sMin.roadPosition.s = units::math::max(touchedRoad.second.sMin.roadPosition.s, 0_m);
  }

  Result result({{ObjectPointPredefined::Reference, locatedObject.referencePoint}}, touchedRoads, isOnRoute);

  return result;
}

Localizer::Localizer(const OWL::Interfaces::WorldData &worldData) : worldData(worldData) {}

void Localizer::Init()
{
  for (const auto &[laneId, lane] : worldData.GetLanes())
  {
    for (const auto &laneGeometryElement : lane->GetLaneGeometryElements())
    {
      elements.emplace_back(*laneGeometryElement);
    }
  }
  for (const auto &element : elements)
  {
    rTree.insert(std::make_pair(element.search_box, &element));
  }
}

Result Localizer::Locate(const polygon_t &boundingBox, OWL::Interfaces::WorldObject &object) const
{
  const auto &referencePointPosition = object.GetReferencePointPosition();
  const auto &orientation = object.GetAbsOrientation();

  std::vector<Common::Vector2d<units::length::meter_t>> agentBoundary;
  for (point_t point : boundingBox.outer())
  {
    agentBoundary.emplace_back(units::length::meter_t(bg::get<0>(point)), units::length::meter_t(bg::get<1>(point)));
  }
  agentBoundary.pop_back();

  CoarseBoundingBox searchBox = GetSearchBox(agentBoundary);

  Common::Vector2d<units::length::meter_t> referencePoint{referencePointPosition.x, referencePointPosition.y};
  if (object.Is<OWL::Interfaces::MovingObject>())
  {
    const auto &movingObject = object.As<OWL::Interfaces::MovingObject>();
    auto distanceRefToFront = movingObject->GetDistanceReferencePointToLeadingEdge();
  }

  const auto &locatedObject
      = LocateOnGeometryElements(rTree, worldData, agentBoundary, searchBox, referencePoint, orientation.yaw);

  auto result = BuildResult(locatedObject);
  if (result.isOnRoute)
  {
    CreateLaneAssignments(object, locatedObject);
  }

  return result;
}

GlobalRoadPositions Localizer::Locate(const Common::Vector2d<units::length::meter_t> &point,
                                      const units::angle::radian_t &hdg) const
{
  auto locatedObject = LocateOnGeometryElements(rTree, worldData, point, hdg);
  for (auto &[roadId, position] : locatedObject)
  {
    position.roadPosition.s = units::math::min(position.roadPosition.s, worldData.GetRoads().at(roadId)->GetLength());
    position.roadPosition.s = units::math::max(position.roadPosition.s, 0_m);
  }
  return locatedObject;
}

void Localizer::Unlocate(OWL::Interfaces::WorldObject &object) const
{
  object.ClearLaneAssignments();
}

}  // namespace Localization
}  // namespace World
