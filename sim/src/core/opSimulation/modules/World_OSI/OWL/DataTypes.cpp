/********************************************************************************
 * Copyright (c) 2020 HLRS, University of Stuttgart
 *               2018-2021 in-tech GmbH
 *               2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  DataTypes.cpp
//! @brief This file provides the basic datatypes of the osi world layer (OWL)
//-----------------------------------------------------------------------------

#include "DataTypes.h"

#include <exception>
#include <list>
#include <memory>
#include <osi3/osi_groundtruth.pb.h>
#include <string>
#include <tuple>

#include <QtGlobal>

#include "OWL/LaneGeometryElement.h"
#include "OWL/LaneGeometryJoint.h"
#include "OWL/OwlHelper.h"
#include "OWL/Primitives.h"
#include "OsiDefaultValues.h"
#include "WorldObjectAdapter.h"
#include "common/commonHelper.h"
#include "include/roadInterface/roadInterface.h"
#include "include/roadInterface/roadLaneInterface.h"
#include "include/roadInterface/roadLaneSectionInterface.h"

namespace OWL
{

namespace Implementation
{

Lane::Lane(osi3::Lane *osiLane, osi3::LogicalLane *osiLogicalLane, const Interfaces::Section *section, OdId odId)
    : osiLane(osiLane),
      osiLogicalLane(osiLogicalLane),
      odId(odId),
      section(section),
      leftLane(section ? new InvalidLane() : nullptr),
      rightLane(section ? new InvalidLane() : nullptr)
{
}

Lane::~Lane()
{
  for (auto laneGeometryElement : laneGeometryElements)
  {
    delete laneGeometryElement;
  }
  if (leftLaneIsDummy)
  {
    delete leftLane;
  }
  if (rightLaneIsDummy)
  {
    delete rightLane;
  }
}

void Lane::CopyToGroundTruth(osi3::GroundTruth &target) const
{
  auto newLane = target.add_lane();
  newLane->CopyFrom(*osiLane);
  auto newLogicalLane = target.add_logical_lane();
  newLogicalLane->CopyFrom(*osiLogicalLane);
}

Id Lane::GetId() const
{
  return osiLane->id().value();
}

Id Lane::GetLogicalLaneId() const
{
  return osiLogicalLane->id().value();
}

OdId Lane::GetOdId() const
{
  return odId;
}

bool Lane::Exists() const
{
  return osiLane->id().value() != InvalidId;
}

const Interfaces::Section &Lane::GetSection() const
{
  return *section;
}

const Interfaces::Road &Lane::GetRoad() const
{
  return section->GetRoad();
}

int Lane::GetRightLaneCount() const
{
  int count = 0;
  const Interfaces::Lane *currentLane = &(GetRightLane());

  while (currentLane->Exists())
  {
    count++;
    currentLane = &(currentLane->GetRightLane());
  }

  return count;
}

units::length::meter_t Lane::GetLength() const
{
  return length;
}

std::tuple<const Primitive::LaneGeometryJoint *, const Primitive::LaneGeometryJoint *> Lane::GetNeighbouringJoints(
    units::length::meter_t distance) const
{
  const Primitive::LaneGeometryJoint *nextJoint = nullptr;
  const Primitive::LaneGeometryJoint *prevJoint = nullptr;

  const auto &nextJointIt
      = std::find_if(laneGeometryJoints.cbegin(),
                     laneGeometryJoints.cend(),
                     [distance](const Primitive::LaneGeometryJoint &joint) { return joint.sOffset > distance; });

  if (nextJointIt != laneGeometryJoints.cend())
  {
    nextJoint = &(*nextJointIt);
  }

  if (nextJointIt != laneGeometryJoints.cbegin())
  {
    const auto &prevJointIt = std::prev(nextJointIt);
    prevJoint = &(*prevJointIt);
  }

  return {prevJoint, nextJoint};
}

const Primitive::LaneGeometryJoint::Points Lane::GetInterpolatedPointsAtDistance(units::length::meter_t distance) const
{
  Primitive::LaneGeometryJoint::Points interpolatedPoints{{0.0_m, 0.0_m}, {0.0_m, 0.0_m}, {0.0_m, 0.0_m}};
  const Primitive::LaneGeometryJoint *prevJoint;
  const Primitive::LaneGeometryJoint *nextJoint;
  std::tie(prevJoint, nextJoint) = GetNeighbouringJoints(distance);

  if (!prevJoint && !nextJoint)
  {
    return interpolatedPoints;
  }
  else if (prevJoint && !nextJoint)
  {
    return prevJoint->points;
  }
  else if (nextJoint && !prevJoint)
  {
    return nextJoint->points;
  }

  double interpolationFactor = (distance - prevJoint->sOffset) / (nextJoint->sOffset - prevJoint->sOffset);

  interpolatedPoints.left
      = prevJoint->points.left * (1.0 - interpolationFactor) + nextJoint->points.left * interpolationFactor;
  interpolatedPoints.right
      = prevJoint->points.right * (1.0 - interpolationFactor) + nextJoint->points.right * interpolationFactor;
  interpolatedPoints.reference
      = prevJoint->points.reference * (1.0 - interpolationFactor) + nextJoint->points.reference * interpolationFactor;

  return interpolatedPoints;
}

units::curvature::inverse_meter_t Lane::GetCurvature(units::length::meter_t distance) const
{
  const Primitive::LaneGeometryJoint *prevJoint;
  const Primitive::LaneGeometryJoint *nextJoint;
  std::tie(prevJoint, nextJoint) = GetNeighbouringJoints(distance);

  if (!prevJoint && !nextJoint)
  {
    return 0.0_i_m;
  }
  else if (prevJoint && !nextJoint)
  {
    return prevJoint->curvature;
  }
  else if (nextJoint && !prevJoint)
  {
    return 0.0_i_m;
  }

  double interpolationFactor = (distance - prevJoint->sOffset) / (nextJoint->sOffset - prevJoint->sOffset);
  units::curvature::inverse_meter_t interpolatedCurvature
      = (1 - interpolationFactor) * prevJoint->curvature + interpolationFactor * nextJoint->curvature;

  return interpolatedCurvature;
}

units::length::meter_t Lane::GetWidth(units::length::meter_t distance) const
{
  const Primitive::LaneGeometryJoint *prevJoint;
  const Primitive::LaneGeometryJoint *nextJoint;
  std::tie(prevJoint, nextJoint) = GetNeighbouringJoints(distance);

  if (!prevJoint && !nextJoint)
  {
    return 0.0_m;
  }
  else if (prevJoint && !nextJoint)
  {
    return (prevJoint->points.left - prevJoint->points.right).Length();
  }
  else if (nextJoint && !prevJoint)
  {
    return 0.0_m;
  }

  units::dimensionless::scalar_t interpolationFactor
      = (distance - prevJoint->sOffset) / (nextJoint->sOffset - prevJoint->sOffset);
  units::length::meter_t nextWidth{(nextJoint->points.left - nextJoint->points.right).Length()};
  units::length::meter_t prevWidth{(prevJoint->points.left - prevJoint->points.right).Length()};
  units::length::meter_t interpolatedWidth = (1.0 - interpolationFactor) * prevWidth + interpolationFactor * nextWidth;

  return interpolatedWidth;
}

units::angle::radian_t Lane::GetDirection(units::length::meter_t distance) const
{
  auto [prevJoint, nextJoint] = GetNeighbouringJoints(distance);

  if (!prevJoint)
  {
    return 0.0_rad;
  }

  return prevJoint->sHdg;
}

const Interfaces::Lane &Lane::GetLeftLane() const
{
  return *leftLane;
}

const Interfaces::Lane &Lane::GetRightLane() const
{
  return *rightLane;
}

const std::vector<Id> Lane::GetLeftLaneBoundaries() const
{
  std::vector<Id> laneBoundaries;
  for (const auto &laneBoundary : osiLane->classification().left_lane_boundary_id())
  {
    laneBoundaries.push_back(laneBoundary.value());
  }
  return laneBoundaries;
}

const std::vector<Id> Lane::GetRightLaneBoundaries() const
{
  std::vector<Id> laneBoundaries;
  for (const auto &laneBoundary : osiLane->classification().right_lane_boundary_id())
  {
    laneBoundaries.push_back(laneBoundary.value());
  }
  return laneBoundaries;
}

const std::vector<Id> Lane::GetLeftLogicalLaneBoundaries() const
{
  std::vector<Id> logicallaneBoundaries;
  for (const auto &laneBoundary : osiLogicalLane->left_boundary_id())
  {
    logicallaneBoundaries.push_back(laneBoundary.value());
  }
  return logicallaneBoundaries;
}

const std::vector<Id> Lane::GetRightLogicalLaneBoundaries() const
{
  std::vector<Id> logicallaneBoundaries;
  for (const auto &laneBoundary : osiLogicalLane->right_boundary_id())
  {
    logicallaneBoundaries.push_back(laneBoundary.value());
  }
  return logicallaneBoundaries;
}

units::length::meter_t Lane::GetDistance(MeasurementPoint measurementPoint) const
{
  if (measurementPoint == MeasurementPoint::RoadStart)
  {
    return units::length::meter_t(osiLogicalLane->start_s());
  }

  if (measurementPoint == MeasurementPoint::RoadEnd)
  {
    return units::length::meter_t(osiLogicalLane->end_s());
  }

  throw std::invalid_argument("measurement point not within valid bounds");
}

LaneType Lane::GetLaneType() const
{
  return laneType;
}

bool Lane::Covers(units::length::meter_t distance) const
{
  return section->Covers(distance);
}

void Lane::SetLeftLane(const Interfaces::Lane &lane, bool transferLaneBoundary)
{
  if (leftLaneIsDummy)
  {
    leftLaneIsDummy = false;
    delete leftLane;
  }

  leftLane = &lane;
  osiLane->mutable_classification()->add_left_adjacent_lane_id()->set_value(lane.GetId());

  auto adjacentLogicalLane = osiLogicalLane->add_left_adjacent_lane();
  adjacentLogicalLane->mutable_other_lane_id()->set_value(lane.GetLogicalLaneId());
  adjacentLogicalLane->set_start_s(osiLogicalLane->start_s());
  adjacentLogicalLane->set_start_s_other(osiLogicalLane->start_s());
  adjacentLogicalLane->set_end_s(osiLogicalLane->end_s());
  adjacentLogicalLane->set_end_s_other(osiLogicalLane->end_s());

  if (transferLaneBoundary)
  {
    for (const auto &laneBoundary : lane.GetRightLaneBoundaries())
    {
      osiLane->mutable_classification()->add_left_lane_boundary_id()->set_value(laneBoundary);
    }
    for (const auto &logicallaneBoundary : lane.GetRightLogicalLaneBoundaries())
    {
      osiLogicalLane->add_left_boundary_id()->set_value(logicallaneBoundary);
    }
  }
}

void Lane::SetRightLane(const Interfaces::Lane &lane, bool transferLaneBoundary)
{
  if (rightLaneIsDummy)
  {
    rightLaneIsDummy = false;
    delete rightLane;
  }

  rightLane = &lane;
  osiLane->mutable_classification()->add_right_adjacent_lane_id()->set_value(lane.GetId());

  auto adjacentLogicalLane = osiLogicalLane->add_right_adjacent_lane();
  adjacentLogicalLane->mutable_other_lane_id()->set_value(lane.GetLogicalLaneId());
  adjacentLogicalLane->set_start_s(osiLogicalLane->start_s());
  adjacentLogicalLane->set_start_s_other(osiLogicalLane->start_s());
  adjacentLogicalLane->set_end_s(osiLogicalLane->end_s());
  adjacentLogicalLane->set_end_s_other(osiLogicalLane->end_s());

  if (transferLaneBoundary)
  {
    for (const auto &laneBoundary : lane.GetLeftLaneBoundaries())
    {
      osiLane->mutable_classification()->add_right_lane_boundary_id()->set_value(laneBoundary);
    }
    for (const auto &logicallaneBoundary : lane.GetLeftLogicalLaneBoundaries())
    {
      osiLogicalLane->add_right_boundary_id()->set_value(logicallaneBoundary);
    }
  }
}

void Lane::SetLeftLaneBoundaries(const std::vector<OWL::Id> &laneBoundaryIds)
{
  for (const auto &id : laneBoundaryIds)
  {
    osiLane->mutable_classification()->add_left_lane_boundary_id()->set_value(id);
  }
}

void Lane::SetRightLaneBoundaries(const std::vector<OWL::Id> &laneBoundaryIds)
{
  for (const auto &id : laneBoundaryIds)
  {
    osiLane->mutable_classification()->add_right_lane_boundary_id()->set_value(id);
  }
}

void Lane::SetLeftLogicalLaneBoundaries(const std::vector<OWL::Id> &laneBoundaryIds)
{
  for (const Id id : laneBoundaryIds)
  {
    osiLogicalLane->add_left_boundary_id()->set_value(id);
  }
}

void Lane::SetRightLogicalLaneBoundaries(const std::vector<OWL::Id> &laneBoundaryIds)
{
  for (const Id id : laneBoundaryIds)
  {
    osiLogicalLane->add_right_boundary_id()->set_value(id);
  }
}

void Lane::SetLaneType(LaneType specifiedType)
{
  laneType = specifiedType;
}

const Interfaces::LaneAssignments &Lane::GetWorldObjects(bool direction) const
{
  return worldObjects.Get(direction);
}

const Interfaces::TrafficSigns &Lane::GetTrafficSigns() const
{
  return trafficSigns;
}

const Interfaces::RoadMarkings &Lane::GetRoadMarkings() const
{
  return roadMarkings;
}

const Interfaces::TrafficLights &Lane::GetTrafficLights() const
{
  return trafficLights;
}

void Lane::AddMovingObject(Interfaces::MovingObject &movingObject, const LaneOverlap &laneOverlap)
{
  worldObjects.Insert(laneOverlap, &movingObject);
}

void Lane::AddStationaryObject(Interfaces::StationaryObject &stationaryObject, const LaneOverlap &laneOverlap)
{
  worldObjects.Insert(laneOverlap, &stationaryObject);
  stationaryObjects.push_back({laneOverlap, &stationaryObject});
}

void Lane::AddWorldObject(Interfaces::WorldObject &worldObject, const LaneOverlap &laneOverlap)
{
  if (worldObject.Is<MovingObject>())
  {
    AddMovingObject(*worldObject.As<MovingObject>(), laneOverlap);
  }
  else if (worldObject.Is<StationaryObject>())
  {
    AddStationaryObject(*worldObject.As<StationaryObject>(), laneOverlap);
  }
}

void Lane::AddTrafficSign(Interfaces::TrafficSign &trafficSign)
{
  trafficSigns.push_back(&trafficSign);
}

void Lane::AddRoadMarking(Interfaces::RoadMarking &roadMarking)
{
  roadMarkings.push_back(&roadMarking);
}

void Lane::AddTrafficLight(Interfaces::TrafficLight &trafficLight)
{
  trafficLights.push_back(&trafficLight);
}

void Lane::ClearMovingObjects()
{
  worldObjects.Clear();
  for (const auto &[laneOverlap, object] : stationaryObjects)
  {
    worldObjects.Insert(laneOverlap, object);
  }
}

void Lane::AddNext(const Interfaces::Lane *lane, bool atBeginOfOtherLane)
{
  next.push_back(lane->GetId());
  auto successorLane = osiLogicalLane->add_successor_lane();
  successorLane->mutable_other_lane_id()->set_value(lane->GetLogicalLaneId());
  successorLane->set_at_begin_of_other_lane(atBeginOfOtherLane);
}

void Lane::AddPrevious(const Interfaces::Lane *lane, bool atBeginOfOtherLane)
{
  previous.push_back(lane->GetId());
  auto predecessorLane = osiLogicalLane->add_predecessor_lane();
  predecessorLane->mutable_other_lane_id()->set_value(lane->GetLogicalLaneId());
  predecessorLane->set_at_begin_of_other_lane(atBeginOfOtherLane);
}

void Lane::SetLanePairings()
{
  for (const auto predecesser : previous)
  {
    for (const auto successor : next)
    {
      auto lanePairing = osiLane->mutable_classification()->add_lane_pairing();
      lanePairing->mutable_antecessor_lane_id()->set_value(predecesser);
      lanePairing->mutable_successor_lane_id()->set_value(successor);
    }
  }
  if (previous.empty())
  {
    for (const auto successor : next)
    {
      auto lanePairing = osiLane->mutable_classification()->add_lane_pairing();
      lanePairing->mutable_antecessor_lane_id()->set_value(-1);
      lanePairing->mutable_successor_lane_id()->set_value(successor);
    }
  }
  if (next.empty())
  {
    for (const auto predecesser : previous)
    {
      auto lanePairing = osiLane->mutable_classification()->add_lane_pairing();
      lanePairing->mutable_antecessor_lane_id()->set_value(predecesser);
      lanePairing->mutable_successor_lane_id()->set_value(-1);
    }
  }
}

void Lane::AddOverlappingLane(OWL::Id overlappingLaneId,
                              units::length::meter_t startS,
                              units::length::meter_t endS,
                              units::length::meter_t startSOther,
                              units::length::meter_t endSOther)
{
  auto overlappingLane = osiLogicalLane->add_overlapping_lane();
  overlappingLane->mutable_other_lane_id()->set_value(overlappingLaneId);
  overlappingLane->set_start_s(startS.value());
  overlappingLane->set_end_s(endS.value());
  overlappingLane->set_start_s_other(startSOther.value());
  overlappingLane->set_end_s_other(endSOther.value());
}

const Interfaces::LaneGeometryElements &Lane::GetLaneGeometryElements() const
{
  return laneGeometryElements;
}

void Lane::AddLaneGeometryJoint(const Common::Vector2d<units::length::meter_t> &pointLeft,
                                const Common::Vector2d<units::length::meter_t> &pointCenter,
                                const Common::Vector2d<units::length::meter_t> &pointRight,
                                units::length::meter_t sOffset,
                                units::curvature::inverse_meter_t curvature,
                                units::angle::radian_t heading)
{
  Primitive::LaneGeometryJoint newJoint;

  newJoint.points.left = pointLeft;
  newJoint.points.reference = pointCenter;
  newJoint.points.right = pointRight;
  newJoint.curvature = curvature;
  newJoint.sHdg = heading;
  newJoint.sOffset = sOffset;

  if (laneGeometryJoints.empty())
  {
    laneGeometryJoints.push_back(newJoint);
    auto osiCenterpoint = osiLane->mutable_classification()->add_centerline();
    osiCenterpoint->set_x(pointCenter.x.value());
    osiCenterpoint->set_y(pointCenter.y.value());
    return;
  }

  const Primitive::LaneGeometryJoint &previousJoint = laneGeometryJoints.back();

  if (previousJoint.sOffset >= sOffset)
  {
    return;  //Do not add the same point twice
  }

  length = sOffset - laneGeometryJoints.front().sOffset;
  Primitive::LaneGeometryElement *newElement = new Primitive::LaneGeometryElement(previousJoint, newJoint, this);
  laneGeometryElements.push_back(newElement);
  laneGeometryJoints.push_back(newJoint);
  auto osiCenterpoint = osiLane->mutable_classification()->add_centerline();
  osiCenterpoint->set_x(pointCenter.x.value());
  osiCenterpoint->set_y(pointCenter.y.value());
  osiLogicalLane->set_end_s(sOffset.value());
  if (osiLogicalLane->left_adjacent_lane_size() > 0)
  {
    osiLogicalLane->mutable_left_adjacent_lane(0)->set_end_s(sOffset.value());
    osiLogicalLane->mutable_left_adjacent_lane(0)->set_end_s_other(sOffset.value());
  }
  if (osiLogicalLane->right_adjacent_lane_size() > 0)
  {
    osiLogicalLane->mutable_right_adjacent_lane(0)->set_end_s(sOffset.value());
    osiLogicalLane->mutable_right_adjacent_lane(0)->set_end_s_other(sOffset.value());
  }
}

Section::Section(units::length::meter_t sOffset) : sOffset(sOffset) {}

void Section::AddNext(const Interfaces::Section &section)
{
  nextSections.push_back(&section);
}

void Section::AddPrevious(const Interfaces::Section &section)
{
  previousSections.push_back(&section);
}

void Section::AddLane(const Interfaces::Lane &lane)
{
  lanes.push_back(&lane);
}

void Section::SetRoad(Interfaces::Road *road)
{
  this->road = road;
}

const Interfaces::Road &Section::GetRoad() const
{
  return *road;
}

units::length::meter_t Section::GetDistance(MeasurementPoint measurementPoint) const
{
  if (measurementPoint == MeasurementPoint::RoadStart)
  {
    return GetSOffset();
  }

  if (measurementPoint == MeasurementPoint::RoadEnd)
  {
    return GetSOffset() + GetLength();
  }

  throw std::invalid_argument("measurement point not within valid bounds");
}

bool Section::Covers(units::length::meter_t distance) const
{
  if (GetDistance(MeasurementPoint::RoadStart) <= distance)
  {
    if (nextSections.empty() || &nextSections.front()->GetRoad() != road)
    {
      return ((GetDistance(MeasurementPoint::RoadEnd) + units::length::meter_t(CommonHelper::EPSILON)) >= distance);
    }
    else
    {
      return GetDistance(MeasurementPoint::RoadEnd) > distance;
    }
  }
  return false;
}

bool Section::CoversInterval(units::length::meter_t startDistance, units::length::meter_t endDistance) const
{
  auto sectionStart = GetDistance(MeasurementPoint::RoadStart);
  auto sectionEnd = GetDistance(MeasurementPoint::RoadEnd);

  bool startDistanceSmallerSectionEnd = nextSections.empty() ? startDistance <= sectionEnd : startDistance < sectionEnd;
  bool endDistanceGreaterSectionStart
      = previousSections.empty() ? endDistance >= sectionStart : endDistance > sectionStart;

  return startDistanceSmallerSectionEnd && endDistanceGreaterSectionStart;
}

const Interfaces::Lanes &Section::GetLanes() const
{
  return lanes;
}

units::length::meter_t Section::GetLength() const
{
  //All lanes must have equal length, so we simple take the length of first lane
  return lanes.empty() ? 0.0_m : lanes.front()->GetLength();
}

void Section::SetCenterLaneBoundary(std::vector<Id> laneBoundaryIds)
{
  centerLaneBoundary = laneBoundaryIds;
}

std::vector<Id> Section::GetCenterLaneBoundary() const
{
  return centerLaneBoundary;
}

void Section::SetCenterLogicalLaneBoundary(std::vector<Id> logicalLaneBoundaryIds)
{
  centerLogicalLaneBoundary = logicalLaneBoundaryIds;
}

std::vector<Id> Section::GetCenterLogicalLaneBoundary() const
{
  return centerLogicalLaneBoundary;
}

void Section::SetReferenceLine(Id referenceLineId)
{
  referenceLine = referenceLineId;
}

Id Section::GetReferenceLine() const
{
  return referenceLine;
}

units::length::meter_t Section::GetSOffset() const
{
  return sOffset;
}

Road::Road(bool isInStreamDirection, const std::string &id) : isInStreamDirection(isInStreamDirection), id(id) {}

const std::string &Road::GetId() const
{
  return id;
}

const Interfaces::Sections &Road::GetSections() const
{
  return sections;
}

void Road::AddSection(Interfaces::Section &section)
{
  section.SetRoad(this);
  sections.push_back(&section);
}

units::length::meter_t Road::GetLength() const
{
  units::length::meter_t length{0};
  for (const auto &section : sections)
  {
    length += section->GetLength();
  }
  return length;
}

const std::string &Road::GetSuccessor() const
{
  return successor;
}

const std::string &Road::GetPredecessor() const
{
  return predecessor;
}

void Road::SetSuccessor(const std::string &successor)
{
  this->successor = successor;
}

void Road::SetPredecessor(const std::string &predecessor)
{
  this->predecessor = predecessor;
}

bool Road::IsInStreamDirection() const
{
  return isInStreamDirection;
}

units::length::meter_t Road::GetDistance(MeasurementPoint mp) const
{
  if (mp == MeasurementPoint::RoadStart)
  {
    return 0_m;
  }
  else
  {
    return GetLength();
  }
}

StationaryObject::StationaryObject(osi3::StationaryObject *osiStationaryObject, void *linkedObject)
    : osiObject{osiStationaryObject}
{
  this->linkedObject = linkedObject;
}

void StationaryObject::CopyToGroundTruth(osi3::GroundTruth &target) const
{
  auto newStationaryObject = target.add_stationary_object();
  newStationaryObject->CopyFrom(*osiObject);
}

mantle_api::Dimension3 StationaryObject::GetDimension() const
{
  return GetDimensionFromOsiObject(osiObject);
}

mantle_api::Orientation3<units::angle::radian_t> StationaryObject::GetAbsOrientation() const
{
  osi3::Orientation3d osiOrientation = osiObject->base().orientation();

  return {units::angle::radian_t(osiOrientation.yaw()),
          units::angle::radian_t(osiOrientation.pitch()),
          units::angle::radian_t(osiOrientation.roll())};
}

mantle_api::Vec3<units::length::meter_t> StationaryObject::GetReferencePointPosition() const
{
  const osi3::Vector3d osiPosition = osiObject->base().position();

  return {units::length::meter_t(osiPosition.x()),
          units::length::meter_t(osiPosition.y()),
          units::length::meter_t(osiPosition.z())};
}

units::velocity::meters_per_second_t StationaryObject::GetAbsVelocityDouble() const
{
  return 0.0_mps;
}

units::acceleration::meters_per_second_squared_t StationaryObject::GetAbsAccelerationDouble() const
{
  return 0.0_mps_sq;
}

const RoadIntervals &StationaryObject::GetTouchedRoads() const
{
  return *touchedRoads;
}

Id StationaryObject::GetId() const
{
  return osiObject->id().value();
}

void StationaryObject::SetReferencePointPosition(const mantle_api::Vec3<units::length::meter_t> &newPosition)
{
  osi3::Vector3d *osiPosition = osiObject->mutable_base()->mutable_position();

  osiPosition->set_x(units::unit_cast<double>(newPosition.x));
  osiPosition->set_y(units::unit_cast<double>(newPosition.y));
  osiPosition->set_z(units::unit_cast<double>(newPosition.z));
}

void StationaryObject::SetDimension(const mantle_api::Dimension3 &newDimension)
{
  osi3::Dimension3d *osiDimension = osiObject->mutable_base()->mutable_dimension();

  osiDimension->set_length(units::unit_cast<double>(newDimension.length));
  osiDimension->set_width(units::unit_cast<double>(newDimension.width));
  osiDimension->set_height(units::unit_cast<double>(newDimension.height));
}

void StationaryObject::SetAbsOrientation(const mantle_api::Orientation3<units::angle::radian_t> &newOrientation)
{
  osi3::Orientation3d *osiOrientation = osiObject->mutable_base()->mutable_orientation();

  osiOrientation->set_yaw(units::unit_cast<double>(newOrientation.yaw));
  osiOrientation->set_pitch(units::unit_cast<double>(newOrientation.pitch));
  osiOrientation->set_roll(units::unit_cast<double>(newOrientation.roll));
}

void StationaryObject::AddLaneAssignment(const Interfaces::Lane &lane,
                                         const std::optional<RoadPosition> &referencePoint)
{
  assignedLanes.push_back(&lane);
  const auto logicalLaneAssignment = osiObject->mutable_classification()->add_logical_lane_assignment();
  logicalLaneAssignment->mutable_assigned_lane_id()->set_value(lane.GetLogicalLaneId());
  if (referencePoint.has_value())
  {
    logicalLaneAssignment->set_s_position(referencePoint->s.value());
    logicalLaneAssignment->set_t_position(referencePoint->t.value());
    logicalLaneAssignment->set_angle_to_lane(referencePoint->hdg.value());
  }
}

const Interfaces::Lanes &StationaryObject::GetLaneAssignments() const
{
  return assignedLanes;
}

void StationaryObject::ClearLaneAssignments()
{
  assignedLanes.clear();
}

void StationaryObject::SetTouchedRoads(const RoadIntervals &touchedRoads)
{
  this->touchedRoads = &touchedRoads;
}

void StationaryObject::SetSourceReference(const std::string &odId)
{
  auto externalReference = osiObject->add_source_reference();
  externalReference->set_type("net.asam.opendrive");
  externalReference->add_identifier("object");
  externalReference->add_identifier(odId);
}

const RoadIntervals &MovingObject::GetTouchedRoads() const
{
  return *touchedRoads;
}

InvalidLane::~InvalidLane()
{
  if (osiLane)
  {
    delete osiLane;
  }
}

TrafficSign::TrafficSign(osi3::TrafficSign *osiObject) : osiSign{osiObject} {}

void TrafficSign::SetValidForLane(const OWL::Interfaces::Lane &lane, const RoadSignalInterface &specification)
{
  OWL::AssignLaneAndLogicalLane(osiSign->mutable_main_sign(), lane, specification, GetOrientedHOffset(specification));
}

bool TrafficSign::SetSpecification(RoadSignalInterface *signal, Position position)
{
  bool mapping_succeeded = true;

  const auto baseStationary = osiSign->mutable_main_sign()->mutable_base();
  OWL::SetBaseStationary(baseStationary, signal, position);

  OWL::SetSourceReference(osiSign, signal);

  const auto mutableOsiClassification = osiSign->mutable_main_sign()->mutable_classification();
  OWL::SetCountryCode(mutableOsiClassification, signal);
  const std::string odType = signal->GetType();

  auto signalsMapping = OpenDriveTypeMapper::GetSignalsMapping(signal->GetCountry());

  if (signalsMapping->trafficSignsTypeOnly.find(odType) != signalsMapping->trafficSignsTypeOnly.end())
  {
    mutableOsiClassification->set_type(signalsMapping->trafficSignsTypeOnly.at(odType));
  }
  else if (signalsMapping->trafficSignsWithValue.find(odType) != signalsMapping->trafficSignsWithValue.end())
  {
    mutableOsiClassification->set_type(signalsMapping->trafficSignsWithValue.at(odType));

    if (signal->GetValue() == std::nullopt)
    {
      if (signal->GetCountry() == "CN" && (odType == "SpeedLimit-38" || odType == "EndOfSpeedLimit-39"))
      {
        mutableOsiClassification->mutable_value()->set_value(100);
        mutableOsiClassification->mutable_value()->set_value_unit(
            osi3::TrafficSignValue_Unit::TrafficSignValue_Unit_UNIT_KILOMETER_PER_HOUR);
        throw std::invalid_argument("Speed limit value of OpenDRIVE signal (id: " + signal->GetId() + ", type: " + odType + ") is not set correctly as the country-specific value is missing. A default value = 100 km/h for China is set instead.");
      }
      else if (signal->GetCountry() == "US" && odType == "R2-1")
      {
        mutableOsiClassification->mutable_value()->set_value(70);
        mutableOsiClassification->mutable_value()->set_value_unit(
            osi3::TrafficSignValue_Unit::TrafficSignValue_Unit_UNIT_MILE_PER_HOUR);
        throw std::invalid_argument("Speed limit value of OpenDRIVE signal (id: " + signal->GetId() + ", type: " + odType + ") is not set correctly as the country-specific value is missing. A default value = 70 mph for United States is set instead.");
      }
    }

    mutableOsiClassification->mutable_value()->set_value(signal->GetValue().value_or(0.0));
    mutableOsiClassification->mutable_value()->set_value_unit(
        OpenDriveTypeMapper::OdToOsiTrafficSignUnit(signal->GetUnit()));
  }
  else if (signalsMapping->trafficSignsWithText.find(odType) != signalsMapping->trafficSignsWithText.end())
  {
    mutableOsiClassification->set_type(signalsMapping->trafficSignsWithText.at(odType));
    mutableOsiClassification->mutable_value()->set_text(signal->GetText());
  }
  else if (signalsMapping->trafficSignsWithTextValueAndUnit.find(odType)
           != signalsMapping->trafficSignsWithTextValueAndUnit.end())
  {
    const auto &valueAndDefaultValues = signalsMapping->trafficSignsWithTextValueAndUnit.at(odType);
    const auto &defaultValueAndUnit = valueAndDefaultValues.second;

    mutableOsiClassification->set_type(valueAndDefaultValues.first);

    mutableOsiClassification->mutable_value()->set_text(signal->GetText());

    if (signal->GetValue().has_value())
    {
      mutableOsiClassification->mutable_value()->set_value(signal->GetValue().value());
      mutableOsiClassification->mutable_value()->set_value_unit(
          OpenDriveTypeMapper::OdToOsiTrafficSignUnit(signal->GetUnit()));
    }
    else
    {
      mutableOsiClassification->mutable_value()->set_value(defaultValueAndUnit.first);
      mutableOsiClassification->mutable_value()->set_value_unit(defaultValueAndUnit.second);
    }
  }
  else if (signalsMapping->trafficSignsPredefinedValueAndUnit.find(odType)
           != signalsMapping->trafficSignsPredefinedValueAndUnit.end())
  {
    const auto &specification = signalsMapping->trafficSignsPredefinedValueAndUnit.at(odType);

    mutableOsiClassification->mutable_value()->set_value_unit(specification.first);

    const auto subType = signal->GetSubType();
    if (specification.second.find(subType) != specification.second.end())
    {
      const auto &valueAndType = specification.second.at(subType);

      mutableOsiClassification->mutable_value()->set_value(valueAndType.first);
      mutableOsiClassification->set_type(valueAndType.second);
    }
    else
    {
      mutableOsiClassification->set_type(
          osi3::TrafficSign_MainSign_Classification_Type::TrafficSign_MainSign_Classification_Type_TYPE_OTHER);
      mapping_succeeded = false;
    }
  }
  else if (signalsMapping->trafficSignsSubTypeDefinesValue.find(odType)
           != signalsMapping->trafficSignsSubTypeDefinesValue.end())
  {
    const auto &valueAndUnit = signalsMapping->trafficSignsSubTypeDefinesValue.at(odType);

    mutableOsiClassification->set_type(valueAndUnit.first);

    const double value = std::stoi(signal->GetSubType());

    if (std::abs(value + 1) < CommonHelper::EPSILON && signal->GetCountry() == "DE"
        && (odType == "274" || odType == "275" || odType == "278" || odType == "279"))
    {
      mutableOsiClassification->mutable_value()->set_value(130);
      mutableOsiClassification->mutable_value()->set_value_unit(
          osi3::TrafficSignValue_Unit::TrafficSignValue_Unit_UNIT_KILOMETER_PER_HOUR);
      throw std::invalid_argument("Speed limit subtype of OpenDRIVE signal (id: " + signal->GetId() + ", type: " + odType + ") is not set correctly as the country-specific subtype is missing. A default value = 130 km/h for Germany is set instead.");
    }

    mutableOsiClassification->mutable_value()->set_value(value);
    mutableOsiClassification->mutable_value()->set_value_unit(valueAndUnit.second);
  }
  else
  {
    mutableOsiClassification->set_type(
        osi3::TrafficSign_MainSign_Classification_Type::TrafficSign_MainSign_Classification_Type_TYPE_OTHER);
    mapping_succeeded = false;
  }

  return mapping_succeeded;
}

bool TrafficSign::AddSupplementarySign(RoadSignalInterface *odSignal, Position position)
{
  auto *supplementarySign = osiSign->add_supplementary_sign();

  const auto baseStationary = supplementarySign->mutable_base();
  OWL::SetBaseStationary(baseStationary, odSignal, position);

  if (odSignal->GetType() == "1004")
  {
    supplementarySign->mutable_classification()->set_type(
        osi3::TrafficSign_SupplementarySign_Classification_Type::
            TrafficSign_SupplementarySign_Classification_Type_TYPE_SPACE);
    auto osiValue = supplementarySign->mutable_classification()->mutable_value()->Add();

    if (odSignal->GetSubType() == "30")
    {
      osiValue->set_value(odSignal->GetValue().value_or(0.0));
      osiValue->set_value_unit(osi3::TrafficSignValue_Unit::TrafficSignValue_Unit_UNIT_METER);
      osiValue->set_text(std::to_string(odSignal->GetValue().value_or(0.0)) + " m");
      return true;
    }
    else if (odSignal->GetSubType() == "31")
    {
      osiValue->set_value(odSignal->GetValue().value_or(0.0));
      osiValue->set_value_unit(osi3::TrafficSignValue_Unit::TrafficSignValue_Unit_UNIT_KILOMETER);
      osiValue->set_text(std::to_string(odSignal->GetValue().value_or(0.0)) + " km");
      return true;
    }
    else if (odSignal->GetSubType() == "32")
    {
      osiValue->set_value(100.0);
      osiValue->set_value_unit(osi3::TrafficSignValue_Unit::TrafficSignValue_Unit_UNIT_METER);
      osiValue->set_text("STOP 100 m");
      return true;
    }
    else
    {
      supplementarySign->mutable_classification()->set_type(
          osi3::TrafficSign_SupplementarySign_Classification_Type::
              TrafficSign_SupplementarySign_Classification_Type_TYPE_OTHER);
      return false;
    }
  }
  else
  {
    supplementarySign->mutable_classification()->set_type(
        osi3::TrafficSign_SupplementarySign_Classification_Type::
            TrafficSign_SupplementarySign_Classification_Type_TYPE_OTHER);
    return false;
  }
}

std::pair<double, CommonTrafficSign::Unit> TrafficSign::GetValueAndUnitInSI(
    const double osiValue, const osi3::TrafficSignValue_Unit osiUnit) const
{
  if (OpenDriveTypeMapper::unitConversionMap.find(osiUnit) != OpenDriveTypeMapper::unitConversionMap.end())
  {
    const auto &conversionParameters = OpenDriveTypeMapper::unitConversionMap.at(osiUnit);

    return {osiValue * conversionParameters.first, conversionParameters.second};
  }
  else
  {
    return {osiValue, CommonTrafficSign::Unit::None};
  }
}

CommonTrafficSign::Entity TrafficSign::GetSpecification(const units::length::meter_t relativeDistance) const
{
  CommonTrafficSign::Entity specification;

  specification.distanceToStartOfRoad = s;
  specification.relativeDistance = relativeDistance;

  const auto osiType = osiSign->main_sign().classification().type();

  if (OpenDriveTypeMapper::typeConversionMap.find(osiType) != OpenDriveTypeMapper::typeConversionMap.end())
  {
    specification.type = OpenDriveTypeMapper::typeConversionMap.at(osiType);
  }

  const auto &osiMainSign = osiSign->main_sign().classification().value();
  const auto valueAndUnit = GetValueAndUnitInSI(osiMainSign.value(), osiMainSign.value_unit());

  specification.value = valueAndUnit.first;
  specification.unit = valueAndUnit.second;
  specification.text = osiSign->main_sign().classification().value().text();

  std::transform(osiSign->supplementary_sign().cbegin(),
                 osiSign->supplementary_sign().cend(),
                 std::back_inserter(specification.supplementarySigns),
                 [this, relativeDistance](const auto &osiSupplementarySign)
                 {
                   CommonTrafficSign::Entity supplementarySignSpecification;
                   supplementarySignSpecification.distanceToStartOfRoad = s;
                   supplementarySignSpecification.relativeDistance = relativeDistance;

                   if (osiSupplementarySign.classification().type()
                       == osi3::TrafficSign_SupplementarySign_Classification_Type::
                           TrafficSign_SupplementarySign_Classification_Type_TYPE_SPACE)
                   {
                     const auto &osiSupplementarySignValueStruct = osiSupplementarySign.classification().value().Get(0);
                     const auto supplementarySignValueAndUnit = GetValueAndUnitInSI(
                         osiSupplementarySignValueStruct.value(), osiSupplementarySignValueStruct.value_unit());
                     supplementarySignSpecification.type = CommonTrafficSign::Type::DistanceIndication;
                     supplementarySignSpecification.value = supplementarySignValueAndUnit.first;
                     supplementarySignSpecification.unit = supplementarySignValueAndUnit.second;
                     supplementarySignSpecification.text = osiSupplementarySignValueStruct.text();
                   }

                   return supplementarySignSpecification;
                 });

  return specification;
}

mantle_api::Vec3<units::length::meter_t> TrafficSign::GetReferencePointPosition() const
{
  const osi3::Vector3d osiPosition = osiSign->main_sign().base().position();

  return {units::length::meter_t(osiPosition.x()),
          units::length::meter_t(osiPosition.y()),
          units::length::meter_t(osiPosition.z())};
}

mantle_api::Dimension3 TrafficSign::GetDimension() const
{
  return GetDimensionFromOsiObject(&osiSign->main_sign());
}

bool TrafficSign::IsValidForLane(OWL::Id laneId) const
{
  auto assignedLanes = osiSign->main_sign().classification().assigned_lane_id();
  for (auto lane : assignedLanes)
  {
    if (lane.value() == laneId)
    {
      return true;
    }
  }
  return false;
}

void TrafficSign::CopyToGroundTruth(osi3::GroundTruth &target) const
{
  auto newTrafficSign = target.add_traffic_sign();
  newTrafficSign->CopyFrom(*osiSign);
}

RoadMarking::RoadMarking(osi3::RoadMarking *osiObject) : osiSign{osiObject} {}

void RoadMarking::SetValidForLane(const OWL::Interfaces::Lane &lane, const RoadSignalInterface &specification)
{
  OWL::AssignLaneAndLogicalLane(osiSign, lane, specification, GetOrientedHOffset(specification));
}

void RoadMarking::SetValidForLane(const OWL::Interfaces::Lane &lane, const RoadObjectInterface &specification)
{
  OWL::AssignLaneAndLogicalLane(osiSign, lane, specification, specification.GetHdg());
}

bool RoadMarking::SetSpecification(RoadSignalInterface *signal, Position position)
{
  bool mapping_succeeded = true;

  const auto baseStationary = osiSign->mutable_base();

  OWL::SetBaseStationary(baseStationary, signal, position);
  // road marking should always have neglectable height (override user base values)
  baseStationary->mutable_position()->set_z(0.0);
  baseStationary->mutable_dimension()->set_height(0.0);
  // As OpenDRIVE 1.6 does not define a length, set to it to 0.5m (taken from stop line)
  baseStationary->mutable_dimension()->set_length(0.5);

  OWL::SetSourceReference(osiSign, signal);

  const auto mutableOsiClassification = osiSign->mutable_classification();
  OWL::SetCountryCode(mutableOsiClassification, signal);
  const std::string odType = signal->GetType();

  auto signalsMapping = OpenDriveTypeMapper::GetSignalsMapping(signal->GetCountry());

  mutableOsiClassification->set_type(
      osi3::RoadMarking_Classification_Type::RoadMarking_Classification_Type_TYPE_SYMBOLIC_TRAFFIC_SIGN);
  mutableOsiClassification->set_monochrome_color(
      osi3::RoadMarking_Classification_Color::RoadMarking_Classification_Color_COLOR_WHITE);
  if (signalsMapping->roadMarkings.find(odType) != signalsMapping->roadMarkings.end())
  {
    mutableOsiClassification->set_traffic_main_sign_type(signalsMapping->roadMarkings.at(odType));
  }
  else
  {
    mutableOsiClassification->set_traffic_main_sign_type(
        osi3::TrafficSign_MainSign_Classification_Type::TrafficSign_MainSign_Classification_Type_TYPE_OTHER);
    mapping_succeeded = false;
  }

  return mapping_succeeded;
}

bool RoadMarking::SetSpecification(RoadObjectInterface *object, Position position)
{
  bool mapping_succeeded = true;

  const auto baseStationary = osiSign->mutable_base();

  baseStationary->mutable_position()->set_x(units::unit_cast<double>(position.xPos));
  baseStationary->mutable_position()->set_y(units::unit_cast<double>(position.yPos));
  baseStationary->mutable_position()->set_z(0.0);
  baseStationary->mutable_dimension()->set_width(units::unit_cast<double>(object->GetWidth()));
  baseStationary->mutable_dimension()->set_length(units::unit_cast<double>(object->GetLength()));
  baseStationary->mutable_orientation()->set_yaw(
      units::unit_cast<double>(CommonHelper::SetAngleToValidRange(position.yawAngle)));
  baseStationary->mutable_orientation()->set_pitch(
      units::unit_cast<double>(CommonHelper::SetAngleToValidRange(object->GetPitch())));
  baseStationary->mutable_orientation()->set_roll(
      units::unit_cast<double>(CommonHelper::SetAngleToValidRange(object->GetRoll())));

  OWL::SetSourceReference(osiSign, object);

  const auto mutableOsiClassification = osiSign->mutable_classification();

  mutableOsiClassification->set_type(
      osi3::RoadMarking_Classification_Type::RoadMarking_Classification_Type_TYPE_SYMBOLIC_TRAFFIC_SIGN);
  mutableOsiClassification->set_monochrome_color(
      osi3::RoadMarking_Classification_Color::RoadMarking_Classification_Color_COLOR_WHITE);
  if (object->GetType() == RoadObjectType::crosswalk)
  {
    mutableOsiClassification->set_traffic_main_sign_type(
        osi3::TrafficSign_MainSign_Classification_Type::TrafficSign_MainSign_Classification_Type_TYPE_ZEBRA_CROSSING);
  }
  else
  {
    mutableOsiClassification->set_traffic_main_sign_type(
        osi3::TrafficSign_MainSign_Classification_Type::TrafficSign_MainSign_Classification_Type_TYPE_OTHER);
    mapping_succeeded = false;
  }

  return mapping_succeeded;
}

CommonTrafficSign::Entity RoadMarking::GetSpecification(const units::length::meter_t relativeDistance) const
{
  CommonTrafficSign::Entity specification;

  specification.distanceToStartOfRoad = s;
  specification.relativeDistance = relativeDistance;

  const auto osiType = osiSign->classification().traffic_main_sign_type();

  if (OpenDriveTypeMapper::typeConversionMap.find(osiType) != OpenDriveTypeMapper::typeConversionMap.end())
  {
    specification.type = OpenDriveTypeMapper::typeConversionMap.at(osiType);
  }

  specification.text = osiSign->classification().value().text();

  return specification;
}

mantle_api::Vec3<units::length::meter_t> RoadMarking::GetReferencePointPosition() const
{
  const osi3::Vector3d osiPosition = osiSign->base().position();

  return {units::length::meter_t(osiPosition.x()),
          units::length::meter_t(osiPosition.y()),
          units::length::meter_t(osiPosition.z())};
}

mantle_api::Dimension3 RoadMarking::GetDimension() const
{
  return GetDimensionFromOsiObject(osiSign);
}

bool RoadMarking::IsValidForLane(OWL::Id laneId) const
{
  auto assignedLanes = osiSign->classification().assigned_lane_id();
  for (auto lane : assignedLanes)
  {
    if (lane.value() == laneId)
    {
      return true;
    }
  }
  return false;
}

void RoadMarking::CopyToGroundTruth(osi3::GroundTruth &target) const
{
  auto newRoadMarking = target.add_road_marking();
  newRoadMarking->CopyFrom(*osiSign);
}

LaneBoundary::LaneBoundary(osi3::LaneBoundary *osiLaneBoundary,
                           units::length::meter_t width,
                           units::length::meter_t sStart,
                           units::length::meter_t sEnd,
                           LaneMarkingSide side)
    : osiLaneBoundary(osiLaneBoundary), sStart(sStart), sEnd(sEnd), width(width), side(side)
{
}

Id LaneBoundary::GetId() const
{
  return osiLaneBoundary->id().value();
}

units::length::meter_t LaneBoundary::GetWidth() const
{
  return width;
}

units::length::meter_t LaneBoundary::GetSStart() const
{
  return sStart;
}

units::length::meter_t LaneBoundary::GetSEnd() const
{
  return sEnd;
}

LaneMarking::Type LaneBoundary::GetType() const
{
  return OpenDriveTypeMapper::OsiToOdLaneMarkingType(osiLaneBoundary->classification().type());
}

LaneMarking::Color LaneBoundary::GetColor() const
{
  return OpenDriveTypeMapper::OsiToOdLaneMarkingColor(osiLaneBoundary->classification().color());
}

LaneMarkingSide LaneBoundary::GetSide() const
{
  return side;
}

void LaneBoundary::AddBoundaryPoint(const Common::Vector2d<units::length::meter_t> &point,
                                    units::angle::radian_t heading)
{
  constexpr units::length::meter_t doubleLineDistance{0.15};
  auto boundaryPoint = osiLaneBoundary->add_boundary_line();
  switch (side)
  {
    case LaneMarkingSide::Single:
      boundaryPoint->mutable_position()->set_x(point.x.value());
      boundaryPoint->mutable_position()->set_y(point.y.value());
      break;
    case LaneMarkingSide::Left:
      boundaryPoint->mutable_position()->set_x(
          units::unit_cast<double>(point.x - doubleLineDistance * units::math::sin(heading)));
      boundaryPoint->mutable_position()->set_y(
          units::unit_cast<double>(point.y + doubleLineDistance * units::math::cos(heading)));
      break;
    case LaneMarkingSide::Right:
      boundaryPoint->mutable_position()->set_x(
          units::unit_cast<double>(point.x + doubleLineDistance * units::math::sin(heading)));
      boundaryPoint->mutable_position()->set_y(
          units::unit_cast<double>(point.y - doubleLineDistance * units::math::cos(heading)));
      break;
  }
  boundaryPoint->set_width(units::unit_cast<double>(width));
}

void LaneBoundary::CopyToGroundTruth(osi3::GroundTruth &target) const
{
  auto newLaneBoundary = target.add_lane_boundary();
  newLaneBoundary->CopyFrom(*osiLaneBoundary);
}

LogicalLaneBoundary::LogicalLaneBoundary(osi3::LogicalLaneBoundary *osiLogicalLaneBoundary,
                                         units::length::meter_t sStart,
                                         units::length::meter_t sEnd)
    : osiLogicalLaneBoundary(osiLogicalLaneBoundary), sStart(sStart), sEnd(sEnd)
{
}

Id LogicalLaneBoundary::GetId() const
{
  return osiLogicalLaneBoundary->id().value();
}

units::length::meter_t LogicalLaneBoundary::GetSStart() const
{
  return sStart;
}

units::length::meter_t LogicalLaneBoundary::GetSEnd() const
{
  return sEnd;
}

void LogicalLaneBoundary::AddBoundaryPoint(const Common::Vector2d<units::length::meter_t> &point,
                                           units::length::meter_t s,
                                           units::length::meter_t t)
{
  auto boundaryPoint = osiLogicalLaneBoundary->add_boundary_line();
  boundaryPoint->mutable_position()->set_x(point.x.value());
  boundaryPoint->mutable_position()->set_y(point.y.value());
  boundaryPoint->set_s_position(s.value());
  boundaryPoint->set_t_position(t.value());
}

void LogicalLaneBoundary::SetPhysicalBoundaryIds(const std::vector<OWL::Id> &laneBoundaryIds)
{
  osiLogicalLaneBoundary->clear_physical_boundary_id();
  for (const OWL::Id id : laneBoundaryIds)
  {
    osiLogicalLaneBoundary->add_physical_boundary_id()->set_value(id);
  }
}

void LogicalLaneBoundary::CopyToGroundTruth(osi3::GroundTruth &target) const
{
  auto newLaneBoundary = target.add_logical_lane_boundary();
  newLaneBoundary->CopyFrom(*osiLogicalLaneBoundary);
}

ReferenceLine::ReferenceLine(osi3::ReferenceLine *osiReferenceLine) : osiReferenceLine(osiReferenceLine) {}

Id ReferenceLine::GetId() const
{
  return osiReferenceLine->id().value();
}

void ReferenceLine::AddPoint(const Common::Vector2d<units::length::meter_t> &point, units::length::meter_t s)
{
  auto boundaryPoint = osiReferenceLine->add_poly_line();
  boundaryPoint->mutable_world_position()->set_x(point.x.value());
  boundaryPoint->mutable_world_position()->set_y(point.y.value());
  boundaryPoint->set_s_position(s.value());
}

void ReferenceLine::CopyToGroundTruth(osi3::GroundTruth &target) const
{
  auto newReferenceLine = target.add_reference_line();
  newReferenceLine->CopyFrom(*osiReferenceLine);
}

Lane::LaneAssignmentCollector::LaneAssignmentCollector() noexcept
{
  downstreamOrderAssignments.reserve(INITIAL_COLLECTION_SIZE);
  upstreamOrderAssignments.reserve(INITIAL_COLLECTION_SIZE);
}

void Lane::LaneAssignmentCollector::Insert(const LaneOverlap &laneOverlap, const OWL::Interfaces::WorldObject *object)
{
  downstreamOrderAssignments.emplace_back(laneOverlap, object);
  upstreamOrderAssignments.emplace_back(laneOverlap, object);
  dirty = true;
}

void Lane::LaneAssignmentCollector::Clear()
{
  downstreamOrderAssignments.clear();
  upstreamOrderAssignments.clear();
  dirty = false;
}

void Lane::LaneAssignmentCollector::Sort() const
{
  std::sort(downstreamOrderAssignments.begin(),
            downstreamOrderAssignments.end(),
            [](const auto &lhs, const auto &rhs)
            {
              return lhs.first.sMin.roadPosition.s < rhs.first.sMin.roadPosition.s
                  || (lhs.first.sMin.roadPosition.s == rhs.first.sMin.roadPosition.s
                      && lhs.first.sMax.roadPosition.s < rhs.first.sMax.roadPosition.s);
            });

  std::sort(upstreamOrderAssignments.begin(),
            upstreamOrderAssignments.end(),
            [](const auto &lhs, const auto &rhs)
            {
              return lhs.first.sMax.roadPosition.s > rhs.first.sMax.roadPosition.s
                  || (lhs.first.sMax.roadPosition.s == rhs.first.sMax.roadPosition.s
                      && lhs.first.sMin.roadPosition.s > rhs.first.sMin.roadPosition.s);
            });

  dirty = false;
}

const Interfaces::LaneAssignments &Lane::LaneAssignmentCollector::Get(bool downstream) const
{
  if (dirty)
  {
    Sort();
  }

  if (downstream)
  {
    return downstreamOrderAssignments;
  }
  else
  {
    return upstreamOrderAssignments;
  }
}

// namespace Implementation

}  //namespace Implementation

}  // namespace OWL
