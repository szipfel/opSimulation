/********************************************************************************
 * Copyright (c) 2020 HLRS, University of Stuttgart
 *               2018-2019 in-tech GmbH
 *               2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <MantleAPI/Common/orientation.h>

namespace OWL
{
namespace Primitive
{

/// This struct represents 3d vectors in cartesian coordinate system
struct Vector3
{
  /// x-value
  double x;
  /// y-value
  double y;
  /// z-value
  double z;
};

/// This struct represents the absolute coordinates (i. e. world coordinates)
struct AbsCoordinate
{
  /// x coordinate
  units::length::meter_t x;
  /// y coordinate
  units::length::meter_t y;
  /// heading
  units::angle::radian_t hdg;
};

/// This struct represents the acceleration of the object in absolute coordinates (i. e. world coordinates)
struct AbsAcceleration
{
  /// x-value of the acceleration of the object
  units::acceleration::meters_per_second_squared_t ax;
  /// y-value of the acceleration of the object
  units::acceleration::meters_per_second_squared_t ay;
  /// z-value of the acceleration of the object
  units::acceleration::meters_per_second_squared_t az;
};

/// This struct represents the orientation of the object in absolute coordinates (i. e. world coordinates)
struct AbsOrientation
{
  /// Yaw angles of the object
  units::angle::radian_t yaw;
  /// Pitch angles of the object
  units::angle::radian_t pitch;
  /// Roll angles of the object
  units::angle::radian_t roll;
};

/// This struct represents the orientation rate of the object in absolute coordinates (i. e. world coordinates)
struct AbsOrientationRate
{
  /// Yaw rate of the object
  units::angular_velocity::radians_per_second_t yawRate;
  /// Pitch rate of the object
  units::angular_velocity::radians_per_second_t pitchRate;
  /// Roll rate of the object
  units::angular_velocity::radians_per_second_t rollRate;
};

/// This struct represents the orientation rate of the object in absolute coordinates (i. e. world coordinates)
struct AbsOrientationAcceleration
{
  /// Yaw acceleration of the object
  units::angular_acceleration::radians_per_second_squared_t yawAcceleration;
  /// Pitch acceleration of the object
  units::angular_acceleration::radians_per_second_squared_t pitchAcceleration;
  /// Roll acceleration of the object
  units::angular_acceleration::radians_per_second_squared_t rollAcceleration;
};

/// This struct represents the road coordinates
struct RoadCoordinate
{
  /// coordinate along driving direction
  units::length::meter_t s;
  /// coordinate perpendicular to driving direction
  units::length::meter_t t;
  /// orientation w.r.t reference line
  units::angle::radian_t yaw;
};

/// This struct represents the velocity of the object in road coordinates
struct RoadVelocity
{
  /// velocity along driving direction
  double vs;
  /// velocity perpendicular to driving direction
  double vt;
  /// velocity perpendicular to road surface
  double vz;
};

/// This struct represents the acceleration of the object in road coordinates
struct RoadAcceleration
{
  /// acceleration along driving direction
  double as;
  /// acceleration perpendicular to driving direction
  double at;
  /// acceleration perpendicular to road surface
  double az;
};

/// This struct represents the orientation rate of the object in road coordinates
struct RoadOrientationRate
{
  /// Heading rate
  double hdgRate;
};

/// Position of the object with respect to the lane
struct LanePosition
{
  /// along center line of lane
  double v;
  /// perpendicular to center line of lane
  double w;
};

/// Velocity of the object with respect to the lane
struct LaneVelocity
{
  /// velocity along center line of lane
  double vv;
  /// velocity perpendicular to center line of lane
  double vw;
  /// velocity perpendicular to lane
  double vz;
};

/// Acceleration of the object with respect to the lane
struct LaneAcceleration
{
  /// acceleration along center line of lane
  double av;
  /// acceleration perpendicular to center line of lane
  double aw;
  /// acceleration perpendicular to lane
  double az;
};

/// Orientation of the object with respect to the lane
struct LaneOrientation
{
  /// heading
  units::angle::radian_t hdg;
};

/// Change rate of the orientation of the object with respect to the lane
struct LaneOrientationRate
{
  /// change rate of the heading
  double hdgRate;
};

}  // namespace Primitive
}  // namespace OWL
