/********************************************************************************
 * Copyright (c) 2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  RadioImplementation.h
//! @brief This file contains the implementation of RadioInterface.h
//-----------------------------------------------------------------------------

#pragma once

#include <memory>
#include <osi3/osi_object.pb.h>
#include <vector>

#include "include/radioInterface.h"

/// Single signal sent by a sender
struct RadioSignal
{
  units::length::meter_t positionX;      ///< x-position of the sender
  units::length::meter_t positionY;      ///< y-position of the sender
  units::power::watt_t signalStrength;   ///< Signal strength of sender (transmitting power [W])
  osi3::MovingObject objectInformation;  ///< Information to broadcast
};

/**
 * @brief This is a utility class for Car2X communication. Senders register themselves every timestep by calling the
 * send function. After that the receivers may call the receive function to get all the senders around them. The radio
 * cloud implementation is responsible for the calculation which signals a receiver can hear.
 */
class RadioImplementation : public RadioInterface
{
public:
  RadioImplementation() = default;
  ~RadioImplementation(){};

  //-----------------------------------------------------------------------------
  //! Broadcasts object-metadata and sensor informations to cloud.
  //! @param[in] positionX            x position of sender
  //! @param[in] postionY             y position of sender
  //! @param[in] signalStrength       signal strength of sender (transmitting power [W])
  //! @param[in] objectInformation    sensor data
  //-----------------------------------------------------------------------------
  void Send(units::length::meter_t positionX,
            units::length::meter_t postionY,
            units::power::watt_t signalStrength,
            osi3::MovingObject objectInformation) override;

  //-----------------------------------------------------------------------------
  //! Retrieve available information at current postion from cloud.
  //! @param[in] positionX   x position of receiver
  //! @param[in] positionY   y position of receiver
  //! @param[in] sensitivity   sensitivity of receiver (implemented as surface power density [W/m2])
  //! @return data of senders "visible" at current position
  //-----------------------------------------------------------------------------
  std::vector<osi3::MovingObject> Receive(units::length::meter_t positionX,
                                          units::length::meter_t positionY,
                                          units::sensitivity sensitivity) override;

  void Reset() override;

private:
  //-----------------------------------------------------------------------------
  //! Checks if sender is within proximity of receiver ("is visible").
  //! Physical model: isotropic radiator
  //! @param[in] signalStrength   signal strength of sender [W]
  //! @param[in] distance   distance between sender and receiver
  //! @param[in] sensitivity   sensitivity of receiver [W/m2]
  //! @return bool is sender within proximity
  //-----------------------------------------------------------------------------
  bool CanHearSignal(units::power::watt_t signalStrength,
                     units::length::meter_t distance,
                     units::sensitivity sensitivity);

  std::vector<RadioSignal> signalVector;
};
