/********************************************************************************
 * Copyright (c) 2017-2020 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include "Localization.h"
#include "WorldObjectAdapter.h"
#include "include/trafficObjectInterface.h"

/// Class representing an adapter for a traffic object
class TrafficObjectAdapter : public WorldObjectAdapter, public TrafficObjectInterface
{
private:
  bool isCollidable;
  units::angle::radian_t laneDirection;
  const World::Localization::Localizer& localizer;

  mutable World::Localization::Result locateResult;
  mutable std::vector<GlobalRoadPosition> boundaryPoints;

  OpenDriveId openDriveId;

  void InitLaneDirection(units::angle::radian_t hdg);

public:
  /// @brief TrafficObjectAdapter constructor
  /// @param id           id of the openpass entity
  /// @param worldData    Reference to the world data
  /// @param localizer    Reference to the world locallizer
  /// @param position     Absolute position of the entity
  /// @param dimension    Dimension of the entity
  /// @param orientation  Absolute orientation of the entity
  /// @param odId         Opendrive ID
  TrafficObjectAdapter(const openpass::type::EntityId id,
                       OWL::Interfaces::WorldData& worldData,
                       const World::Localization::Localizer& localizer,
                       mantle_api::Vec3<units::length::meter_t> position,
                       mantle_api::Dimension3 dimension,
                       mantle_api::Orientation3<units::angle::radian_t> orientation,
                       const OpenDriveId odId);

  ObjectTypeOSI GetType() const override;

  /// @return Returns true, if the traffic object is collidable
  bool GetIsCollidable() const override;
  Common::Vector2d<units::velocity::meters_per_second_t> GetVelocity(ObjectPoint point
                                                                     = ObjectPointPredefined::Reference) const override;
  Common::Vector2d<units::acceleration::meters_per_second_squared_t> GetAcceleration(
      ObjectPoint point = ObjectPointPredefined::Reference) const override;
  /// @return Getter function to return the direction of lane
  units::angle::radian_t GetLaneDirection() const;
  bool Locate() override;
  void Unlocate() override;

  const RoadIntervals& GetTouchedRoads() const override;

  Common::Vector2d<units::length::meter_t> GetAbsolutePosition(const ObjectPoint& objectPoint) const override;

  const GlobalRoadPositions& GetRoadPosition(const ObjectPoint& point) const override;

  OpenDriveId GetOpenDriveId() const override;

  // object is not inteded to be copied or assigned
  TrafficObjectAdapter(const TrafficObjectAdapter&) = delete;
  TrafficObjectAdapter(TrafficObjectAdapter&&) = delete;
  TrafficObjectAdapter& operator=(const TrafficObjectAdapter&) = delete;
  TrafficObjectAdapter& operator=(TrafficObjectAdapter&&) = delete;
  ~TrafficObjectAdapter() override;
};
