/********************************************************************************
 * Copyright (c) 2017-2019 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "LocalizationElement.h"

namespace World
{
namespace Localization
{

/// Class to convert world coordinates to the road coordinates
class WorldToRoadCoordinateConverter
{
  const LocalizationElement& element;

public:
  //! WorldToRoadCoordinateConverter constructor
  //!
  //! @param[in] localizationElement  LocalizationElement
  WorldToRoadCoordinateConverter(const LocalizationElement& localizationElement) : element{localizationElement} {}

  /// @brief Check if a 2d point is convertible to road coordinates
  /// @param point A 2d vector point in world coordinates
  /// @return True, if the point is convertible
  bool IsConvertible(const Common::Vector2d<units::length::meter_t>& point) const;

  /// @brief Get the road coordinates from the given 2d point and its heading angle
  /// @param point A 2d vector point in world coordinates
  /// @param hdg   Heading angle
  /// @return Road position
  RoadPosition GetRoadCoordinate(const Common::Vector2d<units::length::meter_t>& point,
                                 units::angle::radian_t hdg) const;

  /// @brief Get S coordinate from the given 2d point in world coordinates
  /// @param point A 2d vector point in world coordinates
  /// @return S coordinate
  units::length::meter_t GetS(const Common::Vector2d<units::length::meter_t>& point) const;

private:
  Common::Vector2d<units::length::meter_t> GetIntersectionPoint(
      const Common::Vector2d<units::length::meter_t>& point) const;
  units::length::meter_t CalcS(const Common::Vector2d<units::length::meter_t>& intersectionPoint) const;
  units::length::meter_t CalcT(const Common::Vector2d<units::length::meter_t>& point,
                               const Common::Vector2d<units::length::meter_t>& intersectionPoint) const;
  units::angle::radian_t CalcYaw(units::angle::radian_t hdg) const;
  Common::Vector2d<units::length::meter_t> ProjectOntoReferenceAxis(
      const Common::Vector2d<units::length::meter_t>& point) const;

  bool IsLeftOfReferenceAxis(const Common::Vector2d<units::length::meter_t>& vector) const;
};

}  // namespace Localization
}  // namespace World
