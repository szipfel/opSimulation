/********************************************************************************
 * Copyright (c) 2019 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <gmock/gmock.h>

#include "include/agentBlueprintInterface.h"
#include "include/agentBlueprintProviderInterface.h"

class FakeAgentBlueprintProvider : public AgentBlueprintProviderInterface
{
public:
  MOCK_CONST_METHOD0(GetDefaultSystem, System());
  MOCK_CONST_METHOD1(SampleSystem, System(const std::string &agentProfileName));
  MOCK_CONST_METHOD1(SampleVehicleModelName, std::string(const std::string &agentProfileName));
};
