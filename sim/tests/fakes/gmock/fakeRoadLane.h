/********************************************************************************
 * Copyright (c) 2020 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <gmock/gmock.h>

#include "include/roadInterface/roadLaneInterface.h"

class FakeRoadLane : public RoadLaneInterface
{
public:
  MOCK_METHOD5(AddWidth,
               bool(units::length::meter_t sOffset,
                    units::length::meter_t a,
                    double b,
                    units::unit_t<units::inverse<units::length::meter>> c,
                    units::unit_t<units::inverse<units::squared<units::length::meter>>> d));
  MOCK_METHOD5(AddBorder,
               bool(units::length::meter_t sOffset,
                    units::length::meter_t a,
                    double b,
                    units::unit_t<units::inverse<units::length::meter>> c,
                    units::unit_t<units::inverse<units::squared<units::length::meter>>> d));
  MOCK_METHOD6(AddRoadMark,
               bool(units::length::meter_t sOffset,
                    RoadLaneRoadDescriptionType descType,
                    RoadLaneRoadMarkType roadMark,
                    RoadLaneRoadMarkColor color,
                    RoadLaneRoadMarkLaneChange laneChange,
                    RoadLaneRoadMarkWeight weight));
  MOCK_METHOD1(AddSuccessor, bool(int id));
  MOCK_METHOD1(AddPredecessor, bool(int id));
  MOCK_CONST_METHOD0(GetId, int());
  MOCK_CONST_METHOD0(GetType, RoadLaneType());
  MOCK_CONST_METHOD0(GetWidths, const RoadLaneWidths &());
  MOCK_CONST_METHOD0(GetBorders, const RoadLaneWidths &());
  MOCK_CONST_METHOD0(GetSuccessor, const std::vector<int> &());
  MOCK_CONST_METHOD0(GetPredecessor, const std::vector<int> &());
  MOCK_METHOD1(SetInDirection, void(bool inDirection));
  MOCK_CONST_METHOD0(GetInDirection, bool());
  MOCK_CONST_METHOD0(GetLaneSection, RoadLaneSectionInterface *());
  MOCK_CONST_METHOD0(GetRoadMarks, const RoadLaneRoadMarks &());
};
