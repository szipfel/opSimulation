/********************************************************************************
 * Copyright (c) 2023 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <numeric>

#include "brakesystem.h"
#include "common/longitudinalSignal.h"
#include "fakeAgent.h"
#include "fakeParameter.h"

using ::testing::NiceMock;
using ::testing::Return;
using ::testing::ReturnRef;

TEST(BrakeSystem, BrakeWithResponseTime)
{
  NiceMock<FakeParameter> fakeParameters;
  std::map<std::string, double> fakeParametersDouble;

  int fakeCycleTimeMs = 10;
  double fakeBrakeResponseTimeMs = 100.00;
  double fakeFrontAxlePercentage = 0.3;
  double fakeBrakeDecelerationInclineRate = 1.00;
  double fakeBrakeDecelerationDeclineRate = 100.00;

  fakeParametersDouble.insert(std::pair<std::string, double>("FrontAxlePercentage", fakeFrontAxlePercentage));
  fakeParametersDouble.insert(
      std::pair<std::string, double>("BrakeDecelerationInclineRate", fakeBrakeDecelerationInclineRate));
  fakeParametersDouble.insert(
      std::pair<std::string, double>("BrakeDecelerationDeclineRate", fakeBrakeDecelerationDeclineRate));
  fakeParametersDouble.insert(std::pair<std::string, double>("BrakeResponseTimeMs", fakeBrakeResponseTimeMs));

  ON_CALL(fakeParameters, GetParametersDouble()).WillByDefault(ReturnRef(fakeParametersDouble));

  NiceMock<FakeAgent> fakeAgent;

  auto fakeVehicleModelParameters = std::make_shared<mantle_api::VehicleProperties>();
  fakeVehicleModelParameters->performance.max_deceleration = 10.0_mps_sq;
  fakeVehicleModelParameters->mass = 1500_kg;

  fakeVehicleModelParameters->front_axle.wheel_diameter = 0.5_m;
  fakeVehicleModelParameters->rear_axle.wheel_diameter = 0.5_m;

  ON_CALL(fakeAgent, GetVehicleModelParameters()).WillByDefault(Return(fakeVehicleModelParameters));
  ON_CALL(fakeAgent, GetMaxDeceleration()).WillByDefault(Return(10.0_mps_sq));

  ActionBrakeSystem implementation(
      "", false, 0, 0, 0, fakeCycleTimeMs, nullptr, nullptr, &fakeParameters, nullptr, nullptr, &fakeAgent);

  std::shared_ptr<SignalInterface const> outputSignal;

  double fakeBrakePedalPos = 1.0;
  const auto fakeLongitudinalSignal
      = std::make_shared<LongitudinalSignal const>(ComponentState::Acting, 0.0, fakeBrakePedalPos, 1);
  implementation.UpdateInput(0, std::make_shared<BoolSignal const>(false), 0);
  implementation.UpdateInput(1, fakeLongitudinalSignal, 0);
  int fakeTimeMs = 0;
  std::vector<double> wheelBrakeTorque;
  while ((double)(fakeBrakeResponseTimeMs - (double)fakeTimeMs) > std::numeric_limits<double>::epsilon())
  {
    implementation.Trigger(fakeTimeMs);
    implementation.UpdateOutput(0, outputSignal, fakeTimeMs);
    wheelBrakeTorque = std::static_pointer_cast<SignalVectorDouble const>(outputSignal)->value;
    ASSERT_DOUBLE_EQ(*std::min_element(wheelBrakeTorque.begin(), wheelBrakeTorque.end()), 0.0);
    fakeTimeMs += fakeCycleTimeMs;
  }

  implementation.Trigger(fakeTimeMs);
  implementation.UpdateOutput(0, outputSignal, fakeTimeMs);
  wheelBrakeTorque = std::static_pointer_cast<SignalVectorDouble const>(outputSignal)->value;

  ASSERT_TRUE(*std::min_element(wheelBrakeTorque.begin(), wheelBrakeTorque.end())
              < -std::numeric_limits<double>::epsilon());
}

TEST(BrakeSystem, RequestPrefill)
{
  NiceMock<FakeParameter> fakeParameters;
  std::map<std::string, double> fakeParametersDouble;

  int fakeCycleTimeMs = 10;
  double fakeBrakeResponseTimeMs = 100.00;
  double fakeFrontAxlePercentage = 0.3;
  double fakeBrakeDecelerationInclineRate = 1.00;
  double fakeBrakeDecelerationDeclineRate = 100.00;

  fakeParametersDouble.insert(std::pair<std::string, double>("FrontAxlePercentage", fakeFrontAxlePercentage));
  fakeParametersDouble.insert(
      std::pair<std::string, double>("BrakeDecelerationInclineRate", fakeBrakeDecelerationInclineRate));
  fakeParametersDouble.insert(
      std::pair<std::string, double>("BrakeDecelerationDeclineRate", fakeBrakeDecelerationDeclineRate));
  fakeParametersDouble.insert(std::pair<std::string, double>("BrakeResponseTimeMs", fakeBrakeResponseTimeMs));

  ON_CALL(fakeParameters, GetParametersDouble()).WillByDefault(ReturnRef(fakeParametersDouble));

  NiceMock<FakeAgent> fakeAgent;

  auto fakeVehicleModelParameters = std::make_shared<mantle_api::VehicleProperties>();
  fakeVehicleModelParameters->performance.max_deceleration = 10.0_mps_sq;
  fakeVehicleModelParameters->mass = 1500_kg;

  fakeVehicleModelParameters->front_axle.wheel_diameter = 0.5_m;
  fakeVehicleModelParameters->rear_axle.wheel_diameter = 0.5_m;

  ON_CALL(fakeAgent, GetVehicleModelParameters()).WillByDefault(Return(fakeVehicleModelParameters));
  ON_CALL(fakeAgent, GetMaxDeceleration()).WillByDefault(Return(10.0_mps_sq));

  ActionBrakeSystem implementation(
      "", false, 0, 0, 0, fakeCycleTimeMs, nullptr, nullptr, &fakeParameters, nullptr, nullptr, &fakeAgent);

  std::shared_ptr<SignalInterface const> outputSignal;

  implementation.UpdateInput(0, std::make_shared<BoolSignal const>(true), 0);
  int fakeTimeMs = 0;
  std::vector<double> wheelBrakeTorque;
  while ((double)(fakeBrakeResponseTimeMs - (double)fakeTimeMs) > std::numeric_limits<double>::epsilon())
  {
    implementation.Trigger(fakeTimeMs);
    implementation.UpdateOutput(0, outputSignal, fakeTimeMs);
    wheelBrakeTorque = std::static_pointer_cast<SignalVectorDouble const>(outputSignal)->value;
    ASSERT_DOUBLE_EQ(*std::min_element(wheelBrakeTorque.begin(), wheelBrakeTorque.end()), 0.0);
    fakeTimeMs += fakeCycleTimeMs;
  }

  double fakeBrakePedalPos = 1.0;
  const auto fakeLongitudinalSignal
      = std::make_shared<LongitudinalSignal const>(ComponentState::Acting, 0.0, fakeBrakePedalPos, 1);
  implementation.UpdateInput(0, std::make_shared<BoolSignal const>(false), fakeTimeMs);
  implementation.UpdateInput(1, fakeLongitudinalSignal, fakeTimeMs);
  implementation.Trigger(fakeTimeMs);
  implementation.UpdateOutput(0, outputSignal, fakeTimeMs);
  wheelBrakeTorque = std::static_pointer_cast<SignalVectorDouble const>(outputSignal)->value;

  ASSERT_TRUE(*std::min_element(wheelBrakeTorque.begin(), wheelBrakeTorque.end())
              < -std::numeric_limits<double>::epsilon());
}

TEST(BrakeSystem, BrakeWithLinearDecelerationRate)
{
  NiceMock<FakeParameter> fakeParameters;
  std::map<std::string, double> fakeParametersDouble;

  int fakeCycleTimeMs = 10;
  double fakeBrakeResponseTimeMs = 0.0;
  double fakeFrontAxlePercentage = 0.3;
  double fakeBrakeDecelerationInclineRate = 3.00;
  double fakeBrakeDecelerationDeclineRate = 1.00;
  double fakeVehicleMass = 1500;
  fakeParametersDouble.insert(std::pair<std::string, double>("FrontAxlePercentage", fakeFrontAxlePercentage));
  fakeParametersDouble.insert(
      std::pair<std::string, double>("BrakeDecelerationInclineRate", fakeBrakeDecelerationInclineRate));
  fakeParametersDouble.insert(
      std::pair<std::string, double>("BrakeDecelerationDeclineRate", fakeBrakeDecelerationDeclineRate));
  fakeParametersDouble.insert(std::pair<std::string, double>("BrakeResponseTimeMs", fakeBrakeResponseTimeMs));

  ON_CALL(fakeParameters, GetParametersDouble()).WillByDefault(ReturnRef(fakeParametersDouble));

  NiceMock<FakeAgent> fakeAgent;

  auto fakeVehicleModelParameters = std::make_shared<mantle_api::VehicleProperties>();
  fakeVehicleModelParameters->performance.max_deceleration = 10.0_mps_sq;
  fakeVehicleModelParameters->mass = fakeVehicleMass * 1_kg;

  fakeVehicleModelParameters->front_axle.wheel_diameter = 0.5_m;
  fakeVehicleModelParameters->rear_axle.wheel_diameter = 0.5_m;

  ON_CALL(fakeAgent, GetVehicleModelParameters()).WillByDefault(Return(fakeVehicleModelParameters));
  ON_CALL(fakeAgent, GetMaxDeceleration()).WillByDefault(Return(10.0_mps_sq));

  ActionBrakeSystem implementation(
      "", false, 0, 0, 0, fakeCycleTimeMs, nullptr, nullptr, &fakeParameters, nullptr, nullptr, &fakeAgent);

  std::shared_ptr<SignalInterface const> outputSignal;
  implementation.UpdateInput(0, std::make_shared<BoolSignal const>(false), 0);

  int fakeTimeMs = 0;
  std::vector<double> wheelBrakeTorque;

  double fakeBrakePedalPos = 1.0;
  implementation.UpdateInput(
      1, std::make_shared<LongitudinalSignal const>(ComponentState::Acting, 0.0, fakeBrakePedalPos, 1), fakeTimeMs);
  implementation.Trigger(fakeTimeMs);
  implementation.UpdateOutput(0, outputSignal, fakeTimeMs);
  wheelBrakeTorque = std::static_pointer_cast<SignalVectorDouble const>(outputSignal)->value;

  double expectedWheelTorqueFrontAxleWheel = -fakeCycleTimeMs / 1000.0 * fakeBrakeDecelerationInclineRate
                                           * fakeVehicleMass * fakeFrontAxlePercentage
                                           * fakeVehicleModelParameters->front_axle.wheel_diameter.value() / 2.0 * 0.5;
  ASSERT_EQ(wheelBrakeTorque.size(), 4);
  ASSERT_DOUBLE_EQ(wheelBrakeTorque[0], wheelBrakeTorque[1]);
  ASSERT_DOUBLE_EQ(wheelBrakeTorque[2], wheelBrakeTorque[3]);
  ASSERT_DOUBLE_EQ(wheelBrakeTorque[0] / fakeFrontAxlePercentage, wheelBrakeTorque[2] / (1 - fakeFrontAxlePercentage));
  ASSERT_DOUBLE_EQ(wheelBrakeTorque[0], expectedWheelTorqueFrontAxleWheel);
  fakeTimeMs += fakeCycleTimeMs;
  fakeBrakePedalPos = 0.0;
  implementation.UpdateInput(
      1, std::make_shared<LongitudinalSignal const>(ComponentState::Acting, 0.0, fakeBrakePedalPos, 1), fakeTimeMs);
  implementation.Trigger(fakeTimeMs);
  implementation.UpdateOutput(0, outputSignal, fakeTimeMs);
  wheelBrakeTorque = std::static_pointer_cast<SignalVectorDouble const>(outputSignal)->value;
  expectedWheelTorqueFrontAxleWheel += fakeCycleTimeMs / 1000.0 * fakeBrakeDecelerationDeclineRate * fakeVehicleMass
                                     * fakeFrontAxlePercentage
                                     * fakeVehicleModelParameters->front_axle.wheel_diameter.value() / 2.0 * 0.5;
  ASSERT_DOUBLE_EQ(wheelBrakeTorque[0], expectedWheelTorqueFrontAxleWheel);
}

TEST(BrakeSystem, BrakeWithMaximumDeceleration)
{
  NiceMock<FakeParameter> fakeParameters;
  std::map<std::string, double> fakeParametersDouble;

  int fakeCycleTimeMs = 1000;
  double fakeBrakeResponseTimeMs = 0.0;
  double fakeFrontAxlePercentage = 0.3;
  double fakeBrakeDecelerationInclineRate = 10.00;
  double fakeBrakeDecelerationDeclineRate = 40.00;
  double fakeVehicleMass = 1500;
  fakeParametersDouble.insert(std::pair<std::string, double>("FrontAxlePercentage", fakeFrontAxlePercentage));
  fakeParametersDouble.insert(
      std::pair<std::string, double>("BrakeDecelerationInclineRate", fakeBrakeDecelerationInclineRate));
  fakeParametersDouble.insert(
      std::pair<std::string, double>("BrakeDecelerationDeclineRate", fakeBrakeDecelerationDeclineRate));
  fakeParametersDouble.insert(std::pair<std::string, double>("BrakeResponseTimeMs", fakeBrakeResponseTimeMs));

  ON_CALL(fakeParameters, GetParametersDouble()).WillByDefault(ReturnRef(fakeParametersDouble));

  NiceMock<FakeAgent> fakeAgent;

  auto fakeVehicleModelParameters = std::make_shared<mantle_api::VehicleProperties>();
  fakeVehicleModelParameters->performance.max_deceleration = 10.0_mps_sq;
  fakeVehicleModelParameters->mass = fakeVehicleMass * 1_kg;

  fakeVehicleModelParameters->front_axle.wheel_diameter = 0.5_m;
  fakeVehicleModelParameters->rear_axle.wheel_diameter = 0.5_m;

  ON_CALL(fakeAgent, GetVehicleModelParameters()).WillByDefault(Return(fakeVehicleModelParameters));
  ON_CALL(fakeAgent, GetMaxDeceleration()).WillByDefault(Return(10.0_mps_sq));

  ActionBrakeSystem implementation(
      "", false, 0, 0, 0, fakeCycleTimeMs, nullptr, nullptr, &fakeParameters, nullptr, nullptr, &fakeAgent);

  std::shared_ptr<SignalInterface const> outputSignal;
  implementation.UpdateInput(0, std::make_shared<BoolSignal const>(false), 0);

  int fakeTimeMs = 0;
  std::vector<double> wheelBrakeTorque;

  double fakeBrakePedalPos = 1.0;
  implementation.UpdateInput(
      1, std::make_shared<LongitudinalSignal const>(ComponentState::Acting, 0.0, fakeBrakePedalPos, 1), fakeTimeMs);
  implementation.Trigger(fakeTimeMs);
  implementation.UpdateOutput(0, outputSignal, fakeTimeMs);
  wheelBrakeTorque = std::static_pointer_cast<SignalVectorDouble const>(outputSignal)->value;
  fakeTimeMs += fakeCycleTimeMs;
  double SumDeceleration = std::accumulate(wheelBrakeTorque.begin(), wheelBrakeTorque.end(), 0) * 2
                         / fakeVehicleModelParameters->front_axle.wheel_diameter.value() / fakeVehicleMass;
  ASSERT_TRUE(fakeVehicleModelParameters->performance.max_deceleration.value() >= std::abs(SumDeceleration));
  implementation.Trigger(fakeTimeMs);
  implementation.UpdateOutput(0, outputSignal, fakeTimeMs);
  wheelBrakeTorque = std::static_pointer_cast<SignalVectorDouble const>(outputSignal)->value;
  fakeTimeMs += fakeCycleTimeMs;
  SumDeceleration = std::accumulate(wheelBrakeTorque.begin(), wheelBrakeTorque.end(), 0) * 2
                  / fakeVehicleModelParameters->front_axle.wheel_diameter.value() / fakeVehicleMass;
  ASSERT_TRUE(fakeVehicleModelParameters->performance.max_deceleration.value() >= std::abs(SumDeceleration));
  fakeBrakePedalPos = 0.0;
  implementation.UpdateInput(
      1, std::make_shared<LongitudinalSignal const>(ComponentState::Acting, 0.0, fakeBrakePedalPos, 1), fakeTimeMs);
  implementation.Trigger(fakeTimeMs);
  implementation.UpdateOutput(0, outputSignal, fakeTimeMs);
  wheelBrakeTorque = std::static_pointer_cast<SignalVectorDouble const>(outputSignal)->value;
  SumDeceleration = std::accumulate(wheelBrakeTorque.begin(), wheelBrakeTorque.end(), 0) * 2
                  / fakeVehicleModelParameters->front_axle.wheel_diameter.value() / fakeVehicleMass;
  ASSERT_DOUBLE_EQ(0.0, std::abs(SumDeceleration));
}