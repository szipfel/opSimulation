################################################################################
# Copyright (c) 2020-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################
# include_guard is necessary here to avoid repetitive processing of FmuWrapper, as SSPWrapper also includes FmuWrapper
include_guard(GLOBAL)
set(COMPONENT_TEST_NAME AlgorithmFmuWrapper_Tests)
set(COMPONENT_SOURCE_DIR ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src)

add_openpass_target(
  NAME ${COMPONENT_TEST_NAME} TYPE test COMPONENT module
  DEFAULT_MAIN

  SOURCES
    ChannelDefinitionParserUnitTests.cpp
    FmuFileHelperTests.cpp
    FmuWriteAndReadValuesTests.cpp
    OsmpFmuUnitTests.cpp
    SignalTranslatorTests.cpp
    ${COMPONENT_SOURCE_DIR}/SignalInterface/AccelerationSignalParser.cpp
    ${COMPONENT_SOURCE_DIR}/SignalInterface/CompCtrlSignalParser.cpp
    ${COMPONENT_SOURCE_DIR}/SignalInterface/DynamicsSignalParser.cpp
    ${COMPONENT_SOURCE_DIR}/SignalInterface/LongitudinalSignalParser.cpp
    ${COMPONENT_SOURCE_DIR}/ChannelDefinitionParser.cpp
    ${COMPONENT_SOURCE_DIR}/SignalInterface/OsiSensorDataParser.cpp
    ${COMPONENT_SOURCE_DIR}/SignalInterface/SteeringSignalParser.cpp
    ${COMPONENT_SOURCE_DIR}/SignalInterface/TrafficUpdateSignalParser.cpp
    ${COMPONENT_SOURCE_DIR}/SignalInterface/SignalMessageVisitor.cpp
    ${COMPONENT_SOURCE_DIR}/fmuFileHelper.cpp
    ${COMPONENT_SOURCE_DIR}/SignalInterface/SignalTranslator.cpp
    ${COMPONENT_SOURCE_DIR}/FmiImporter/src/Common/fmuChecker.c
    ${COMPONENT_SOURCE_DIR}/FmiImporter/src/FMI1/fmi1_check.c
    ${COMPONENT_SOURCE_DIR}/FmiImporter/src/FMI1/fmi1_cs_sim.c
    ${COMPONENT_SOURCE_DIR}/FmiImporter/src/FMI1/fmi1_me_sim.c
    ${COMPONENT_SOURCE_DIR}/FmiImporter/src/FMI2/fmi2_check.c
    ${COMPONENT_SOURCE_DIR}/FmiImporter/src/FMI2/fmi2_cs_sim.c
    ${COMPONENT_SOURCE_DIR}/FmiImporter/src/FMI2/fmi2_me_sim.c

    ${COMPONENT_SOURCE_DIR}/FmuHandler.cpp
    ${COMPONENT_SOURCE_DIR}/FmuCalculations.cpp
    ${COMPONENT_SOURCE_DIR}/FmuCommunication.cpp
    ${COMPONENT_SOURCE_DIR}/FmuHelper.cpp
    ${COMPONENT_SOURCE_DIR}/ChannelDefinitionParser.cpp

    ${OPENPASS_SIMCORE_DIR}/core/opSimulation/framework/sampler.cpp
    ${OPENPASS_SIMCORE_DIR}/core/opSimulation/modules/World_OSI/OWL/DataTypes.cpp
    ${OPENPASS_SIMCORE_DIR}/core/opSimulation/modules/World_OSI/OWL/MovingObject.cpp
    ${OPENPASS_SIMCORE_DIR}/core/opSimulation/modules/World_OSI/OWL/OpenDriveTypeMapper.cpp

  HEADERS
    ${COMPONENT_SOURCE_DIR}/fmuFileHelper.h
    ${COMPONENT_SOURCE_DIR}/SignalInterface/SignalTranslator.h
    ${COMPONENT_SOURCE_DIR}/FmuCalculations.h

  INCDIRS
    ${COMPONENT_SOURCE_DIR}
    ${COMPONENT_SOURCE_DIR}/FmiImporter/include
    ${OPENPASS_SIMCORE_DIR}/core/opSimulation/modules/World_OSI
    # FMILibrary internals
    ${FMILibrary_INCLUDE_DIR}
    ${FMILibrary_INCLUDE_DIR}/FMI
    ${FMILibrary_INCLUDE_DIR}/FMI1
    ${FMILibrary_INCLUDE_DIR}/FMI2
    ${FMILibrary_INCLUDE_DIR}/JM
    ${OPENPASS_SIMCORE_DIR}/core/opSimulation

  LIBRARIES
    Qt5::Core
    Common
    ${FMILibrary_LIBRARY_DIR}

  LINKOSI static
)
