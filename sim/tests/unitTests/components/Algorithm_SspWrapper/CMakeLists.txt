################################################################################
# Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

set(COMPONENT_TEST_NAME AlgorithmSspWrapper_Tests_1)
set(COMPONENT_SOURCE_DIR ${OPENPASS_SIMCORE_DIR}/components/Algorithm_SspWrapper)

add_openpass_target(
        NAME ${COMPONENT_TEST_NAME} TYPE test COMPONENT module
        DEFAULT_MAIN

        SOURCES
        SSPFileHelperTests.cpp
        SSPLoggingTests.cpp
        SSPParserTests.cpp

        ${COMPONENT_SOURCE_DIR}/SsdToSspNetworkParser.cpp
        ${COMPONENT_SOURCE_DIR}/OSMPConnectorFactory.cpp
        ${COMPONENT_SOURCE_DIR}/ScalarConnectorFactory.cpp
        ${COMPONENT_SOURCE_DIR}/ParserTypes.cpp
        ${COMPONENT_SOURCE_DIR}/SspLogger.cpp

        ${COMPONENT_SOURCE_DIR}/Importer/SsdFile.cpp
        ${COMPONENT_SOURCE_DIR}/Importer/FileElements/SsdSystem.cpp
        ${COMPONENT_SOURCE_DIR}/Importer/FileElements/SsdComponent.cpp
        ${COMPONENT_SOURCE_DIR}/Importer/FileElements/SsdURI.cpp
        ${COMPONENT_SOURCE_DIR}/Importer/SsdFileImporter.cpp

        ${COMPONENT_SOURCE_DIR}/Visitors/Network/SspTriggerVisitor.cpp
        ${COMPONENT_SOURCE_DIR}/Visitors/Network/SspInitVisitor.cpp
        ${COMPONENT_SOURCE_DIR}/Visitors/Network/CalcParamInitVisitor.cpp
        ${COMPONENT_SOURCE_DIR}/Visitors/Connector/PropagateDataVisitor.cpp
        ${COMPONENT_SOURCE_DIR}/Visitors/Connector/TriggerSignalVisitor.cpp
        ${COMPONENT_SOURCE_DIR}/Visitors/Connector/UpdateOutputSignalVisitor.cpp
        ${COMPONENT_SOURCE_DIR}/Visitors/Connector/UpdateInputSignalVisitor.cpp
        ${COMPONENT_SOURCE_DIR}/Visitors/SspVisitorHelper.cpp

        ${COMPONENT_SOURCE_DIR}/SSPElements/NetworkElement.cpp
        ${COMPONENT_SOURCE_DIR}/SSPElements/Component.cpp
        ${COMPONENT_SOURCE_DIR}/SSPElements/System.cpp
        ${COMPONENT_SOURCE_DIR}/SSPElements/Connection.cpp
        ${COMPONENT_SOURCE_DIR}/SSPElements/Connector/Connector.cpp
        ${COMPONENT_SOURCE_DIR}/SSPElements/Connector/ConnectorHelper.cpp
        ${COMPONENT_SOURCE_DIR}/SSPElements/Connector/ScalarConnectorBase.cpp
        ${COMPONENT_SOURCE_DIR}/SSPElements/Connector/OSMPConnectorBase.cpp
        ${COMPONENT_SOURCE_DIR}/SSPElements/Connector/GroupConnector.cpp
        ${COMPONENT_SOURCE_DIR}/SSPElements/Enumeration/Enumeration.cpp

        ${OPENPASS_SIMCORE_DIR}/core/opSimulation/framework/sampler.cpp
        ${OPENPASS_SIMCORE_DIR}/core/opSimulation/modelElements/parameters.cpp
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/fmuWrapper.cpp
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/fmuFileHelper.cpp
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/FmuHandler.cpp
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/FmuCalculations.cpp
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/FmuCommunication.cpp
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/FmuHelper.cpp
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/ChannelDefinitionParser.cpp
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/FmiImporter/src/Common/fmuChecker.c
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/SignalInterface/SignalTranslator.cpp
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/SignalInterface/AccelerationSignalParser.cpp
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/SignalInterface/CompCtrlSignalParser.cpp
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/SignalInterface/DynamicsSignalParser.cpp
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/SignalInterface/LongitudinalSignalParser.cpp
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/SignalInterface/OsiSensorDataParser.cpp
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/SignalInterface/SignalMessageVisitor.cpp
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/SignalInterface/SteeringSignalParser.cpp
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/SignalInterface/TrafficUpdateSignalParser.cpp

        ${OPENPASS_SIMCORE_DIR}/core/opSimulation/modules/World_OSI/OWL/DataTypes.cpp
        ${OPENPASS_SIMCORE_DIR}/core/opSimulation/modules/World_OSI/OWL/MovingObject.cpp
        ${OPENPASS_SIMCORE_DIR}/core/opSimulation/modules/World_OSI/OWL/OpenDriveTypeMapper.cpp


        HEADERS
        SimpleAffineFMU.h

        ${COMPONENT_SOURCE_DIR}/SsdToSspNetworkParser.h
        ${COMPONENT_SOURCE_DIR}/ParserTypes.h
        ${COMPONENT_SOURCE_DIR}/OSMPConnectorFactory.h
        ${COMPONENT_SOURCE_DIR}/ScalarConnectorFactory.h
        ${COMPONENT_SOURCE_DIR}/SspLogger.h

        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/fmuWrapper.h
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/fmuFileHelper.h
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/FmuHandler.h
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/FmuCalculations.h
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/FmuHelper.h
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/SignalInterface/SignalTranslator.h
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/SignalInterface/AccelerationSignalParser.h
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/SignalInterface/CompCtrlSignalParser.h
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/SignalInterface/DynamicsSignalParser.h
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/SignalInterface/LongitudinalSignalParser.h
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/SignalInterface/OsiSensorDataParser.h
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/SignalInterface/SteeringSignalParser.h
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/SignalInterface/TrafficUpdateSignalParser.h

        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/ChannelDefinitionParser.h

        ${COMPONENT_SOURCE_DIR}/Importer/SsdFile.h
        ${COMPONENT_SOURCE_DIR}/Importer/FileElements/SsdSystem.h
        ${COMPONENT_SOURCE_DIR}/Importer/FileElements/SsdComponent.h
        ${COMPONENT_SOURCE_DIR}/Importer/FileElements/SsdURI.h
        ${COMPONENT_SOURCE_DIR}/Importer/SsdFileImporter.h

        ${COMPONENT_SOURCE_DIR}/Visitors/Network/SspNetworkVisitorInterface.h
        ${COMPONENT_SOURCE_DIR}/Visitors/Network/SspTriggerVisitor.h
        ${COMPONENT_SOURCE_DIR}/Visitors/Network/SspInitVisitor.h
        ${COMPONENT_SOURCE_DIR}/Visitors/Network/ParameterInitializationVisitor.h
        ${COMPONENT_SOURCE_DIR}/Visitors/Network/CalcParamInitVisitor.h
        ${COMPONENT_SOURCE_DIR}/Visitors/Connector/PropagateDataVisitor.h
        ${COMPONENT_SOURCE_DIR}/Visitors/Connector/TriggerSignalVisitor.h
        ${COMPONENT_SOURCE_DIR}/Visitors/Connector/TriggerVisit.h
        ${COMPONENT_SOURCE_DIR}/Visitors/Connector/UpdateOutputSignalVisitor.h
        ${COMPONENT_SOURCE_DIR}/Visitors/Connector/UpdateOutputVisitor.h
        ${COMPONENT_SOURCE_DIR}/Visitors/Connector/UpdateInputSignalVisitor.h
        ${COMPONENT_SOURCE_DIR}/Visitors/Connector/UpdateInputVisitor.h
        ${COMPONENT_SOURCE_DIR}/Visitors/SspVisitorHelper.h

        ${COMPONENT_SOURCE_DIR}/SSPElements/NetworkElement.h
        ${COMPONENT_SOURCE_DIR}/SSPElements/Component.h
        ${COMPONENT_SOURCE_DIR}/SSPElements/System.h
        ${COMPONENT_SOURCE_DIR}/SSPElements/Connection.h
        ${COMPONENT_SOURCE_DIR}/SSPElements/Connector/Connector.h
        ${COMPONENT_SOURCE_DIR}/SSPElements/Connector/ConnectorHelper.h
        ${COMPONENT_SOURCE_DIR}/SSPElements/Connector/ConnectorInterface.h
        ${COMPONENT_SOURCE_DIR}/SSPElements/Connector/ScalarConnector.h
        ${COMPONENT_SOURCE_DIR}/SSPElements/Connector/ScalarConnectorBase.h
        ${COMPONENT_SOURCE_DIR}/SSPElements/Connector/OSMPConnectorBase.h
        ${COMPONENT_SOURCE_DIR}/SSPElements/Connector/OSMPConnector.h
        ${COMPONENT_SOURCE_DIR}/SSPElements/Connector/GroupConnector.h
        ${COMPONENT_SOURCE_DIR}/SSPElements/Enumeration/Enumeration.h

        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/FmiImporter/src/FMI1/fmi1_check.c
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/FmiImporter/src/FMI1/fmi1_cs_sim.c
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/FmiImporter/src/FMI1/fmi1_me_sim.c
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/FmiImporter/src/FMI2/fmi2_check.c
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/FmiImporter/src/FMI2/fmi2_cs_sim.c
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/FmiImporter/src/FMI2/fmi2_me_sim.c

        INCDIRS
        ${COMPONENT_SOURCE_DIR}
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/FmiImporter/include
        ${OPENPASS_SIMCORE_DIR}/core/opSimulation
        ${OPENPASS_SIMCORE_DIR}/core/opSimulation/modules/World_OSI
        Algorithm_FmuWrapper
        # FMILibrary internals
        ${FMILibrary_INCLUDE_DIR}
        ${FMILibrary_INCLUDE_DIR}/FMI
        ${FMILibrary_INCLUDE_DIR}/FMI1
        ${FMILibrary_INCLUDE_DIR}/FMI2
        ${FMILibrary_INCLUDE_DIR}/JM
        ${OPENPASS_SIMCORE_DIR}/include
        ${OPENPASS_SIMCORE_DIR}/core

        LIBRARIES
        Algorithm_FmuWrapper
        CoreCommon
        SimulationCore
        Qt5::Core
        Qt5::Xml
        Common
        ${FMILibrary_LIBRARY_DIR}

        RESOURCES
        Resources

        LINKOSI static
)

set(COMPONENT_TEST_NAME AlgorithmSspWrapper_Tests_2)
add_openpass_target(
        NAME ${COMPONENT_TEST_NAME} TYPE test COMPONENT module
        DEFAULT_MAIN

        SOURCES
        SSPNetworkTests.cpp
        SSPImporterTests.cpp
        SSPAlgorithmTests.cpp
        SimpleAffineFMU.cpp

        ${COMPONENT_SOURCE_DIR}/SsdToSspNetworkParser.cpp
        ${COMPONENT_SOURCE_DIR}/OSMPConnectorFactory.cpp
        ${COMPONENT_SOURCE_DIR}/ScalarConnectorFactory.cpp
        ${COMPONENT_SOURCE_DIR}/ParserTypes.cpp
        ${COMPONENT_SOURCE_DIR}/SspLogger.cpp

        ${COMPONENT_SOURCE_DIR}/Importer/SsdFile.cpp
        ${COMPONENT_SOURCE_DIR}/Importer/FileElements/SsdSystem.cpp
        ${COMPONENT_SOURCE_DIR}/Importer/FileElements/SsdComponent.cpp
        ${COMPONENT_SOURCE_DIR}/Importer/FileElements/SsdURI.cpp
        ${COMPONENT_SOURCE_DIR}/Importer/SsdFileImporter.cpp

        ${COMPONENT_SOURCE_DIR}/Visitors/Network/SspTriggerVisitor.cpp
        ${COMPONENT_SOURCE_DIR}/Visitors/Network/SspInitVisitor.cpp
        ${COMPONENT_SOURCE_DIR}/Visitors/Network/CalcParamInitVisitor.cpp
        ${COMPONENT_SOURCE_DIR}/Visitors/Connector/PropagateDataVisitor.cpp
        ${COMPONENT_SOURCE_DIR}/Visitors/Connector/TriggerSignalVisitor.cpp
        ${COMPONENT_SOURCE_DIR}/Visitors/Connector/UpdateOutputSignalVisitor.cpp
        ${COMPONENT_SOURCE_DIR}/Visitors/Connector/UpdateInputSignalVisitor.cpp
        ${COMPONENT_SOURCE_DIR}/Visitors/SspVisitorHelper.cpp

        ${COMPONENT_SOURCE_DIR}/SSPElements/NetworkElement.cpp
        ${COMPONENT_SOURCE_DIR}/SSPElements/Component.cpp
        ${COMPONENT_SOURCE_DIR}/SSPElements/System.cpp
        ${COMPONENT_SOURCE_DIR}/SSPElements/Connection.cpp
        ${COMPONENT_SOURCE_DIR}/SSPElements/Connector/Connector.cpp
        ${COMPONENT_SOURCE_DIR}/SSPElements/Connector/ConnectorHelper.cpp
        ${COMPONENT_SOURCE_DIR}/SSPElements/Connector/ScalarConnectorBase.cpp
        ${COMPONENT_SOURCE_DIR}/SSPElements/Connector/OSMPConnectorBase.cpp
        ${COMPONENT_SOURCE_DIR}/SSPElements/Connector/GroupConnector.cpp
        ${COMPONENT_SOURCE_DIR}/SSPElements/Enumeration/Enumeration.cpp

        ${OPENPASS_SIMCORE_DIR}/core/opSimulation/framework/sampler.cpp
        ${OPENPASS_SIMCORE_DIR}/core/opSimulation/modelElements/parameters.cpp
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/fmuWrapper.cpp
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/fmuFileHelper.cpp
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/FmuHandler.cpp
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/FmuCalculations.cpp
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/FmuCommunication.cpp
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/FmuHelper.cpp
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/ChannelDefinitionParser.cpp
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/FmiImporter/src/Common/fmuChecker.c
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/SignalInterface/SignalTranslator.cpp
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/SignalInterface/AccelerationSignalParser.cpp
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/SignalInterface/CompCtrlSignalParser.cpp
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/SignalInterface/DynamicsSignalParser.cpp
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/SignalInterface/LongitudinalSignalParser.cpp
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/SignalInterface/OsiSensorDataParser.cpp
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/SignalInterface/SignalMessageVisitor.cpp
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/SignalInterface/SteeringSignalParser.cpp
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/SignalInterface/TrafficUpdateSignalParser.cpp

        ${OPENPASS_SIMCORE_DIR}/core/opSimulation/modules/World_OSI/OWL/DataTypes.cpp
        ${OPENPASS_SIMCORE_DIR}/core/opSimulation/modules/World_OSI/OWL/MovingObject.cpp
        ${OPENPASS_SIMCORE_DIR}/core/opSimulation/modules/World_OSI/OWL/OpenDriveTypeMapper.cpp


        HEADERS
        SimpleAffineFMU.h

        ${COMPONENT_SOURCE_DIR}/SsdToSspNetworkParser.h
        ${COMPONENT_SOURCE_DIR}/ParserTypes.h
        ${COMPONENT_SOURCE_DIR}/OSMPConnectorFactory.h
        ${COMPONENT_SOURCE_DIR}/ScalarConnectorFactory.h
        ${COMPONENT_SOURCE_DIR}/SspLogger.h

        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/fmuWrapper.h
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/fmuFileHelper.h
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/FmuHandler.h
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/FmuCalculations.h
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/FmuHelper.h
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/SignalInterface/SignalTranslator.h
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/SignalInterface/AccelerationSignalParser.h
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/SignalInterface/CompCtrlSignalParser.h
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/SignalInterface/DynamicsSignalParser.h
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/SignalInterface/LongitudinalSignalParser.h
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/SignalInterface/OsiSensorDataParser.h
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/SignalInterface/SteeringSignalParser.h
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/SignalInterface/TrafficUpdateSignalParser.h
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/ChannelDefinitionParser.h

        ${COMPONENT_SOURCE_DIR}/Importer/SsdFile.h
        ${COMPONENT_SOURCE_DIR}/Importer/FileElements/SsdSystem.h
        ${COMPONENT_SOURCE_DIR}/Importer/FileElements/SsdComponent.h
        ${COMPONENT_SOURCE_DIR}/Importer/FileElements/SsdURI.h
        ${COMPONENT_SOURCE_DIR}/Importer/SsdFileImporter.h

        ${COMPONENT_SOURCE_DIR}/Visitors/Network/SspNetworkVisitorInterface.h
        ${COMPONENT_SOURCE_DIR}/Visitors/Network/SspTriggerVisitor.h
        ${COMPONENT_SOURCE_DIR}/Visitors/Network/SspInitVisitor.h
        ${COMPONENT_SOURCE_DIR}/Visitors/Network/ParameterInitializationVisitor.h
        ${COMPONENT_SOURCE_DIR}/Visitors/Network/CalcParamInitVisitor.h
        ${COMPONENT_SOURCE_DIR}/Visitors/Connector/PropagateDataVisitor.h
        ${COMPONENT_SOURCE_DIR}/Visitors/Connector/TriggerSignalVisitor.h
        ${COMPONENT_SOURCE_DIR}/Visitors/Connector/TriggerVisit.h
        ${COMPONENT_SOURCE_DIR}/Visitors/Connector/UpdateOutputSignalVisitor.h
        ${COMPONENT_SOURCE_DIR}/Visitors/Connector/UpdateOutputVisitor.h
        ${COMPONENT_SOURCE_DIR}/Visitors/Connector/UpdateInputSignalVisitor.h
        ${COMPONENT_SOURCE_DIR}/Visitors/Connector/UpdateInputVisitor.h
        ${COMPONENT_SOURCE_DIR}/Visitors/SspVisitorHelper.h

        ${COMPONENT_SOURCE_DIR}/SSPElements/NetworkElement.h
        ${COMPONENT_SOURCE_DIR}/SSPElements/Component.h
        ${COMPONENT_SOURCE_DIR}/SSPElements/System.h
        ${COMPONENT_SOURCE_DIR}/SSPElements/Connection.h
        ${COMPONENT_SOURCE_DIR}/SSPElements/Connector/Connector.h
        ${COMPONENT_SOURCE_DIR}/SSPElements/Connector/ConnectorHelper.h
        ${COMPONENT_SOURCE_DIR}/SSPElements/Connector/ConnectorInterface.h
        ${COMPONENT_SOURCE_DIR}/SSPElements/Connector/ScalarConnector.h
        ${COMPONENT_SOURCE_DIR}/SSPElements/Connector/ScalarConnectorBase.h
        ${COMPONENT_SOURCE_DIR}/SSPElements/Connector/OSMPConnectorBase.h
        ${COMPONENT_SOURCE_DIR}/SSPElements/Connector/OSMPConnector.h
        ${COMPONENT_SOURCE_DIR}/SSPElements/Connector/GroupConnector.h
        ${COMPONENT_SOURCE_DIR}/SSPElements/Enumeration/Enumeration.h

        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/FmiImporter/src/FMI1/fmi1_check.c
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/FmiImporter/src/FMI1/fmi1_cs_sim.c
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/FmiImporter/src/FMI1/fmi1_me_sim.c
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/FmiImporter/src/FMI2/fmi2_check.c
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/FmiImporter/src/FMI2/fmi2_cs_sim.c
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/FmiImporter/src/FMI2/fmi2_me_sim.c

        INCDIRS
        ${COMPONENT_SOURCE_DIR}
        ${OPENPASS_SIMCORE_DIR}/components/Algorithm_FmuWrapper/src/FmiImporter/include
        ${OPENPASS_SIMCORE_DIR}/core/opSimulation
        ${OPENPASS_SIMCORE_DIR}/core/opSimulation/modules/World_OSI
        Algorithm_FmuWrapper
        # FMILibrary internals
        ${FMILibrary_INCLUDE_DIR}
        ${FMILibrary_INCLUDE_DIR}/FMI
        ${FMILibrary_INCLUDE_DIR}/FMI1
        ${FMILibrary_INCLUDE_DIR}/FMI2
        ${FMILibrary_INCLUDE_DIR}/JM
        ${OPENPASS_SIMCORE_DIR}/include
        ${OPENPASS_SIMCORE_DIR}/core

        LIBRARIES
        Algorithm_FmuWrapper
        CoreCommon
        SimulationCore
        Qt5::Core
        Qt5::Xml
        Common
        ${FMILibrary_LIBRARY_DIR}

        RESOURCES
        Resources

        LINKOSI static
)
include(${CMAKE_CURRENT_SOURCE_DIR}/../Algorithm_FmuWrapper/CMakeLists.txt)