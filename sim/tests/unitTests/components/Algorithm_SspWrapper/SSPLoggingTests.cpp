/*******************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <osi3/osi_sensordata.pb.h>
#include <sstream>
#include <vector>

#include "SsdToSspNetworkParser.h"
#include "common/sensorDataSignal.h"
#include "dontCare.h"
#include "fakeAgent.h"
#include "fakeCallback.h"
#include "fakeFmuWrapper.h"
#include "fakeParameter.h"
#include "fakeRadio.h"
#include "fakeScenarioControl.h"
#include "fakeWorld.h"
#include "include/fmuHandlerInterface.h"
#include "sim/src/components/Algorithm_FmuWrapper/src/FmuHelper.h"
#include "sim/src/components/Algorithm_SspWrapper/AlgorithmSspWrapper.h"
#include "sim/src/components/Algorithm_SspWrapper/SSPElements/Connector/ConnectorHelper.h"
#include "sim/src/components/Algorithm_SspWrapper/SSPElements/Connector/OSMPConnector.h"
#include "sim/src/components/Algorithm_SspWrapper/Visitors/Connector/UpdateInputSignalVisitor.h"
#include "sim/src/components/Algorithm_SspWrapper/Visitors/Connector/UpdateOutputSignalVisitor.h"

using ::testing::_;
using ::testing::AtLeast;
using ::testing::DontCare;
using ::testing::Eq;
using ::testing::Gt;
using ::testing::Mock;
using ::testing::NiceMock;
using ::testing::Return;
using ::testing::ReturnArg;
using ::testing::ReturnRef;
using ::testing::StrictMock;

//using namespace ssp;

class SSPLoggingTests : public ::testing::Test
{
public:
  SSPLoggingTests() { SspLogger::SetLogger(&fakeCallback, 0, ""); }

protected:
  NiceMock<FakeWorld> fakeWorld;
  NiceMock<FakeAgent> fakeAgent;
  NiceMock<FakeParameter> fakeParameter;
  StrictMock<FakeCallback> fakeCallback;
  const std::shared_ptr<NiceMock<FakeScenarioControl>> fakeScenarioControl{
      std::make_shared<NiceMock<FakeScenarioControl>>()};
};

TEST_F(SSPLoggingTests, ParserNoLogging)
{
  //Strict mock -> when a error is logged it will fail, because every call needs to have an expect_call
  EXPECT_CALL(fakeCallback, Log(Gt(CbkLogLevel::Error), _, _, _)).Times(AtLeast(0));
  EXPECT_CALL(fakeCallback, Log(CbkLogLevel::Error, _, _, _)).Times(0);  //For strict mock not needed

  SsdToSspNetworkParser ssdToSspNetWorkParser{"SspWrapper",
                                              false,
                                              0,
                                              0,
                                              0,
                                              100,
                                              nullptr,
                                              &fakeWorld,
                                              &fakeParameter,
                                              nullptr,
                                              &fakeAgent,
                                              &fakeCallback,
                                              fakeScenarioControl};

  auto ssdSystem = std::make_shared<SsdSystem>("0");
  openpass::parameter::internal::ParameterSetLevel3 parameters;
  parameters.emplace_back("Logging", false);
  parameters.emplace_back("CsvOutput", false);

  auto componentFirst = std::make_shared<SsdComponent>("component1", "source1", SspComponentType::x_none);
  componentFirst->SetPriority(0);
  componentFirst->SetWriteMessageParameters({std::make_pair("output_osiType", "file_type")});
  componentFirst->EmplaceConnector("connector1", SspParserTypes::ScalarConnector{"output", {ConnectorType::Real, {}}});
  componentFirst->SetParameters(parameters);
  ssdSystem->AddComponent(std::move(componentFirst));

  auto componentSecond = std::make_shared<SsdComponent>("component2", "source2", SspComponentType::x_none);
  componentSecond->SetPriority(1);
  componentSecond->EmplaceConnector("connector2", SspParserTypes::ScalarConnector{"output", {ConnectorType::Real, {}}});
  componentSecond->SetParameters(parameters);
  ssdSystem->AddComponent(std::move(componentSecond));

  std::map<std::string, std::string> connection;
  connection.insert(std::make_pair("startElement", "component1"));
  connection.insert(std::make_pair("endElement", "component2"));
  connection.insert(std::make_pair("startConnector", "connector1"));
  connection.insert(std::make_pair("endConnector", "connector2"));
  ssdSystem->AddConnection(std::make_shared<std::map<std::string, std::string>>(connection));

  std::vector<std::shared_ptr<SsdFile>> ssdFiles{std::make_shared<SsdFile>("SystemStructure.ssd", ssdSystem)};

  openpass::common::RuntimeInformation runtimeInformation{{openpass::common::framework}, {"", "outputDirectory", ""}};

  ON_CALL(fakeParameter, GetRuntimeInformation()).WillByDefault(testing::ReturnRef(runtimeInformation));
  ON_CALL(fakeAgent, GetId()).WillByDefault(Return(0));

  EXPECT_NO_THROW(ssdToSspNetWorkParser.GetRootSystem(ssdFiles));
}

TEST_F(SSPLoggingTests, ParserErrorLogging)
{
  //Strict mock -> when a error is logged it will fail, because every call needs to have an expect_call
  EXPECT_CALL(fakeCallback, Log(Gt(CbkLogLevel::Error), _, _, _)).Times(AtLeast(0));
  EXPECT_CALL(fakeCallback, Log(CbkLogLevel::Error, _, _, _)).Times(1);

  SsdToSspNetworkParser ssdToSspNetWorkParser{"SspWrapper",
                                              false,
                                              0,
                                              0,
                                              0,
                                              100,
                                              nullptr,
                                              &fakeWorld,
                                              &fakeParameter,
                                              nullptr,
                                              &fakeAgent,
                                              &fakeCallback,
                                              fakeScenarioControl};

  auto ssdSystem = std::make_shared<SsdSystem>("0");
  openpass::parameter::internal::ParameterSetLevel3 parameters;
  parameters.emplace_back("Logging", false);
  parameters.emplace_back("CsvOutput", false);

  auto componentFirst = std::make_shared<SsdComponent>("component1", "source1", SspComponentType::x_fmu_sharedlibrary);
  componentFirst->SetPriority(0);
  componentFirst->SetWriteMessageParameters({std::make_pair("output_osiType", "file_type")});
  componentFirst->EmplaceConnector("connector1", SspParserTypes::ScalarConnector{"output", {ConnectorType::Real, {}}});
  componentFirst->SetParameters(parameters);
  ssdSystem->AddComponent(std::move(componentFirst));

  auto componentSecond = std::make_shared<SsdComponent>("component2", "source2", SspComponentType::x_fmu_sharedlibrary);
  componentSecond->SetPriority(1);
  componentSecond->EmplaceConnector("connector2", SspParserTypes::ScalarConnector{"input", {ConnectorType::Real, {}}});
  componentSecond->SetParameters(parameters);
  ssdSystem->AddComponent(std::move(componentSecond));

  std::map<std::string, std::string> connection;
  connection.insert(std::make_pair("startElement", "component1"));
  connection.insert(std::make_pair("endElement", "component2"));
  connection.insert(std::make_pair("startConnector", "connector1"));
  connection.insert(std::make_pair("wrongConnector", "connector2"));
  ssdSystem->AddConnection(std::make_shared<std::map<std::string, std::string>>(connection));

  std::vector<std::shared_ptr<SsdFile>> ssdFiles{std::make_shared<SsdFile>("", ssdSystem)};

  openpass::common::RuntimeInformation runtimeInformation{{openpass::common::framework}, {"", "outputDirectory", ""}};

  ON_CALL(fakeParameter, GetRuntimeInformation()).WillByDefault(testing::ReturnRef(runtimeInformation));
  ON_CALL(fakeAgent, GetId()).WillByDefault(Return(0));

  EXPECT_THROW(ssdToSspNetWorkParser.GetRootSystem(ssdFiles), std::runtime_error);
}

TEST_F(SSPLoggingTests, VisistTestNoError)
{
  //Strict mock -> when a error is logged it will fail, because every call needs to have an expect_call
  EXPECT_CALL(fakeCallback, Log(Gt(CbkLogLevel::Error), _, _, _)).Times(AtLeast(0));
  EXPECT_CALL(fakeCallback, Log(CbkLogLevel::Error, _, _, _)).Times(0);  // not needed for strict mocks

  FmuVariables fmuVariables;
  fmuVariables.emplace<FMI2>();
  std::get<FMI2>(fmuVariables)
      .emplace("SensorData.base.lo",
               *std::make_shared<FmuVariable2>(
                   0, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_discrete));
  std::get<FMI2>(fmuVariables)
      .emplace("SensorData.base.hi",
               *std::make_shared<FmuVariable2>(
                   1, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_discrete));
  std::get<FMI2>(fmuVariables)
      .emplace("SensorData.size",
               *std::make_shared<FmuVariable2>(
                   2, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_discrete));

  auto fakeFmuWrapper = std::make_shared<FakeFmuWrapper>();

  osi3::SensorData sensorData{};
  sensorData.mutable_version()->set_version_major(1);
  sensorData.mutable_version()->set_version_minor(2);
  sensorData.mutable_version()->set_version_patch(3);

  std::string serializedSensorData{};
  sensorData.SerializeToString(&serializedSensorData);
  fmi_integer_t lo{}, hi{}, size;
  size.emplace<FMI2>(serializedSensorData.length());
  FmuHelper::encode_pointer_to_integer<FMI2>(serializedSensorData.data(), hi, lo);

  EXPECT_CALL(*fakeFmuWrapper, GetFmuVariables()).WillRepeatedly(ReturnRef(fmuVariables));
  auto baseLoValue = FmuValue{.intValue = std::get<FMI2>(lo)};
  EXPECT_CALL(*fakeFmuWrapper, GetValue(0, VariableType::Int)).WillRepeatedly(ReturnRef(baseLoValue));
  auto baseHiValue = FmuValue{.intValue = std::get<FMI2>(hi)};
  EXPECT_CALL(*fakeFmuWrapper, GetValue(1, VariableType::Int)).WillRepeatedly(ReturnRef(baseHiValue));
  auto sizeValue = FmuValue{.intValue = std::get<FMI2>(size)};
  EXPECT_CALL(*fakeFmuWrapper, GetValue(2, VariableType::Int)).WillRepeatedly(ReturnRef(sizeValue));

  EXPECT_CALL(*fakeFmuWrapper, SetValue(_, 0, VariableType::Int))
      .WillRepeatedly(
          [&baseLoValue](auto value, auto, auto)
          {
            // baseLoValue = value;
          });
  EXPECT_CALL(*fakeFmuWrapper, SetValue(_, 1, VariableType::Int))
      .WillRepeatedly(
          [&baseHiValue](auto value, auto, auto)
          {
            // baseHiValue = value;
          });
  EXPECT_CALL(*fakeFmuWrapper, SetValue(_, 2, VariableType::Int))
      .WillRepeatedly(
          [&sizeValue](auto value, auto, auto)
          {
            // sizeValue = value;
          });

  ON_CALL(*fakeFmuWrapper, GetFmuVariables()).WillByDefault(ReturnRef(fmuVariables));

  std::shared_ptr<std::map<std::string, FmuFileHelper::TraceEntry>> targetOutputTracesMap
      = std::make_shared<std::map<std::string, FmuFileHelper::TraceEntry>>();

  std::shared_ptr<ssp::OsmpConnector<osi3::SensorData, FMI2>> sensorViewConnector
      = std::make_shared<ssp::OsmpConnector<osi3::SensorData, FMI2>>("SensorData", "SensorData", fakeFmuWrapper, 10);
  sensorViewConnector->SetWriteBinaryTrace(".", targetOutputTracesMap, "SensorData");

  auto sensorDataSignal = std::make_shared<SensorDataSignal const>(sensorData);
  ssp::UpdateInputSignalVisitor inVisitor{2, sensorDataSignal, 0, &fakeWorld, &fakeAgent, &fakeCallback};
  inVisitor.Visit(sensorViewConnector.get());

  ON_CALL(*fakeFmuWrapper, UpdateOutput)
      .WillByDefault(
          [sensorViewConnector](int localLinkId, std::shared_ptr<const SignalInterface> &data, int time) mutable
          {
            const std::shared_ptr<const osi3::SensorData> sensorData
                = std::dynamic_pointer_cast<const osi3::SensorData>(sensorViewConnector->GetMessage());
            data = std::make_shared<const SensorDataSignal>(*sensorData);
          });
  Mock::AllowLeak(fakeFmuWrapper.get());

  std::shared_ptr<SignalInterface const> outSignal{};
  ssp::UpdateOutputSignalVisitor outVisitor{6, outSignal, 0, &fakeWorld, &fakeAgent, &fakeCallback};
  outVisitor.Visit(sensorViewConnector.get());
  auto sensorDataOut = std::dynamic_pointer_cast<SensorDataSignal const>(outVisitor.data)->sensorData;
}