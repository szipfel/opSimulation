/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include "common/dynamicsSignal.h"
#include "controllerSwitchImpl.h"
#include "fakeScenarioControl.h"

using ::testing::Eq;
using ::testing::Ne;
using ::testing::Return;

TEST(ControllerSwitch_UnitsTests, ForwardsDefaultSignal)
{
  auto scenarioControl = std::make_shared<FakeScenarioControl>();
  ON_CALL(*scenarioControl, UseCustomController()).WillByDefault(Return(false));

  ControllerSwitchImplementation controllerSwitch(
      "", false, 0, 0, 0, 100, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, scenarioControl);

  auto signal0 = std::make_shared<DynamicsSignal>();
  signal0->dynamicsInformation.positionX = 10.0_m;
  controllerSwitch.UpdateInput(0, signal0, 0);

  auto signal1 = std::make_shared<DynamicsSignal>();
  signal1->dynamicsInformation.positionX = 20.0_m;
  controllerSwitch.UpdateInput(1, signal1, 0);

  std::shared_ptr<SignalInterface const> resultSignal;

  controllerSwitch.UpdateOutput(0, resultSignal, 0);

  auto resultDynamicsSignal = std::dynamic_pointer_cast<DynamicsSignal const>(resultSignal);
  ASSERT_THAT(resultDynamicsSignal, Ne(nullptr));
  ASSERT_THAT(resultDynamicsSignal->dynamicsInformation.positionX, Eq(10.0_m));
}

TEST(ControllerSwitch_UnitsTests, ForwardsCustomSignal)
{
  auto scenarioControl = std::make_shared<FakeScenarioControl>();
  ON_CALL(*scenarioControl, UseCustomController()).WillByDefault(Return(true));

  ControllerSwitchImplementation controllerSwitch(
      "", false, 0, 0, 0, 100, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, scenarioControl);

  auto signal0 = std::make_shared<DynamicsSignal>();
  signal0->dynamicsInformation.positionX = 10.0_m;
  controllerSwitch.UpdateInput(0, signal0, 0);

  auto signal1 = std::make_shared<DynamicsSignal>();
  signal1->dynamicsInformation.positionX = 20.0_m;
  controllerSwitch.UpdateInput(1, signal1, 0);

  std::shared_ptr<SignalInterface const> resultSignal;

  controllerSwitch.UpdateOutput(0, resultSignal, 0);

  auto resultDynamicsSignal = std::dynamic_pointer_cast<DynamicsSignal const>(resultSignal);
  ASSERT_THAT(resultDynamicsSignal, Ne(nullptr));
  ASSERT_THAT(resultDynamicsSignal->dynamicsInformation.positionX, Eq(20.0_m));
}
