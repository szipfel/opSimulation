/********************************************************************************
 * Copyright (c) 2023 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <numeric>

#include "fakeAgent.h"
#include "fakeParameter.h"
#include "include/agentInterface.h"
#include "motionmodel.h"

using ::testing::_;
using ::testing::NiceMock;
using ::testing::Return;
using ::testing::ReturnRef;

TEST(MotionModel, CheckVehicleVelocity)
{
  NiceMock<FakeAgent> fakeAgent;

  auto fakeVehicleModelParameters = std::make_shared<mantle_api::VehicleProperties>();
  double fakeMass = 1500;
  double fakeXPositionCOG = 2;
  double fakeYPositionCOG = 0.0;
  double fakeAirDragCoefficient = 0.0;
  double fakeFrontSurface = 0.0;
  double fakeMomentInertiaYaw = 10.0;
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("XPositionCOG", std::to_string(fakeXPositionCOG)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("YPositionCOG", std::to_string(fakeYPositionCOG)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("AirDragCoefficient", std::to_string(fakeAirDragCoefficient)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("FrontSurface", std::to_string(fakeFrontSurface)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("MomentInertiaYaw", std::to_string(fakeMomentInertiaYaw)));

  fakeVehicleModelParameters->mass = fakeMass * 1_kg;

  fakeVehicleModelParameters->front_axle.wheel_diameter = 0.5_m;
  fakeVehicleModelParameters->front_axle.track_width = 2.0_m;
  fakeVehicleModelParameters->front_axle.bb_center_to_axle_center.x = 2.0_m;
  fakeVehicleModelParameters->rear_axle.wheel_diameter = 0.5_m;
  fakeVehicleModelParameters->rear_axle.track_width = 2.0_m;
  fakeVehicleModelParameters->rear_axle.bb_center_to_axle_center.x = 0.0_m;

  Common::Vector2d fakeVelocity = {0.0, 0.0};
  double fakeYawVelocity = 10.0;

  ON_CALL(fakeAgent, GetVehicleModelParameters()).WillByDefault(Return(fakeVehicleModelParameters));

  ON_CALL(fakeAgent, SetVelocityVector(_))
      .WillByDefault(
          [&](mantle_api::Vec3<units::velocity::meters_per_second_t> velocity)
          {
            fakeVelocity.x = velocity.x.value();
            fakeVelocity.y = velocity.y.value();
          });

    int fakeCycleTimeMs = 10;

  Common::Vector2d fakeAcceleration = {10.0, 10.0};
  double fakeYawAcceleration = 10.0;
  ON_CALL(fakeAgent, GetYawRate()).WillByDefault(Return(0.0_rad_per_s));
  ON_CALL(fakeAgent, GetYawAcceleration()).WillByDefault(Return((fakeYawAcceleration * 1_rad_per_s_sq)));
  ON_CALL(fakeAgent, GetYaw()).WillByDefault(Return(0.0_rad));
  ON_CALL(fakeAgent, GetVelocity(_))
      .WillByDefault(Return(Common::Vector2d{fakeVelocity.x * 1_mps, fakeVelocity.y * 1_mps}));
  ON_CALL(fakeAgent, GetAcceleration(_))
      .WillByDefault(Return(Common::Vector2d{fakeAcceleration.x * 1_mps_sq, fakeAcceleration.y * 1_mps_sq}));
  ON_CALL(fakeAgent, GetId()).WillByDefault(Return(0));
  ON_CALL(fakeAgent, GetPositionX()).WillByDefault(Return(0_m));
  ON_CALL(fakeAgent, GetPositionY()).WillByDefault(Return(0_m));

  Dynamics_MotionModel_Implementation implementation(
      "", false, 0, 0, 0, fakeCycleTimeMs, nullptr, nullptr, nullptr, nullptr, nullptr, &fakeAgent);

  std::vector<double> fakeLongitudinalTireForce = {0.0, 0.0, 0.0, 0.0};
  std::vector<double> fakeLateralTireForce = {0.0, 0.0, 0.0, 0.0};
  std::vector<double> fakeWheelAngle = {0.0, 0.0, 0.0, 0.0};
  std::vector<double> fakeSelfAligningTorque = {0.0, 0.0, 0.0, 0.0};

  SignalVectorDouble fakeSignalLongitudinalTireForce(fakeLongitudinalTireForce);
  SignalVectorDouble fakeSignalLateralTireForce(fakeLateralTireForce);
  SignalVectorDouble fakeSignalWheelAngle(fakeWheelAngle);
  SignalVectorDouble fakeSignalSelfAligningTorque(fakeSelfAligningTorque);

  implementation.UpdateInput(0, std::make_shared<SignalVectorDouble const>(fakeSignalLongitudinalTireForce), 0);
  implementation.UpdateInput(1, std::make_shared<SignalVectorDouble const>(fakeSignalLateralTireForce), 0);
  implementation.UpdateInput(2, std::make_shared<SignalVectorDouble const>(fakeSignalWheelAngle), 0);
  implementation.UpdateInput(3, std::make_shared<SignalVectorDouble const>(fakeSignalSelfAligningTorque), 0);

  implementation.Trigger(0);

  ASSERT_DOUBLE_EQ(fakeAcceleration.x * fakeCycleTimeMs / 1000.0, fakeVelocity.x);
  ASSERT_DOUBLE_EQ(fakeAcceleration.y * fakeCycleTimeMs / 1000.0, fakeVelocity.y);
}

TEST(MotionModel, CheckVehicleAcceleration)
{
  NiceMock<FakeAgent> fakeAgent;

  auto fakeVehicleModelParameters = std::make_shared<mantle_api::VehicleProperties>();
  double fakeMass = 1500;
  double fakeXPositionCOG = 2;
  double fakeYPositionCOG = 0.0;
  double fakeAirDragCoefficient = 0.0;
  double fakeFrontSurface = 0.0;
  double fakeMomentInertiaYaw = 10.0;
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("XPositionCOG", std::to_string(fakeXPositionCOG)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("YPositionCOG", std::to_string(fakeYPositionCOG)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("AirDragCoefficient", std::to_string(fakeAirDragCoefficient)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("FrontSurface", std::to_string(fakeFrontSurface)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("MomentInertiaYaw", std::to_string(fakeMomentInertiaYaw)));

  fakeVehicleModelParameters->mass = fakeMass * 1_kg;

  fakeVehicleModelParameters->front_axle.wheel_diameter = 0.5_m;
  fakeVehicleModelParameters->front_axle.track_width = 2.0_m;
  fakeVehicleModelParameters->front_axle.bb_center_to_axle_center.x = 2.0_m;
  fakeVehicleModelParameters->rear_axle.wheel_diameter = 0.5_m;
  fakeVehicleModelParameters->rear_axle.track_width = 2.0_m;
  fakeVehicleModelParameters->rear_axle.bb_center_to_axle_center.x = 0.0_m;

  Common::Vector2d fakeAcceleration = {0.0, 0.0};
  double fakeYawAcceleration = 0.0;
  double fakeYawRate = 0.0;

  ON_CALL(fakeAgent, GetVehicleModelParameters()).WillByDefault(Return(fakeVehicleModelParameters));

  ON_CALL(fakeAgent, SetAccelerationVector(_))
      .WillByDefault(
          [&](mantle_api::Vec3<units::acceleration::meters_per_second_squared_t> acceleration)
          {
            fakeAcceleration.x = acceleration.x.value();
            fakeAcceleration.y = acceleration.y.value();
          });

  ON_CALL(fakeAgent, SetYawAcceleration(_))
      .WillByDefault([&](units::angular_acceleration::radians_per_second_squared_t yawAcceleration)
                     { fakeYawAcceleration = yawAcceleration.value(); });

  ON_CALL(fakeAgent, SetYawRate(_))
      .WillByDefault([&](units::angular_velocity::radians_per_second_t yawRate) { fakeYawRate = yawRate.value(); });

  int fakeCycleTimeMs = 10;

  Common::Vector2d fakeVelocity = {0.0, 0.0};

  ON_CALL(fakeAgent, GetYawRate()).WillByDefault(Return(0.0_rad_per_s));
  ON_CALL(fakeAgent, GetYawAcceleration()).WillByDefault(Return((fakeYawAcceleration * 1_rad_per_s_sq)));
  ON_CALL(fakeAgent, GetYaw()).WillByDefault(Return(0.0_rad));
  ON_CALL(fakeAgent, GetVelocity(_))
      .WillByDefault(Return(Common::Vector2d{fakeVelocity.x * 1_mps, fakeVelocity.y * 1_mps}));
  ON_CALL(fakeAgent, GetAcceleration(_))
      .WillByDefault(Return(Common::Vector2d{fakeAcceleration.x * 1_mps_sq, fakeAcceleration.y * 1_mps_sq}));
  ON_CALL(fakeAgent, GetId()).WillByDefault(Return(0));
  ON_CALL(fakeAgent, GetPositionX()).WillByDefault(Return(0_m));
  ON_CALL(fakeAgent, GetPositionY()).WillByDefault(Return(0_m));

  Dynamics_MotionModel_Implementation implementation(
      "", false, 0, 0, 0, fakeCycleTimeMs, nullptr, nullptr, nullptr, nullptr, nullptr, &fakeAgent);

  std::vector<double> fakeLongitudinalTireForce = {1000.0, 1000.0, 1000.0, 1000.0};
  std::vector<double> fakeLateralTireForce = {0.0, 0.0, 0.0, 0.0};
  std::vector<double> fakeWheelAngle = {0.0, 0.0, 0.0, 0.0};
  std::vector<double> fakeSelfAligningTorque = {0.0, 0.0, 0.0, 0.0};

  SignalVectorDouble fakeSignalLongitudinalTireForce(fakeLongitudinalTireForce);
  SignalVectorDouble fakeSignalLateralTireForce(fakeLateralTireForce);
  SignalVectorDouble fakeSignalWheelAngle(fakeWheelAngle);
  SignalVectorDouble fakeSignalSelfAligningTorque(fakeSelfAligningTorque);

  std::shared_ptr<SignalInterface const> outputSignal;

  implementation.UpdateInput(0, std::make_shared<SignalVectorDouble const>(fakeSignalLongitudinalTireForce), 0);
  implementation.UpdateInput(1, std::make_shared<SignalVectorDouble const>(fakeSignalLateralTireForce), 0);
  implementation.UpdateInput(2, std::make_shared<SignalVectorDouble const>(fakeSignalWheelAngle), 0);
  implementation.UpdateInput(3, std::make_shared<SignalVectorDouble const>(fakeSignalSelfAligningTorque), 0);

  implementation.Trigger(0);
  implementation.UpdateOutput(0, outputSignal, 0);
  Common::Vector2d<double> fakeTireForce, fakePositionTire;
  double fakeMomentZ = 0.0;
  for (unsigned int idx = 0; idx < fakeLongitudinalTireForce.size(); idx++)
  {
    fakeTireForce.x = fakeLongitudinalTireForce[idx];
    fakeTireForce.y = fakeLateralTireForce[idx];
    fakeTireForce.Rotate(fakeWheelAngle[idx] * 1_rad);
    fakeLongitudinalTireForce[idx] = fakeTireForce.x;
    fakeLateralTireForce[idx] = fakeTireForce.y;
  }

  ASSERT_NEAR(
      (fakeAcceleration.x + (-fakeYawAcceleration * (fakeYPositionCOG))
       + (-fakeYawRate * fakeYawRate * (fakeXPositionCOG))),
      std::accumulate(fakeLongitudinalTireForce.begin(), fakeLongitudinalTireForce.end(), 0) / fakeMass,
      (std::accumulate(fakeLongitudinalTireForce.begin(), fakeLongitudinalTireForce.end(), 0) / fakeMass) * 0.01);
  ASSERT_NEAR(fakeAcceleration.y + (fakeYawAcceleration * (fakeXPositionCOG))
                  + (-fakeYawRate * fakeYawRate * (fakeYPositionCOG)),
              std::accumulate(fakeLateralTireForce.begin(), fakeLateralTireForce.end(), 0) / fakeMass,
              (std::accumulate(fakeLateralTireForce.begin(), fakeLateralTireForce.end(), 0) / fakeMass) * 0.01);
}