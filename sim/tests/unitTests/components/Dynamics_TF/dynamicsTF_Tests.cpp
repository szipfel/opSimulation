/********************************************************************************
 * Copyright (c) 2019-2020 in-tech GmbH
 *               2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <gtest/gtest.h>

#include "dontCare.h"
#include "fakeAgent.h"
#include "fakeParameter.h"
#include "tfImplementation.h"
#include "trajectoryTester.h"

using ::testing::_;
using ::testing::DontCare;
using ::testing::DoubleNear;
using ::testing::NiceMock;
using ::testing::Return;
using ::testing::ReturnRef;
using ::testing::SaveArg;

static void AssertDynamicsSignalEquality(std::shared_ptr<DynamicsSignal const> signal,
                                         units::length::meter_t x,
                                         units::length::meter_t y,
                                         units::angle::radian_t yaw,
                                         units::angular_velocity::radians_per_second_t yawRate,
                                         units::angular_acceleration::radians_per_second_squared_t yawAcceleration,
                                         units::velocity::meters_per_second_t velocityX,
                                         units::velocity::meters_per_second_t velocityY,
                                         units::acceleration::meters_per_second_squared_t acceleration,
                                         units::length::meter_t distance)
{
  ASSERT_THAT(signal->dynamicsInformation.positionX.value(), DoubleNear(x.value(), 1e-3));
  ASSERT_THAT(signal->dynamicsInformation.positionY.value(), DoubleNear(y.value(), 1e-3));
  ASSERT_THAT(signal->dynamicsInformation.yaw.value(), DoubleNear(yaw.value(), 1e-3));
  ASSERT_THAT(signal->dynamicsInformation.yawRate.value(), DoubleNear(yawRate.value(), 1e-3));
  ASSERT_THAT(signal->dynamicsInformation.yawAcceleration.value(), DoubleNear(yawAcceleration.value(), 1e-3));
  ASSERT_THAT(signal->dynamicsInformation.velocityX.value(), DoubleNear(velocityX.value(), 1e-3));
  ASSERT_THAT(signal->dynamicsInformation.velocityY.value(), DoubleNear(velocityY.value(), 1e-3));
  ASSERT_THAT(signal->dynamicsInformation.acceleration.value(), DoubleNear(acceleration.value(), 1e-3));
  ASSERT_THAT(signal->dynamicsInformation.travelDistance.value(), DoubleNear(distance.value(), 1e-3));
}

TEST(TrajectoryFollowerImplementation_WithoutExternalAcceleration_Unittests, DeactivationAfterEndOfTrajectory)
{
  mantle_api::Pose fakePose1{{0_m, 0_m, 0_m}, {0_rad, 0_rad, 0_rad}};
  mantle_api::Pose fakePose2{{0_m, 2_m, 0_m}, {0.1_rad, 0_rad, 0_rad}};
  mantle_api::Pose fakePose3{{0_m, 4_m, 0_m}, {0.31_rad, 0_rad, 0_rad}};

  mantle_api::PolyLinePoint fakePolyLinePoint1{fakePose1, 0_s};
  mantle_api::PolyLinePoint fakePolyLinePoint2{fakePose2, 0.1_s};
  mantle_api::PolyLinePoint fakePolyLinePoint3{fakePose3, 0.2_s};

  mantle_api::PolyLine polyLine{fakePolyLinePoint1, fakePolyLinePoint2, fakePolyLinePoint3};

  Trajectory fakeCoordinates = {"", polyLine};
  auto controlStrategy = std::make_shared<mantle_api::FollowTrajectoryControlStrategy>();
  controlStrategy->trajectory = fakeCoordinates;
  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> scenarioControl{controlStrategy};

  TrajectoryTester trajectoryTester(DontCare<bool>(), true);

  ON_CALL(*trajectoryTester.scenarioControl, GetStrategies(mantle_api::ControlStrategyType::kFollowTrajectory))
      .WillByDefault(Return(scenarioControl));

  std::shared_ptr<TrajectoryFollowerImplementation> trajectoryFollower = trajectoryTester.trajectoryFollower;

  std::shared_ptr<SignalInterface const> resultSignalInterface;
  std::shared_ptr<DynamicsSignal const> result;

  trajectoryFollower->Trigger(0);
  trajectoryFollower->Trigger(100);
  trajectoryFollower->Trigger(200);
  trajectoryFollower->UpdateOutput(0, resultSignalInterface, 200);

  result = std::dynamic_pointer_cast<DynamicsSignal const>(resultSignalInterface);

  ASSERT_EQ(result->componentState, ComponentState::Disabled);
}

TEST(TrajectoryFollowerImplementation_WithoutExternalAcceleration_Unittests, LinearTrajectoryWithoutInterpolation)
{
  mantle_api::Pose fakePose1{{0_m, 0_m, 0_m}, {0_rad, 0_rad, 0_rad}};
  mantle_api::Pose fakePose2{{3_m, 4_m, 0_m}, {0.1_rad, 0_rad, 0_rad}};
  mantle_api::Pose fakePose3{{9_m, 12_m, 0_m}, {0.4_rad, 0_rad, 0_rad}};

  mantle_api::PolyLinePoint fakePolyLinePoint1{fakePose1, 0_s};
  mantle_api::PolyLinePoint fakePolyLinePoint2{fakePose2, 0.2_s};
  mantle_api::PolyLinePoint fakePolyLinePoint3{fakePose3, 0.4_s};

  mantle_api::PolyLine polyLine{fakePolyLinePoint1, fakePolyLinePoint2, fakePolyLinePoint3};

  Trajectory fakeCoordinates = {"", polyLine};
  auto controlStrategy = std::make_shared<mantle_api::FollowTrajectoryControlStrategy>();
  controlStrategy->trajectory = fakeCoordinates;
  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> scenarioControl{controlStrategy};

  TrajectoryTester trajectoryTester(DontCare<bool>(), DontCare<bool>(), 200);

  ON_CALL(*trajectoryTester.scenarioControl, GetStrategies(mantle_api::ControlStrategyType::kFollowTrajectory))
      .WillByDefault(Return(scenarioControl));
  std::shared_ptr<TrajectoryFollowerImplementation> trajectoryFollower = trajectoryTester.trajectoryFollower;

  std::shared_ptr<SignalInterface const> resultSignalInterface;
  std::shared_ptr<DynamicsSignal const> result;

  trajectoryFollower->Trigger(0);
  trajectoryFollower->UpdateOutput(0, resultSignalInterface, 0);

  result = std::dynamic_pointer_cast<DynamicsSignal const>(resultSignalInterface);
  AssertDynamicsSignalEquality(
      result, 3.0_m, 4.0_m, 0.1_rad, 0.5_rad_per_s, 2.5_rad_per_s_sq, 15.0_mps, 20.0_mps, 125.0_mps_sq, 5.0_m);

  trajectoryFollower->Trigger(200);
  trajectoryFollower->UpdateOutput(0, resultSignalInterface, 200);

  result = std::dynamic_pointer_cast<DynamicsSignal const>(resultSignalInterface);
  AssertDynamicsSignalEquality(
      result, 9.0_m, 12.0_m, 0.4_rad, 1.5_rad_per_s, 5.0_rad_per_s_sq, 30.0_mps, 40.0_mps, 125.0_mps_sq, 10.0_m);
}

TEST(TrajectoryFollowerImplementation_WithoutExternalAcceleration_Unittests, LinearTrajectoryWithInterpolation)
{
  mantle_api::Pose fakePose1{{10_m, 10_m, 0_m}, {-1_rad, 0_rad, 0_rad}};
  mantle_api::Pose fakePose2{{13_m, 6_m, 0_m}, {-0.5_rad, 0_rad, 0_rad}};
  mantle_api::Pose fakePose3{{17_m, 10_m, 0_m}, {1.5_rad, 0_rad, 0_rad}};
  mantle_api::Pose fakePose4{{17_m, 13_m, 0_m}, {1.5_rad, 0_rad, 0_rad}};

  mantle_api::PolyLinePoint fakePolyLinePoint1{fakePose1, 0_s};
  mantle_api::PolyLinePoint fakePolyLinePoint2{fakePose2, 0.1_s};
  mantle_api::PolyLinePoint fakePolyLinePoint3{fakePose3, 0.5_s};
  mantle_api::PolyLinePoint fakePolyLinePoint4{fakePose4, 0.8_s};

  mantle_api::PolyLine polyLine{fakePolyLinePoint1, fakePolyLinePoint2, fakePolyLinePoint3, fakePolyLinePoint4};

  Trajectory fakeCoordinates = {"", polyLine};
  auto controlStrategy = std::make_shared<mantle_api::FollowTrajectoryControlStrategy>();
  controlStrategy->trajectory = fakeCoordinates;
  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> scenarioControl{controlStrategy};

  FakeAgent fakeAgent;
  ON_CALL(fakeAgent, GetPositionX()).WillByDefault(Return(10.0_m));
  ON_CALL(fakeAgent, GetPositionY()).WillByDefault(Return(10.0_m));
  ON_CALL(fakeAgent, GetYaw()).WillByDefault(Return(-1.0_rad));

  TrajectoryTester trajectoryTester(DontCare<bool>(), DontCare<bool>(), nullptr, &fakeAgent, 200);

  ON_CALL(*trajectoryTester.scenarioControl, GetStrategies(mantle_api::ControlStrategyType::kFollowTrajectory))
      .WillByDefault(Return(scenarioControl));
  std::shared_ptr<TrajectoryFollowerImplementation> trajectoryFollower = trajectoryTester.trajectoryFollower;

  std::shared_ptr<SignalInterface const> resultSignalInterface;
  std::shared_ptr<DynamicsSignal const> result;

  units::velocity::meters_per_second_t velocityX{0};
  units::velocity::meters_per_second_t velocityY{0};
  units::acceleration::meters_per_second_squared_t acceleration{0};
  units::length::meter_t distance{0.0};

  const units::time::second_t cycleTimeInSeconds{0.2};

  trajectoryFollower->Trigger(0);
  trajectoryFollower->UpdateOutput(0, resultSignalInterface, 0);

  result = std::dynamic_pointer_cast<DynamicsSignal const>(resultSignalInterface);
  distance = (5_m + units::math::sqrt(2_sq_m));
  velocityX = distance / cycleTimeInSeconds * M_SQRT1_2;
  velocityY = distance / cycleTimeInSeconds * M_SQRT1_2;
  acceleration = units::math::hypot(velocityX, velocityY) / cycleTimeInSeconds;
  AssertDynamicsSignalEquality(result,
                               14_m,
                               7_m,
                               0.0_rad,
                               1.0_rad / cycleTimeInSeconds,
                               25.0_rad_per_s_sq,
                               velocityX,
                               velocityY,
                               acceleration,
                               distance);

  trajectoryFollower->Trigger(200);
  trajectoryFollower->UpdateOutput(0, resultSignalInterface, 100);

  result = std::dynamic_pointer_cast<DynamicsSignal const>(resultSignalInterface);
  auto previousVelocity = units::math::hypot(velocityX, velocityY);
  distance = units::math::sqrt(8_sq_m);
  velocityX = distance / cycleTimeInSeconds * M_SQRT1_2;
  velocityY = distance / cycleTimeInSeconds * M_SQRT1_2;
  acceleration = (units::math::hypot(velocityX, velocityY) - previousVelocity) / cycleTimeInSeconds;

  AssertDynamicsSignalEquality(result,
                               16.0_m,
                               9.0_m,
                               1.0_rad,
                               1.0_rad / cycleTimeInSeconds,
                               0.0_rad_per_s_sq,
                               velocityX,
                               velocityY,
                               acceleration,
                               distance);

  trajectoryFollower->Trigger(400);
  trajectoryFollower->UpdateOutput(0, resultSignalInterface, 400);

  result = std::dynamic_pointer_cast<DynamicsSignal const>(resultSignalInterface);

  previousVelocity = units::math::hypot(velocityX, velocityY);
  distance = 1.0_m + units::math::sqrt(2_sq_m);
  velocityX = 0_mps;
  velocityY = distance / cycleTimeInSeconds;
  acceleration = (units::math::hypot(velocityX, velocityY) - previousVelocity) / cycleTimeInSeconds;

  AssertDynamicsSignalEquality(result,
                               17.0_m,
                               11.0_m,
                               1.5_rad,
                               0.5_rad / cycleTimeInSeconds,
                               -12.5_rad_per_s_sq,
                               velocityX,
                               velocityY,
                               acceleration,
                               distance);
}

TEST(TrajectoryFollowerImplementation_WithExternalAcceleration_Unittests, LinearTrajectoryWithInterpolation)
{
  mantle_api::Pose fakePose1{{10_m, 10_m, 0_m}, {0_rad, 0_rad, 0_rad}};
  mantle_api::Pose fakePose2{{13_m, 14_m, 0_m}, {0.2_rad, 0_rad, 0_rad}};
  mantle_api::Pose fakePose3{{15_m, 14_m, 0_m}, {0.4_rad, 0_rad, 0_rad}};
  mantle_api::Pose fakePose4{{15_m, 16_m, 0_m}, {0.6_rad, 0_rad, 0_rad}};
  mantle_api::Pose fakePose5{{17_m, 16_m, 0_m}, {0.8_rad, 0_rad, 0_rad}};

  mantle_api::PolyLinePoint fakePolyLinePoint1{fakePose1, 0_s};
  mantle_api::PolyLinePoint fakePolyLinePoint2{fakePose2, 0.2_s};
  mantle_api::PolyLinePoint fakePolyLinePoint3{fakePose3, 0.4_s};
  mantle_api::PolyLinePoint fakePolyLinePoint4{fakePose4, 0.6_s};
  mantle_api::PolyLinePoint fakePolyLinePoint5{fakePose5, 0.8_s};

  mantle_api::PolyLine polyLine{
      fakePolyLinePoint1, fakePolyLinePoint2, fakePolyLinePoint3, fakePolyLinePoint4, fakePolyLinePoint5};

  Trajectory fakeCoordinates{"", polyLine};
  auto controlStrategy = std::make_shared<mantle_api::FollowTrajectoryControlStrategy>();
  controlStrategy->trajectory = fakeCoordinates;
  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> scenarioControl{controlStrategy};

  FakeAgent fakeAgent;
  ON_CALL(fakeAgent, GetPositionX()).WillByDefault(Return(10.0_m));
  ON_CALL(fakeAgent, GetPositionY()).WillByDefault(Return(10.0_m));
  ON_CALL(fakeAgent, GetYaw()).WillByDefault(Return(0.0_rad));

  TrajectoryTester trajectoryTester(DontCare<bool>(), DontCare<bool>(), nullptr, &fakeAgent, 200);

  ON_CALL(*trajectoryTester.scenarioControl, GetStrategies(mantle_api::ControlStrategyType::kFollowTrajectory))
      .WillByDefault(Return(scenarioControl));
  std::shared_ptr<TrajectoryFollowerImplementation> trajectoryFollower = trajectoryTester.trajectoryFollower;

  std::shared_ptr<SignalInterface const> resultSignalInterface;
  std::shared_ptr<DynamicsSignal const> result;
  auto inputSignal = std::make_shared<AccelerationSignal>(ComponentState::Acting, -50.0_mps_sq);

  trajectoryFollower->Trigger(0);
  trajectoryFollower->UpdateOutput(0, resultSignalInterface, 0);

  result = std::dynamic_pointer_cast<DynamicsSignal const>(resultSignalInterface);
  AssertDynamicsSignalEquality(
      result, 13.0_m, 14.0_m, 0.2_rad, 1.0_rad_per_s, 5.0_rad_per_s_sq, 15.0_mps, 20.0_mps, 125.0_mps_sq, 5.0_m);

  trajectoryFollower->UpdateInput(1, inputSignal, 0);
  trajectoryFollower->Trigger(200);
  trajectoryFollower->UpdateOutput(0, resultSignalInterface, 200);

  result = std::dynamic_pointer_cast<DynamicsSignal const>(resultSignalInterface);
  AssertDynamicsSignalEquality(
      result, 15.0_m, 15.0_m, 0.5_rad, 1.5_rad_per_s, 2.5_rad_per_s_sq, 0.0_mps, 15.0_mps, -50.0_mps_sq, 3.0_m);
  trajectoryFollower->UpdateInput(1, inputSignal, 0);
  trajectoryFollower->Trigger(400);
  trajectoryFollower->UpdateOutput(0, resultSignalInterface, 400);

  result = std::dynamic_pointer_cast<DynamicsSignal const>(resultSignalInterface);
  AssertDynamicsSignalEquality(
      result, 15.0_m, 16.0_m, 0.6_rad, 0.5_rad_per_s, -5.0_rad_per_s_sq, 0.0_mps, 5.0_mps, -50.0_mps_sq, 1.0_m);

  inputSignal = std::make_shared<AccelerationSignal>(ComponentState::Acting, 0.0_mps_sq);
  trajectoryFollower->UpdateInput(1, inputSignal, 0);
  trajectoryFollower->Trigger(600);
  trajectoryFollower->UpdateOutput(0, resultSignalInterface, 600);

  result = std::dynamic_pointer_cast<DynamicsSignal const>(resultSignalInterface);
  AssertDynamicsSignalEquality(
      result, 16.0_m, 16.0_m, 0.7_rad, 0.5_rad_per_s, 0.0_rad_per_s_sq, 5.0_mps, 0.0_mps, 0.0_mps_sq, 1.0_m);
}

TEST(TrajectoryFollowerImplementation_WithExternalAcceleration_Unittests, DeactivationForNegativeVelocity)
{
  mantle_api::Pose fakePose1{{10_m, 10_m, 0_m}, {0_rad, 0_rad, 0_rad}};
  mantle_api::Pose fakePose2{{13_m, 14_m, 0_m}, {0.2_rad, 0_rad, 0_rad}};
  mantle_api::Pose fakePose3{{15_m, 14_m, 0_m}, {0.4_rad, 0_rad, 0_rad}};
  mantle_api::Pose fakePose4{{15_m, 16_m, 0_m}, {0.6_rad, 0_rad, 0_rad}};
  mantle_api::Pose fakePose5{{17_m, 16_m, 0_m}, {0.8_rad, 0_rad, 0_rad}};

  mantle_api::PolyLinePoint fakePolyLinePoint1{fakePose1, 0_s};
  mantle_api::PolyLinePoint fakePolyLinePoint2{fakePose2, 0.2_s};
  mantle_api::PolyLinePoint fakePolyLinePoint3{fakePose3, 0.4_s};
  mantle_api::PolyLinePoint fakePolyLinePoint4{fakePose4, 0.6_s};
  mantle_api::PolyLinePoint fakePolyLinePoint5{fakePose5, 0.8_s};

  mantle_api::PolyLine polyLine{
      fakePolyLinePoint1, fakePolyLinePoint2, fakePolyLinePoint3, fakePolyLinePoint4, fakePolyLinePoint5};

  Trajectory fakeCoordinates{"", polyLine};
  auto controlStrategy = std::make_shared<mantle_api::FollowTrajectoryControlStrategy>();
  controlStrategy->trajectory = fakeCoordinates;
  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> scenarioControl{controlStrategy};

  FakeAgent fakeAgent;
  ON_CALL(fakeAgent, GetPositionX()).WillByDefault(Return(10.0_m));
  ON_CALL(fakeAgent, GetPositionY()).WillByDefault(Return(10.0_m));
  ON_CALL(fakeAgent, GetYaw()).WillByDefault(Return(0.0_rad));

  TrajectoryTester trajectoryTester(DontCare<bool>(), true, nullptr, &fakeAgent, 200);

  ON_CALL(*trajectoryTester.scenarioControl, GetStrategies(mantle_api::ControlStrategyType::kFollowTrajectory))
      .WillByDefault(Return(scenarioControl));
  std::shared_ptr<TrajectoryFollowerImplementation> trajectoryFollower = trajectoryTester.trajectoryFollower;

  std::shared_ptr<SignalInterface const> resultSignalInterface;
  std::shared_ptr<DynamicsSignal const> result;
  auto inputSignal = std::make_shared<AccelerationSignal>(ComponentState::Acting, -50.0_mps_sq);

  trajectoryFollower->Trigger(0);

  trajectoryFollower->UpdateInput(1, inputSignal, 0);
  trajectoryFollower->Trigger(200);

  trajectoryFollower->UpdateInput(1, inputSignal, 0);
  trajectoryFollower->Trigger(400);

  trajectoryFollower->UpdateInput(1, inputSignal, 0);
  trajectoryFollower->Trigger(600);

  trajectoryFollower->UpdateOutput(0, resultSignalInterface, 600);

  result = std::dynamic_pointer_cast<DynamicsSignal const>(resultSignalInterface);

  ASSERT_EQ(result->componentState, ComponentState::Disabled);
}

TEST(TrajectoryFollowerImplementation_WithExternalAcceleration_Unittests, MultipleTimestepsWithinTwoCoordinates)
{
  mantle_api::Pose fakePose1{{0_m, 0_m, 0_m}, {0_rad, 0_rad, 0_rad}};
  mantle_api::Pose fakePose2{{12_m, 0_m, 0_m}, {0_rad, 0_rad, 0_rad}};

  mantle_api::PolyLinePoint fakePolyLinePoint1{fakePose1, 0_s};
  mantle_api::PolyLinePoint fakePolyLinePoint2{fakePose2, 0.4_s};

  mantle_api::PolyLine polyLine{fakePolyLinePoint1, fakePolyLinePoint2};

  Trajectory fakeCoordinates{"", polyLine};
  auto controlStrategy = std::make_shared<mantle_api::FollowTrajectoryControlStrategy>();
  controlStrategy->trajectory = fakeCoordinates;
  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> scenarioControl{controlStrategy};

  TrajectoryTester trajectoryTester(DontCare<bool>(), DontCare<bool>());

  ON_CALL(*trajectoryTester.scenarioControl, GetStrategies(mantle_api::ControlStrategyType::kFollowTrajectory))
      .WillByDefault(Return(scenarioControl));
  std::shared_ptr<TrajectoryFollowerImplementation> trajectoryFollower = trajectoryTester.trajectoryFollower;

  std::shared_ptr<SignalInterface const> resultSignalInterface;
  std::shared_ptr<DynamicsSignal const> result;
  auto inputSignal = std::make_shared<AccelerationSignal>(ComponentState::Acting, -150.0_mps_sq);

  trajectoryFollower->Trigger(0);
  trajectoryFollower->UpdateOutput(0, resultSignalInterface, 0);

  result = std::dynamic_pointer_cast<DynamicsSignal const>(resultSignalInterface);
  AssertDynamicsSignalEquality(
      result, 3.0_m, 0.0_m, 0.0_rad, 0.0_rad_per_s, 0.0_rad_per_s_sq, 30.0_mps, 0.0_mps, 300.0_mps_sq, 3.0_m);

  trajectoryFollower->Trigger(100);
  trajectoryFollower->UpdateOutput(0, resultSignalInterface, 100);

  result = std::dynamic_pointer_cast<DynamicsSignal const>(resultSignalInterface);
  AssertDynamicsSignalEquality(
      result, 6.0_m, 0.0_m, 0_rad, 0_rad_per_s, 0.0_rad_per_s_sq, 30.0_mps, 0.0_mps, 0.0_mps_sq, 3.0_m);

  trajectoryFollower->UpdateInput(1, inputSignal, 200);
  trajectoryFollower->Trigger(200);
  trajectoryFollower->UpdateOutput(0, resultSignalInterface, 200);

  result = std::dynamic_pointer_cast<DynamicsSignal const>(resultSignalInterface);
  AssertDynamicsSignalEquality(
      result, 7.5_m, 0.0_m, 0.0_rad, 0_rad_per_s, 0.0_rad_per_s_sq, 15.0_mps, 0.0_mps, -150.0_mps_sq, 1.5_m);

  inputSignal = std::make_shared<AccelerationSignal>(ComponentState::Disabled, 0.0_mps_sq);
  trajectoryFollower->UpdateInput(1, inputSignal, 300);
  trajectoryFollower->Trigger(300);
  trajectoryFollower->UpdateOutput(0, resultSignalInterface, 300);

  result = std::dynamic_pointer_cast<DynamicsSignal const>(resultSignalInterface);
  AssertDynamicsSignalEquality(
      result, 9.0_m, 0.0_m, 0_rad, 0_rad_per_s, 0.0_rad_per_s_sq, 15.0_mps, 0.0_mps, 0.0_mps_sq, 1.5_m);
}
