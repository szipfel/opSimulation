/********************************************************************************
 * Copyright (c) 2018-2019 in-tech GmbH
 *               2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "dontCare.h"
#include "fakeAgent.h"
#include "fakeRadio.h"
#include "fakeWorld.h"
#include "limiterAccVehCompImpl.h"

using ::testing::_;
using ::testing::DontCare;
using ::testing::NiceMock;
using ::testing::Return;

struct LimiterTestParameter
{
  LimiterTestParameter(const double &maximumLimit,
                       const double &minimumLimit,
                       const units::velocity::meters_per_second_t &currentVelocity,
                       const std::string &airDragCoefficient,
                       const std::string &axleRatio,
                       const std::string &frontSurface,
                       const std::string &maximumEngineTorque,
                       const std::string &maximumEngineSpeed,
                       const std::string &minimumEngineSpeed,
                       const units::length::meter_t &staticWheelRadius,
                       const units::mass::kilogram_t &weight,
                       const std::vector<double> &gearRatios)
      : maximumLimit(maximumLimit),
        minimumLimit(minimumLimit),
        currentVelocity(currentVelocity),
        airDragCoefficient(airDragCoefficient),
        axleRatio(axleRatio),
        frontSurface(frontSurface),
        maximumEngineTorque(maximumEngineTorque),
        maximumEngineSpeed(maximumEngineSpeed),
        minimumEngineSpeed(minimumEngineSpeed),
        staticWheelRadius(staticWheelRadius),
        weight(weight),
        gearRatios(gearRatios)
  {
  }

  const double maximumLimit;
  const double minimumLimit;
  const units::velocity::meters_per_second_t currentVelocity;
  const std::string airDragCoefficient;
  const std::string axleRatio;
  const std::string frontSurface;
  const std::string maximumEngineTorque;
  const std::string maximumEngineSpeed;
  const std::string minimumEngineSpeed;
  const units::length::meter_t staticWheelRadius;
  const units::mass::kilogram_t weight;
  const std::vector<double> gearRatios;
};

class MaximumLimit : public ::testing::Test, public ::testing::WithParamInterface<LimiterTestParameter>
{
};

TEST_P(MaximumLimit, CalculateCorrectMaximumValue)
{
  const LimiterTestParameter testInput = GetParam();

  NiceMock<FakeAgent> fakeAgent;
  ON_CALL(fakeAgent, GetVelocity(_)).WillByDefault(Return(Common::Vector2d{testInput.currentVelocity, 0.0_mps}));

  NiceMock<FakeWorld> fakeWorld;
  ON_CALL(fakeWorld, GetFriction()).WillByDefault(Return(1.0));

  LimiterAccelerationVehicleComponentsImplementation limiter("",
                                                             false,
                                                             DontCare<int>(),
                                                             DontCare<int>(),
                                                             DontCare<int>(),
                                                             DontCare<int>(),
                                                             nullptr,
                                                             &fakeWorld,
                                                             nullptr,
                                                             nullptr,
                                                             nullptr,
                                                             &fakeAgent);

  const auto accelerationInputSignal = std::make_shared<AccelerationSignal const>(
      ComponentState::Acting, units::acceleration::meters_per_second_squared_t(INFINITY));

  mantle_api::VehicleProperties fakeVehicleParameters;
  fakeVehicleParameters.mass = testInput.weight;
  fakeVehicleParameters.properties = {{"FrontSurface", testInput.frontSurface},
                                      {"AirDragCoefficient", testInput.airDragCoefficient},
                                      {"MaximumEngineTorque", testInput.maximumEngineTorque},
                                      {"MinimumEngineSpeed", testInput.minimumEngineSpeed},
                                      {"MaximumEngineSpeed", testInput.maximumEngineSpeed},
                                      {"AxleRatio", testInput.axleRatio},
                                      {"FrictionCoefficient", "1.0"},
                                      {"NumberOfGears", std::to_string(testInput.gearRatios.size())}};
  for (size_t i = 0; i < testInput.gearRatios.size(); i++)
  {
    fakeVehicleParameters.properties.insert(
        {"GearRatio" + std::to_string(i + 1), std::to_string(testInput.gearRatios[i])});
  }
  fakeVehicleParameters.rear_axle.wheel_diameter = 2 * testInput.staticWheelRadius;

  const auto vehicleParameterInputSignal = std::make_shared<ParametersVehicleSignal const>(fakeVehicleParameters);

  limiter.UpdateInput(0, accelerationInputSignal, DontCare<int>());
  limiter.UpdateInput(100, vehicleParameterInputSignal, DontCare<int>());

  limiter.Trigger(DontCare<double>());

  std::shared_ptr<SignalInterface const> signalInterface;
  limiter.UpdateOutput(0, signalInterface, DontCare<double>());

  auto resultAcceleration = std::dynamic_pointer_cast<AccelerationSignal const>(signalInterface)->acceleration;

  // ASSERT
  ASSERT_NEAR(resultAcceleration.value(), testInput.maximumLimit, 0.001);
}

INSTANTIATE_TEST_CASE_P(
    Default,
    MaximumLimit,

    // maximumLimit, minimumLimit, currentVelocity, airDragCoefficient, axleRatio, frontSurface,
    // maximumEngineTorque, maximumEngineSpeed, minimumEngineSpeed, staticWheelRadius, weight, gearRatios

    testing::Values(LimiterTestParameter{7.6668,
                                         0,
                                         30.0_kph,
                                         "0.28",
                                         "2.813",
                                         "2.2",
                                         "270",
                                         "6000",
                                         "900",
                                         0.318_m,
                                         1525.0_kg,
                                         {0.0, 5.0, 3.2, 2.143, 1.72, 1.314, 1.0, 0.822, 0.64}},
                    LimiterTestParameter{6.5065,
                                         0,
                                         50.0_kph,
                                         "0.28",
                                         "2.813",
                                         "2.2",
                                         "270",
                                         "6000",
                                         "900",
                                         0.318_m,
                                         1525.0_kg,
                                         {0.0, 5.0, 3.2, 2.143, 1.72, 1.314, 1.0, 0.822, 0.64}},
                    LimiterTestParameter{4.5591,
                                         0,
                                         70.0_kph,
                                         "0.28",
                                         "2.813",
                                         "2.2",
                                         "270",
                                         "6000",
                                         "900",
                                         0.318_m,
                                         1525.0_kg,
                                         {0.0, 5.0, 3.2, 2.143, 1.72, 1.314, 1.0, 0.822, 0.64}},
                    LimiterTestParameter{3.0062,
                                         0,
                                         100.0_kph,
                                         "0.28",
                                         "2.813",
                                         "2.2",
                                         "270",
                                         "6000",
                                         "900",
                                         0.318_m,
                                         1525.0_kg,
                                         {0.0, 5.0, 3.2, 2.143, 1.72, 1.314, 1.0, 0.822, 0.64}},
                    LimiterTestParameter{0.67098,
                                         0,
                                         200.0_kph,
                                         "0.28",
                                         "2.813",
                                         "2.2",
                                         "270",
                                         "6000",
                                         "900",
                                         0.318_m,
                                         1525.0_kg,
                                         {0.0, 5.0, 3.2, 2.143, 1.72, 1.314, 1.0, 0.822, 0.64}}));
