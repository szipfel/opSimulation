/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "common/sensorDataSignal.h"
#include "core/opSimulation/modules/World_OSI/RadioImplementation.h"
#include "fakeAgent.h"
#include "fakeParameter.h"
#include "fakePublisher.h"
#include "fakeRadio.h"
#include "fakeStochastics.h"
#include "fakeWorld.h"
#include "sensorCar2X.h"

using ::testing::_;
using ::testing::Eq;
using ::testing::NiceMock;
using ::testing::Return;
using ::testing::ReturnRef;

class SensorCar2X_Test : public ::testing::Test
{
public:
  SensorCar2X_Test()
  {
    ON_CALL(fakeStochastics, GetLogNormalDistributed(_, _)).WillByDefault(Return(1));

    ON_CALL(fakeParameters, GetParametersDouble()).WillByDefault(ReturnRef(fakeDoubles));

    fakeInts = {{"Id", 0}};
    ON_CALL(fakeParameters, GetParametersInt()).WillByDefault(ReturnRef(fakeInts));

    fakeVehicleModelParameters->properties = {{"SensorPosition/Position1/Longitudinal", "1.0"},
                                              {"SensorPosition/Position1/Lateral", "1.0"},
                                              {"SensorPosition/Position1/Height", "0.0"},
                                              {"SensorPosition/Position1/Pitch", "0.0"},
                                              {"SensorPosition/Position1/Yaw", "0.0"},
                                              {"SensorPosition/Position1/Roll", "0.0"}};

    fakeStrings = {{"Position", "Position1"}};
    ON_CALL(fakeParameters, GetParametersString()).WillByDefault(ReturnRef(fakeStrings));

    ON_CALL(fakeAgentInterface, GetId()).WillByDefault(Return(0));
    ON_CALL(fakeAgentInterface, GetPositionX()).WillByDefault(Return(100_m));
    ON_CALL(fakeAgentInterface, GetPositionY()).WillByDefault(Return(100_m));
    ON_CALL(fakeAgentInterface, GetYaw()).WillByDefault(Return(0_rad));
    ON_CALL(fakeAgentInterface, GetVehicleModelParameters()).WillByDefault(Return(fakeVehicleModelParameters));

    fakeObjects.push_back(&fakeAgentInterface);
    ON_CALL(fakeWorldInterface, GetWorldObjects()).WillByDefault(ReturnRef(fakeObjects));

    movingObject1.mutable_id()->set_value(0);
    movingObject2.mutable_id()->set_value(1);

    car2XObjects = {movingObject1, movingObject2};
    ON_CALL(fakeWorldInterface, GetRadio()).WillByDefault(ReturnRef(fakeRadio));
  }

  NiceMock<FakeWorld> fakeWorldInterface;
  NiceMock<FakeStochastics> fakeStochastics;
  NiceMock<FakeAgent> fakeAgentInterface;
  NiceMock<FakeParameter> fakeParameters;
  NiceMock<FakePublisher> fakePublisher;
  NiceMock<FakeRadio> fakeRadio;
  std::map<std::string, double> fakeDoubles;
  std::map<std::string, int> fakeInts;
  std::map<std::string, const std::string> fakeStrings;
  std::shared_ptr<mantle_api::VehicleProperties> fakeVehicleModelParameters
      = std::make_shared<mantle_api::VehicleProperties>();
  std::vector<const WorldObjectInterface*> fakeObjects;
  osi3::MovingObject movingObject1;
  osi3::MovingObject movingObject2;
  std::vector<osi3::MovingObject> car2XObjects;
};

TEST_F(SensorCar2X_Test, FailureProbabilityEqualNull_DetectsObject)
{
  ON_CALL(fakeStochastics, GetUniformDistributed(_, _)).WillByDefault(Return(1));

  fakeDoubles = {{"FailureProbability", 0}, {"Latency", 0}, {"Sensitivity", 1e-5}};

  EXPECT_CALL(fakeRadio, Receive(Eq(101_m), Eq(101_m), Eq(units::sensitivity(1e-5)))).WillOnce(Return(car2XObjects));

  SensorCar2X sensor("",
                     false,
                     0,
                     0,
                     0,
                     0,
                     &fakeStochastics,
                     &fakeWorldInterface,
                     &fakeParameters,
                     &fakePublisher,
                     nullptr,
                     &fakeAgentInterface);

  osi3::SensorData sensorData = sensor.DetectObjects();

  ASSERT_EQ(sensorData.moving_object_size(), 1);
}

TEST_F(SensorCar2X_Test, FailureProbabilityEqualOne_DetectsNothing)
{
  ON_CALL(fakeStochastics, GetUniformDistributed(0, 1)).WillByDefault(Return(0.5));

  fakeDoubles = {{"FailureProbability", 1}, {"Latency", 0}, {"Sensitivity", 1e-5}};

  EXPECT_CALL(fakeRadio, Receive(Eq(101_m), Eq(101_m), Eq(units::sensitivity(1e-5)))).WillOnce(Return(car2XObjects));

  SensorCar2X sensor("",
                     false,
                     0,
                     0,
                     0,
                     0,
                     &fakeStochastics,
                     &fakeWorldInterface,
                     &fakeParameters,
                     &fakePublisher,
                     nullptr,
                     &fakeAgentInterface);

  osi3::SensorData sensorData = sensor.DetectObjects();

  ASSERT_EQ(sensorData.moving_object_size(), 0);
}

TEST_F(SensorCar2X_Test, FailureProbabilityLessThanDrawFromUniformDistribution_DetectsObject)
{
  ON_CALL(fakeStochastics, GetUniformDistributed(0, 1)).WillByDefault(Return(0.8));

  fakeDoubles = {{"FailureProbability", 0.5}, {"Latency", 0}, {"Sensitivity", 1e-5}};

  EXPECT_CALL(fakeRadio, Receive(Eq(101_m), Eq(101_m), Eq(units::sensitivity(1e-5)))).WillOnce(Return(car2XObjects));

  SensorCar2X sensor("",
                     false,
                     0,
                     0,
                     0,
                     0,
                     &fakeStochastics,
                     &fakeWorldInterface,
                     &fakeParameters,
                     &fakePublisher,
                     nullptr,
                     &fakeAgentInterface);

  osi3::SensorData sensorData = sensor.DetectObjects();

  ASSERT_EQ(sensorData.moving_object_size(), 1);
}

TEST_F(SensorCar2X_Test, FailureProbabilityGreaterThanDrawFromUniformDistribution_DetectsNothing)
{
  ON_CALL(fakeStochastics, GetUniformDistributed(0, 1)).WillByDefault(Return(0.3));

  fakeDoubles = {{"FailureProbability", 0.5}, {"Latency", 0}, {"Sensitivity", 1e-5}};

  EXPECT_CALL(fakeRadio, Receive(Eq(101_m), Eq(101_m), Eq(units::sensitivity(1e-5)))).WillOnce(Return(car2XObjects));

  SensorCar2X sensor("",
                     false,
                     0,
                     0,
                     0,
                     0,
                     &fakeStochastics,
                     &fakeWorldInterface,
                     &fakeParameters,
                     &fakePublisher,
                     nullptr,
                     &fakeAgentInterface);

  osi3::SensorData sensorData = sensor.DetectObjects();

  ASSERT_EQ(sensorData.moving_object_size(), 0);
}