/********************************************************************************
 * Copyright (c) 2019-2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <functional>
#include <iostream>
#include <list>
#include <set>

#include "agentBlueprintProvider.h"
#include "common/globalDefinitions.h"
#include "environment.h"
#include "fakeAgentFactory.h"
#include "fakeCollisionDetection.h"
#include "fakeConfigurationContainer.h"
#include "fakeDataBuffer.h"
#include "fakeEnvironment.h"
#include "fakeObservationNetwork.h"
#include "fakeSpawnPointNetwork.h"
#include "fakeStochastics.h"
#include "fakeWorld.h"
#include "runResult.h"
#include "scheduler.h"
#include "schedulerTasks.h"

using namespace core::scheduling;

using testing::NiceMock;
using testing::Return;
using testing::ReturnRef;

template <typename T>
void ExecuteTasks(T tasks)
{
  for (auto &task : tasks)
  {
    task.func();
  }
}

bool ExecuteSpawn(int time)
{
  std::cout << "spawning task at " << time << std::endl;
  return true;
}

bool ExecuteTrigger(int time)
{
  std::cout << "triggering task at " << time << std::endl;
  return true;
}

bool ExecuteUpdate(int id, int time)
{
  std::cout << "updating task at " << time << " for id: " << id << std::endl;
  return true;
}

TEST(DISABLED_Scheduler, RunWorks)
{
  NiceMock<FakeWorld> fakeWorld;

  NiceMock<core::FakeSpawnPointNetwork> fakeSpawnPointNetwork;
  NiceMock<FakeDataBuffer> fakeDataBuffer;
  NiceMock<FakeCollisionDetection> fakecollisionDetection;
  NiceMock<FakeObservationNetwork> fakeObservationNetwork;
  NiceMock<FakeStochastics> fakestochastics;
  NiceMock<FakeConfigurationContainer> fakeConfigurationContainer;
  NiceMock<FakeEnvironment> fakeEnvironment;
  ON_CALL(fakeEnvironment, GetSimulationTime());

  Scheduler scheduler(fakeWorld,
                      fakeSpawnPointNetwork,
                      fakecollisionDetection,
                      fakeObservationNetwork,
                      fakeDataBuffer,
                      fakeEnvironment);

  core::RunResult runResult{};
  scheduler.Run(0, runResult, nullptr);
}
