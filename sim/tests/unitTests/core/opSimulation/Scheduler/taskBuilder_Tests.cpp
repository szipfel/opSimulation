/********************************************************************************
 * Copyright (c) 2019-2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <vector>

#include "fakeCollisionDetection.h"
#include "fakeDataBuffer.h"
#include "fakeRadio.h"
#include "fakeSpawnPointNetwork.h"
#include "fakeWorld.h"
#include "taskBuilder.h"

using ::testing::_;
using ::testing::Contains;
using ::testing::Eq;
using ::testing::Field;
using ::testing::Gt;
using ::testing::Invoke;
using ::testing::NiceMock;
using ::testing::Not;
using ::testing::Return;
using ::testing::SizeIs;

using namespace core;
using namespace core::scheduling;

TEST(TaskBuilder, SpawningTaskCreation_Works)
{
  NiceMock<FakeCollisionDetection> fakecollisionDetection;
  int currentTime = 0;

  NiceMock<FakeWorld> fakeWorld;
  NiceMock<FakeDataBuffer> fakeDataBuffer;
  RunResult runResult{};
  TaskBuilder taskBuilder(
      currentTime, runResult, 100, &fakeWorld, nullptr, nullptr, &fakecollisionDetection, &fakeDataBuffer, nullptr);

  auto commonTasks = taskBuilder.CreateSpawningTasks();
  ASSERT_THAT(commonTasks, SizeIs(Gt(size_t(0))));
  ASSERT_THAT(commonTasks, Contains(Field(&TaskItem::taskType, Eq(TaskType::Spawning))));
}

TEST(TaskBuilder, PreAgentTaskCreation_Works)
{
  NiceMock<FakeCollisionDetection> fakecollisionDetection;
  int currentTime = 0;

  NiceMock<FakeWorld> fakeWorld;
  NiceMock<FakeDataBuffer> fakeDataBuffer;
  RunResult runResult{};
  TaskBuilder taskBuilder(
      currentTime, runResult, 100, &fakeWorld, nullptr, nullptr, &fakecollisionDetection, &fakeDataBuffer, nullptr);

  auto commonTasks = taskBuilder.CreatePreAgentTasks();
  ASSERT_THAT(commonTasks, SizeIs(Gt(size_t(0))));
  ASSERT_THAT(commonTasks, Contains(Field(&TaskItem::taskType, Eq(TaskType::SyncGlobalData))));
  ASSERT_THAT(commonTasks, Contains(Field(&TaskItem::taskType, Eq(TaskType::CollisionDetector))));
}

TEST(TaskBuilder, SynchronizeTaskCreation_Works)
{
  NiceMock<FakeCollisionDetection> fakecollisionDetection;

  int currentTime = 0;

  NiceMock<FakeWorld> fakeWorld;
  NiceMock<FakeDataBuffer> fakeDataBuffer;
  RunResult runResult{};
  TaskBuilder taskBuilder(
      currentTime, runResult, 100, &fakeWorld, nullptr, nullptr, &fakecollisionDetection, &fakeDataBuffer, nullptr);

  auto finalizeTasks = taskBuilder.CreateSynchronizeTasks();
  ASSERT_THAT(finalizeTasks, SizeIs(Gt(size_t(0))));
}
