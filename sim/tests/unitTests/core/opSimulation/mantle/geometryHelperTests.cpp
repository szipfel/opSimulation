/*******************************************************************************
 * Copyright (c) 2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <MantleAPI/Common/pose.h>
#include <units.h>

#include "geometryHelper.h"
#include "poseUtils.h"

using namespace units::literals;

namespace TranslateGlobalPositionLocallyTest
{

TEST(TranslateGlobalPositionLocally, Stub) {}

}  // namespace TranslateGlobalPositionLocallyTest

namespace TransformPolylinePointsFromWorldToLocalTest
{

TEST(TransformPolylinePointsFromWorldToLocal, Stub) {}

}  // namespace TransformPolylinePointsFromWorldToLocalTest

namespace TransformPositionFromWorldToLocalTest
{

struct Data
{
  struct Actual
  {
    mantle_api::Vec3<units::length::meter_t> world_position;
    mantle_api::Vec3<units::length::meter_t> local_origin;
    mantle_api::Orientation3<units::angle::radian_t> local_orientation;
  } actual;

  struct Expected
  {
    mantle_api::Vec3<units::length::meter_t> value;
    operator mantle_api::Vec3<units::length::meter_t>() const { return value; }
  } expected;

  friend std::ostream &operator<<(std::ostream &os, const Data &data)
  {
    os << "\nACTUAL";
    os << "\n  world_position:    " << data.actual.world_position;
    os << "\n  local_origin:      " << data.actual.local_origin;
    os << "\n  local_orientation: " << data.actual.local_orientation;
    os << "\nEXPECTED";
    os << "\n  transformed_pos:   " << data.expected.value;
    return os;
  }
};

class TransformPositionFromWorldToLocal : public ::testing::TestWithParam<Data>
{
};

INSTANTIATE_TEST_SUITE_P(TranslationSet,
                         TransformPositionFromWorldToLocal,
                         ::testing::Values(Data{Data::Actual{{0_m, 0_m, 0_m}, {0_m, 0_m, 0_m}, {0_deg, 0_deg, 0_deg}},
                                                Data::Expected{{0_m, 0_m, 0_m}}},  // zero test
                                           Data{Data::Actual{{1_m, 2_m, 3_m}, {1_m, 2_m, 3_m}, {0_deg, 0_deg, 0_deg}},
                                                Data::Expected{{0_m, 0_m, 0_m}}},  // point at displaced origin
                                           Data{Data::Actual{{1_m, 2_m, 3_m}, {0_m, 0_m, 0_m}, {0_deg, 0_deg, 0_deg}},
                                                Data::Expected{{1_m, 2_m, 3_m}}},  // same origins
                                           Data{Data::Actual{{0_m, 0_m, 0_m}, {1_m, 2_m, 3_m}, {0_deg, 0_deg, 0_deg}},
                                                Data::Expected{{-1_m, -2_m, -3_m}}}  // displaced origin
                                           ));

INSTANTIATE_TEST_SUITE_P(RotationSet,
                         TransformPositionFromWorldToLocal,
                         ::testing::Values(Data{Data::Actual{
                                                    {0_m, 0_m, 0_m}, {0_m, 0_m, 0_m}, {90_deg, 90_deg, 90_deg}},
                                                Data::Expected{{0_m, 0_m, 0_m}}},  // rotation only
                                           Data{Data::Actual{{2_m, 3_m, 4_m}, {1_m, 1_m, 1_m}, {90_deg, 0_deg, 0_deg}},
                                                Data::Expected{{2_m, -1_m, 3_m}}},  // rotation and translation (yaw)
                                           Data{Data::Actual{{2_m, 3_m, 4_m}, {1_m, 1_m, 1_m}, {0_deg, 90_deg, 0_deg}},
                                                Data::Expected{{-3_m, 2_m, 1_m}}},  // rotation and translation (pitch)
                                           Data{Data::Actual{{2_m, 3_m, 4_m}, {1_m, 1_m, 1_m}, {0_deg, 0_deg, 90_deg}},
                                                Data::Expected{{1_m, 3_m, -2_m}}}  // rotation and translation (roll)
                                           ));

TEST_P(TransformPositionFromWorldToLocal, BasicTest)
{
  core::GeometryHelper gh;

  const auto &[world_position, local_origin, local_orientation] = GetParam().actual;
  const auto actual = gh.TransformPositionFromWorldToLocal(world_position, local_origin, local_orientation);
  ASSERT_TRUE(AreNear(actual, GetParam().expected));
}

}  // namespace TransformPositionFromWorldToLocalTest