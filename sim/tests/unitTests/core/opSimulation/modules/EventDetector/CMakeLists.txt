################################################################################
# Copyright (c) 2020-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################
set(COMPONENT_TEST_NAME EventDetector_Tests)
set(COMPONENT_SOURCE_DIR ${OPENPASS_SIMCORE_DIR}/core/opSimulation/framework)

add_openpass_target(
  NAME ${COMPONENT_TEST_NAME} TYPE test COMPONENT core
  DEFAULT_MAIN
  LINKOSI

  SOURCES
    DetectCollisionTests.cpp
    ${COMPONENT_SOURCE_DIR}/collisionDetector.cpp
    ${COMPONENT_SOURCE_DIR}/srcCollisionPostCrash/collisionDetection_Impact_implementation.cpp
    ${COMPONENT_SOURCE_DIR}/srcCollisionPostCrash/polygon.cpp

  HEADERS
    ${COMPONENT_SOURCE_DIR}/collisionDetector.h
    ${COMPONENT_SOURCE_DIR}/srcCollisionPostCrash/collisionDetection_Impact_implementation.h
    ${COMPONENT_SOURCE_DIR}/srcCollisionPostCrash/polygon.h

  INCDIRS
    ${OPENPASS_SIMCORE_DIR}/core
    ${COMPONENT_SOURCE_DIR}

  LIBRARIES
    Qt5::Core
    Common
)
