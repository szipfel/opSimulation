/********************************************************************************
 * Copyright (c) 2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "collisionDetector.h"
#include "fakeAgent.h"
#include "fakePublisher.h"
#include "fakeRadio.h"
#include "fakeRunResult.h"
#include "fakeTrafficObject.h"
#include "fakeWorld.h"

using ::testing::_;
using ::testing::NiceMock;
using ::testing::Return;
using ::testing::ReturnRef;

class CollisionDetectorTest : public ::testing::Test
{
public:
  CollisionDetectorTest()
  {
    objects = {&objectAsCollisionParter};
    agents = {{0, &agentUnderTest}, {9, &agentAsCollisionPartner}};

    ON_CALL(mockWorld, GetTrafficObjects()).WillByDefault(ReturnRef(objects));
    ON_CALL(mockWorld, GetAgents()).WillByDefault(Return(agents));
  }

protected:
  NiceMock<FakeTrafficObject> objectAsCollisionParter;
  NiceMock<FakeAgent> agentUnderTest;
  NiceMock<FakeAgent> agentAsCollisionPartner;
  NiceMock<FakeWorld> mockWorld;
  NiceMock<FakePublisher> mockPublisher;
  NiceMock<FakeRunResult> mockRunResult;

  std::vector<const TrafficObjectInterface *> objects;
  std::map<int, AgentInterface *> agents;

  void UseAgent()
  {
    objects = {};
    agents = {{0, &agentUnderTest}, {1, &agentAsCollisionPartner}};
    std::shared_ptr<mantle_api::VehicleProperties> vehicleModelParameters
        = std::make_shared<mantle_api::VehicleProperties>();

    ON_CALL(mockWorld, GetTrafficObjects()).WillByDefault(ReturnRef(objects));
    ON_CALL(mockWorld, GetAgents()).WillByDefault(Return(agents));
    ON_CALL(agentAsCollisionPartner, GetVehicleModelParameters()).WillByDefault(Return(vehicleModelParameters));
    ON_CALL(mockWorld, GetAgent(_)).WillByDefault(Return(&agentUnderTest));
    ON_CALL(mockWorld, GetAgent(_)).WillByDefault(Return(&agentAsCollisionPartner));
  }

  void UseObject()
  {
    objects = {&objectAsCollisionParter};
    agents = {{0, &agentUnderTest}};

    ON_CALL(mockWorld, GetTrafficObjects()).WillByDefault(ReturnRef(objects));
    ON_CALL(mockWorld, GetAgents()).WillByDefault(Return(agents));
    ON_CALL(mockWorld, GetAgent(_)).WillByDefault(Return(&agentUnderTest));
  }
};

TEST_F(CollisionDetectorTest, TestCollisionStandardCase)
{
  UseObject();
  ON_CALL(objectAsCollisionParter, GetIsCollidable()).WillByDefault(Return(true));

  ON_CALL(objectAsCollisionParter, GetPositionZ()).WillByDefault(Return(4.0_m));
  ON_CALL(objectAsCollisionParter, GetHeight()).WillByDefault(Return(2.0_m));
  ON_CALL(agentUnderTest, GetHeight()).WillByDefault(Return(5.0_m));

  polygon_t ownBoundingBox;
  ownBoundingBox.outer().push_back(point_t{1, 1});
  ownBoundingBox.outer().push_back(point_t{1, 3});
  ownBoundingBox.outer().push_back(point_t{3, 3});
  ownBoundingBox.outer().push_back(point_t{3, 1});
  ON_CALL(agentUnderTest, GetBoundingBox2D()).WillByDefault(ReturnRef(ownBoundingBox));

  polygon_t otherBoundingBox;
  otherBoundingBox.outer().push_back(point_t{2, 2});
  otherBoundingBox.outer().push_back(point_t{2, 4});
  otherBoundingBox.outer().push_back(point_t{4, 4});
  otherBoundingBox.outer().push_back(point_t{4, 2});
  ON_CALL(objectAsCollisionParter, GetBoundingBox2D()).WillByDefault(ReturnRef(otherBoundingBox));

  CollisionDetector collisionDetector(&mockWorld, &mockPublisher);
  collisionDetector.Initialize(&mockRunResult);
  EXPECT_CALL(mockRunResult, AddCollisionId(_)).Times(1);
  collisionDetector.Trigger(0);
}

TEST_F(CollisionDetectorTest, TestCollisionStandardCase2)
{
  UseObject();

  ON_CALL(objectAsCollisionParter, GetIsCollidable()).WillByDefault(Return(true));

  ON_CALL(objectAsCollisionParter, GetPositionZ()).WillByDefault(Return(4.0_m));
  ON_CALL(objectAsCollisionParter, GetHeight()).WillByDefault(Return(2.0_m));
  ON_CALL(agentUnderTest, GetHeight()).WillByDefault(Return(5.0_m));

  polygon_t ownBoundingBox;
  ownBoundingBox.outer().push_back(point_t{0, 1});
  ownBoundingBox.outer().push_back(point_t{1, 2});
  ownBoundingBox.outer().push_back(point_t{2, 1});
  ownBoundingBox.outer().push_back(point_t{1, 0});
  ON_CALL(agentUnderTest, GetBoundingBox2D()).WillByDefault(ReturnRef(ownBoundingBox));

  polygon_t otherBoundingBox;
  otherBoundingBox.outer().push_back(point_t{1, 1});
  otherBoundingBox.outer().push_back(point_t{2, 2});
  otherBoundingBox.outer().push_back(point_t{3, 1});
  otherBoundingBox.outer().push_back(point_t{2, 0});
  ON_CALL(objectAsCollisionParter, GetBoundingBox2D()).WillByDefault(ReturnRef(otherBoundingBox));

  CollisionDetector collisionDetector(&mockWorld, &mockPublisher);
  collisionDetector.Initialize(&mockRunResult);
  EXPECT_CALL(mockRunResult, AddCollisionId(_)).Times(1);
  collisionDetector.Trigger(0);
}
TEST_F(CollisionDetectorTest, TestCollisionStandardCase3)
{
  UseObject();
  ON_CALL(objectAsCollisionParter, GetIsCollidable()).WillByDefault(Return(true));

  ON_CALL(objectAsCollisionParter, GetPositionZ()).WillByDefault(Return(4.0_m));
  ON_CALL(objectAsCollisionParter, GetHeight()).WillByDefault(Return(2.0_m));
  ON_CALL(agentUnderTest, GetHeight()).WillByDefault(Return(5.0_m));

  polygon_t ownBoundingBox;
  ownBoundingBox.outer().push_back(point_t{1, 1});
  ownBoundingBox.outer().push_back(point_t{1, 3});
  ownBoundingBox.outer().push_back(point_t{3, 3});
  ownBoundingBox.outer().push_back(point_t{3, 1});
  ON_CALL(agentUnderTest, GetBoundingBox2D()).WillByDefault(ReturnRef(ownBoundingBox));

  polygon_t otherBoundingBox;
  otherBoundingBox.outer().push_back(point_t{1, 1});
  otherBoundingBox.outer().push_back(point_t{2, 2});
  otherBoundingBox.outer().push_back(point_t{3, 1});
  otherBoundingBox.outer().push_back(point_t{2, 0});
  ON_CALL(objectAsCollisionParter, GetBoundingBox2D()).WillByDefault(ReturnRef(otherBoundingBox));

  CollisionDetector collisionDetector(&mockWorld, &mockPublisher);
  collisionDetector.Initialize(&mockRunResult);
  EXPECT_CALL(mockRunResult, AddCollisionId(_)).Times(1);
  collisionDetector.Trigger(0);
}

TEST_F(CollisionDetectorTest, TestCollisionStandardCase4)
{
  UseObject();
  ON_CALL(objectAsCollisionParter, GetIsCollidable()).WillByDefault(Return(true));

  ON_CALL(objectAsCollisionParter, GetPositionZ()).WillByDefault(Return(4.0_m));
  ON_CALL(objectAsCollisionParter, GetHeight()).WillByDefault(Return(2.0_m));
  ON_CALL(agentUnderTest, GetHeight()).WillByDefault(Return(5.0_m));

  polygon_t ownBoundingBox;
  ownBoundingBox.outer().push_back(point_t{1, 1});
  ownBoundingBox.outer().push_back(point_t{1, 3});
  ownBoundingBox.outer().push_back(point_t{3, 3});
  ownBoundingBox.outer().push_back(point_t{3, 1});
  ON_CALL(agentUnderTest, GetBoundingBox2D()).WillByDefault(ReturnRef(ownBoundingBox));

  polygon_t otherBoundingBox;
  otherBoundingBox.outer().push_back(point_t{0, 1});
  otherBoundingBox.outer().push_back(point_t{1, 2});
  otherBoundingBox.outer().push_back(point_t{2, 1});
  otherBoundingBox.outer().push_back(point_t{1, 0});
  ON_CALL(objectAsCollisionParter, GetBoundingBox2D()).WillByDefault(ReturnRef(otherBoundingBox));

  CollisionDetector collisionDetector(&mockWorld, &mockPublisher);
  collisionDetector.Initialize(&mockRunResult);
  EXPECT_CALL(mockRunResult, AddCollisionId(_)).Times(1);
  collisionDetector.Trigger(0);
}

TEST_F(CollisionDetectorTest, TestCollisionStandardCase_AgentWithAgent)
{
  UseAgent();

  ON_CALL(agentUnderTest, GetHeight()).WillByDefault(Return(5.0_m));

  polygon_t ownBoundingBox;
  ownBoundingBox.outer().push_back(point_t{1, 1});
  ownBoundingBox.outer().push_back(point_t{1, 3});
  ownBoundingBox.outer().push_back(point_t{3, 3});
  ownBoundingBox.outer().push_back(point_t{3, 1});
  ON_CALL(agentUnderTest, GetBoundingBox2D()).WillByDefault(ReturnRef(ownBoundingBox));

  polygon_t otherBoundingBox;
  otherBoundingBox.outer().push_back(point_t{2, 2});
  otherBoundingBox.outer().push_back(point_t{2, 4});
  otherBoundingBox.outer().push_back(point_t{4, 4});
  otherBoundingBox.outer().push_back(point_t{4, 2});
  ON_CALL(agentAsCollisionPartner, GetBoundingBox2D()).WillByDefault(ReturnRef(otherBoundingBox));

  CollisionDetector collisionDetector(&mockWorld, &mockPublisher);
  collisionDetector.Initialize(&mockRunResult);
  EXPECT_CALL(mockRunResult, AddCollisionId(_)).Times(2);
  collisionDetector.Trigger(0);
}

TEST_F(CollisionDetectorTest, TestCollisionSpecialCaseCrossedHitboxes)
{
  UseObject();
  ON_CALL(objectAsCollisionParter, GetIsCollidable()).WillByDefault(Return(true));

  ON_CALL(objectAsCollisionParter, GetPositionZ()).WillByDefault(Return(4.0_m));
  ON_CALL(objectAsCollisionParter, GetHeight()).WillByDefault(Return(2.0_m));
  ON_CALL(agentUnderTest, GetHeight()).WillByDefault(Return(5.0_m));

  polygon_t ownBoundingBox;
  ownBoundingBox.outer().push_back(point_t{-1, 3});
  ownBoundingBox.outer().push_back(point_t{1, 3});
  ownBoundingBox.outer().push_back(point_t{1, -3});
  ownBoundingBox.outer().push_back(point_t{-1, -3});
  ON_CALL(agentUnderTest, GetBoundingBox2D()).WillByDefault(ReturnRef(ownBoundingBox));

  polygon_t otherBoundingBox;
  otherBoundingBox.outer().push_back(point_t{-3, 1});
  otherBoundingBox.outer().push_back(point_t{3, 1});
  otherBoundingBox.outer().push_back(point_t{3, -1});
  otherBoundingBox.outer().push_back(point_t{-3, -1});
  ON_CALL(objectAsCollisionParter, GetBoundingBox2D()).WillByDefault(ReturnRef(otherBoundingBox));

  CollisionDetector collisionDetector(&mockWorld, &mockPublisher);
  collisionDetector.Initialize(&mockRunResult);
  EXPECT_CALL(mockRunResult, AddCollisionId(_)).Times(1);
  collisionDetector.Trigger(0);
}

TEST_F(CollisionDetectorTest, TestCollisionSpecialCaseBothSameHitbox)
{
  UseObject();
  ON_CALL(objectAsCollisionParter, GetIsCollidable()).WillByDefault(Return(true));

  ON_CALL(objectAsCollisionParter, GetPositionZ()).WillByDefault(Return(4.0_m));
  ON_CALL(objectAsCollisionParter, GetHeight()).WillByDefault(Return(2.0_m));
  ON_CALL(agentUnderTest, GetHeight()).WillByDefault(Return(5.0_m));

  polygon_t ownBoundingBox;
  ownBoundingBox.outer().push_back(point_t{0, 0});
  ownBoundingBox.outer().push_back(point_t{0, 5});
  ownBoundingBox.outer().push_back(point_t{5, 5});
  ownBoundingBox.outer().push_back(point_t{5, 0});
  ON_CALL(agentUnderTest, GetBoundingBox2D()).WillByDefault(ReturnRef(ownBoundingBox));

  polygon_t otherBoundingBox;
  otherBoundingBox.outer().push_back(point_t{0, 0});
  otherBoundingBox.outer().push_back(point_t{0, 5});
  otherBoundingBox.outer().push_back(point_t{5, 5});
  otherBoundingBox.outer().push_back(point_t{5, 0});
  ON_CALL(objectAsCollisionParter, GetBoundingBox2D()).WillByDefault(ReturnRef(otherBoundingBox));

  CollisionDetector collisionDetector(&mockWorld, &mockPublisher);
  collisionDetector.Initialize(&mockRunResult);
  EXPECT_CALL(mockRunResult, AddCollisionId(_)).Times(1);
  collisionDetector.Trigger(0);
}

TEST_F(CollisionDetectorTest, TestCollisionSpecialCaseOneWithinTheOther)
{
  UseObject();
  ON_CALL(objectAsCollisionParter, GetIsCollidable()).WillByDefault(Return(true));

  ON_CALL(objectAsCollisionParter, GetPositionZ()).WillByDefault(Return(4.0_m));
  ON_CALL(objectAsCollisionParter, GetHeight()).WillByDefault(Return(2.0_m));
  ON_CALL(agentUnderTest, GetHeight()).WillByDefault(Return(5.0_m));

  polygon_t ownBoundingBox;
  ownBoundingBox.outer().push_back(point_t{0, 0});
  ownBoundingBox.outer().push_back(point_t{0, 5});
  ownBoundingBox.outer().push_back(point_t{5, 5});
  ownBoundingBox.outer().push_back(point_t{5, 0});
  ON_CALL(agentUnderTest, GetBoundingBox2D()).WillByDefault(ReturnRef(ownBoundingBox));

  polygon_t otherBoundingBox;
  otherBoundingBox.outer().push_back(point_t{1, 1});
  otherBoundingBox.outer().push_back(point_t{1, 3});
  otherBoundingBox.outer().push_back(point_t{3, 3});
  otherBoundingBox.outer().push_back(point_t{3, 1});
  ON_CALL(objectAsCollisionParter, GetBoundingBox2D()).WillByDefault(ReturnRef(otherBoundingBox));

  CollisionDetector collisionDetector(&mockWorld, &mockPublisher);
  collisionDetector.Initialize(&mockRunResult);
  EXPECT_CALL(mockRunResult, AddCollisionId(_)).Times(1);
  collisionDetector.Trigger(0);
}

TEST_F(CollisionDetectorTest, TestCollidableFalseWhenObjectAreColliding)
{
  UseObject();
  ON_CALL(objectAsCollisionParter, GetIsCollidable()).WillByDefault(Return(false));

  ON_CALL(objectAsCollisionParter, GetPositionZ()).WillByDefault(Return(0.0_m));
  ON_CALL(objectAsCollisionParter, GetHeight()).WillByDefault(Return(1.0_m));
  ON_CALL(agentUnderTest, GetHeight()).WillByDefault(Return(5.0_m));

  polygon_t ownBoundingBox;
  ownBoundingBox.outer().push_back(point_t{1, 1});
  ownBoundingBox.outer().push_back(point_t{1, 3});
  ownBoundingBox.outer().push_back(point_t{3, 3});
  ownBoundingBox.outer().push_back(point_t{3, 1});
  ON_CALL(agentUnderTest, GetBoundingBox2D()).WillByDefault(ReturnRef(ownBoundingBox));

  polygon_t otherBoundingBox;
  otherBoundingBox.outer().push_back(point_t{2, 2});
  otherBoundingBox.outer().push_back(point_t{2, 4});
  otherBoundingBox.outer().push_back(point_t{4, 4});
  otherBoundingBox.outer().push_back(point_t{4, 2});
  ON_CALL(objectAsCollisionParter, GetBoundingBox2D()).WillByDefault(ReturnRef(otherBoundingBox));

  CollisionDetector collisionDetector(&mockWorld, &mockPublisher);
  collisionDetector.Initialize(&mockRunResult);
  EXPECT_CALL(mockRunResult, AddCollisionId(_)).Times(0);
  collisionDetector.Trigger(0);
}

TEST_F(CollisionDetectorTest, TestNotCollideDueToZValue)
{
  UseObject();
  ON_CALL(objectAsCollisionParter, GetIsCollidable()).WillByDefault(Return(true));

  ON_CALL(objectAsCollisionParter, GetHeight()).WillByDefault(Return(2.0_m));
  ON_CALL(objectAsCollisionParter, GetPositionZ()).WillByDefault(Return(10.0_m));
  ON_CALL(agentUnderTest, GetHeight()).WillByDefault(Return(5.0_m));

  polygon_t ownBoundingBox;
  ownBoundingBox.outer().push_back(point_t{1, 1});
  ownBoundingBox.outer().push_back(point_t{1, 3});
  ownBoundingBox.outer().push_back(point_t{3, 3});
  ownBoundingBox.outer().push_back(point_t{3, 1});
  ON_CALL(agentUnderTest, GetBoundingBox2D()).WillByDefault(ReturnRef(ownBoundingBox));

  polygon_t otherBoundingBox;
  otherBoundingBox.outer().push_back(point_t{2, 2});
  otherBoundingBox.outer().push_back(point_t{2, 4});
  otherBoundingBox.outer().push_back(point_t{4, 4});
  otherBoundingBox.outer().push_back(point_t{4, 2});
  ON_CALL(objectAsCollisionParter, GetBoundingBox2D()).WillByDefault(ReturnRef(otherBoundingBox));

  CollisionDetector collisionDetector(&mockWorld, &mockPublisher);
  collisionDetector.Initialize(&mockRunResult);
  EXPECT_CALL(mockRunResult, AddCollisionId(_)).Times(0);
  collisionDetector.Trigger(0);
}

TEST_F(CollisionDetectorTest, TestNotCollideBecauseNotOverlapping2DHitbox)
{
  UseObject();
  ON_CALL(objectAsCollisionParter, GetIsCollidable()).WillByDefault(Return(true));

  ON_CALL(objectAsCollisionParter, GetPositionZ()).WillByDefault(Return(0.0_m));
  ON_CALL(objectAsCollisionParter, GetHeight()).WillByDefault(Return(1.0_m));
  ON_CALL(agentUnderTest, GetHeight()).WillByDefault(Return(5.0_m));

  polygon_t ownBoundingBox;
  ownBoundingBox.outer().push_back(point_t{1, 1});
  ownBoundingBox.outer().push_back(point_t{1, 3});
  ownBoundingBox.outer().push_back(point_t{3, 3});
  ownBoundingBox.outer().push_back(point_t{3, 1});
  ON_CALL(agentUnderTest, GetBoundingBox2D()).WillByDefault(ReturnRef(ownBoundingBox));

  polygon_t otherBoundingBox;
  otherBoundingBox.outer().push_back(point_t{4, 4});
  otherBoundingBox.outer().push_back(point_t{4, 6});
  otherBoundingBox.outer().push_back(point_t{6, 6});
  otherBoundingBox.outer().push_back(point_t{6, 4});
  ON_CALL(objectAsCollisionParter, GetBoundingBox2D()).WillByDefault(ReturnRef(otherBoundingBox));

  CollisionDetector collisionDetector(&mockWorld, &mockPublisher);
  collisionDetector.Initialize(&mockRunResult);
  EXPECT_CALL(mockRunResult, AddCollisionId(_)).Times(0);
  collisionDetector.Trigger(0);
}

TEST_F(CollisionDetectorTest, TestNotCollideBecauseNotOverlapping2DHitbox_AgentWithAgent)
{
  UseAgent();

  ON_CALL(agentUnderTest, GetHeight()).WillByDefault(Return(5.0_m));

  polygon_t ownBoundingBox;
  ownBoundingBox.outer().push_back(point_t{1, 1});
  ownBoundingBox.outer().push_back(point_t{1, 3});
  ownBoundingBox.outer().push_back(point_t{3, 3});
  ownBoundingBox.outer().push_back(point_t{3, 1});
  ON_CALL(agentUnderTest, GetBoundingBox2D()).WillByDefault(ReturnRef(ownBoundingBox));

  polygon_t otherBoundingBox;
  otherBoundingBox.outer().push_back(point_t{4, 4});
  otherBoundingBox.outer().push_back(point_t{4, 6});
  otherBoundingBox.outer().push_back(point_t{6, 6});
  otherBoundingBox.outer().push_back(point_t{6, 4});
  ON_CALL(agentAsCollisionPartner, GetBoundingBox2D()).WillByDefault(ReturnRef(otherBoundingBox));

  CollisionDetector collisionDetector(&mockWorld, &mockPublisher);
  collisionDetector.Initialize(&mockRunResult);
  EXPECT_CALL(mockRunResult, AddCollisionId(_)).Times(0);
  collisionDetector.Trigger(0);
}

TEST_F(CollisionDetectorTest, TestCollisionStandardCaseAgentcollidesNotWithOtherAgent)
{
  UseAgent();

  ON_CALL(agentUnderTest, GetHeight()).WillByDefault(Return(5.0_m));

  polygon_t ownBoundingBox;
  ownBoundingBox.outer().push_back(point_t{1, 1});
  ownBoundingBox.outer().push_back(point_t{1, 3});
  ownBoundingBox.outer().push_back(point_t{3, 3});
  ownBoundingBox.outer().push_back(point_t{3, 1});
  ON_CALL(agentUnderTest, GetBoundingBox2D()).WillByDefault(ReturnRef(ownBoundingBox));

  polygon_t otherBoundingBox;
  otherBoundingBox.outer().push_back(point_t{4, 4});
  otherBoundingBox.outer().push_back(point_t{4, 5});
  otherBoundingBox.outer().push_back(point_t{5, 5});
  otherBoundingBox.outer().push_back(point_t{5, 4});
  ON_CALL(agentAsCollisionPartner, GetBoundingBox2D()).WillByDefault(ReturnRef(otherBoundingBox));

  CollisionDetector collisionDetector(&mockWorld, &mockPublisher);
  collisionDetector.Initialize(&mockRunResult);
  EXPECT_CALL(mockRunResult, AddCollisionId(_)).Times(0);
  collisionDetector.Trigger(0);
}

TEST_F(CollisionDetectorTest, TestCollisionBorderCaseCollisionInASinglePoint)
{
  UseObject();
  ON_CALL(objectAsCollisionParter, GetIsCollidable()).WillByDefault(Return(true));

  ON_CALL(objectAsCollisionParter, GetPositionZ()).WillByDefault(Return(1.0_m));
  ON_CALL(objectAsCollisionParter, GetHeight()).WillByDefault(Return(1.0_m));
  ON_CALL(agentUnderTest, GetHeight()).WillByDefault(Return(1.0_m));

  polygon_t ownBoundingBox;
  ownBoundingBox.outer().push_back(point_t{1, 1});
  ownBoundingBox.outer().push_back(point_t{1, 3});
  ownBoundingBox.outer().push_back(point_t{3, 3});
  ownBoundingBox.outer().push_back(point_t{3, 1});
  ON_CALL(agentUnderTest, GetBoundingBox2D()).WillByDefault(ReturnRef(ownBoundingBox));

  polygon_t otherBoundingBox;
  otherBoundingBox.outer().push_back(point_t{3, 3});
  otherBoundingBox.outer().push_back(point_t{3, 5});
  otherBoundingBox.outer().push_back(point_t{5, 5});
  otherBoundingBox.outer().push_back(point_t{5, 3});
  ON_CALL(objectAsCollisionParter, GetBoundingBox2D()).WillByDefault(ReturnRef(otherBoundingBox));

  CollisionDetector collisionDetector(&mockWorld, &mockPublisher);
  collisionDetector.Initialize(&mockRunResult);
  EXPECT_CALL(mockRunResult, AddCollisionId(_)).Times(0);
  collisionDetector.Trigger(0);
}

TEST_F(CollisionDetectorTest, TestCollisionBorderCaseCollisionOnSingleEdge)
{
  UseObject();
  ON_CALL(objectAsCollisionParter, GetIsCollidable()).WillByDefault(Return(true));

  ON_CALL(objectAsCollisionParter, GetPositionZ()).WillByDefault(Return(0.0_m));
  ON_CALL(objectAsCollisionParter, GetHeight()).WillByDefault(Return(1.0_m));
  ON_CALL(agentUnderTest, GetHeight()).WillByDefault(Return(5.0_m));

  polygon_t ownBoundingBox;
  ownBoundingBox.outer().push_back(point_t{1, 1});
  ownBoundingBox.outer().push_back(point_t{1, 3});
  ownBoundingBox.outer().push_back(point_t{3, 3});
  ownBoundingBox.outer().push_back(point_t{3, 1});
  ON_CALL(agentUnderTest, GetBoundingBox2D()).WillByDefault(ReturnRef(ownBoundingBox));

  polygon_t otherBoundingBox;
  otherBoundingBox.outer().push_back(point_t{3, 3});
  otherBoundingBox.outer().push_back(point_t{3, 5});
  otherBoundingBox.outer().push_back(point_t{5, 5});
  otherBoundingBox.outer().push_back(point_t{5, 3});
  ON_CALL(objectAsCollisionParter, GetBoundingBox2D()).WillByDefault(ReturnRef(otherBoundingBox));

  CollisionDetector collisionDetector(&mockWorld, &mockPublisher);
  collisionDetector.Initialize(&mockRunResult);
  EXPECT_CALL(mockRunResult, AddCollisionId(_)).Times(0);
  collisionDetector.Trigger(0);
}

TEST_F(CollisionDetectorTest, TestCollisionBorderCaseCollisionAreaWithNoCollisionVolume)
{
  UseObject();
  ON_CALL(objectAsCollisionParter, GetIsCollidable()).WillByDefault(Return(true));

  ON_CALL(objectAsCollisionParter, GetPositionZ()).WillByDefault(Return(0.0_m));
  ON_CALL(objectAsCollisionParter, GetHeight()).WillByDefault(Return(1.0_m));
  ON_CALL(agentUnderTest, GetHeight()).WillByDefault(Return(5.0_m));

  polygon_t ownBoundingBox;
  ownBoundingBox.outer().push_back(point_t{1, 1});
  ownBoundingBox.outer().push_back(point_t{1, 3});
  ownBoundingBox.outer().push_back(point_t{3, 3});
  ownBoundingBox.outer().push_back(point_t{3, 1});
  ON_CALL(agentUnderTest, GetBoundingBox2D()).WillByDefault(ReturnRef(ownBoundingBox));

  polygon_t otherBoundingBox;
  otherBoundingBox.outer().push_back(point_t{3, 1});
  otherBoundingBox.outer().push_back(point_t{3, 3});
  otherBoundingBox.outer().push_back(point_t{5, 3});
  otherBoundingBox.outer().push_back(point_t{5, 1});
  ON_CALL(objectAsCollisionParter, GetBoundingBox2D()).WillByDefault(ReturnRef(otherBoundingBox));

  CollisionDetector collisionDetector(&mockWorld, &mockPublisher);
  collisionDetector.Initialize(&mockRunResult);
  EXPECT_CALL(mockRunResult, AddCollisionId(_)).Times(0);
  collisionDetector.Trigger(0);
}

TEST_F(CollisionDetectorTest, TestCollisionPartners)
{
  UseObject();
  ON_CALL(objectAsCollisionParter, GetIsCollidable()).WillByDefault(Return(true));

  ON_CALL(objectAsCollisionParter, GetPositionZ()).WillByDefault(Return(0.0_m));
  ON_CALL(objectAsCollisionParter, GetHeight()).WillByDefault(Return(1.0_m));
  ON_CALL(agentUnderTest, GetHeight()).WillByDefault(Return(5.0_m));

  std::vector<std::pair<ObjectTypeOSI, int>> collisionPartners;
  collisionPartners.push_back(std::make_pair(objectAsCollisionParter.GetType(), objectAsCollisionParter.GetId()));
  ON_CALL(agentUnderTest, GetCollisionPartners()).WillByDefault(Return(collisionPartners));

  polygon_t ownBoundingBox;
  ownBoundingBox.outer().push_back(point_t{1, 1});
  ownBoundingBox.outer().push_back(point_t{1, 3});
  ownBoundingBox.outer().push_back(point_t{3, 3});
  ownBoundingBox.outer().push_back(point_t{3, 1});
  ON_CALL(agentUnderTest, GetBoundingBox2D()).WillByDefault(ReturnRef(ownBoundingBox));

  polygon_t otherBoundingBox;
  otherBoundingBox.outer().push_back(point_t{2, 2});
  otherBoundingBox.outer().push_back(point_t{2, 4});
  otherBoundingBox.outer().push_back(point_t{4, 4});
  otherBoundingBox.outer().push_back(point_t{4, 2});
  ON_CALL(objectAsCollisionParter, GetBoundingBox2D()).WillByDefault(ReturnRef(otherBoundingBox));

  CollisionDetector collisionDetector(&mockWorld, &mockPublisher);
  collisionDetector.Initialize(&mockRunResult);
  EXPECT_CALL(mockRunResult, AddCollisionId(_)).Times(0);
  collisionDetector.Trigger(0);
}

TEST_F(CollisionDetectorTest, TestAgentWithZeroNoHeight)
{
  UseObject();
  ON_CALL(objectAsCollisionParter, GetIsCollidable()).WillByDefault(Return(true));

  ON_CALL(objectAsCollisionParter, GetPositionZ()).WillByDefault(Return(0.0_m));
  ON_CALL(objectAsCollisionParter, GetHeight()).WillByDefault(Return(0.0_m));
  ON_CALL(agentUnderTest, GetHeight()).WillByDefault(Return(0.0_m));

  polygon_t ownBoundingBox;
  ownBoundingBox.outer().push_back(point_t{1, 1});
  ownBoundingBox.outer().push_back(point_t{1, 3});
  ownBoundingBox.outer().push_back(point_t{3, 3});
  ownBoundingBox.outer().push_back(point_t{3, 1});
  ON_CALL(agentUnderTest, GetBoundingBox2D()).WillByDefault(ReturnRef(ownBoundingBox));

  polygon_t otherBoundingBox;
  otherBoundingBox.outer().push_back(point_t{2, 2});
  otherBoundingBox.outer().push_back(point_t{2, 4});
  otherBoundingBox.outer().push_back(point_t{4, 4});
  otherBoundingBox.outer().push_back(point_t{4, 2});
  ON_CALL(objectAsCollisionParter, GetBoundingBox2D()).WillByDefault(ReturnRef(otherBoundingBox));

  CollisionDetector collisionDetector(&mockWorld, &mockPublisher);
  collisionDetector.Initialize(&mockRunResult);
  EXPECT_CALL(mockRunResult, AddCollisionId(_)).Times(1);
  collisionDetector.Trigger(0);
}

TEST_F(CollisionDetectorTest, TestCollisionCaseBrokenHitboxes)
{
  //Test for that this hitbox will not cause a runtime exception

  UseObject();
  ON_CALL(objectAsCollisionParter, GetIsCollidable()).WillByDefault(Return(true));

  ON_CALL(objectAsCollisionParter, GetPositionZ()).WillByDefault(Return(4.0_m));
  ON_CALL(objectAsCollisionParter, GetHeight()).WillByDefault(Return(2.0_m));
  ON_CALL(agentUnderTest, GetHeight()).WillByDefault(Return(5.0_m));

  //PolygonDefinitionWrongOrder
  polygon_t ownBoundingBox;
  ownBoundingBox.outer().push_back(point_t{1, 1});
  ownBoundingBox.outer().push_back(point_t{1, 3});
  ownBoundingBox.outer().push_back(point_t{3, 1});
  ownBoundingBox.outer().push_back(point_t{3, 3});
  ON_CALL(agentUnderTest, GetBoundingBox2D()).WillByDefault(ReturnRef(ownBoundingBox));

  polygon_t otherBoundingBox;
  otherBoundingBox.outer().push_back(point_t{2, 2});
  otherBoundingBox.outer().push_back(point_t{2, 4});
  otherBoundingBox.outer().push_back(point_t{4, 2});
  otherBoundingBox.outer().push_back(point_t{4, 4});
  ON_CALL(objectAsCollisionParter, GetBoundingBox2D()).WillByDefault(ReturnRef(otherBoundingBox));

  CollisionDetector collisionDetector(&mockWorld, &mockPublisher);
  collisionDetector.Initialize(&mockRunResult);
  EXPECT_CALL(mockRunResult, AddCollisionId(_)).Times(0);
  EXPECT_NO_THROW(collisionDetector.Trigger(0));
}

TEST_F(CollisionDetectorTest, TestCollisionCaseBrokenHitboxes2)
{
  //Test for that this hitbox will not cause a runtime exception

  UseObject();
  ON_CALL(objectAsCollisionParter, GetIsCollidable()).WillByDefault(Return(true));

  ON_CALL(objectAsCollisionParter, GetPositionZ()).WillByDefault(Return(4.0_m));
  ON_CALL(objectAsCollisionParter, GetHeight()).WillByDefault(Return(2.0_m));
  ON_CALL(agentUnderTest, GetHeight()).WillByDefault(Return(5.0_m));

  polygon_t ownBoundingBox;
  ownBoundingBox.outer().push_back(point_t{1, 1});
  ownBoundingBox.outer().push_back(point_t{1, 1});
  ownBoundingBox.outer().push_back(point_t{1, 1});
  ownBoundingBox.outer().push_back(point_t{1, 1});
  ON_CALL(agentUnderTest, GetBoundingBox2D()).WillByDefault(ReturnRef(ownBoundingBox));

  polygon_t otherBoundingBox;
  ownBoundingBox.outer().push_back(point_t{0, 0});
  ownBoundingBox.outer().push_back(point_t{0, 5});
  ownBoundingBox.outer().push_back(point_t{5, 5});
  ownBoundingBox.outer().push_back(point_t{5, 0});
  ON_CALL(objectAsCollisionParter, GetBoundingBox2D()).WillByDefault(ReturnRef(otherBoundingBox));

  CollisionDetector collisionDetector(&mockWorld, &mockPublisher);
  collisionDetector.Initialize(&mockRunResult);
  EXPECT_CALL(mockRunResult, AddCollisionId(_)).Times(0);
  EXPECT_NO_THROW(collisionDetector.Trigger(0));
}

TEST_F(CollisionDetectorTest, TestCollisionCaseBrokenHitboxes3)
{
  //Test for that this hitbox will not cause a runtime exception

  UseObject();
  ON_CALL(objectAsCollisionParter, GetIsCollidable()).WillByDefault(Return(true));

  ON_CALL(objectAsCollisionParter, GetPositionZ()).WillByDefault(Return(4.0_m));
  ON_CALL(objectAsCollisionParter, GetHeight()).WillByDefault(Return(2.0_m));
  ON_CALL(agentUnderTest, GetHeight()).WillByDefault(Return(5.0_m));

  polygon_t ownBoundingBox;
  ownBoundingBox.outer().push_back(point_t{0, 3});
  ownBoundingBox.outer().push_back(point_t{0, -3});
  ownBoundingBox.outer().push_back(point_t{0, 3});
  ownBoundingBox.outer().push_back(point_t{0, -3});
  ON_CALL(agentUnderTest, GetBoundingBox2D()).WillByDefault(ReturnRef(ownBoundingBox));

  polygon_t otherBoundingBox;
  otherBoundingBox.outer().push_back(point_t{3, 0});
  otherBoundingBox.outer().push_back(point_t{-3, 0});
  otherBoundingBox.outer().push_back(point_t{3, 0});
  otherBoundingBox.outer().push_back(point_t{-3, 0});
  ON_CALL(objectAsCollisionParter, GetBoundingBox2D()).WillByDefault(ReturnRef(otherBoundingBox));

  CollisionDetector collisionDetector(&mockWorld, &mockPublisher);
  collisionDetector.Initialize(&mockRunResult);
  EXPECT_CALL(mockRunResult, AddCollisionId(_)).Times(1);
  EXPECT_NO_THROW(collisionDetector.Trigger(0));
}
