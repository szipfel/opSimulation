/********************************************************************************
 * Copyright (c) 2019-2021 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "AgentAdapter.h"
#include "Generators/laneGeometryElementGenerator.h"
#include "fakeAgent.h"
#include "fakeLane.h"
#include "fakeLaneManager.h"
#include "fakeMovingObject.h"
#include "fakeSection.h"
#include "fakeWorld.h"
#include "fakeWorldData.h"

using namespace OWL;

using ::testing::_;
using ::testing::IsNull;
using ::testing::Return;
using ::testing::ReturnRef;

struct Entity
{
  AgentInterface& agent;
  Fakes::MovingObject& movingObject;
};

class AgentManager
{
  std::vector<AgentAdapter*> agents;
  const std::unordered_map<OWL::Id, OWL::Interfaces::Road*> roads;

public:
  FakeWorld fakeWorld;
  Fakes::WorldData fakeWorldData;
  std::vector<Fakes::MovingObject*> fakeMovingObjects;

  AgentManager()
  {
    ON_CALL(fakeWorld, GetWorldData()).WillByDefault(Return(static_cast<void*>(&fakeWorldData)));
    ON_CALL(fakeWorldData, GetRoads()).WillByDefault(ReturnRef(roads));
  }

  Entity Create(double distanceToRoadStart, double distanceToRoadEnd)
  {
    // do not change order, as agent adapter needs the faked world data

    auto fakeMovingObject = new Fakes::MovingObject();
    ON_CALL(fakeWorldData, AddMovingObject(_, _)).WillByDefault(ReturnRef(*fakeMovingObject));
    World::Localization::Localizer localizer{fakeWorldData};

    auto agent = new AgentAdapter(&fakeWorld, nullptr, &fakeWorldData, localizer);
    fakeMovingObject->SetLinkedObjectForTesting(static_cast<void*>(agent));

    ON_CALL(*fakeMovingObject, GetDistance(OWL::MeasurementPoint::RoadStart, _))
        .WillByDefault(Return(distanceToRoadStart));
    ON_CALL(*fakeMovingObject, GetDistance(OWL::MeasurementPoint::RoadEnd, _)).WillByDefault(Return(distanceToRoadEnd));

    agents.push_back(agent);
    fakeMovingObjects.push_back(fakeMovingObject);

    return {*agent, *fakeMovingObject};
  }

  ~AgentManager()
  {
    for (auto agent : agents)
    {
      delete agent;
    }
    for (auto fakeMovingObject : fakeMovingObjects)
    {
      delete fakeMovingObject;
    }
  }
};
