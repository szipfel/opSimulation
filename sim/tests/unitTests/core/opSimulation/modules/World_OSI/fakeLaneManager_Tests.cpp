/********************************************************************************
 * Copyright (c) 2018-2021 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "fakeLaneManager.h"

using namespace OWL;

using ::testing::ElementsAre;
using ::testing::Eq;
using ::testing::IsEmpty;
using ::testing::Ne;
using ::testing::Ref;
using ::testing::SizeIs;

TEST(FakeLaneManager, SingleLaneTest)
{
  FakeLaneManager flm(1, 1, 3.0_m, {1000_m}, "TestRoadId");

  EXPECT_THAT(flm.lanes[0][0]->Exists(), Eq(true));
  EXPECT_THAT(flm.lanes[0][0]->GetWidth(0_m).value(), Eq(3.0));
  EXPECT_THAT(flm.lanes[0][0]->GetLeftLane(), Ref(flm.invalidLane));
  EXPECT_THAT(flm.lanes[0][0]->GetRightLane(), Ref(flm.invalidLane));
  EXPECT_THAT(flm.lanes[0][0]->GetPrevious(), IsEmpty());
  EXPECT_THAT(flm.lanes[0][0]->GetNext(), IsEmpty());
}

TEST(FakeLaneManager, GetLaneTest)
{
  FakeLaneManager flm(1, 1, 3.0_m, {1000_m}, "TestRoadId");

  auto& lane = flm.GetLane(0, 0);
  EXPECT_THAT(lane.Exists(), Eq(true));
  EXPECT_THAT(lane.GetWidth(0_m).value(), Eq(3.0));
  EXPECT_THAT(lane.GetLeftLane(), Ref(flm.invalidLane));
  EXPECT_THAT(lane.GetRightLane(), Ref(flm.invalidLane));
  EXPECT_THAT(flm.lanes[0][0]->GetPrevious(), IsEmpty());
  EXPECT_THAT(flm.lanes[0][0]->GetNext(), IsEmpty());
}

TEST(FakeLaneManager, SingleColumnTest)
{
  FakeLaneManager flm(1, 2, 3.0_m, {1000_m}, "TestRoadId");

  EXPECT_THAT(flm.lanes[0][0]->Exists(), Eq(true));
  EXPECT_THAT(flm.lanes[0][0]->GetWidth(0_m).value(), Eq(3.0));
  EXPECT_THAT(flm.lanes[0][0]->GetLeftLane(), Ref(flm.invalidLane));
  EXPECT_THAT(&flm.lanes[0][0]->GetRightLane(), Eq(flm.lanes[0][1]));
  EXPECT_THAT(flm.lanes[0][0]->GetPrevious(), IsEmpty());
  EXPECT_THAT(flm.lanes[0][0]->GetNext(), IsEmpty());

  EXPECT_THAT(flm.lanes[0][1]->Exists(), Eq(true));
  EXPECT_THAT(flm.lanes[0][1]->GetWidth(0_m).value(), Eq(3.0));
  EXPECT_THAT(&flm.lanes[0][1]->GetLeftLane(), Eq(flm.lanes[0][0]));
  EXPECT_THAT(flm.lanes[0][1]->GetRightLane(), Ref(flm.invalidLane));
  EXPECT_THAT(flm.lanes[0][0]->GetPrevious(), IsEmpty());
  EXPECT_THAT(flm.lanes[0][0]->GetNext(), IsEmpty());
}

TEST(FakeLaneManager, SingleRowTest)
{
  FakeLaneManager flm(2, 1, 3.0_m, {1000_m, 1000_m}, "TestRoadId");

  EXPECT_THAT(flm.lanes[0][0]->Exists(), Eq(true));
  EXPECT_THAT(flm.lanes[0][0]->GetWidth(0_m).value(), Eq(3.0));
  EXPECT_THAT(flm.lanes[0][0]->GetLeftLane(), Ref(flm.invalidLane));
  EXPECT_THAT(flm.lanes[0][0]->GetRightLane(), Ref(flm.invalidLane));
  EXPECT_THAT(flm.lanes[0][0]->GetPrevious(), IsEmpty());
  EXPECT_THAT(flm.lanes[0][0]->GetNext(), ElementsAre(flm.lanes[1][0]->GetId()));

  EXPECT_THAT(flm.lanes[1][0]->Exists(), Eq(true));
  EXPECT_THAT(flm.lanes[1][0]->GetWidth(0_m).value(), Eq(3.0));
  EXPECT_THAT(flm.lanes[1][0]->GetLeftLane(), Ref(flm.invalidLane));
  EXPECT_THAT(flm.lanes[1][0]->GetRightLane(), Ref(flm.invalidLane));
  EXPECT_THAT(flm.lanes[1][0]->GetNext(), IsEmpty());
  EXPECT_THAT(flm.lanes[1][0]->GetPrevious(), ElementsAre(flm.lanes[0][0]->GetId()));
}

TEST(FakeLaneManager, MatrixTest)
{
  FakeLaneManager flm(2, 3, 3.0_m, {1000_m, 1000_m}, "TestRoadId");

  EXPECT_THAT(flm.lanes[0][0]->Exists(), Eq(true));
  EXPECT_THAT(flm.lanes[0][0]->GetWidth(0_m).value(), Eq(3.0));
  EXPECT_THAT(flm.lanes[0][0]->GetLeftLane(), Ref(flm.invalidLane));
  EXPECT_THAT(&flm.lanes[0][0]->GetRightLane(), Eq(flm.lanes[0][1]));
  EXPECT_THAT(flm.lanes[0][0]->GetPrevious(), IsEmpty());
  EXPECT_THAT(flm.lanes[0][0]->GetNext(), ElementsAre(flm.lanes[1][0]->GetId()));

  EXPECT_THAT(flm.lanes[1][2]->Exists(), Eq(true));
  EXPECT_THAT(flm.lanes[1][2]->GetWidth(0_m).value(), Eq(3.0));
  EXPECT_THAT(&flm.lanes[1][2]->GetLeftLane(), Eq(flm.lanes[1][1]));
  EXPECT_THAT(flm.lanes[1][2]->GetRightLane(), Ref(flm.invalidLane));
  EXPECT_THAT(flm.lanes[1][2]->GetNext(), IsEmpty());
  EXPECT_THAT(flm.lanes[1][2]->GetPrevious(), ElementsAre(flm.lanes[0][2]->GetId()));
}

TEST(FakeLaneManager, SectionConnectionTest)
{
  FakeLaneManager flm(2, 2, 3.0_m, {1000_m, 1000_m}, "TestRoadId");

  EXPECT_THAT(&flm.lanes[0][0]->GetSection(), &flm.lanes[0][1]->GetSection());
  EXPECT_THAT(&flm.lanes[1][0]->GetSection(), &flm.lanes[1][1]->GetSection());
  EXPECT_THAT(&flm.lanes[0][0]->GetSection(), Ne(&flm.lanes[1][1]->GetSection()));

  // test const access
  const OWL::Lane* nextLane = flm.lanes[0][0];
  EXPECT_THAT(&nextLane->GetSection(), &flm.lanes[0][0]->GetSection());
}

TEST(FakeLaneManager, ApplyMovingObjectTest)
{
  FakeLaneManager flm(2, 2, 3.0_m, {1000_m, 1000_m}, "TestRoadId");
  Fakes::MovingObject fmo00;
  Fakes::MovingObject fmo01;
  Fakes::MovingObject fmo11;

  OWL::LaneOverlap overlap1{GlobalRoadPosition{"", 0, 1_m, 0_m, 0_rad},
                            GlobalRoadPosition{"", 0, 1_m, 0_m, 0_rad},
                            GlobalRoadPosition{"", 0, 1_m, 0_m, 0_rad},
                            GlobalRoadPosition{"", 0, 1_m, 0_m, 0_rad}};
  OWL::LaneOverlap overlap2{GlobalRoadPosition{"", 0, 2_m, 0_m, 0_rad},
                            GlobalRoadPosition{"", 0, 2_m, 0_m, 0_rad},
                            GlobalRoadPosition{"", 0, 2_m, 0_m, 0_rad},
                            GlobalRoadPosition{"", 0, 2_m, 0_m, 0_rad}};
  OWL::LaneOverlap overlap3{GlobalRoadPosition{"", 0, 3_m, 0_m, 0_rad},
                            GlobalRoadPosition{"", 0, 3_m, 0_m, 0_rad},
                            GlobalRoadPosition{"", 0, 3_m, 0_m, 0_rad},
                            GlobalRoadPosition{"", 0, 3_m, 0_m, 0_rad}};
  flm.AddWorldObject(0, 0, fmo00, overlap1);
  flm.AddWorldObject(0, 0, fmo01, overlap2);
  flm.AddWorldObject(1, 1, fmo11, overlap3);

  ASSERT_THAT(flm.lanes[0][0]->GetWorldObjects(true), SizeIs(2));
  ASSERT_THAT(flm.lanes[0][1]->GetWorldObjects(true), SizeIs(0));
  ASSERT_THAT(flm.lanes[1][0]->GetWorldObjects(true), SizeIs(0));
  ASSERT_THAT(flm.lanes[1][1]->GetWorldObjects(true), SizeIs(1));

  ASSERT_THAT(flm.lanes[0][0]->GetWorldObjects(false), SizeIs(2));
  ASSERT_THAT(flm.lanes[0][1]->GetWorldObjects(false), SizeIs(0));
  ASSERT_THAT(flm.lanes[1][0]->GetWorldObjects(false), SizeIs(0));
  ASSERT_THAT(flm.lanes[1][1]->GetWorldObjects(false), SizeIs(1));
}

TEST(FakeLaneManager, SetWidth)
{
  FakeLaneManager flm(2, 2, 3.0_m, {1000_m, 1000_m}, "TestRoadId");

  flm.SetWidth(0, 0, 1.0_m, 10.0_m);
  flm.SetWidth(0, 1, 2.0_m, 20.0_m);
  flm.SetWidth(1, 0, 3.0_m, 30.0_m);
  flm.SetWidth(1, 1, 4.0_m, 40.0_m);

  // default values (set by constructor)
  EXPECT_THAT(flm.lanes[0][0]->GetWidth(0.0_m).value(), Eq(3.0));
  EXPECT_THAT(flm.lanes[0][1]->GetWidth(0.0_m).value(), Eq(3.0));
  EXPECT_THAT(flm.lanes[1][0]->GetWidth(0.0_m).value(), Eq(3.0));
  EXPECT_THAT(flm.lanes[1][1]->GetWidth(0.0_m).value(), Eq(3.0));

  // special values at distance x
  EXPECT_THAT(flm.lanes[0][0]->GetWidth(10.0_m).value(), Eq(1.0));
  EXPECT_THAT(flm.lanes[0][1]->GetWidth(20.0_m).value(), Eq(2.0));
  EXPECT_THAT(flm.lanes[1][0]->GetWidth(30.0_m).value(), Eq(3.0));
  EXPECT_THAT(flm.lanes[1][1]->GetWidth(40.0_m).value(), Eq(4.0));
}

TEST(FakeLaneManager, SetLength)
{
  FakeLaneManager flm(1, 2, 3.0_m, {1000_m, 1000_m}, "TestRoadId");

  flm.SetLength(0, 0, 1.0_m);
  EXPECT_THAT(flm.lanes[0][0]->GetLength().value(), Eq(1.0));
  EXPECT_THAT(flm.lanes[0][0]->GetDistance(OWL::MeasurementPoint::RoadStart).value(), Eq(0.0));
  EXPECT_THAT(flm.lanes[0][0]->GetDistance(OWL::MeasurementPoint::RoadEnd).value(), Eq(1.0));

  flm.SetLength(0, 1, 2.0_m, 20.0_m);

  EXPECT_THAT(flm.lanes[0][1]->GetLength().value(), Eq(2.0));
  EXPECT_THAT(flm.lanes[0][1]->GetDistance(OWL::MeasurementPoint::RoadStart).value(), Eq(20.0));
  EXPECT_THAT(flm.lanes[0][1]->GetDistance(OWL::MeasurementPoint::RoadEnd).value(), Eq(22.0));
}

////////////////////////////////////////////////////////////////////////////
TEST(FakeLaneManager, SectionCoversValidPosition_ReturnsTrue)
{
  FakeLaneManager flm(1, 1, 3.0_m, {200_m}, "TestRoadId");

  const auto& sections = flm.GetSections();
  const std::vector<units::length::meter_t> distances = {0.0_m, 50.0_m, 200.0_m};

  ASSERT_EQ(sections.size(), 1);
  const auto& section = sections.begin()->second;

  for (auto& distance : distances)
  {
    EXPECT_THAT(section->Covers(distance), Eq(true));
  }
}

TEST(FakeLaneManager, SectionCoversInValidPosition_ReturnsFalse)
{
  FakeLaneManager flm(1, 1, 3.0_m, {200_m}, "TestRoadId");

  const auto& sections = flm.GetSections();
  const std::vector<units::length::meter_t> distances = {-1.0_m, 201.0_m};

  ASSERT_EQ(sections.size(), 1);
  const auto& section = sections.begin()->second;

  for (auto& distance : distances)
  {
    EXPECT_THAT(section->Covers(distance), Eq(false));
  }
}

TEST(FakeLaneManager, SectionCoversIntervalValidPositions_ReturnsTrue)
{
  FakeLaneManager flm(1, 1, 3.0_m, {200_m}, "TestRoadId");

  const auto& sections = flm.GetSections();
  std::vector<units::length::meter_t> startDistances = {0.0_m, 30.0_m, 190.0_m};
  std::vector<units::length::meter_t> endDistances = {10.0_m, 40.0_m, 200.0_m};

  ASSERT_EQ(sections.size(), 1);
  const auto& section = sections.begin()->second;

  for (unsigned cnt = 0; cnt < 3; cnt++)
  {
    bool result = section->CoversInterval(startDistances[cnt], endDistances[cnt]);
    EXPECT_THAT(result, Eq(true));
  }
}

TEST(FakeLaneManager, SectionCoversIntervalOneDistanceOutsideSectionRange_ReturnsTrue)
{
  FakeLaneManager flm(1, 1, 3.0_m, {200_m}, "TestRoadId");

  const auto& sections = flm.GetSections();
  std::vector<units::length::meter_t> startDistances = {-10.0_m, 190.0_m};
  std::vector<units::length::meter_t> endDistances = {10.0_m, 210.0_m};

  ASSERT_EQ(sections.size(), 1);
  const auto& section = sections.begin()->second;

  for (unsigned cnt = 0; cnt < 2; cnt++)
  {
    EXPECT_THAT(section->CoversInterval(startDistances[cnt], endDistances[cnt]), Eq(true));
  }
}

TEST(FakeLaneManager, SectionCoversIntervalBothDistancesOutsideSectionRange_ReturnsFalse)
{
  FakeLaneManager flm(1, 1, 3.0_m, {200_m}, "TestRoadId");

  const auto& sections = flm.GetSections();
  std::vector<units::length::meter_t> startDistances = {-50.0_m, 210.0_m};
  std::vector<units::length::meter_t> endDistances = {-10.0_m, 300.0_m};

  ASSERT_EQ(sections.size(), 1);
  const auto& section = sections.begin()->second;

  for (unsigned cnt = 0; cnt < 2; cnt++)
  {
    EXPECT_THAT(section->CoversInterval(startDistances[cnt], endDistances[cnt]), Eq(false));
  }
}
