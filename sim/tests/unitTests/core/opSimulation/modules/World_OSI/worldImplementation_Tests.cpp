/********************************************************************************
 * Copyright (c) 2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "DataTypes.h"
#include "OWL/fakes/fakeTrafficLight.h"
#include "OWL/fakes/fakeWorldData.h"
#include "WorldImplementation.h"
#include "fakeDataBuffer.h"
#include "openPassTypes.h"

using ::testing::_;
using ::testing::NiceMock;
using ::testing::Return;
using ::testing::ReturnRef;

using namespace OWL::Interfaces;

struct TrafficLightTestFixture
{
  static constexpr OWL::Id EXPECTED_ID_RED = 1;
  static constexpr OWL::Id EXPECTED_ID_YELLOW = 2;
  static constexpr OWL::Id EXPECTED_ID_OFF = 3;
  static constexpr OWL::Id EXPECTED_ID_NOT_AVAILABLE = 0xDEADBEEF;

  NiceMock<OWL::Fakes::WorldData> fakeWorldData;
  NiceMock<OWL::Fakes::TrafficLight> fakeTrafficLight;
  NiceMock<FakeDataBuffer> fakeDataBuffer;

  WorldImplementation world{nullptr, nullptr, &fakeDataBuffer, fakeWorldData};
  std::unordered_map<std::string, OWL::Id> ids{{"TrafficLightRed", EXPECTED_ID_RED},
                                               {"TrafficLightYellow", EXPECTED_ID_YELLOW},
                                               {"TrafficLightOff", EXPECTED_ID_OFF}};

  TrafficLightTestFixture(OWL::Id expected_id)
  {
    ON_CALL(fakeWorldData, GetTrafficSignIdMapping()).WillByDefault(ReturnRef(ids));
    ON_CALL(fakeWorldData, GetSignalType(expected_id)).WillByDefault(Return(OWL::SignalType::TrafficLight));
    ON_CALL(fakeWorldData, GetTrafficLight(expected_id)).WillByDefault(ReturnRef(fakeTrafficLight));
  }
};

TEST(SetTrafficSignalStateTest, IfTrafficSignalStateSet_ChangeIsLogged)
{
  TrafficLightTestFixture fixture(TrafficLightTestFixture::EXPECTED_ID_OFF);
  EXPECT_CALL(
      fixture.fakeDataBuffer,
      PutAcyclic(openpass::type::EntityId(TrafficLightTestFixture::EXPECTED_ID_OFF), "TrafficLightController", _));
  fixture.world.SetTrafficSignalState("TrafficLightOff", "off");
}

TEST(SetTrafficSignalStateTest, SetTrafficSignalStateRed)
{
  TrafficLightTestFixture fixture(TrafficLightTestFixture::EXPECTED_ID_RED);
  EXPECT_CALL(fixture.fakeTrafficLight, SetState(CommonTrafficLight::State::Red));
  fixture.world.SetTrafficSignalState("TrafficLightRed", "red");
}

TEST(SetTrafficSignalStateTest, SetTrafficSignalStateYellow)
{
  TrafficLightTestFixture fixture(TrafficLightTestFixture::EXPECTED_ID_YELLOW);
  EXPECT_CALL(fixture.fakeTrafficLight, SetState(CommonTrafficLight::State::Yellow));
  fixture.world.SetTrafficSignalState("TrafficLightYellow", "yellow");
}

TEST(SetTrafficSignalStateTest, SetTrafficSignalStateOff)
{
  TrafficLightTestFixture fixture(TrafficLightTestFixture::EXPECTED_ID_OFF);
  EXPECT_CALL(fixture.fakeTrafficLight, SetState(CommonTrafficLight::State::Off));
  fixture.world.SetTrafficSignalState("TrafficLightOff", "off");
}

TEST(SetTrafficSignalStateTest, SignalIsNotDefinedInScenery_WhenEvaluate_ThenThrowRuntimeError)
{
  TrafficLightTestFixture fixture(TrafficLightTestFixture::EXPECTED_ID_NOT_AVAILABLE);
  EXPECT_THROW(fixture.world.SetTrafficSignalState("TrafficLightGreen", "green"), std::runtime_error);
}

/// traffic signs follow the same semantics as traffic lights
/// but are not allowed to set a traffic signal state
/// the same fixture is used, but the state is artifically set to the wrong type
TEST(SetTrafficSignalStateTest, SignalIsNotTrafficLight_WhenEvaluate_ThenThrowRuntimeError)
{
  // arbitrary existing id
  TrafficLightTestFixture fixture(TrafficLightTestFixture::EXPECTED_ID_OFF);

  // override expectation with wrong signal type
  ON_CALL(fixture.fakeWorldData, GetSignalType(TrafficLightTestFixture::EXPECTED_ID_OFF))
      .WillByDefault(Return(OWL::SignalType::TrafficSign));

  EXPECT_THROW(fixture.world.SetTrafficSignalState("TrafficLightOff", "off"), std::runtime_error);
}