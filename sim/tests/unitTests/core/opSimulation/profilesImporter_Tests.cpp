/********************************************************************************
 * Copyright (c) 2019 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "common/helper/importerHelper.h"
#include "profilesImporter.h"

using ::testing::Eq;
using ::testing::SizeIs;
using ::testing::UnorderedElementsAre;

using namespace Importer;

TEST(ProfilesImporter_ImportAllVehicleComponentsOfSystemProfile, GivenValidXml_ImportsValues)
{
  QDomElement validXml = documentRootFromString(
      "<root>"
      "<Components>"
      "<Component Type=\"ComponentA\">"
      "<Profiles>"
      "<Profile Name=\"No warning\" Probability=\"0.5\"/>"
      "<Profile Name=\"Standard\" Probability=\"0.5\"/>"
      "</Profiles>"
      "<SensorLinks>"
      "<SensorLink SensorId=\"0\" InputId=\"Camera\"/>"
      "<SensorLink SensorId=\"1\" InputId=\"OtherSensor\"/>"
      "<SensorLink SensorId=\"2\" InputId=\"Camera\"/>"
      "</SensorLinks>"
      "</Component>"
      "</Components>"
      "</root>");

  SystemProfile profiles;

  EXPECT_NO_THROW(ProfilesImporter::ImportAllVehicleComponentsOfSystemProfile(validXml, profiles));

  ASSERT_EQ(profiles.vehicleComponents.size(), (size_t)1);

  VehicleComponent resultVehicleComponent = profiles.vehicleComponents.front();

  ASSERT_EQ(resultVehicleComponent.componentProfiles.size(), (size_t)2);
  ASSERT_EQ(resultVehicleComponent.sensorLinks.size(), (size_t)3);
}

TEST(ProfilesImporter_ImportAllVehicleComponentsOfSystemProfile, WithMissingComponentsTag_Throws)
{
  QDomElement missingComponentsTag = documentRootFromString(
      "<root>"
      "<Component Type=\"ComponentA\">"
      "<Profiles>"
      "<Profile Name=\"No warning\" Probability=\"0.5\"/>"
      "<Profile Name=\"Standard\" Probability=\"0.3\"/>"
      "</Profiles>"
      "<SensorLinks>"
      "<SensorLink SensorId=\"0\" InputId=\"Camera\"/>"
      "<SensorLink SensorId=\"1\" InputId=\"OtherSensor\"/>"
      "<SensorLink SensorId=\"2\" InputId=\"Camera\"/>"
      "</SensorLinks>"
      "</Component>"
      "</root>");

  SystemProfile profiles;
  EXPECT_THROW(ProfilesImporter::ImportAllVehicleComponentsOfSystemProfile(missingComponentsTag, profiles),
               std::runtime_error);
}

TEST(ProfilesImporter_ImportAllVehicleComponentsOfSystemProfile, SumOfProbabilityGreatherOne_Throws)
{
  QDomElement sumOfProbabilityGreatherOne = documentRootFromString(
      "<root>"
      "<Components>"
      "<Component Type=\"ComponentA\">"
      "<Profiles>"
      "<Profile Name=\"No warning\" Probability=\"0.5\"/>"
      "<Profile Name=\"Standard\" Probability=\"0.8\"/>"
      "</Profiles>"
      "<SensorLinks>"
      "<SensorLink SensorId=\"0\" InputId=\"Camera\"/>"
      "<SensorLink SensorId=\"1\" InputId=\"OtherSensor\"/>"
      "<SensorLink SensorId=\"2\" InputId=\"Camera\"/>"
      "</SensorLinks>"
      "</Component>"
      "</Components>"
      "</root>");

  SystemProfile profiles;
  EXPECT_THROW(ProfilesImporter::ImportAllVehicleComponentsOfSystemProfile(sumOfProbabilityGreatherOne, profiles),
               std::runtime_error);
}

TEST(ProfilesImporter_ImportAllSensorsOfSystemProfile, GivenValidXml_ImportsValues)
{
  QDomElement validXml = documentRootFromString(
      "<root>"
      "<Sensors>"
      "<Sensor Id=\"0\" Position=\"PositionA\">"
      "<Profile Type=\"Geometric2D\" Name=\"Camera\"/>"
      "</Sensor>"
      "<Sensor Id=\"1\" Position=\"PositionB\">"
      "<Profile Type=\"OtherSensor\" Name=\"Camera\"/>"
      "</Sensor>"
      "</Sensors>"
      "</root>");

  SystemProfile profiles;
  EXPECT_NO_THROW(ProfilesImporter::ImportAllSensorsOfSystemProfile(validXml, profiles));

  openpass::sensors::Parameters resultSensors = profiles.sensors;

  ASSERT_EQ(resultSensors.size(), (size_t)2);

  openpass::sensors::Parameter resultSensorParameter = resultSensors.front();
  openpass::sensors::Profile resultSensorProfile = resultSensorParameter.profile;

  ASSERT_EQ(resultSensorParameter.positionName, "PositionA");
  ASSERT_EQ(resultSensorProfile.type, "Geometric2D");
  ASSERT_EQ(resultSensorProfile.name, "Camera");
}

TEST(ProfilesImporter_ImportAllSensorsOfSystemProfile, SensorsTagMissing_Throws)
{
  QDomElement sensorsTagMissing = documentRootFromString(
      "<root>"
      "<Sensor Id=\"0\" Position=\"PositionA\">"
      "<Profile Type=\"Geometric2D\" Name=\"Camera\"/>"
      "</Sensor>"
      "<Sensor Id=\"1\" Position=\"PositionB\">"
      "<Profile Type=\"OtherSensor\" Name=\"Camera\"/>"
      "</Sensor>"
      "</root>");
  SystemProfile profiles;
  EXPECT_THROW(ProfilesImporter::ImportAllSensorsOfSystemProfile(sensorsTagMissing, profiles), std::runtime_error);
}

TEST(ProfilesImporter_ImportAllSensorsOfSystemProfile, PositionElementMissing_Throws)
{
  QDomElement positionTagMissing = documentRootFromString(
      "<root>"
      "<Sensors>"
      "<Sensor Id=\"0\">"
      "<Profile Type=\"Geometric2D\" Name=\"Camera\"/>"
      "</Sensor>"
      "</Sensors>"
      "</root>");
  SystemProfile profiles;
  EXPECT_THROW(ProfilesImporter::ImportAllSensorsOfSystemProfile(positionTagMissing, profiles), std::runtime_error);
}

TEST(ProfilesImporter_ImportAllSensorsOfSystemProfile, ProfileTagMissing_Throws)
{
  QDomElement profileTagMissing = documentRootFromString(
      "<root>"
      "<Sensors>"
      "<Sensor Id=\"0\" Position=\"PositionA\">"
      "</Sensor>"
      "</Sensors>"
      "</root>");
  SystemProfile profiles;
  EXPECT_THROW(ProfilesImporter::ImportAllSensorsOfSystemProfile(profileTagMissing, profiles), std::runtime_error);
}

TEST(ProfilesImporter_ImportAgentProfiles, MissingAtLeastOneElement_Throws)
{
  QDomElement missingAtLeastOneElement = documentRootFromString(
      "<root>"
      "<AgentProfile Name = \"Superhero\">"
      "<DriverProfiles>"
      "<DriverProfile Name = \"Regular\" Probability = \"0.5\"/>"
      "<DriverProfile Name = \"Trajectory\" Probability = \"0.5\"/>"
      "</DriverProfiles>"
      "<ADASProfiles>"
      "<ADASProfile Name = \"ComponentA\" Probability = \"0.5\"/>"
      "</ADASProfiles>"
      "<VehicleModels>"
      "</VehicleModels>"
      "</AgentProfile>"
      "</root>");
  Profiles profiles;
  EXPECT_THROW(ProfilesImporter::ImportAgentProfiles(missingAtLeastOneElement, profiles), std::runtime_error);
}

TEST(ProfilesImporter_ImportAgentProfiles, WrongProbabilities_Throws)
{
  QDomElement wrongProbabilities = documentRootFromString(
      "<root>"
      "<AgentProfile Name = \"Superhero\">"
      "<DriverProfiles>"
      "<DriverProfile Name = \"Regular\" Probability = \"0.5\"/>"
      "<DriverProfile Name = \"Trajectory\" Probability = \"0.5\"/>"
      "</DriverProfiles>"
      "<ADASProfiles>"
      "<ADASProfile Name = \"ComponentA\" Probability = \"0.5\"/>"
      "<ADASProfile Name = \"AEB\" Probability = \"1.0\"/>"
      "</ADASProfiles>"
      "<VehicleModels>"
      "<VehicleModel Name = \"Batmobile\" Probability = \"1.5\"/>"
      "</VehicleModels>"
      "</AgentProfile>"
      "</root>");
  Profiles profiles;
  EXPECT_THROW(ProfilesImporter::ImportAgentProfiles(wrongProbabilities, profiles), std::runtime_error);
}

TEST(ProfilesImporter_ImportAgentProfiles, MissingTag_Throws)
{
  QDomElement missingTag = documentRootFromString(
      "<root>"
      "<AgentProfile Name = \"Superhero\">"
      "<ADASProfiles>"
      "<ADASProfile Name = \"ComponentA\" Probability = \"0.5\"/>"
      "<ADASProfile Name = \"AEB\" Probability = \"1.0\"/>"
      "</ADASProfiles>"
      "<VehicleModels>"
      "<VehicleModel Name = \"Batmobile\" Probability = \"1.0\"/>"
      "</VehicleModels>"
      "</AgentProfile>"
      "</root>");
  Profiles profiles;
  EXPECT_THROW(ProfilesImporter::ImportAgentProfiles(missingTag, profiles), std::runtime_error);
}
