################################################################################
# Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# Install file for building FMILibrary with Conan
################################################################################

from conan import ConanFile
from conan.errors import ConanInvalidConfiguration
from conan.tools.files import apply_conandata_patches
from conans import tools, AutoToolsBuildEnvironment, CMake
import os

required_conan_version = ">=1.47.0"

class FmiConan(ConanFile):
    name = "FMILibrary"
    license = "2-Clause BSD"
    author = "Michael Scharfenberg michael.scharfenberg@itk-engineering.de"
    url = "https://github.com/modelon-community"
    description = "The Functional Mock-up Interface (or FMI) defines a standardized interface to be used in computer simulations to develop complex cyber-physical systems"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared": False, "fPIC": True}
    generators = "cmake"
    short_paths = True

    def export_sources(self):
        for patch in self.conan_data.get("patches", {}).get(self.version, []):
            self.copy(patch["patch_file"])

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC
        
    def source(self): 
        url = self.conan_data["sources"][self.version]["url"]
        sha256 = self.conan_data["sources"][self.version]["sha256"]
        git = tools.Git(folder=self.name)
        git.clone(url, self.version, "--recursive")
        os.chdir(self.name)
        if self.settings.os == "Linux":
            os.system('dos2unix src/Import/src/FMI1/fmi1_import_capi.c src/Import/src/FMI2/fmi2_import_capi.c src/Util/include/JM/jm_portability.h')
        for patch in self.conan_data.get("patches", {}).get(self.version, []):
            print("patch name is ", patch)
            patch_file = os.path.join(self.folders.base_source, patch["patch_file"])
            print("patch_file name is ", patch_file)
            os.system("patch -l -p1 <" + patch_file)

    def build(self):
        if self.settings.os == "Windows":
            cmake = CMake(self, generator="MSYS Makefiles")
        else:
            cmake = CMake(self)
        cmake.configure(source_folder=self.name,
                        defs={"FMILIB_INSTALL_PREFIX":"fmi",
                              "FMILIB_BUILD_STATIC_LIB":"OFF",
                              "FMILIB_BUILD_SHARED_LIB":"ON"})

        cmake.build()
        cmake.install()

    def package(self):
        self.copy("*", src="fmi")

    def package_info(self):
        self.cpp_info.libs = ["fmi"]
        