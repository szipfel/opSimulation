#!/bin/bash

################################################################################
# Copyright (c) 2021 in-tech GmbH
#               2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# This script packs the artifacts
################################################################################

MYDIR="$(dirname "$(readlink -f $0)")"
cd "$MYDIR/../../../../dist" || exit 1

SIM_NAME=openPASS_SIM
E2E_NAME=openPASS_EndToEndTests

mkdir -p ../artifacts/testreport || exit 1

if [[ "${OSTYPE}" = "msys" ]]; then
  mv opSimulation/testreport ../artifacts/testreport/linux
  $MYDIR/util_zip.sh ../artifacts/${E2E_NAME}.zip opSimulation/artifacts
else
  mv opSimulation/testreport ../artifacts/testreport/windows
  $MYDIR/util_tar.sh ../artifacts/${E2E_NAME}.tar.gz opSimulation/artifacts
fi

rm -rf opSimulation/artifacts opSimulation/configs opSimulation/results
mkdir opSimulation/configs

if [[ "${OSTYPE}" = "msys" ]]; then
  $MYDIR/util_zip.sh ../artifacts/${SIM_NAME}.zip opSimulation
else
  $MYDIR/util_tar.sh ../artifacts/${SIM_NAME}.tar.gz opSimulation
fi

